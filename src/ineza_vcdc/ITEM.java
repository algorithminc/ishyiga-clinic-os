/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

/**
 *
 * @author ishyiga
 */
public class ITEM {
   public String  CODE,NAME_PRODUCT,CODE_BAR,CODE_CLASSEMENT,
           ITEMS_CLASS,TAX_CODE,ITEM_INN_RWA_CODE;
   public int ID_PRODUCT;
   public double TVA_VALUE;
    public ITEM(int ID_PRODUCT,String CODE, String NAME_PRODUCT, String CODE_BAR,
            String CODE_CLASSEMENT, double TVA_VALUE,String ITEMS_CLASS,String TAX_CODE) {
        this.ID_PRODUCT = ID_PRODUCT;
        this.CODE = CODE;
        this.NAME_PRODUCT = NAME_PRODUCT;
        this.CODE_BAR = CODE_BAR;
        this.CODE_CLASSEMENT = CODE_CLASSEMENT;
        this.ITEMS_CLASS = ITEMS_CLASS;
          
        { this.TAX_CODE = TAX_CODE;}
        
        this.TVA_VALUE=TVA_VALUE;
    }
  
}
