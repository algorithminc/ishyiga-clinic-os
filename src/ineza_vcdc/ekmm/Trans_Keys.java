/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc.ekmm;

import java.util.Date;

/**
 *
 * @author Danny
 */
public class Trans_Keys {
    
    private String SignKey;
    private String intKey;
    private String commKey;
    private String RESP_MSG;
    private Date CURRENT_DATE;
    private String Token;

    public Trans_Keys() {
    }
    
    public Trans_Keys(String SignKey, String intKey, String commKey,Date CURRENT,String token) {
        this.SignKey = SignKey;
        this.intKey = intKey;
        this.commKey = commKey;
        this.CURRENT_DATE=CURRENT;
        this.Token=token;
    }

    public void setRESP_MSG(String RESP_MSG) {
        this.RESP_MSG = RESP_MSG;
    }

    public String getRESP_MSG() {
        return RESP_MSG;
    }
    
    

    public String getCommKey() {
        return commKey;
    }

    public String getIntKey() {
        return intKey;
    }

    public String getSignKey() {
        return SignKey;
    }
    
    public String getToken()
    {
        return Token;
    }
    
    
    
}
