
package ineza_vcdc.ekmm;


import java.security.*; 
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;



/**
 *
 * @author Danny
 */
public class AES_ENC 
{
    
    private static final String ALGO = "AES"; 
    private static final String base32Chars =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
    Base32ToHex base32=new Base32ToHex();
    

    public AES_ENC() throws Exception {
//        String data=longToHex(1296, 5)+longToHex(0, 5)+longToHex(1, 2)+longToHex(1, 4);         
////        String key="5LDAUV6R7P3XVWUXMXHWVJRFBBBHWRLC2Z6UK4MSKVR7ANKB2L4A";
//        String key="RIKKPANVNCUFWT27FGRWYHEIS65ONXKF2AGAHS4GYLZP2MP4ORMA";
//        
//        String hexKey=base32.convertToHex(base32.decode(key)).toUpperCase();
//        System.out.println(hexKey);
////        
//        System.out.println(encrypt("000008A300000008A30000030000003B", hexKey)); 
    }
    
    
 
    public static final String longToHex(long lval, int nbByte) {
        long decValue = 0;
        String hexString = null;
        hexString = Long.toHexString(lval);
        int nbChar = hexString.length();
        if ((nbChar< (nbByte * 2))) {
            for (int i = (nbChar + 1); (i 
                        <= (nbByte * 2)); i++) {
                hexString = ("0" + hexString);
            }
            
        }
        
        return hexString;
    }
    
    private final byte[] HexToByteArray(String hex, int n) {
        int NumberChars = hex.length(); 
        if (((NumberChars % 2) 
                    != 0)) {
            hex = ("0" + hex);
        }
        
        byte[] bytes=new byte[n];
        int j = 0;
        for (int i = 0; (i 
                    <= (NumberChars - 1)); i = (i + 2)) { 
//            b[i] = (byte) Integer.parseInt(str[i], 16);
            bytes[j] = (byte) Integer.parseInt(hex.substring(i, i+2), 16);
            j = (j + 1);
        }        
        return bytes;
    }
    
    public  String encrypt(String Data,String key1) throws Exception {
    Key key = generateKey(key1);
    Cipher c = Cipher.getInstance("AES/ECB/NoPadding");
    c.init(Cipher.ENCRYPT_MODE, key);
//    byte[] encVal = c.doFinal(Data.getBytes());
    byte[] encVal = c.doFinal(HexToByteArray(Data, 16));
//    SDC_CRYPTO_BASE32 base32 = new SDC_CRYPTO_BASE32(); 
    String encryptedValue = encode(encVal);
    return encryptedValue;
}
    
    private Key generateKey(String keys) throws Exception { 
    Key key = new SecretKeySpec(HexToByteArray(keys, 32), ALGO);
    return key;
}
    static public String encode(final byte[] bytes) {
        int i = 0, index = 0, digit = 0;
        int currByte, nextByte;
        StringBuffer base32 
           = new StringBuffer((bytes.length + 7) * 8 / 5);

        while (i < bytes.length) {
            currByte = (bytes[i] >= 0) ? bytes[i] : (bytes[i] + 256);

            /* Is the current digit going to span a byte boundary? */
            if (index > 3) {
                if ((i + 1) < bytes.length) {
                    nextByte = (bytes[i + 1] >= 0) 
                       ? bytes[i + 1] : (bytes[i + 1] + 256);
                } else {
                    nextByte = 0;
                }

                digit = currByte & (0xFF >> index);
                index = (index + 5) % 8;
                digit <<= index;
                digit |= nextByte >> (8 - index);
                i++;
            } else {
                digit = (currByte >> (8 - (index + 5))) & 0x1F;
                index = (index + 5) % 8;
                if (index == 0)
                    i++;
            }
            base32.append(base32Chars.charAt(digit));
        }

        return base32.toString();
    }
    
    
}
