package ineza_vcdc.ekmm;


import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Danny
 */
public class Base32 {
    private static final String base32Chars =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
    private static final int[] base32Lookup =
    { 0xFF,0xFF,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F, 
      0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, 
      0xFF,0x00,0x01,0x02,0x03,0x04,0x05,0x06, 
      0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E, 
      0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16, 
      0x17,0x18,0x19,0xFF,0xFF,0xFF,0xFF,0xFF, 
      0xFF,0x00,0x01,0x02,0x03,0x04,0x05,0x06, 
      0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E, 
      0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16, 
      0x17,0x18,0x19,0xFF,0xFF,0xFF,0xFF,0xFF  
    };

    /**
     * Encodes byte array to Base32 String.
     *
     */
    
    public Base32() {
    } 

    public String hmacSha1(String value, String key) {
        try {
            // Get an hmac_sha1 key from the raw key bytes
//            byte[] keyBytes = key.getBytes();   
            byte[] keyBytes = decode(key);
            SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");

            // Get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(signingKey);

            // Compute the hmac on input data bytes
            byte[] rawHmac = mac.doFinal(value.getBytes());
//            System.out.println(rawHmac.length);
            
            //take at least 10
            byte[] encVal =new byte [10];
            //System.arraycopy(rawHmac, 0, encVal, 0, 10);
            System.arraycopy(rawHmac, 10, encVal, 0, 10);
            
            String k= encode(encVal);
            
            return k;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    } 
    
    static public String encode(final byte[] bytes) {
        int i = 0, index = 0, digit = 0;
        int currByte, nextByte;
        StringBuffer base32 
           = new StringBuffer((bytes.length + 7) * 8 / 5);

        while (i < bytes.length) {
            currByte = (bytes[i] >= 0) ? bytes[i] : (bytes[i] + 256);

            /* Is the current digit going to span a byte boundary? */
            if (index > 3) {
                if ((i + 1) < bytes.length) {
                    nextByte = (bytes[i + 1] >= 0) 
                       ? bytes[i + 1] : (bytes[i + 1] + 256);
                } else {
                    nextByte = 0;
                }

                digit = currByte & (0xFF >> index);
                index = (index + 5) % 8;
                digit <<= index;
                digit |= nextByte >> (8 - index);
                i++;
            } else {
                digit = (currByte >> (8 - (index + 5))) & 0x1F;
                index = (index + 5) % 8;
                if (index == 0)
                    i++;
            }
            base32.append(base32Chars.charAt(digit));
        }

        return base32.toString();
    }

    /**
     * Decodes the given Base32 String to a raw byte array.
     *
     * @param base32
     * @return Decoded <code>base32</code> String as a raw byte array.
     */
    static public byte[] decode(final String base32) {
        int i, index, lookup, offset, digit;
        byte[] bytes = new byte[base32.length() * 5 / 8];

        for (i = 0, index = 0, offset = 0; i < base32.length(); i++) {
            lookup = base32.charAt(i) - '0';

            /* Skip chars outside the lookup table */
            if (lookup < 0 || lookup >= base32Lookup.length) {
                continue;
            }

            digit = base32Lookup[lookup];

            /* If this digit is not in the table, ignore it */
            if (digit == 0xFF) {
                continue;
            }

            if (index <= 3) {
                index = (index + 5) % 8;
                if (index == 0) {
                    bytes[offset] |= digit;
                    offset++;
                    if (offset >= bytes.length)
                        break;
                } else {
                    bytes[offset] |= digit << (8 - index);
                }
            } else {
                index = (index + 5) % 8;
                bytes[offset] |= (digit >>> index);
                offset++;

                if (offset >= bytes.length) {
                    break;
                }
                bytes[offset] |= digit << (8 - index);
            }
        }
        return bytes;
    }

    /** For testing, take a command-line argument in Base32, decode, 
     * print in hex, encode, print
     *
     * @param args
     */
    /*static public void main(String[] args) {
//        if (args.length == 0) {
//            System.out.println("Supply a Base32-encoded argument.");
//            
//            args[0]="danny";
//        }
        //String data = "19052014061749100498296         ALG01001604      1483  0,0         1750,0            0,0 18,0        27000,0        4118,64  0,0            0,0            0,0  0,0            0,0            0,0NSSDC00012345220140519061749         9        11\n";
        String d="20170220082009999000001         DAT01000006        2818,00        3200,00         488,14 0,00           0,00           0,00 0,00           0,00           0,00 0,00           0,00           0,00NSSDC99999901120170220082010        15        28";
        String canonical="application/json,,/api/v1/clients/validateBill,MON, 28 Jan 2019 9:40:00 GMT";
        System.out.println("leng:"+d.length());
        Base32 bb=new Base32();
        String kkey="qbUgCABX4ZU5FubCDgCHAbZLSepjtjhNeS4lcCB9qyZ0+btE3aiA1n9PBAdEwDmH3Ru2AiIdFNb6eaMq8PuFuQ==";
//        System.out.println(" Original: " +bb.Base32_hmacSha1("danny", "123456789"));
        System.out.println(" Original: " +bb.hmacSha1(canonical,kkey));
    }*/
}