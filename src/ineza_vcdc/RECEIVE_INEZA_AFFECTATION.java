/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

/**
 *
 * @author AlgorithmDev
 */
//feb0120 Etienne object for  Health facilities as pharmacies
public class RECEIVE_INEZA_AFFECTATION {
    String HSP_CODE,COUNCIL_NUMBER;
    long LAST_UPDATED;

    public RECEIVE_INEZA_AFFECTATION(String HSP_CODE, String COUNCIL_NUMBER) {
        this.HSP_CODE = HSP_CODE;
        this.COUNCIL_NUMBER = COUNCIL_NUMBER; 
    }
   
    public RECEIVE_INEZA_AFFECTATION(String xml) {
           try
           {
        this.HSP_CODE = HTTP_URL.Igicu.getTagValue("HSP_CODE", xml);
        this.COUNCIL_NUMBER=HTTP_URL.Igicu.getTagValue("DOCTOR_COUNCIL_NUMBER", xml); 
           }
           catch (Throwable d)
           {
           this.HSP_CODE=null;
           }
    }
}
