/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

/**
 *
 * @author ishyiga
 */
public class INEZA_ITEM_INSTRUCTIONS {
    
    
 String   INN_RWA_CODE,CATEGORY,INSTRUCTION;

    public INEZA_ITEM_INSTRUCTIONS(String INN_RWA_CODE, String CATEGORY, String INSTRUCTION) {
        this.INN_RWA_CODE = INN_RWA_CODE;
        this.CATEGORY = CATEGORY;
        this.INSTRUCTION = INSTRUCTION;
    }
 
      public INEZA_ITEM_INSTRUCTIONS(String xml) {
           try
           {
          
               
        this.INN_RWA_CODE     = HTTP_URL.Igicu.getTagValue("INN_RWA_CODE", xml);
        this.INSTRUCTION     = HTTP_URL.Igicu.getTagValue("INSTRUCTION", xml); 
        this.CATEGORY     = HTTP_URL.Igicu.getTagValue("CATEGORY", xml);  
        
           }
           catch (Throwable d)
           {
           this.INN_RWA_CODE=null;
           }
    }
    
   
    @Override
     public String toString()
     {
     return INN_RWA_CODE+" "+INSTRUCTION+" "+CATEGORY;
     }
     
   
}
