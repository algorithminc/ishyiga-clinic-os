
package ineza_vcdc;

import ecommerce.EETransaction;
import ecommerce.EETransactionList;
import globalaccount.Account;
import ineza_vcdc.ekmm.ekmm;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener; 
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import static sdc.comm.CommISHYIGA.KoherezaKuIgicu;

/**
 *
 * @author ishyiga
 */
public class ODT_INTERFACE_PRESCRIPTION extends JFrame implements ActionListener{

    String title;
    Account umukozi;
    JButton ADD_LINE, REMOVE_LINE, PUBLISH,LOADRUGS;
    JList allProductList; 
    JTextField searchAllDrugs = new JTextField(15);
     JList ROUTET;
    JTextField NOTET, QUANTITY_REQUESTEDT, REFILLT, TAKE_FREQUENCET, FREQUENCE_HOURYT, 
            TAKEN_MESURET, DUREET;
    JLabel NOTEL, ROUTEL, QUANTITY_REQUESTEDL, TAKE_FREQUENCEL, FREQUENCE_HOURYL, TAKEN_MESUREL, 
            DUREEL,costy;
    double total = 0;
    JTable jtable;
    LinkedList<RECEIVE_INEZA_ITEM> prescriptionDrugList = new LinkedList();
    LinkedList<RECEIVE_INEZA_ITEM> list;
    ODT prescription;
    String insuraString;
    
    Connection conn;
    INEZA_AFFILIATES ria;
    String ID_BRANCH_COMPANY,SSN,TIN,MRC;
            
    public ODT_INTERFACE_PRESCRIPTION(Connection conn,Account umukozi, 
            INEZA_AFFILIATES ria,String MRC, String TIN, String SSN) {

       this.conn=conn;
        this.umukozi=umukozi;
        this.ria=ria;
        this.MRC=MRC;this.TIN=TIN;this.SSN=SSN;
        
        this.ID_BRANCH_COMPANY = umukozi.BP_EMPLOYE;
        this.title = "ISHYIGA CLINIC LITE/ELECTRONIC PRESCRIPTION. SESSION FOR OM : "
               + umukozi.AUTO_DEPOT 
                + " / NAME " + umukozi.NAMES_ACCOUNT+" / PATIENT "+ria.BENEFICIARY_FIRST_NAME; 
prescription = new ODT("ALG", umukozi.TOKEN, ria,
                                    umukozi.BP_EMPLOYE, MRC, "" + new Date(),
                                    umukozi.UNIVERSE_ACCOUNT);
        
        initiate();
    }
Statement stato;
    private void initiate() {
        Container content = getContentPane();
        setTitle(title);
        setSize(900, 700);
        content.add(pane());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        try{stato=conn.createStatement();} catch(SQLException s){System.err.println(" statement inititate prescr "+s);}
        this.list = getItems(stato,"_DRUGS");
        allProductList.setListData(list.toArray());
        
        activity(); addLINE();LOADRUGS();REMOVELINE();publish();superSearchALLIt();
        setVisible(true);
    }

    private JPanel pane() {
        JPanel masterBoutton = new JPanel();
        masterBoutton.setLayout(new GridLayout(2, 1));
        masterBoutton.add(guhitamo());
        masterBoutton.add(basket()); 

        return masterBoutton;

    }

    public JPanel guhitamo() {
        JPanel jp = new JPanel();
        javax.swing.border.Border LoweredBevelBorder = BorderFactory.createLoweredBevelBorder();
        TitledBorder EtchedBorderRaised = BorderFactory.createTitledBorder(LoweredBevelBorder, 
                "PRESCRIBING",
                TitledBorder.LEFT, TitledBorder.TOP,
                new Font("Arial", Font.BOLD, 16), Color.GREEN);
        jp.setBorder(EtchedBorderRaised); 
        jp.setLayout(new GridLayout(1, 2));
        
        allProductList= new JList();
         JScrollPane medicamentList = new JScrollPane(allProductList);
        medicamentList.setPreferredSize(new Dimension(430, 250));
        allProductList.setBackground(new Color(255,192,203));
        
        searchAllDrugs.setText(" search Drugs... ");
        searchAllDrugs.setBackground(Color.pink);
        
        
        LOADRUGS = new JButton("Load Drugs");
        LOADRUGS.setBackground(Color.white); 
        
         JPanel masterlineDRUGS = new JPanel();
        masterlineDRUGS.add(medicamentList);
        masterlineDRUGS.add(LOADRUGS);
        masterlineDRUGS.add(searchAllDrugs);
        
        jp.add(masterlineDRUGS);
        costy = new JLabel("");
        NOTET = new JTextField(10);
        ROUTET = new JList();

        String[] routes = new String[]{
            "Oral", "Injection", "Sublingual and buccal", "Rectal", "Vaginal",
            "Ocular", "Otic", "Nasal"};
        ROUTET.setListData(routes); 
        QUANTITY_REQUESTEDT = new JTextField(10);
        TAKE_FREQUENCET = new JTextField(10);
        FREQUENCE_HOURYT = new JTextField(10);
        NOTEL = new JLabel("INSTRUCTION NOTES");
        ROUTEL = new JLabel(" ROUTES");
        QUANTITY_REQUESTEDL = new JLabel(" QUANTITY");
        TAKE_FREQUENCEL = new JLabel(" FREQUENCY(NUMBER)");
        FREQUENCE_HOURYL = new JLabel(" EVERY X HOURS(NUMBER)");
        TAKEN_MESURET = new JTextField(10);
        DUREET = new JTextField(10);
        TAKEN_MESUREL = new JLabel(" MEASUREMENT(TEXT) ");
        DUREEL = new JLabel(" TO BE TAKEN IN X DAYS(NUMBER) ");
         JPanel masterline3 = new JPanel();
        masterline3.setLayout(new GridLayout(9, 2));

        masterline3.add(TAKE_FREQUENCEL);
        masterline3.add(TAKE_FREQUENCET);
        masterline3.add(FREQUENCE_HOURYL);
        masterline3.add(FREQUENCE_HOURYT);
        masterline3.add(TAKEN_MESUREL);
        masterline3.add(TAKEN_MESURET);
        masterline3.add(DUREEL);
        masterline3.add(DUREET); 
        masterline3.add(QUANTITY_REQUESTEDL);
        masterline3.add(QUANTITY_REQUESTEDT); 
        masterline3.add(new JLabel("REFILL OPTIONS")); 
        masterline3.add(RadioButton());
        masterline3.add(NOTEL);
        masterline3.add(NOTET);
        masterline3.add(ROUTEL);
        JScrollPane spRoute = new JScrollPane(ROUTET);
        spRoute.setPreferredSize(new Dimension(100, 20));
        masterline3.add(spRoute);
      

        jp.add(masterline3);
        
        
        return jp;
    }

    public JPanel basket() {
        JPanel jp = new JPanel();
         javax.swing.border.Border LoweredBevelBorder = BorderFactory.createLoweredBevelBorder();
        TitledBorder EtchedBorderRaised = BorderFactory.createTitledBorder(LoweredBevelBorder, 
                "BASKET AND OPTIONS",
                TitledBorder.LEFT, TitledBorder.TOP,
                new Font("Arial", Font.BOLD, 16), Color.BLUE);
        jp.setBorder(EtchedBorderRaised); 
        
        PUBLISH = new JButton("PUBLISH");
        PUBLISH.setBackground(Color.orange);

        ADD_LINE = new JButton("ADD");
        REMOVE_LINE = new JButton("REMOVE");

        ADD_LINE.setBackground(new Color(164, 255, 203));
        REMOVE_LINE.setBackground(new Color(255, 163, 105));
        
        jp.add(ADD_LINE);
        jp.add(REMOVE_LINE); 
        jp.add(PUBLISH);
        jtable = new javax.swing.JTable();
        JScrollPane agasekeView = new JScrollPane(jtable);
        agasekeView.setPreferredSize(new Dimension(800, 200));
        jp.add(agasekeView);
        
        return jp;
    }

    
      
     String REFILL = "NO";

    @Override
    public void actionPerformed(ActionEvent e) {
        REFILL = e.getActionCommand();
    }

    public JPanel RadioButton() {

//Create the radio buttons.
        JRadioButton refill_2= new JRadioButton("REFILL");
        refill_2.setMnemonic(KeyEvent.VK_R);
        refill_2.setActionCommand("REFILL");
        JRadioButton NO = new JRadioButton("NO");
        NO.setMnemonic(KeyEvent.VK_N);
        NO.setActionCommand("NO");
        NO.setSelected(true);

        ButtonGroup group = new ButtonGroup();
        group.add(refill_2);
        group.add(NO);

//Register a listener for the radio buttons.
        refill_2.addActionListener(this);
        NO.addActionListener(this);

//Put the radio buttons in a column in a panel.
        JPanel radioPanel = new JPanel(new GridLayout(1, 2));
        radioPanel.add(refill_2);
        radioPanel.add(NO);
        add(radioPanel, BorderLayout.LINE_START);
//  setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
        return radioPanel;
    }    
     
        private void activity() {
        ADD_LINE.addActionListener((ActionEvent ae) -> {
            if (ADD_LINE == ae.getSource()) {
          
            }
        });
    }

 
    private void addLINE() {
        ADD_LINE.addActionListener((ActionEvent ae) -> {

            if (ae.getSource() == ADD_LINE
                    && allProductList.getSelectedValue() != null
                    && ROUTET.getSelectedValue() != null) {

                if (prescription != null) {

                    prescription.COUNCIL_NUMBER = umukozi.AUTO_DEPOT;
                    prescription.BENEFICIARY = ria.AFFILIATES_NUMBER;
                    prescription.INSURANCE = ria.ASSURANCE;

                    RECEIVE_INEZA_ITEM pk = (RECEIVE_INEZA_ITEM) allProductList.getSelectedValue();
 
                    String vin = DbhandlerVCDC.getInstruction2(prescription.HSP_CODE,
                            prescription.ria.BENEFICIARY_NUMBER,
                            pk.INN_RWA_CODE, 10, prescription.GLOBAL_ACCOUNT, "MALE",
                            10, stato);

                    System.out.println("  vin " + vin);
                    String CONTRE = DbhandlerVCDC.getContrIndicationTXT(
                            prescription.ria.BENEFICIARY_NUMBER, pk.ATC_CODE, stato);

                    boolean winjize = true;
                    // System.out.println(CONTRE + "  CONTRECONTRECONTRE " + pk.ATC_CODE);
                    if (!vin.equals("")) {
                        winjize = false;
                        JOptionPane.showMessageDialog(null, "   " + vin, "CONTRE INDICATION DETECTED",
                                JOptionPane.PLAIN_MESSAGE);
                        //break;
                    }

                    if (!CONTRE.equals("")) {
                        winjize = false;
                        JOptionPane.showMessageDialog(null, "   " + CONTRE, "CONTRE INDICATION DETECTED",
                                JOptionPane.PLAIN_MESSAGE);
                        //break;
                    }

                    boolean existe = false;
                    for (int i = 0; i < prescription.laListe.size(); i++) {
                        ODT_LIST l = prescription.laListe.get(i);
                        if (l.INN_RW_CODE_REQUESTED.equals(pk.INN_RWA_CODE)) {
                            existe = true;
                            winjize = false;
                            JOptionPane.showMessageDialog(null, "EXIST", "REMOVE FIRST", JOptionPane.PLAIN_MESSAGE);
                            break;
                        } 
                        String interraction = DbhandlerVCDC.getInteraction(
                                l.INN_RW_CODE_REQUESTED,
                                (pk.INN_RWA_CODE), stato);
                        if (interraction != null) {
                            winjize = false;
                            JOptionPane.showMessageDialog(null, "INTERRACTION DETECTED", " " + interraction, JOptionPane.PLAIN_MESSAGE);
                            break;
                        } 
                    }
                    
                    if (winjize) {
                        String NOTE = NOTET.getText();
                        String ROUTE = (String) ROUTET.getSelectedValue();
                        int QUANTITY_REQUESTED = Integer.parseInt(QUANTITY_REQUESTEDT.getText());
                        int TAKE_FREQUENCE = Integer.parseInt(TAKE_FREQUENCET.getText());
                        int FREQUENCE_HOURY = Integer.parseInt(FREQUENCE_HOURYT.getText());
                        String MESURE = "" + (TAKEN_MESURET.getText());
                        String DUREE = "" + Integer.parseInt(DUREET.getText());
                        String CODE_BUP = pk.INN_RWA_CODE;//getCode(pk.INN_RWA_CODE);
                        String AVAILABILITY = "NOT FOUND";

                        if (CODE_BUP != null && !CODE_BUP.equals("NOTFOUND")) {
                            String valueToSend = " <PRODUCT_CODE>" + CODE_BUP + "</PRODUCT_CODE></header>  </request>";
                            String response = KoherezaKuIgicu(ID_BRANCH_COMPANY, "LAST_DATE_STOCK ", valueToSend);
                            AVAILABILITY = HTTP_URL.Igicu.getTagValue("LAST_TIME_UPDATE", response);
                        }
                        ODT_LIST li = new ODT_LIST(pk.INN_RWA_CODE, pk.ITEM_NAME, NOTE,
                                AVAILABILITY, ROUTE, QUANTITY_REQUESTED, TAKE_FREQUENCE,
                                FREQUENCE_HOURY, pk.SELLING_PRICE, MESURE, DUREE, REFILL, "NEW");
                        prescription.laListe.add(li);
                        getFormule2();
                        clearLine();
                    }
                    //PRINT_QR.doClick(); 
                }
            } else {
                JOptionPane.showMessageDialog(null, " PLEASE SELECT EACH DRUG-DISEASE-ROUTE");
            }
        });
    }

    JTable myworkJtable = new JTable();
    JTable myworkJtable2 = new JTable();

    public void rebaliste() {
        myworkJtable.addMouseListener(new MouseListener() {
            public void actionPerformed(ActionEvent ae) {
            }

            @Override
            public void mouseExited(MouseEvent ae) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (myworkJtable.getSelectedRow() != -1) {

                    System.out.println(".mouseClicked()" + laListe);
                    System.out.println(".mouseClicked()" + laListe.size());

                    if (laListe != null && laListe.size() > 0) {
                        int row = myworkJtable.getSelectedRow();
                        System.out.println(".rowrow ()" + row);

                        ODT toreprint = laListe.get(row);

                        if (!toreprint.SYNCED_RESPONSE.equals("YES")) {
                            int n = JOptionPane.showConfirmDialog(null, "THIS eRX FAILED TO SYNC DO YOU WANT TO RESEND IT ?",
                                    "ATTENTION", JOptionPane.YES_NO_OPTION);
                            if (n == 0) {
                                prescription = new ODT(toreprint.PUSHED_XML, toreprint.MRC, stato);
                                eeCommerceSendODTREPUSH(ID_BRANCH_COMPANY);
                            }
                        }
                        new Print_QR(new ODT(toreprint.PUSHED_XML, toreprint.MRC, stato),umukozi.NAMES_ACCOUNT);
                    } else {
                        JOptionPane.showMessageDialog(null, "NO AVAILABLE ODT TO PRINT", "PRINTING",
                                JOptionPane.WARNING_MESSAGE);
                    }
                }
            }

            ;
@Override
            public void mousePressed(MouseEvent e) {
            }

            ;
@Override
            public void mouseReleased(MouseEvent e) {
            }

            ;
@Override
            public void mouseEntered(MouseEvent e) {
            }
        });
    }

    LinkedList<ODT> laListe() {
        LinkedList<ODT> laListenow = new LinkedList();
        try {
            String affSql = "";
            if (ria != null) {
                affSql = "and BENEFICIAL_NEMBER='" + ria.BENEFICIARY_NUMBER + "' ";
            }
            String sql = "SELECT  * FROM VCDC.VIRTUAL_CARD_ODT_SENT where GLOBAL_ACCOUNT='"
                    + umukozi.UNIVERSE_ACCOUNT + "' " + affSql + " ORDER BY HEURE";
            System.out.println(sql);
            ResultSet outResult = stato.executeQuery(sql);

            while (outResult.next()) {

                laListenow.add(new ODT(outResult.getString("ID_RECEIPT"),
                        outResult.getString("GLOBAL_ACCOUNT"),
                        outResult.getString("BENEFICIAL_NEMBER"),
                        outResult.getString("SYNCED_RESPONSE"),
                        outResult.getString("MRC"),
                        outResult.getString("HEURE"),
                        outResult.getString("PUSHED_XML")));
            }
        } catch (SQLException e) {
            System.out.println(e + "formule");
        }
        return laListenow;
    }
   LinkedList<ODT> laListe;
   Color[][] s1Color;

    void getFormule2() {

        //MEDICATION	STRENGT	AMOUNT	TAKE	FREQUENCE HOURY	QTE	ROUTE	NOTE
        String[] S = new String[]{
            "CODE",
            "ITEM", "TAKE", "FREQUENCE HOURY",
            "ROUTE", "QUANTITE", "NOTE",
            "AVAILABILITE"
        };

        int size = prescription.laListe.size();

        String s1[][] = new String[size][9];

        total = 0;

        s1Color = new Color[size][9];
        for (int j = 0; j < size; j++) {

            ODT_LIST li = prescription.laListe.get(j);

            s1[j][0] = li.INN_RW_CODE_REQUESTED;
            s1[j][1] = li.ITEM_NAME;
            s1[j][2] = "" + li.TAKE_FREQUENCE;
            s1[j][3] = "" + li.FREQUENCE_HOURY;
            s1[j][4] = li.ROUTE;
            s1[j][5] = "" + li.QUANTITY_REQUESTED;
            s1[j][6] = "" + li.NOTE;
            s1[j][7] = "" + li.AVAILABILITE;
            s1Color[j][7] = li.availColor;

            s1Color[j][0] = Color.white;
            total += li.QUANTITY_REQUESTED * li.PRICE;
        }

        costy.setText(" " + total);
        jtable.setModel(new javax.swing.table.DefaultTableModel(s1, S));

        MyRenderer myRenderer = new  MyRenderer();
        jtable.setDefaultRenderer(Object.class, myRenderer);

        jtable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(200);//.setWidth(200 
        jtable.doLayout();
        jtable.setRowHeight(30);
        jtable.validate();
    }

    void clean() {

        //MEDICATION	STRENGT	AMOUNT	TAKE	FREQUENCE HOURY	QTE	ROUTE	NOTE
        String[] S = new String[]{
            "CODE",
            "ITEM", "TAKE", "FREQUENCE HOURY",
            "ROUTE", "QUANTITE", "NOTE",
            "AVAILABILITE"
        };

        // int size = prescription.laListe.size();
        String s1[][] = new String[0][9];

        total = 0;

        costy.setText(" " + total);
        jtable.setModel(new javax.swing.table.DefaultTableModel(s1, S));
        jtable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jtable.getColumnModel().getColumn(1).setPreferredWidth(200);//.setWidth(200 
        jtable.doLayout();
        jtable.setRowHeight(30);
        jtable.validate(); 
        clearLine();
    }

    void clearLine() { 
        searchAllDrugs.setText(" search Drugs... ");
        //patientID.setText("...search patient ID ..."); 
        NOTET.setText("");
        ROUTET.clearSelection();
        QUANTITY_REQUESTEDT.setText("");
        TAKE_FREQUENCET.setText("");
        FREQUENCE_HOURYT.setText("");
        TAKEN_MESURET.setText("");
        DUREET.setText("");
        QUANTITY_REQUESTEDT.setBackground(Color.white);
        allProductList.clearSelection();
        // allDiseaseList.clearSelection();
    }

    private void REMOVELINE() {
        REMOVE_LINE.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == REMOVE_LINE && jtable.getSelectedRow() != -1) {
                ODT_LIST pj = prescription.laListe.remove(jtable.getSelectedRow());
                getFormule2();
                // PRINT_QR.doClick();
            }
        }); 
    } 

    private void LOADRUGS() {
        LOADRUGS.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == LOADRUGS) { 
                if (ria != null) {
                    Jobber.injizaItem(conn, ria.ASSURANCE);
                } else {
                    JOptionPane.showMessageDialog(null, "SELECT AN INSURANCE");
                }
            }
        });

    }

 

    void superSearchALL(String find) {

        prescriptionDrugList.clear();
        String[] splited = find.split(" ");
//System.out.println(find+cashier.allProduct.size());
        for (int i = 0; i < list.size(); i++) {
            RECEIVE_INEZA_ITEM p = list.get(i);
            int trouve = 0;
            for (String findo : splited) {
                if (p.ITEM_NAME.contains(findo)
                        || p.ATC_CODE.contains(findo)
                        || p.INN.contains(findo)) {
                    trouve++;
                } else {
                    break;
                }
            }
            if (trouve == splited.length) {
                prescriptionDrugList.add(p);
            }
        }
        allProductList.setBackground(new Color(255, 255, 204));
        allProductList.setListData(prescriptionDrugList.toArray());
    }
   

//
//    void simulateClosestPharmacy() {
//        INEZA_AVAILABILITY[][] igisubizo
//                = new INEZA_AVAILABILITY[15][prescription.laListe.size()];
//        String[] header = new String[prescription.laListe.size()];
//        int i = 0, j = 0;
//        for (ODT_LIST line : prescription.laListe) {
//            i = 0;
//
//            header[j] = line.INN_RW_CODE_REQUESTED;
//            try {
//                String availability = HTTP_URL.Igicu.kohereza(
//                        "GET_AVAILABILITY_STOCK", "bridge",
//                        "<CODE_PRODUCT>" + line.INN_RW_CODE_REQUESTED + "</CODE_PRODUCT> "
//                        + "<CRITERIA>Discount</CRITERIA>"
//                        + "</header></request>");
//                System.out.println("umurwayi bampaye beneficialy number 3\n " + availability);
//
//                if (availability.equals("THE IS NO PHARMACY HAVE THIS DRUG"))//get patient live
//                {
//                    System.out.println("umurwayi kumeneka  " + line.INN_RW_CODE_REQUESTED);
//                } else if (!availability.equals("NULL RESULT")
//                        && !availability.equals(""))//get patient live
//                {
//                    String[] sp = availability.split("</PHARMACY>");
//
//                    for (String sp1 : sp) {
//                        System.out.println(" umwe \n " + sp1);
//                        INEZA_AVAILABILITY ria = new INEZA_AVAILABILITY(sp1);
//                        if (ria.NAMES != null) {
//                            igisubizo[i][j] = (ria);
//                            line.AHO_YABONESTE++;
//                        } else {
//                            System.err.println(ria.ID_CLIENT + "uyu ntiyinjirane " + ria + "  benbenbenbenbenben  ");
//                        }
//                        i++;
//                        if (i >= 15) {
//                            System.out.println("breaking " + sp1);
//                            break;
//                        }
//                    }
//                }
//            } catch (Exception ex) {
//                Logger.getLogger(ODT_INTERFACE.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            j++;
//        }
//        String ahoYaboneste = "";
//        for (ODT_LIST line : prescription.laListe) {
//            ahoYaboneste += "\n " + line.INN_RW_CODE_REQUESTED + " " + line.AHO_YABONESTE;
//        }
//
//        JOptionPane.showMessageDialog(null, ahoYaboneste);
//        nyerekaIzije(igisubizo, header);
//        huzaPharmacy(igisubizo, header);
//
//    }
//
//    void huzaPharmacy(INEZA_AVAILABILITY[][] igisubizo, String[] header) {
//        LinkedList<INEZA_AVAILABILITY_PHARMACY> pharmacies = new LinkedList();
//
//        for (int i = 0; i < 15; i++) {
//            for (int j = 0; j < header.length; j++) {
//
//                if (igisubizo[i][j] != null) {
//                    INEZA_AVAILABILITY ineza = igisubizo[i][j];
//                    boolean nayibonye = false;
//                    for (INEZA_AVAILABILITY_PHARMACY ph : pharmacies) {
//                        nayibonye = ph.addItem(ineza);
//                    }
//
//                    if (!nayibonye) {
//                        INEZA_AVAILABILITY_PHARMACY ph = new INEZA_AVAILABILITY_PHARMACY(ineza.ID_CLIENT, ineza.NAMES, ineza.PHONE, ineza.FIELD,
//                                ineza.LOCATION, ineza.DISTRICT, ineza.ID_COMPANY_BRANCH, ineza.STATUS,
//                                ineza.STOCK_STATUS, ineza.WORKING_HOURS, ineza.WORKING_DAYS);
//                        ph.addItem(ineza);
//                        pharmacies.add(ph);
//                    }
//                }
//            }
//        }
//
//        SortedSet<INEZA_AVAILABILITY_PHARMACY> list = new TreeSet<>();
//
//        System.out.println("ineza_vcdc.ODT_INTERFACE.huzaPharmacy()" + pharmacies.size());
//
//        for (INEZA_AVAILABILITY_PHARMACY ph : pharmacies) {
//            list.add(ph);
//        }
//
//        nyerekaUmuti(list, header.length);
//    }
//
//    void nyerekaUmuti(SortedSet<INEZA_AVAILABILITY_PHARMACY> list, int length) {
//
//        String[] header = new String[]{
//            "ID_CLIENT", "NAMES", "TOTAL_MOUNT", "NEEDED " + length, "DISCOUNT"};
//        String s1[][] = new String[list.size()][5];
//        s1Color = new Color[list.size()][5];
//
//        Iterator it = list.iterator();
//        int vv = 0;
//        while (it.hasNext()) {
//            INEZA_AVAILABILITY_PHARMACY ph = ((INEZA_AVAILABILITY_PHARMACY) it.next());
//
//            // System.out.println("ineza " + vv);
//            // if(vv<15)
//            {
//                s1[vv][0] = ph.ID_CLIENT;
//                s1[vv][1] = ph.NAMES;
//                s1[vv][2] = "" + ph.TOTAL_MOUNT;
//                s1[vv][3] = "" + ph.FOUND_ITEM;
//                s1[vv][4] = "" + ph.DISCOUNT_AMOUNT;
//            }
//            vv++;
//        }
//
//        myworkJtable.setModel(new javax.swing.table.DefaultTableModel(s1, header));
//
//        ODT_INTERFACE.MyRenderer myRenderer = new ODT_INTERFACE.MyRenderer();
//        myworkJtable.setDefaultRenderer(Object.class, myRenderer);
//
//        myworkJtable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
//        myworkJtable.getColumnModel().getColumn(1).setPreferredWidth(200);//.setWidth(200 
//        myworkJtable.doLayout();
//        myworkJtable.setRowHeight(30);
//        myworkJtable.validate();
//
//        JScrollPane agasekeView = new JScrollPane(myworkJtable);
//        agasekeView.setPreferredSize(new Dimension(800, 600));
//        JOptionPane.showMessageDialog(null, (agasekeView));
//
//    }
//
//    void nyerekaIzije(INEZA_AVAILABILITY[][] igisubizo, String[] header) {
//
//        String s1[][] = new String[15][header.length];
//
//        total = 0;
//
//        s1Color = new Color[15][header.length];
//        for (int i = 0; i < 15; i++) {
//            for (int j = 0; j < header.length; j++) {
//
//                if (igisubizo[i][j] == null) {
//                    s1[i][j] = "NA";
//                    s1Color[i][j] = Color.red;
//                } else {
//                    s1[i][j] = igisubizo[i][j].NAMES;
//                    s1Color[i][j] = Color.white;
//                }
//            }
//        }
//
//        myworkJtable.setModel(new javax.swing.table.DefaultTableModel(s1, header));
//
//        ODT_INTERFACE.MyRenderer myRenderer = new ODT_INTERFACE.MyRenderer();
//        myworkJtable.setDefaultRenderer(Object.class, myRenderer);
//
//        myworkJtable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
//        myworkJtable.getColumnModel().getColumn(1).setPreferredWidth(200);//.setWidth(200 
//        myworkJtable.doLayout();
//        myworkJtable.setRowHeight(30);
//        myworkJtable.validate();
//
//        JScrollPane agasekeView = new JScrollPane(myworkJtable);
//        agasekeView.setPreferredSize(new Dimension(800, 200));
//        JOptionPane.showMessageDialog(null, (agasekeView));
//
//    }

    private void publish() {
        PUBLISH.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == PUBLISH) {
                //simulateClosestPharmacy();
                eeCommerceSendODT(ID_BRANCH_COMPANY);
                new Print_QR(prescription,umukozi.NAMES_ACCOUNT);
                
                ///PAR L ORDRE DU GENERAL
//                if (prescription.laListe.size() > 1) {
//                    simulateClosestPharmacy();
//                } else {
//                    JOptionPane.showMessageDialog(null,
//                            "TO USE OUR PRICE COMPARATOR WE NEED "
//                            + "MORE THAN 1 ITEM", "DATA CONFIDENTIALITY",
//                            JOptionPane.WARNING_MESSAGE);
//                }
                prescription = null;
                clean();
            }
        });

    }

    void baraIbikenewe() {
        DUREET.addKeyListener( new KeyListener() {
            public void actionPerformed(ActionEvent ae) { }
 @Override  public void keyTyped(KeyEvent e) {
                String cc = "" + e.getKeyChar();
                String find = (DUREET.getText() + cc).toUpperCase(); 
                try {
                    if (allProductList.getSelectedValue() != null) {
                        RECEIVE_INEZA_ITEM pk = (RECEIVE_INEZA_ITEM) allProductList.getSelectedValue();
                        if (pk.ITEM_NAME != null && !pk.ITEM_NAME.toUpperCase().contains("ML")) {
                            int TAKE_FREQUENCE = Integer.parseInt(TAKE_FREQUENCET.getText());
                            int FREQUENCE_HOURY = Integer.parseInt(FREQUENCE_HOURYT.getText());
                            int DUREE = Integer.parseInt(find);
                            QUANTITY_REQUESTEDT.setText("" + (TAKE_FREQUENCE * DUREE * 24 / FREQUENCE_HOURY));
                            QUANTITY_REQUESTEDT.setBackground(Color.green);
                        }

                    }
                } catch (NumberFormatException o) {
                    JOptionPane.showMessageDialog(null, "ENTER NUMBERS WHERE NEEDED PLEASE",
                            " NUMBER FORMAT", JOptionPane.ERROR_MESSAGE);
                }

            }

            @Override  public void keyPressed(KeyEvent e) {  } 
            @Override  public void keyReleased(KeyEvent e) {}
        });
    }

    private void superSearchALLIt() {
        searchAllDrugs.addKeyListener(
                new KeyListener() {
            public void actionPerformed(ActionEvent ae) {
            }

            @Override
            public void keyTyped(KeyEvent e) {

                String cc = "" + e.getKeyChar();
                String find = (searchAllDrugs.getText() + cc).toUpperCase();
                if (cc.hashCode() == 8 && find.length() > 0) {
                    find = (find.substring(0, find.length() - 1));
                }
                superSearchALL(find);
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
    }
    
     
    public static String uzuzaDouble(String toPad) {

        try {
            Double.parseDouble(toPad);
            // System.out.println(" paaaaaaaaaaaaaaaaaaaaaaaddddddd      " + toPad);
            if (toPad != null) {
                String sp[] = toPad.split("\\.");

                ///  System.err.println(" "+sp.length);
                if (sp != null && sp.length > 1 && sp[1] != null && sp[1].length() == 2) {
                    return toPad;
                } else {
                    for (int i = 0; i < 10; i++) {
                        if (toPad.contains("." + i)) {
                            toPad = toPad + "0";
                        }
                    }
                }
            }
        } catch (Throwable ex) {
            //  System.out.println(ex + " toPadtoPadtoPadtoPad      " + toPad);
            return toPad;

        }
        return toPad;
    }

    public final static DateFormat DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    public static String padSpaces(String toPad, int length) {
        toPad = uzuzaDouble(toPad);

        String pads = "";
        if (toPad != null) {
            for (int i = 0; i < (length - toPad.length()); i++) {
                pads += " ";
            }
            return (pads + toPad).replaceAll("\\.", ",");
        } else {
            for (int i = 0; i < (length); i++) {
                pads += " ";
            }
            return pads;
        }
    }

//12/07/2018 12:08:29
    String curukuraTime(String time) {

//        String[] sides = time.split(" ");
//        String[] date = sides[0].split("/");
        //String res2 = date[2] + date[1] + date[0] + sides[1];
        String res2 = time.substring(0, 19);
        System.err.println("res2res2res2res2  " + res2);
        res2 = res2.replaceAll("-", "").replaceAll(":", "").replaceAll(" ", "");
        System.err.println("res2res2res2res2  " + res2);
        return res2;

    }

    String cleanTime(String time) {
        String res2 = time.replaceAll("/", "");
        res2 = res2.replaceAll(" ", "");
        res2 = res2.replaceAll(":", "");

        return res2;
    }

    String makeSingature(double TOTAL_A) {
        try {
            System.out.println(prescription.TIME + " CDT " + TOTAL_A);
            String CDT = curukuraTime(prescription.TIME); // CIS Date and Time on the receipt 14 characters Format: YYYYMMDDhhmmss‘20120605213455’
            System.out.println(prescription.TIME + " CDT " + CDT);
            String TIN2 = padSpaces(TIN, 9);// Tax Identification Number 9 characters ‘123456789’
            String CTIN = padSpaces("", 9);// Client’s Tax Identification number 9 characters ‘123456789’ Or, if not present: ‘ ’
            String MRC2 = padSpaces(MRC, 11);// Machine Registration Code 11 characters ‘ABC01012345’
            String RRN = padSpaces("" + prescription.ODT_CODE, 10);// Receipt Run Number 10 characters ‘ 12345’

            String TR1 = padSpaces("0.0", 5);//Tax Rate 1 5 characters amount ‘18,00’
            String TV1 = padSpaces("" + TOTAL_A, 15);//Taxable Amount 1 15 characters amount ‘ 500,00’
            String TA1 = padSpaces("0.0", 15);//Tax Amount 1 15 characters amount ‘ 76,27’
            String TR2 = padSpaces("0.18", 5);//Tax Rate 2 5 characters amount ‘ 0,00’
            String TV2 = padSpaces("0.0", 15);//Taxable Amount 2 15 characters amount ‘ 500,00’
            String TA2 = padSpaces("0.0", 15);//Tax Amount 2 15 characters amount ‘ 0,00’
            String TR3 = padSpaces("0.0", 5);//Tax Rate 3 5 characters amount ‘ 0,00’
            String TV3 = padSpaces("0.0", 15);//Taxable Amount 3 15 characters amount ‘ 0,00’
            String TA3 = padSpaces("0.0", 15);//Tax Amount 3 15 characters amount ‘ 0,00’
            String TR4 = padSpaces("0.0", 5);//Tax Rate 4 5 characters amount ‘ 0,00’
            String TV4 = padSpaces("0.0", 15);//Taxable Amount 4 15 characters amount ‘ 0,00’
            String TA4 = padSpaces("0.0", 15);//Tax Amount 4 15 characters amount ‘ 0,00’
            String RT = padSpaces("0.0", 1);//Receipt Type 1 character ‘N’ or ‘C’ or ‘T’ or ‘P’
            String TT = padSpaces("0.0", 1);//Transaction Type 1 character ‘S’ or ‘R’
            String SDCID = padSpaces("" + SSN, 12);//Sales Data Controller ID 12 characters ‘SDC001012345’
            String SDCDT = curukuraTime("" + prescription.TIME);//SDC Date and Time on the receipt 14 characters Format: YYYYMMDDhhmmss ‘20120605213455’
            System.out.println(prescription.TIME + " SDCDT " + SDCDT);
            String SDCRTC = padSpaces("" + prescription.ODT_CODE, 10);//SDC Receipt Type Counter 10 characters ‘ 123’
            String SDCTC = padSpaces("" + prescription.ODT_CODE, 10);//SDC Total Receipt Counter 10 characters ‘ 1234’
            System.out.println(prescription.ODT_CODE + " SDCTC " + SDCTC);
            String res2 = CDT + TIN2 + CTIN + MRC2 + RRN + TR2 + TV2 + TA2 + TR1
                    + TV1 + TA1 + TR3 + TV3 + TA3 + TR4 + TV4 + TA4 + RT + TT
                    + SDCID + SDCDT + SDCRTC + SDCTC;
            System.out.println(" data " + res2);
            return res2;

        } catch (Exception ex) {
            System.out.println(" ex " + ex);

        }
        return "";
    }

    public void eeCommerceSendODT(String ID_COMPANY_BRANCH) {

        double totalprice = 0;
        LinkedList<EETransactionList> laCommande = new LinkedList<>();
        for (int j = 0; j < prescription.laListe.size(); j++) {
            ODT_LIST ci = prescription.laListe.get(j);

            laCommande.add(new EETransactionList(ci.INN_RW_CODE_REQUESTED, ci.QUANTITY_REQUESTED,
                    ci.PRICE, "NA", "NA", ci.gauche() + ci.droite(), ""));

            totalprice += ci.PRICE * ci.QUANTITY_REQUESTED;
        }

        String startdate = DATEFORMAT.format((new Date()).getTime());
        prescription.TIME = startdate;

        int num = DbhandlerVCDC.insertDOT(prescription, conn);
        String makeSigna = makeSingature(totalprice);
        System.out.println("Cercle makeSigna : " + makeSigna);

        String SIGNATURE = ekmm.getSignature(makeSigna, "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567");
        prescription.SIGNATURE = SIGNATURE;
        System.out.println("Cercle SIGNATURE : " + SIGNATURE);
        prescription.ODT_CODE = "" + num;
        String SYNCED_RESPONSE;
//D_INITIATOR

        EETransaction lacmd = new EETransaction(umukozi.BP_EMPLOYE, umukozi.AUTO_DEPOT,
                "DIRECT_PHARMACY", "", startdate,
                "" + num, "ORDER", "NA", "NA", "NA", totalprice, 0,
                SIGNATURE, prescription.ria.BENEFICIARY_NUMBER, "", "", ria.ASSURANCE, laCommande, null);
        // lacmd.
        String reso = ecommerce.Ecommerce.KoherezaTransaction(lacmd, "EE_CCE_SEND_TRANSACTION");
        System.out.println("Cercle response : " + reso);
        if (reso != null && reso.toUpperCase().contains("SUCCES")) {
            SYNCED_RESPONSE = "YES";
            JOptionPane.showMessageDialog(null, num + " WELL PUBLISHED");
        } else {
            SYNCED_RESPONSE = "FAILED";
            JOptionPane.showMessageDialog(null, num + " FAILED TO PUBLISH\n" + reso);
        }

        DbhandlerVCDC.updateHDOT(SIGNATURE, lacmd.koraUrutondeXml(),
                SYNCED_RESPONSE, prescription.ria.BENEFICIARY_NUMBER,
                prescription.TIME, prescription.GLOBAL_ACCOUNT, prescription.MRC, conn);
    }

    public void eeCommerceSendODTREPUSH(String ID_COMPANY_BRANCH) {

        double totalprice = 0;
        LinkedList<EETransactionList> laCommande = new LinkedList<>();
        for (int j = 0; j < prescription.laListe.size(); j++) {
            ODT_LIST ci = prescription.laListe.get(j);

            laCommande.add(new EETransactionList(ci.INN_RW_CODE_REQUESTED, ci.QUANTITY_REQUESTED,
                    ci.PRICE, "NA", "NA", ci.gauche() + ci.droite(), ""));

            totalprice += ci.PRICE * ci.QUANTITY_REQUESTED;
        }

        EETransaction lacmd = new EETransaction(umukozi.BP_EMPLOYE, umukozi.AUTO_DEPOT,
                "DIRECT_PHARMACY", "", prescription.TIME,
                "" + prescription.ODT_CODE, "ORDER", "NA", "NA", "NA", totalprice, 0,
                prescription.SIGNATURE, prescription.BENEFICIARY, "", "", (String) prescription.INSURANCE, laCommande, null);
        String SYNCED_RESPONSE;
        String reso = ecommerce.Ecommerce.KoherezaTransaction(lacmd, "EE_CCE_SEND_TRANSACTION");
        System.out.println("Cercle response : " + reso);
        if (reso != null && reso.toUpperCase().contains("SUCCES")) {
            SYNCED_RESPONSE = "YES";
            JOptionPane.showMessageDialog(null, prescription.ODT_CODE + " WELL PUBLISHED");
        } else {
            SYNCED_RESPONSE = "FAILED";
            JOptionPane.showMessageDialog(null, prescription.ODT_CODE + " FAILED TO PUBLISH\n" + reso);
        }

        DbhandlerVCDC.updateHDOTREPUSHED(
                SYNCED_RESPONSE, prescription.ria.BENEFICIARY_NUMBER,
                prescription.TIME, prescription.GLOBAL_ACCOUNT, prescription.MRC, conn);
    }

  
    public static LinkedList<RECEIVE_INEZA_ITEM> getItems(Statement stat,String types) {

        LinkedList<RECEIVE_INEZA_ITEM> listlotItem = new LinkedList<>();
        try {
            String sql = "SELECT  ITEM_INN_RWA_CODE,ITEM_NAME,ITEM_RHIA_NAME,"
                    + " INN,ATC_CODE,MANUFACTURER,"
                    + " SELLING_PRICE FROM VCDC.RECEIVE_INEZA"+types+" ORDER BY ITEM_NAME";
            System.out.println(sql);
            ResultSet outResult = stat.executeQuery(sql);

            while (outResult.next()) {
                listlotItem.add(new RECEIVE_INEZA_ITEM(
                        outResult.getString("ITEM_INN_RWA_CODE"), outResult.getString("ITEM_NAME"),
                        outResult.getString("ITEM_RHIA_NAME"),
                        outResult.getString("INN"),
                        outResult.getString("ATC_CODE"),
                        outResult.getString("MANUFACTURER"), outResult.getDouble("SELLING_PRICE")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e + "formule");
        }
        return listlotItem;
    }

    public static RECEIVE_INEZA_ITEM getItem(String ITEM_INN_RWA_CODE, Statement stato) {

        try {
            String sql = "SELECT  ITEM_INN_RWA_CODE,ITEM_NAME,ITEM_RHIA_NAME,"
                    + " INN,ATC_CODE,MANUFACTURER,"
                    + " SELLING_PRICE FROM VCDC.RECEIVE_INEZA_DRUGS WHERE"
                    + " ITEM_INN_RWA_CODE='" + ITEM_INN_RWA_CODE + "'";
            System.out.println(sql);
            ResultSet outResult = stato.executeQuery(sql);

            while (outResult.next()) {
                return (new RECEIVE_INEZA_ITEM(
                        outResult.getString("ITEM_INN_RWA_CODE"), outResult.getString("ITEM_NAME"),
                        outResult.getString("ITEM_RHIA_NAME"),
                        outResult.getString("INN"),
                        outResult.getString("ATC_CODE"),
                        outResult.getString("MANUFACTURER"), outResult.getDouble("SELLING_PRICE")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e + "formule");
        }
        return null;
    }
    
    
    public class MyRenderer extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            if (s1Color[row][column] != null) {
                c.setBackground(s1Color[row][column]);
            }
            return c;

        }

    }
}


