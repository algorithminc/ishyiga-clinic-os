/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

/**
 *
 * @author ishyiga
 */
public class INEZA_RECEIPT_ITEM {
public String INN_RWA_CODE,BATCH,EXPIRE,BON_LIV,ID_COMPANY_BRANCH;
public double ITEM_UNITY_PRICE, TOT_AMOUNT, VAT_AMONT;
public int ITEM_QUANTITY,ID_INVOICE; 

    public INEZA_RECEIPT_ITEM(String INN_RWA_CODE, String BATCH, String EXPIRE, 
            double ITEM_UNITY_PRICE, double TOT_AMOUNT, double VAT_AMONT, 
            int ITEM_QUANTITY, String BON_LIV,String ID_COMPANY_BRANCH,int ID) {
        this.ID_INVOICE = ID;
        this.ID_COMPANY_BRANCH = ID_COMPANY_BRANCH;
         this.INN_RWA_CODE = INN_RWA_CODE;
        this.BATCH = BATCH;
        this.EXPIRE = EXPIRE;
        this.ITEM_UNITY_PRICE = ITEM_UNITY_PRICE;
        this.TOT_AMOUNT = TOT_AMOUNT;
        this.VAT_AMONT = VAT_AMONT;
        this.ITEM_QUANTITY = ITEM_QUANTITY;
        this.BON_LIV = BON_LIV;
    }
 

}
