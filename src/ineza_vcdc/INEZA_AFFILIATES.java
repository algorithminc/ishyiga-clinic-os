/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

import java.awt.Dimension;
import java.util.LinkedList;
import javax.swing.JLabel; 
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author ishyiga
 */
//Feb,01,2020 Aimable affiliate object
public class INEZA_AFFILIATES {

//  <SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
//   <SOAP-ENV:Body>
//      <ns1:findAffiliateResponse xmlns:ns1="urn:affiliateServer">
//         <Errorcode xsi:type="xsd:integer">200</Errorcode>
//         <numero_affiliate xsi:type="xsd:string">0000064</numero_affiliate>
//         <nom_affiliate xsi:type="xsd:string">NIZEYIMANA</nom_affiliate>
//         <prenom_affiliate xsi:type="xsd:string">THEOPHILE</prenom_affiliate>
////         <numero_bene xsi:type="xsd:string">000006400</numero_bene>
//         <first_name xsi:type="xsd:string">NYIRANSABIMANA</first_name>
//         <second_name xsi:type="xsd:string">VINCENTIE</second_name>
//         <valid xsi:type="xsd:string">TRUE</valid>
//         <relationship xsi:type="xsd:string">Conjoint</relationship>
//         <matr xsi:type="xsd:string">1501925100000A</matr>
//         <affectation xsi:type="xsd:string">MINISANTE/CELLULE D'APPUI A L'APPROCHE CONTRACTUEL</affectation>
//         <nid xsi:type="xsd:string"/>
//         <dob xsi:type="xsd:date">2065-07-01</dob>
//         <gender xsi:type="xsd:string">F</gender>
//         <rssb_time xsi:type="xsd:dateTime">2020-05-13 05:42:08</rssb_time>
//      </ns1:findAffiliateResponse>
//   </SOAP-ENV:Body>
//</SOAP-ENV:Envelope>  
    
    
    
    
    String AFFILIATES_NUMBER, AFFILIATE_NOM,AFFILIATE_PRENOM,
            BENEFICIARY_NUMBER, BENEFICIARY_FIRST_NAME, BENEFICIARY_LAST_NAME,
            RELATIONSHIP, GENDER, EMPLOYER, EMPLOYER_CATEGORY,
            AFFILIATION_STATUS, PHONE_NUMBER, ID_CARD_NUMBER,ALLEGIES;

    String DOB,ASSURANCE;

    String LAST_UPDATE;
    int PERCENTAGE =15,PIN;

    public INEZA_AFFILIATES(String AFFILIATES_NUMBER, String BENEFICIARY_NUMBER,
            String BENEFICIARY_FIRST_NAME, String BENEFICIARY_LAST_NAME, String RELATIONSHIP,
            String GENDER, String EMPLOYER, String EMPLOYER_CATEGORY, String AFFILIATION_STATUS,
            String PHONE_NUMBER, String ID_CARD_NUMBER, String DOB, 
            String LAST_UPDATE,String AFFILIATE_NOM,String AFFILIATE_PRENOM, int PIN,
            int PERCENTAGE,String ASSURANCE) {
        
        this.PIN=PIN;
        this.AFFILIATES_NUMBER = AFFILIATES_NUMBER;
        this.AFFILIATE_NOM =AFFILIATE_NOM;
        this.AFFILIATE_PRENOM =AFFILIATE_PRENOM;
        this.BENEFICIARY_NUMBER = BENEFICIARY_NUMBER;
        this.BENEFICIARY_FIRST_NAME = BENEFICIARY_FIRST_NAME;
        this.BENEFICIARY_LAST_NAME = BENEFICIARY_LAST_NAME;
        this.RELATIONSHIP = RELATIONSHIP;
        this.GENDER = GENDER;
        this.EMPLOYER = EMPLOYER;
        this.EMPLOYER_CATEGORY = EMPLOYER_CATEGORY;
        this.AFFILIATION_STATUS = AFFILIATION_STATUS;
        this.PHONE_NUMBER = PHONE_NUMBER;
        this.ID_CARD_NUMBER = ID_CARD_NUMBER;
        this.DOB = DOB;
        this.LAST_UPDATE = LAST_UPDATE;
        this.PERCENTAGE=PERCENTAGE;
        this.ASSURANCE = ASSURANCE;
    }

   public INEZA_AFFILIATES(String xml,String ASSURANCE) {
        try {
            this.AFFILIATES_NUMBER = HTTP_URL.Igicu.getTagValue("AFFILIATES_NUMBER", xml);
            this.BENEFICIARY_NUMBER = HTTP_URL.Igicu.getTagValue("BENEFICIARY_NUMBER", xml);
            this.BENEFICIARY_FIRST_NAME = HTTP_URL.Igicu.getTagValue("BENEFICIARY_FIRST_NAME", xml);
            this.BENEFICIARY_LAST_NAME = HTTP_URL.Igicu.getTagValue("BENEFICIARY_LAST_NAME", xml);
            this.AFFILIATE_NOM = HTTP_URL.Igicu.getTagValue("AFFILIATE_NOM", xml);
            this.AFFILIATE_PRENOM = HTTP_URL.Igicu.getTagValue("AFFILIATE_PRENOM", xml);
            this.RELATIONSHIP = HTTP_URL.Igicu.getTagValue("RELATIONSHIP", xml);
            this.GENDER = HTTP_URL.Igicu.getTagValue("GENDER", xml);
            this.EMPLOYER = HTTP_URL.Igicu.getTagValue("EMPLOYER", xml);
            this.EMPLOYER_CATEGORY = HTTP_URL.Igicu.getTagValue("EMPLOYER_CATEGORY", xml);
            this.AFFILIATION_STATUS = HTTP_URL.Igicu.getTagValue("AFFILIATION_STATUS", xml); 
            if(AFFILIATION_STATUS!=null)
            {this.AFFILIATION_STATUS=AFFILIATION_STATUS.replaceAll("&amp;", " ");}
            this.PHONE_NUMBER = HTTP_URL.Igicu.getTagValue("PHONE_NUMBER", xml);
            this.ID_CARD_NUMBER = HTTP_URL.Igicu.getTagValue("ID_CARD_NUMBER", xml);
            this.DOB = HTTP_URL.Igicu.getTagValue("DOB", xml);
            this.LAST_UPDATE = HTTP_URL.Igicu.getTagValue("LAST_UPDATED", xml);
            this.ALLEGIES = HTTP_URL.Igicu.getTagValue("ALLERGIES", xml);
            this.PIN =1010;
            this.PERCENTAGE = HTTP_URL.Igicu.getTagValueInteger("PERCENTAGE", xml);
        } catch (Throwable d) {
            System.err.println(xml+" \n INEZA_AFFILIATES "+d);
            this.DOB = null;
            this.ASSURANCE=ASSURANCE;
        }
    }
public INEZA_AFFILIATES(String xml,boolean isRssb) {
        try {
            xml=xml.replaceAll(" xsi:type=\"xsd:integer\"", "");
             xml=xml.replaceAll(" xsi:type=\"xsd:string\"", "");
             xml=xml.replaceAll(" xsi:type=\"xsd:date\"", "");
              xml=xml.replaceAll(" xsi:type=\"xsd:dateTime", "");
             System.out.println("DOB  "+xml);
             
            this.AFFILIATES_NUMBER = HTTP_URL.Igicu.getTagValue("numero_affiliate", xml);
            this.BENEFICIARY_NUMBER = HTTP_URL.Igicu.getTagValue("numero_bene", xml);
            this.BENEFICIARY_FIRST_NAME = HTTP_URL.Igicu.getTagValue("first_name", xml);
            this.BENEFICIARY_LAST_NAME = HTTP_URL.Igicu.getTagValue("second_name", xml);
            this.AFFILIATE_NOM = HTTP_URL.Igicu.getTagValue("nom_affiliate", xml);
            this.AFFILIATE_PRENOM = HTTP_URL.Igicu.getTagValue("prenom_affiliate", xml);
            
            this.RELATIONSHIP = HTTP_URL.Igicu.getTagValue("relationship", xml);
            this.GENDER = HTTP_URL.Igicu.getTagValue("gender", xml);
            this.EMPLOYER = HTTP_URL.Igicu.getTagValue("affectation", xml);
            this.EMPLOYER_CATEGORY = HTTP_URL.Igicu.getTagValue("matr", xml) ;
            setStatus(HTTP_URL.Igicu.getTagValue("valid", xml));
            
            this.PHONE_NUMBER =""; //HTTP_URL.Igicu.getTagValue("PHONE_NUMBER", xml);
            this.ID_CARD_NUMBER = HTTP_URL.Igicu.getTagValue("nid", xml);
            this.DOB = HTTP_URL.Igicu.getTagValue("dob", xml);
            System.out.println("DOB  "+DOB);
            this.LAST_UPDATE = "";// HTTP_URL.Igicu.getTagValue("ID_PATIENT", xml);
            this.ASSURANCE="RSSB";
        } catch (Throwable d) {
            this.DOB = null;
        }
    } 

private void setStatus(String income)
{

    if(income.equals("TRUE"))
    { this.AFFILIATION_STATUS ="ACTIVATED"; }
    else
    {this.AFFILIATION_STATUS ="DESACTIVATED";}
}

    @Override
    public String toString() {

        return AFFILIATES_NUMBER + " \n "
                + BENEFICIARY_NUMBER + " \n "
                + BENEFICIARY_FIRST_NAME + " \n "
                + BENEFICIARY_LAST_NAME + " \n "
                 + ALLEGIES + " \n "
                + RELATIONSHIP + " \n "
                + GENDER + " \n "
                + EMPLOYER + " \n "
                + EMPLOYER_CATEGORY + " \n "
                + AFFILIATION_STATUS + " \n "
                + PHONE_NUMBER + " \n "
                + ID_CARD_NUMBER + " \n " + DOB;
    }
 

 public static void setPatientInfo(INEZA_AFFILIATES p, LinkedList<JLabel> aliste) {
     int i=1;
     aliste.get(i).setText(p.AFFILIATES_NUMBER); i +=2;
     aliste.get(i).setText(p.BENEFICIARY_NUMBER); i +=2;
     aliste.get(i).setText(p.ASSURANCE); i +=2;
     aliste.get(i).setText(p.BENEFICIARY_FIRST_NAME); i +=2;
     aliste.get(i).setText(p.ALLEGIES); i +=2;
     
     JTextArea jt = new JTextArea(10, 10);
     jt.setText(p.ALLEGIES);
     JScrollPane jst= new JScrollPane(jt);
     jst.setPreferredSize(new Dimension(50, 50));
     JOptionPane.showMessageDialog(null, jst); 
     
     aliste.get(i).setText(p.RELATIONSHIP); i +=2;
     aliste.get(i).setText(p.GENDER); i +=2;
     aliste.get(i).setText(p.DOB); i +=2;
     aliste.get(i).setText(p.AFFILIATION_STATUS); i +=2;
     aliste.get(i).setText(p.PHONE_NUMBER); i +=2;
 }
    public static void getPatientInfo( LinkedList<JLabel> aliste) {
       
        aliste.add(new JLabel("MAIN NUMBER"));
        aliste.add(new JLabel(""));
        aliste.add(new JLabel("BENEF. NUMBER"));
        aliste.add(new JLabel(""));
        aliste.add(new JLabel("INSURANCE"));
        aliste.add(new JLabel(""));
        aliste.add(new JLabel("BENEF. FIRST NAME"));
        aliste.add(new JLabel(""));
        aliste.add(new JLabel("ALLERGIES"));
        aliste.add(new JLabel(""));
        aliste.add(new JLabel("RELATIONSHIP"));
        aliste.add(new JLabel(""));
        aliste.add(new JLabel("GENDER"));
        aliste.add(new JLabel(""));
        aliste.add(new JLabel("DATE OF BIRTH"));
        aliste.add(new JLabel(""));
        aliste.add(new JLabel("STATUS"));
        aliste.add(new JLabel(""));
        aliste.add(new JLabel("PHONE NUMBER"));
        aliste.add(new JLabel(""));
 
    }

}
