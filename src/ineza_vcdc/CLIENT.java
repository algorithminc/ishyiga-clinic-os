package ineza_vcdc;

/**
 *
 * @author Corneille
 */
public class CLIENT {

    String NUM_AFFILIATION,
            NOM_CLIENT,
            PRENOM_CLIENT,
            ASSURANCE,
            EMPLOYEUR,
            SECTEUR,
            VISA,
            CODE,
            AGE,
            SEXE,
            LIEN,
            BENEFICIAIRE;
    int PERCENTAGE;

    public CLIENT(String NUM_AFFILIATION, String NOM_CLIENT, String PRENOM_CLIENT, int PERCENTAGE, String ASSURANCE, String EMPLOYEUR, String SECTEUR, String VISA, String CODE, String AGE, String SEXE, String LIEN, String BENEFICIAIRE) {
        this.NUM_AFFILIATION = NUM_AFFILIATION;
        this.NOM_CLIENT = NOM_CLIENT;
        this.PRENOM_CLIENT = PRENOM_CLIENT;
        this.PERCENTAGE = PERCENTAGE;
        this.ASSURANCE = ASSURANCE;
        this.EMPLOYEUR = EMPLOYEUR;
        this.SECTEUR = SECTEUR;
        this.VISA = VISA;
        this.CODE = CODE;
        this.AGE = AGE;
        this.SEXE = SEXE;
        this.LIEN = LIEN;
        this.BENEFICIAIRE = BENEFICIAIRE;
    }

}
