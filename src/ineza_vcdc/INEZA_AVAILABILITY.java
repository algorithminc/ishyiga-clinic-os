/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

/**
 *
 * @author ishyiga
 */
public class INEZA_AVAILABILITY {
    
String ID_CLIENT,NAMES,PHONE,FIELD,LOCATION,DISTRICT,ID_COMPANY_BRANCH,
        STATUS,STOCK_STATUS,WORKING_HOURS,WORKING_DAYS;
double PRICE;
int DISCOUNT,QUANTITE;

    public INEZA_AVAILABILITY(String ID_CLIENT, String NAMES, String PHONE, String FIELD,
            String LOCATION, String DISTRICT, int DISCOUNT, String ID_COMPANY_BRANCH, 
            String STATUS, String STOCK_STATUS, String WORKING_HOURS, String WORKING_DAYS,
            double PRICE,int QUANTITE) {
        this.ID_CLIENT = ID_CLIENT;
        this.NAMES = NAMES;
        this.PHONE = PHONE;
        this.FIELD = FIELD;
        this.LOCATION = LOCATION;
        this.DISTRICT = DISTRICT;
        this.DISCOUNT = DISCOUNT;
        this.ID_COMPANY_BRANCH = ID_COMPANY_BRANCH;
        this.STATUS = STATUS;
        this.STOCK_STATUS = STOCK_STATUS;
        this.WORKING_HOURS = WORKING_HOURS;
        this.WORKING_DAYS = WORKING_DAYS;
        this.PRICE = PRICE;
        this.QUANTITE=QUANTITE;
    }

     public INEZA_AVAILABILITY(String xml) {
        try
           {
               this.ID_CLIENT =  HTTP_URL.Igicu.getTagValue("ID_CLIENT", xml); 
        this.NAMES = HTTP_URL.Igicu.getTagValue("NAMES", xml);
        this.PHONE = HTTP_URL.Igicu.getTagValue("PHONE", xml);
        this.FIELD = HTTP_URL.Igicu.getTagValue("FIELD", xml);
        this.LOCATION = HTTP_URL.Igicu.getTagValue("LOCATION", xml);
        this.DISTRICT = HTTP_URL.Igicu.getTagValue("DISTRICT", xml);
        this.DISCOUNT = HTTP_URL.Igicu.getTagValueInteger("DISCOUNT", xml);
        this.ID_COMPANY_BRANCH = HTTP_URL.Igicu.getTagValue("ID_COMPANY_BRANCH", xml);
        this.STATUS = HTTP_URL.Igicu.getTagValue("STATUS", xml);
        this.STOCK_STATUS = HTTP_URL.Igicu.getTagValue("STOCK_STATUS", xml);
        this.WORKING_HOURS = HTTP_URL.Igicu.getTagValue("WORKING_HOURS", xml);
        this.WORKING_DAYS = HTTP_URL.Igicu.getTagValue("WORKING_DAYS", xml);
        this.PRICE =   HTTP_URL.Igicu.getTagValueDouble("PRICE", xml);
        this.QUANTITE = HTTP_URL.Igicu.getTagValueInteger("QUANTITE", xml);
        
        }
           catch (Throwable d)
           {
           this.NAMES=null;
               System.err.println(xml+" \n "+d);
           }
    }


    
}
