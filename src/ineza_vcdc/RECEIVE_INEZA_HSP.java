/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

/**
 *
 * @author AlgorithmDev
 */

public class RECEIVE_INEZA_HSP {
    
    String HSP_CODE,HSP_NAME,DISTRICT,PHONE_NUMBER,CATEGORY;
    String LAST_UPDATED;

    public RECEIVE_INEZA_HSP(String HSP_CODE, String 
            HSP_NAME, String DISTRICT, String PHONE_NUMBER,
            String CATEGORY, String LAST_UPDATED) {
        this.HSP_CODE = HSP_CODE;
        this.HSP_NAME = HSP_NAME;
        this.DISTRICT = DISTRICT;
        this.PHONE_NUMBER = PHONE_NUMBER;
        this.CATEGORY = CATEGORY;
        this.LAST_UPDATED = LAST_UPDATED;
    }
     public RECEIVE_INEZA_HSP(String xml) {
           try
           {
          
               
        this.HSP_CODE     = HTTP_URL.Igicu.getTagValue("HEALTH_FACILITY_CODE", xml);
        this.HSP_NAME     = HTTP_URL.Igicu.getTagValue("HEALTH_FACILITY_NAME", xml);
        this.DISTRICT     = HTTP_URL.Igicu.getTagValue("DISTRICT", xml);
        this.PHONE_NUMBER = HTTP_URL.Igicu.getTagValue("PHONE_NUMBER", xml);
        this.CATEGORY     = HTTP_URL.Igicu.getTagValue("CATEGORY", xml);
        this.LAST_UPDATED = (HTTP_URL.Igicu.getTagValue("LAST_UPDATED", xml)); 
        
           }
           catch (Throwable d)
           {
           this.HSP_CODE=null;
           }
    }
    
   
    @Override
     public String toString()
     {
     return HSP_CODE+" "+HSP_NAME;
     }
     
     
}
