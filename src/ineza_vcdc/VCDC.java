//package ineza_vcdc;
//
//import ebm.sdc.rra2.RRA_SDC_SOFT;
//import java.sql.Connection;
//import java.sql.Statement;
//import java.util.LinkedList;
//import javax.swing.JOptionPane;
////import javax.swing.JOptionPane;
//
///**
// *
// * @author Corneille
// */
//public class VCDC {
//
//    public static String getVcdcReceipt(Statement stat, String model, Statement statBackup,
//            Connection conn, String ssn,int PORT_SDC, RRA_SDC_SOFT soft) {
//        
//        LinkedList<Integer> laListe = DbhandlerVCDC.getIdInvoiceUnPushedReceipt("INVOICE", stat);
//        int countSuccess = 0;
//        String respo = " <PULL_VCIS_RECEIPT> ";
//        for (int i = 0; i < laListe.size(); i++)// 
//        {
//
//            INEZA_RECEIPT rece = BUILD_VCDC_XML.receipt(laListe.get(i), stat,statBackup, ssn, model);
//            if (rece == null) {
//                continue;
//            }
//            char type = 'N';
//            if (!rece.extData.contains(",NS,")) {
//                type = 'T';
//            }
//            
////            int invId = BUILD_XML.getJoinerId(stat, "" + type, "INVOICE", rece.ID_INVOICE);
//            String invoice = BUILD_VCDC_XML.doXmlReceipt5V(rece,rece.ID_INVOICE,stat);
//            
//            System.out.println("ineza_vcdc.VCDC.getVcdcReceipt() invoice "+invoice); 
//            
//            if(invoice== null)
//            {   
//            JOptionPane.showMessageDialog(null,
//                    " VCDC MISSING ORDONNANCE "+rece.ID_INVOICE
//                    + " \n CALL ISHYIGA TEAM ");
//            break;
//            }
//            System.out.println("details ordo: "+invoice);
//            
//          //s   JOptionPane.showMessageDialog(null,"build \n "+i);
//            ebm.sdc.rra2.RRA_RECEIVED_PACKAGE rec = ebm.sdc.rra2.RRA_SDC_SOFT.sendToSDCSOFT
//                   ("TRN_INEZA_RECEIPT", rece.ID_INVOICE + "@@" + invoice,
//                    ebm.sdc.rra2.RraSoftIshyiga.user, PORT_SDC,
//                    ebm.sdc.rra2.RraSoftIshyiga.softIP,soft);
//            if (rec == null) {
////                GuiSdc.console += ("\n TRNRECEIPT RECEIPT rece  is null " + invoice);
//                continue;
//            }
//            
//            if (rec.data.equals("true")) {
//                DbhandlerVCDC.updatePUSHEDReceipt("" + rece.ID_INVOICE, "INVOICE", stat);
//                countSuccess++;
//            }
//            
//            String list = BUILD_VCDC_XML.doItemXml(rece);
//
//            ebm.sdc.rra2.RRA_SDC_SOFT.sendToSDCSOFT("TRN_INEZA_RECEIPT_ITEM", rece.ID_INVOICE + "@@" + list,
//                    ebm.sdc.rra2.RraSoftIshyiga.user, PORT_SDC, 
//                    ebm.sdc.rra2.RraSoftIshyiga.softIP,soft);
//
//        }
//        respo += "<TOTAL_INVOICE>" + laListe.size() + "</TOTAL_INVOICE> "
//                + "<TOTAL_INVOICE_SUCCESS>" + countSuccess + "</TOTAL_INVOICE_SUCCESS> ";
//
//        countSuccess = 0;
//        laListe = DbhandlerVCDC.getIdInvoiceUnPushedReceipt("REFUND", stat);
//        for (int i = 0; i < laListe.size(); i++)// 
//        {
//            INEZA_RECEIPT rece = BUILD_VCDC_XML.refund(laListe.get(i), stat, statBackup, ssn, model);
//
//            char type = 'N';
//            if (rece == null) {
////                textArea.append("\n TRNRECEIPTREFUND RECEIPT rece  is null "+i);
//                continue;
//            }
//            if (!rece.extData.contains(",NR,")) {
//                type = 'T';
//            }
//
////            int invId = BUILD_XML.getJoinerId(stat, "" + type, "REFUND", rece.ID_INVOICE);
//            
//            String invoice = (BUILD_VCDC_XML.doXmlReceipt5V(rece, rece.ID_INVOICE_REFUNDED, statBackup));
//              if(invoice== null)
//            {   
//            JOptionPane.showMessageDialog(null,
//                    " VCDC MISSING ORDONNANCE "+rece.ID_INVOICE
//                    + " \n CALL ISHYIGA TEAM ");
//            continue;
//            }
//            ebm.sdc.rra2.RRA_RECEIVED_PACKAGE rec = ebm.sdc.rra2.RRA_SDC_SOFT.sendToSDCSOFT("TRN_INEZA_REFUND", rece.ID_INVOICE + "@@" + invoice,
//                    ebm.sdc.rra2.RraSoftIshyiga.user, PORT_SDC, ebm.sdc.rra2.RraSoftIshyiga.softIP,soft);
//
//            if (rec.data.equals("true")) {
//                DbhandlerVCDC.updatePUSHEDReceipt("" + rece.ID_INVOICE, "REFUND", stat);
//                countSuccess++;
//            }
//           String list = BUILD_VCDC_XML.doItemXml(rece);
//
//            ebm.sdc.rra2.RRA_SDC_SOFT.sendToSDCSOFT("TRN_INEZA_ITEM_REFUND", rece.ID_INVOICE + "@@" + list,
//                    ebm.sdc.rra2.RraSoftIshyiga.user, PORT_SDC, ebm.sdc.rra2.RraSoftIshyiga.softIP,soft);
//}
//
//        respo += "<TOTAL_REFUND>" + laListe.size() + "</TOTAL_REFUND> "
//                + "<TOTAL_REFUND_SUCCESS>" + countSuccess + "</TOTAL_REFUND_SUCCESS> ";
//
//        return respo;
//    }
//
//}
