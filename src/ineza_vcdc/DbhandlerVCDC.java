/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;
 
import ineza_vcdc.virtual_card.PATIENT_WATCHING;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author ishyiga
 */
public class DbhandlerVCDC {

    static public boolean insertPatient(INEZA_AFFILIATES ria,
            Connection conn) {
        try {
            
            
String sql="insert into VCDC.RECEIVE_INEZA_AFFILIATES"
        + "(AFFILIATES_NUMBER,BENEFICIARY_NUMBER, "
        + "           BENEFICIARY_FIRST_NAME,BENEFICIARY_LAST_NAME, "
        + "           RELATIONSHIP,GENDER,EMPLOYER,EMPLOYER_CATEGORY, "
        + "           AFFILIATION_STATUS,PHONE_NUMBER,ID_CARD_NUMBER,DOB"
        + "           , AFFILIATE_NOM,AFFILIATE_PRENOM,PIN,ALLEGIES,PERCENTAGE,INSURANCE) "
        + " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
           // System.out.println(sql);

            try (PreparedStatement psInsert = conn.prepareStatement(sql)) {
 
                
                psInsert.setString(1, ria.AFFILIATES_NUMBER);
                psInsert.setString(2, ria.BENEFICIARY_NUMBER);
                psInsert.setString(3, ria.BENEFICIARY_FIRST_NAME);
                psInsert.setString(4, ria.BENEFICIARY_LAST_NAME);
                psInsert.setString(5, ria.RELATIONSHIP);
                psInsert.setString(6, ria.GENDER);
                psInsert.setString(7, ria.EMPLOYER);
                psInsert.setString(8, ria.EMPLOYER_CATEGORY);
                psInsert.setString(9, ria.AFFILIATION_STATUS);
                psInsert.setString(10, ria.PHONE_NUMBER);
                psInsert.setString(11, ria.ID_CARD_NUMBER);
                psInsert.setString(12, ria.DOB);
                psInsert.setString(13, ria.AFFILIATE_NOM);
                psInsert.setString(14, ria.AFFILIATE_PRENOM);
                psInsert.setInt(15, ria.PIN);
                 psInsert.setString(16, ria.ALLEGIES);
                 psInsert.setInt(17, ria.PERCENTAGE);
                 psInsert.setString(18, ria.ASSURANCE); 

                psInsert.executeUpdate();

            }
            System.out.println("Patient insertion");
            return true;
        } catch (SQLException ex) {
            System.err.println(" " + ex);
            JOptionPane.showMessageDialog(null, "inseert! \n " + ex);
            return false;
        }
    }
 
     

     static public boolean insertSMSfincial(SMS sms,
            Connection conn) {
        try {

            try (PreparedStatement psInsert = conn.prepareStatement
        ("insert into VCDC.VCDC_SMS_FINANCIAL"
                    + "(SOURCE_FIN,DESTINATION, "
                    + "           BODY_SMS,SMS_AMOUNT,SMS_DATE) "
                    + " values (?,?,?,?,?)")) {

                psInsert.setString(1, sms.Saurce);
                psInsert.setString(2, sms.Destination);
                psInsert.setString(3, sms.Body);  
                psInsert.setDouble(4, sms.amount);
                psInsert.setTimestamp(5, sms.time); 
                
                psInsert.executeUpdate();
            }
            return true;
        } catch (SQLException ex) {

             System.out.println(" SQLException "+ex);
            return false;
        }
    }


     static public boolean insertItem(RECEIVE_INEZA_ITEM rii,
            Connection conn) {
        try {

            try (PreparedStatement psInsert = conn.prepareStatement("insert into VCDC.RECEIVE_INEZA_DRUGS"
                    + "(ITEM_INN_RWA_CODE,ITEM_NAME, "
                    + "           ITEM_RHIA_NAME,INN,ATC_CODE,SELLING_PRICE, "
                    + "           MANUFACTURER,ID_PRODUCT ) "
                    + " values (?,?,?,?,?,?,?,?)")) {

                psInsert.setString(1, rii.INN_RWA_CODE);
                psInsert.setString(2, rii.ITEM_NAME);
                psInsert.setString(3, rii.ITEM_RHIA_NAME);  
                psInsert.setString(4, rii.INN);
                psInsert.setString(5, rii.ATC_CODE);
                psInsert.setDouble(6, rii.SELLING_PRICE);
                psInsert.setString(7, rii.MANUFACTURER); 
                psInsert.setInt(8, rii.ID_PRODUCT);
                
                psInsert.executeUpdate();
            }
            return true;
        } catch (SQLException ex) {

             System.out.println(" SQLException "+ex);
            return false;
        }
    }

    static public boolean insertAffectation(RECEIVE_INEZA_AFFECTATION rii,
            Connection conn) {
        try {  
            try (PreparedStatement psInsert = conn.prepareStatement(
                    "insert into VCDC.PROVIDERS_AFFECTATIONS"
                    + "(HSP_CODE,DOCTOR_COUNCIL_NUMBER ) "
                    + " values (?,?)")) {

                psInsert.setString(1, rii.HSP_CODE);
                psInsert.setString(2, rii.COUNCIL_NUMBER); 
                psInsert.executeUpdate();
            }
            return true;
        } catch (SQLException ex) {

             System.out.println(" SQLException "+ex);
            return false;
        }
    }

    
    
    static public boolean insertPrescriber2(RECEIVE_INEZA_PRESCRIBERS rip,
            Connection conn) {
        try {

            try (PreparedStatement psInsert = conn.prepareStatement("insert into VCDC.RECEIVE_INEZA_PRESCRIBERS"
                    + "(PRESCRIBER_NAME,PRESCRIBER_TYPE, "
                    + " COUNCIL_NUMBER,QUALIFICATION, PHONE_NUMBER,"
                    + " EMAIL,LAST_UPDATE) "
                    + " values (?,?,?,?,?,?,?)")) {

                psInsert.setString(1, rip.PRESCRIBER_NAME);
                psInsert.setString(2, rip.PRESCRIBER_TYPE);
                psInsert.setString(3, rip.COUNCIL_NUMBER);
                psInsert.setString(4, rip.QUALIFICATION);
                psInsert.setString(5, rip.PHONE_NUMBER);
                psInsert.setString(6, rip.EMAIL);
                psInsert.setString(7, rip.LAST_UPDATE);

                psInsert.executeUpdate();

            }
            System.out.println("Inserted prescriber" + rip.PRESCRIBER_NAME);
            return true;
        } catch (SQLException ex) {
            System.err.println(rip.PRESCRIBER_NAME + " " + ex);
            return false;
        }
    }

    static public boolean insertHSP(RECEIVE_INEZA_HSP hsp,
            Connection conn) {
         
        try {
       
            try (PreparedStatement psInsert = conn.prepareStatement
        ("insert into VCDC.RECEIVE_INEZA_HEALTH_SERVICE_PROVIDERS"
                    + "(HSP_CODE,HSP_NAME, " +
"           DISTRICT,PHONE_NUMBER, CATEGORY" +
"           ,LAST_UPDATE) "
                    + " values (?,?,?,?,?,?)")) { 
                
                psInsert.setString(1, hsp.HSP_CODE);
                psInsert.setString(2, hsp.HSP_NAME);
                psInsert.setString(3, hsp.DISTRICT);
                psInsert.setString(4, hsp.PHONE_NUMBER);
                psInsert.setString(5, hsp.CATEGORY);
                psInsert.setString(6, hsp.LAST_UPDATED);

                psInsert.executeUpdate();
            }
            return true;
        } catch (SQLException ex) {

            System.err.println(" "+ex);
            
            return false;
        }
    }
 
     static public boolean insertINSTR(INEZA_ITEM_INSTRUCTIONS hsp,
            Connection conn) {
         
          
        try {
       
            try (PreparedStatement psInsert = conn.prepareStatement
        ("insert into VCDC.RECEIVE_INEZA_DRUGS_INSTRUCTIONS"
                    + "(INN_RWA_CODE,CATEGORY ,INSTRUCTION) "
                    + " values (?,?,?)")) { 
                
                psInsert.setString(1, hsp.INN_RWA_CODE);
                psInsert.setString(2, hsp.CATEGORY);
                psInsert.setString(3, hsp.INSTRUCTION); 

                psInsert.executeUpdate();
            }
            return true;
        } catch (SQLException ex) {

            System.err.println(" "+ex);
            
            return false;
        }
    }
 

    public static String getInteraction(String left, String droite, Statement stat) {

        try {
            String sql = "select INDICATION from VCDC.VIRTUAL_CARD_INTERRACTION "
                    + " WHERE ( INN_CODE_GAUCHE='" + left + "' AND "
                    + " INN_CODE_DROIT ='" + droite + "')  OR (INN_CODE_GAUCHE='" + droite + "' AND "
                    + " INN_CODE_DROIT ='" + left + "')";
            System.out.println("sql  " + sql);

            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
                    return outResult.getString(1);
                }
            }
        } catch (SQLException e) {

            System.out.println("ndamemneste " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
        }
        return null;
    }
    
     public static String getOpenSms( double amount, Statement stat) {

        try {
            
//       String FINTECH,      String sql = "SELECT * FROM VCDC.VCDC_SMS_FINANCIAL"
//                    + " WHERE STATUS='OPEN' and SOURCE_FIN='"+FINTECH+"' and SMS_AMOUNT= "+amount;
           
            
           String sql = "SELECT * FROM VCDC.VCDC_SMS_FINANCIAL"
                    + " WHERE STATUS='OPEN' and   SMS_AMOUNT= "+amount;
            System.out.println("sql  " + sql); 
            
            String response = "";

            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
         
                    response
                            += "<LIN>" 
                            +" <ID_FIN>" + outResult.getInt("ID_FIN") + "</ID_FIN>"
                            + "<SOURCE_FIN>" + outResult.getString("SOURCE_FIN")+ "</SOURCE_FIN>"
                            + "<DESTINATION>" + outResult.getString("DESTINATION") + "</DESTINATION>"
                            + "<BODY_SMS>" + outResult.getString("BODY_SMS") + "</BODY_SMS>"
                            + "<SMS_TIME>" + outResult.getTimestamp("SMS_DATE") + "</SMS_TIME>"
                            + "<SMS_AMOUNT>" + outResult.getDouble("SMS_AMOUNT") + "</SMS_AMOUNT>"
                            + "<STATUS>" +  outResult.getString("STATUS") + "</STATUS>"
                            + "</LIN>"
                            ;  
                    
                }
                if(response.equals(""))
                { return "NO TRANSACTION FOUND";}
                 return response;
            }
        } catch (SQLException e) {
            System.err.println("Problem getting structures:   " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
        }
        
          System.out.println("TRANSACTION found " );
        return "NO TRANSACTION FOUND";
    } 
    public static String getInstruction2(String provider, String beneficial_number
            ,String inn_code_rw,double WEIGTH, String specialite,String gender,
            int AGE,Statement stat) {

        try { 
            
            String Resultat=""; 
            
String sql = "select * from VCDC.RECEIVE_INEZA_DRUGS_INSTRUCTIONS WHERE "
                    + " INN_RWA_CODE='"+inn_code_rw+"' ";
            System.out.println(" sql "+sql);
            
               try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
                     
                 String CATEGORY     =  outResult.getString("CATEGORY");
                 String INSTRUCTION  =  outResult.getString("INSTRUCTION");
                     
                 if(CATEGORY.equals("SPECIALITE"))
                 {
                 boolean ins = specialite.contains(INSTRUCTION);
                 
                 if(!ins)
                 {Resultat +="\n  NOT_AUTHORIZED: TO PRESCRIBE "+inn_code_rw+" YOU ARE "+specialite+" NEEDED "+INSTRUCTION;}
                 }
                 if(CATEGORY.equals("GENDER"))
                 {
                  boolean ins = gender.equals(INSTRUCTION); 
                 if(!ins)
                 {Resultat +="\n NOT_AUTHORIZED: GENDER YOU ARE "+gender+" ALLOWED "+INSTRUCTION;}
                 
                 }
                 if(CATEGORY.equals("WEIGTH") && INSTRUCTION.equals("##"))
                 {
                     String []sp = INSTRUCTION.split("##");
                     int min = Integer.parseInt(sp[0]) ;
                     int max = Integer.parseInt(sp[1]) ; 
                 boolean ins = (min<= WEIGTH && WEIGTH <= max); 
                 
                 if(!ins)
                 {Resultat +="\n NOT_AUTHORIZED: YOUR WEIGTH IS "+WEIGTH+" ALLOWED "+INSTRUCTION;}
                 
                 }
                 if(CATEGORY.equals("AGE"))
                 {
                   String []sp = INSTRUCTION.split("##");
                     int min = Integer.parseInt(sp[0]) ;
                     int max = Integer.parseInt(sp[1]) ; 
                 boolean ins = (min<=AGE  && AGE <= max); 
                 
                 if(!ins)
                 {Resultat +="\n NOT_AUTHORIZED: YOUR AGE IS "+WEIGTH+" ALLOWED "+INSTRUCTION;}
                 
                 }
                 
                  if(CATEGORY.equals("MESSAGE"))
                 {
                  Resultat +="\n WARNING: READ CAREFUL THE FOLLOWING MESSAGE \n "+INSTRUCTION; 
                 }
                } 
            }
  return Resultat;
        } catch (SQLException e) {

            System.out.println("ndamemneste " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
            return "ERROR : "+e;
        } 
    }

    
 public static String getInstructionExist(String INN_RWA_CODE, String CATEGORY 
         ,Statement stat) {

        try { 
             
String sql = "select * from VCDC.RECEIVE_INEZA_DRUGS_INSTRUCTIONS WHERE "
                    + " INN_RWA_CODE='"+INN_RWA_CODE+"' AND CATEGORY='"+CATEGORY+"' ";
           
            
 try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
                     
                 return   outResult.getString("CATEGORY"); 
                } 
            }
        } catch (SQLException e) {

            System.out.println("ndamemneste " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
            return "ERROR : "+e;
        } 
        
        
  return "NO INSTRUTIONS FOUND";
    }

    
    
    public static String getLastLongPatient(Statement stat) {

        try {
            String sql = "select max(LAST_UPDATE) from VCDC.RECEIVE_INEZA_AFFILIATES  ";
            System.out.println("sql  " + sql);

            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
                    return outResult.getString(1);
                }
            }
        } catch (SQLException e) {

            System.out.println("ndamemneste " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
        }
        return "2020-01-02 11:33:19";
    }

    public static LinkedList<PATIENT_WATCHING> getContrIndication(String ID_CARD, Statement stat) {

        LinkedList<PATIENT_WATCHING> laListe = new LinkedList<>();
        try {
            String sql = "select  * from VCDC.VIRTUAL_CARD_PATIENT_WATCHING WHERE "
                    + "ID_CARD='" + ID_CARD + "' ";
            System.out.println("sql  " + sql);

            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
                    laListe.add(new PATIENT_WATCHING(outResult.getString("ID_CARD"),
                            outResult.getString("INTERRACTION"), outResult.getString("NOTES")));

                }
            }
        } catch (SQLException e) {

            System.out.println("ndamemneste " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
        }
        return laListe;
    }
    
   public static boolean imPublic(String HSP_CODE,Statement stat)
    {
   String category="";
     String sql = "select * from VCDC.RECEIVE_INEZA_HEALTH_SERVICE_PROVIDERS where"
                    + "    HSP_CODE  = '" + HSP_CODE + "'  "; 
           System.out.println(sql);
           
            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult != null && outResult.next()) { 
         category = outResult.getString("CATEGORY");  
                }  
        } catch (SQLException e) { 
            System.err.println("ndamemneste " + e); 
        }
        
        String privateCategory = "PC_PH" ;
         
        System.out.println("category "+category);
            return category!=null && !category.equals("") && !privateCategory.contains(category);
}  
    
    
    
    public static boolean getAffectation(String hsp_code,String council_number, Statement stat) {
        
        
        boolean amPublic = imPublic(hsp_code, stat);  
        System.out.println("i amPublic  "+amPublic);

      if(amPublic) {return true;} 
        try { 
            String sql = "select  * FROM VCDC.PROVIDERS_AFFECTATIONS WHERE "
                    + "  HSP_CODE = '"+hsp_code+"' and DOCTOR_COUNCIL_NUMBER= '"+council_number+"' ";
          System.out.println("sql  " + sql);

            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
                    return true;

                }
            }
        } catch (SQLException e) {

            System.out.println("ndamemneste " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
        }
        return false;
    }

    public static String getContrIndicationTXT(String ID_CARD, String code, Statement stat) {
        String res = "";
        try {
            String sql = "select  * from VCDC.VIRTUAL_CARD_PATIENT_WATCHING WHERE "
                    + "ID_CARD='" + ID_CARD + "' and  WATCHED like '%"+code+"%'";
            System.out.println("sql  " + sql);

            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
                    res += outResult.getString("WATCHED") + " : " + outResult.getString("NOTES") + " \n";

                }
            }
        } catch (SQLException e) {

            System.out.println("ndamemneste " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
        }

        System.out.println("resresresres " + res);
        
        
        return res;
    }

    public static String getPatient(String BENEFICIARY_NUMBER, Statement stat,
            Connection conneVCDC) {
        
        try { 
            
  String sql = "select * from VCDC.RECEIVE_INEZA_AFFILIATES where"
                    + " BENEFICIARY_NUMBER = '" + BENEFICIARY_NUMBER + "'   ";
 System.out.println("Inside getpatient "+sql);

            String response = "";

            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) { 
                    response
                            += //"<LIN>" 
                            " <AFFILIATES_NUMBER>" + outResult.getString(1) + "</AFFILIATES_NUMBER>"
                            + "<AFFILIATE_NOM>" + outResult.getString("AFFILIATE_NOM") + "</AFFILIATE_NOM>"
                            + "<AFFILIATE_PRENOM>" + outResult.getString("AFFILIATE_PRENOM") + "</AFFILIATE_PRENOM>"
                            + "<BENEFICIARY_NUMBER>" + outResult.getString(2) + "</BENEFICIARY_NUMBER>"
                            + "<BENEFICIARY_FIRST_NAME>" + outResult.getString(3) + "</BENEFICIARY_FIRST_NAME>"
                            + "<BENEFICIARY_LAST_NAME>" + outResult.getString(4) + "</BENEFICIARY_LAST_NAME>"
                            + "<RELATIONSHIP>" + outResult.getString(5) + "</RELATIONSHIP>"
                            + "<GENDER>" + outResult.getString("GENDER") + "</GENDER>"
                            + "<EMPLOYER>" + outResult.getString("EMPLOYER") + "</EMPLOYER>"
                            + "<EMPLOYER_CATEGORY>" + outResult.getString("EMPLOYER_CATEGORY") + "</EMPLOYER_CATEGORY>"
                            + "<AFFILIATION_STATUS>" + outResult.getString("AFFILIATION_STATUS") + "</AFFILIATION_STATUS>"
                            + "<PHONE_NUMBER>" + outResult.getString("PHONE_NUMBER") + "</PHONE_NUMBER>"
                            + "<ID_CARD_NUMBER>" + outResult.getString("ID_CARD_NUMBER") + "</ID_CARD_NUMBER>"
                            + "<DOB>" + outResult.getString("DOB") + "</DOB>"
                            + "<LAST_UPDATE>" + outResult.getString("LAST_UPDATE") + "</LAST_UPDATE>"
                            + "<ASSURANCE>RAMA</ASSURANCE>"
                            + "<PIN>" + outResult.getInt("PIN") + "</PIN>"
                            + "<ALLERGIES>" + outResult.getString("ALLEGIES") + "</ALLEGIES>"
                            + "<PERCENTAGE>15</PERCENTAGE>" //+ "</LIN>"
                            ;
                    return response;
                }

            }
        } catch (SQLException e) {

            System.err.println("Problem getting structures:   " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
        }
        return "ERROR : NO AFFILIATE FOUND";
    }

    public static String getItem(String ITEM_INN_RWA_CODE, Statement stat) {

        try {
            String sql = "select * from VCDC.RECEIVE_INEZA_DRUGS where" 
                    + "  ITEM_INN_RWA_CODE  = '" + ITEM_INN_RWA_CODE + "'  ";

            System.out.println("ineza_vcdc.DbhandlerVCDC.getItem() "+sql);
            String response = "";

            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
                    System.out.println("++++" + outResult.getString(1) + ";" + outResult.getString(2) + ";"
                            + outResult.getString(3)
                            + ";" + outResult.getString(4) + ";" + outResult.getString(5));

                    response
                            += //"<LIN>" 
                            " <ITEM_INN_RWA_CODE>" + outResult.getString(1) + "</ITEM_INN_RWA_CODE>"
                            + "<ITEM_NAME>" + outResult.getString(2) + "</ITEM_NAME>"
                            + "<ITEM_RHIA_NAME>" + outResult.getString(3) + "</ITEM_RHIA_NAME>"
                            + "<INN>" + outResult.getString(6) + "</INN>"
                            + "<ATC_CODE>" + outResult.getString("ATC_CODE") + "</ATC_CODE>"
                            + "<SELLING_PRICE>" + outResult.getString("SELLING_PRICE") + "</SELLING_PRICE>"
                            + "<MANUFACTURER>" + outResult.getString("MANUFACTURER") + "</MANUFACTURER>"
                            + "<SELLING_UNIT>" + outResult.getString("SELLING_UNIT") + "</SELLING_UNIT>"
                            ;
                    return response;
                }

            }
        } catch (SQLException e) {

            System.err.println("Problem getting structures:   " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
        }
        return "ITEM NOT FOUND";
    }
 
    public static int getMaxItemIdProduct(Statement stat) {

        try {
            String sql = "select max(ID_PRODUCT) from VCDC.RECEIVE_INEZA_DRUGS ";

            System.out.println("ineza_vcdc.DbhandlerVCDC.getItem() "+sql);
            String response = "";

            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
                   return outResult.getInt(1);
                }

            }
        } catch (SQLException e) { 
            System.err.println("Problem getting structures:   " + e); 
        }
        return 0;
    }
 

      public static String getPrescriber(String COUNCIL_NUMBER, Statement stat) {

        try {
            String sql = "select * from VCDC.RECEIVE_INEZA_PRESCRIBERS where"
                     + "    COUNCIL_NUMBER  = '" + COUNCIL_NUMBER + "' or PRESCRIBER_NAME  like '%" + COUNCIL_NUMBER + "%' "
                    + " order by PRESCRIBER_NAME"; 
           
            
  System.out.println("sql 2 " + sql);
 
            String response = "";

            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
         
                    response
                            += "<LIN>" 
                            +" <PRESCRIBER_NAME>" + outResult.getString("PRESCRIBER_NAME") + "</PRESCRIBER_NAME>"
                            + "<PRESCRIBER_TYPE>" + outResult.getString("PRESCRIBER_TYPE") + "</PRESCRIBER_TYPE>"
                            + "<COUNCIL_NUMBER>" + outResult.getString("COUNCIL_NUMBER") + "</COUNCIL_NUMBER>"
                            + "<QUALIFICATION>" + outResult.getString("QUALIFICATION") + "</QUALIFICATION>"
                            + "<PHONE_NUMBER>" + outResult.getString("PHONE_NUMBER") + "</PHONE_NUMBER>"
                            + "<EMAIL>" + outResult.getString("EMAIL") + "</EMAIL>"
                            + "<LAST_UPDATE>" + outResult.getString("LAST_UPDATE") + "</LAST_UPDATE>"
                            + "</LIN>"
                            ; 
                      return response;
                }
               
            }
        } catch (SQLException e) {
            System.err.println("Problem getting structures:   " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
        }
        
          System.out.println("sql 2 found " );
        return "NO PRESCRIBER FOUND";
    }
      
    public static String getPrescriberALL(String COUNCIL_NUMBER, Statement stat) {

        try {
            String sql = "select * from VCDC.RECEIVE_INEZA_PRESCRIBERS where"
                     + "    COUNCIL_NUMBER  = '" + COUNCIL_NUMBER + "' or PRESCRIBER_NAME  like '%" + COUNCIL_NUMBER + "%' "
                    + " order by PRESCRIBER_NAME"; 
           
            
  System.out.println("sql 2 " + sql);
 
            String response = "";

            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
         
                    response
                            += "<LIN>" 
                            +" <PRESCRIBER_NAME>" + outResult.getString("PRESCRIBER_NAME") + "</PRESCRIBER_NAME>"
                            + "<PRESCRIBER_TYPE>" + outResult.getString("PRESCRIBER_TYPE") + "</PRESCRIBER_TYPE>"
                            + "<COUNCIL_NUMBER>" + outResult.getString("COUNCIL_NUMBER") + "</COUNCIL_NUMBER>"
                            + "<QUALIFICATION>" + outResult.getString("QUALIFICATION") + "</QUALIFICATION>"
                            + "<PHONE_NUMBER>" + outResult.getString("PHONE_NUMBER") + "</PHONE_NUMBER>"
                            + "<EMAIL>" + outResult.getString("EMAIL") + "</EMAIL>"
                            + "<LAST_UPDATE>" + outResult.getString("LAST_UPDATE") + "</LAST_UPDATE>"
                            + "</LIN>"
                            ; 
                    
                }
                if(response.equals(""))
                { return "NO PRESCRIBER FOUND";}
                 return response;
            }
        } catch (SQLException e) {
            System.err.println("Problem getting structures:   " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
        }
        
          System.out.println("sql 2 found " );
        return "NO PRESCRIBER FOUND";
    }
      public static String getRECEIVE_INEZA_HSP(String HSP_CODE, Statement stat) {
 
        try {
            String sql = "select * from VCDC.RECEIVE_INEZA_HEALTH_SERVICE_PROVIDERS where"
                    + "    HSP_CODE  = '" + HSP_CODE + "' or HSP_NAME  like '%" + HSP_CODE + "%' "
                    + " order by HSP_NAME"; 
           
            System.out.println("ineza_vcdc.DbhandlerVCDC.getRECEIVE_INEZA_HEALTH_SERVICE_PROVIDERS()"
                    + " \n "+sql);
            String response="";
            
            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult != null && outResult.next()) {
            
         response += 
                  "<LIN>" 
                +" <HEALTH_FACILITY_CODE>"   + outResult.getString(1) + "</HEALTH_FACILITY_CODE>"
                + "<HEALTH_FACILITY_NAME>" + outResult.getString(2) + "</HEALTH_FACILITY_NAME>"
                + "<DISTRICT>" + outResult.getString(3) + "</DISTRICT>"
                + "<PHONE_NUMBER>" + outResult.getString(4) + "</PHONE_NUMBER>"
                + "<CATEGORY>" + outResult.getString(5) + "</CATEGORY>"
                + "<LAST_UPDATED>"   + outResult.getString(6) + "</LAST_UPDATED>"           
                + "</LIN>"
             ; 
           
                }
 return response;
            }
        } catch (SQLException e) {

            System.err.println("ndamemneste " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
        }
        return "NO HSP FOUND";
    }

    public static long getLastLongItem(Statement stat) {

        try {
            String sql = "select max(LAST_UPDATE) from VCDC.RECEIVE_INEZA_DRUGS  ";
            System.out.println("sql  " + sql);

            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
                    return outResult.getLong(1);
                }
            }
        } catch (SQLException e) {

            System.err.println("Problem getting structures:   " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
        }
        return 0;
    }

    public static long getLastLongPrescriber(Statement stat) {

        try {
            String sql = "select max(LAST_UPDATE) from VCDC.RECEIVE_INEZA_PRESCRIBERS  ";
            System.out.println("sql  " + sql);

            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
                    return outResult.getLong(1);
                }
            }
        } catch (SQLException e) {

            System.err.println("Problem getting structures:   " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
        }
        return 0;
    }

    public static long getLastLongHSP(Statement stat) {

        try {
            String sql = "select max(HSP_CODE) from VCDC.RECEIVE_INEZA_HEALTH_SERVICE_PROVIDERS  ";
             System.out.println("sql  "+sql); 
            
            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
                    return outResult.getLong(1);
                }
            }
        } catch (SQLException e) { 
            System.err.println("Problem getting structures:   " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
        }
        return 0;
    }

    public static boolean updatePatient2(INEZA_AFFILIATES uwuje, Connection conn) { 
        String sql = "update VCDC.RECEIVE_INEZA_AFFILIATES set " 
                + "   EMPLOYER ='" + uwuje.EMPLOYER
                + "', EMPLOYER_CATEGORY ='" + uwuje.EMPLOYER_CATEGORY
                + "', AFFILIATION_STATUS ='" + uwuje.AFFILIATION_STATUS
                + "', PHONE_NUMBER ='" + uwuje.PHONE_NUMBER
                 + "', ALLEGIES ='" + uwuje.ALLEGIES
                + "', ID_CARD_NUMBER ='" + uwuje.ID_CARD_NUMBER
                + "', PIN =" + uwuje.PIN
                + " where  BENEFICIARY_NUMBER ='" + uwuje.BENEFICIARY_NUMBER + "'";
         
        System.out.println(" "+sql); 
        try {
            Statement stat = conn.createStatement();
            stat.execute(sql);
            System.out.println("updated patient");
            return true;
        } catch (SQLException ex) {
            System.err.println("Error " + ex);
            return false;
        }

    }

    public static boolean updateItem(RECEIVE_INEZA_ITEM uwuje, Connection conn) {
        boolean updated = false;

        String sql = "update VCDC.RECEIVE_INEZA_DRUGS set "
                + " ITEM_INN_RWA_CODE ='" + uwuje.INN_RWA_CODE
                + "',ITEM_NAME ='" + uwuje.ITEM_NAME
                + "',ITEM_RHIA_NAME ='" + uwuje.ITEM_RHIA_NAME 
                + "',INN ='" + uwuje.INN
                 + "',SELLING_PRICE =" + uwuje.SELLING_PRICE
                + ",ID_PRODUCT =" + uwuje.ID_PRODUCT
                + ",MANUFACTURER ='" + uwuje.MANUFACTURER
                + "',SELLING_UNIT =" + uwuje.SELLING_UNIT
                + " where ATC_CODE='" + uwuje.ATC_CODE + "'";
        try {
            Statement stat = conn.createStatement();
            updated = stat.execute(sql);
            System.out.println("Update item" + uwuje.ATC_CODE);
            return true;
        } catch (SQLException ex) {
            System.err.println("Error " + ex);
            return false;
        }
//  return updated;
    }

    public static boolean updatePrescriber(RECEIVE_INEZA_PRESCRIBERS uwuje, Connection conn) {
        boolean updated = false;

        String sql = "update VCDC.RECEIVE_INEZA_PRESCRIBERS set "
                + " PRESCRIBER_NAME ='" + uwuje.PRESCRIBER_NAME
                + "', PRESCRIBER_TYPE ='" + uwuje.PRESCRIBER_TYPE
                + "', QUALIFICATION ='" + uwuje.QUALIFICATION
                + "', PHONE_NUMBER ='" + uwuje.PHONE_NUMBER
                + "', EMAIL ='" + uwuje.EMAIL
                + "', LAST_UPDATE ='" + uwuje.LAST_UPDATE
                + "' where COUNCIL_NUMBER='" + uwuje.COUNCIL_NUMBER + "'";

        System.out.println("  " + sql);

        try {
            Statement stat = conn.createStatement();
            stat.execute(sql);
            System.out.println("Updated a prescriber" + uwuje.PRESCRIBER_NAME);
            return true;
        } catch (SQLException ex) {
            System.err.println("Error " + ex);
            return false;
        }

    }

    public static boolean updateHSP(RECEIVE_INEZA_HSP uwuje, Connection conn) {
        boolean updated = false;
//          public RECEIVE_INEZA_HSP(String HSP_CODE, 
//                  String HSP_NAME, String DISTRICT, String PHONE_NUMBER, String CATEGORY, long LAST_UPDATED) {

        String sql = "update VCDC.RECEIVE_INEZA_HEALTH_SERVICE_PROVIDERS set "
                + " HSP_NAME ='" + uwuje.HSP_NAME
                + "',DISTRICT ='" + uwuje.DISTRICT
                + "',PHONE_NUMBER ='" + uwuje.PHONE_NUMBER
                + "',CATEGORY ='" + uwuje.CATEGORY
                + "',LAST_UPDATE ='" + uwuje.LAST_UPDATED
                + "' where HSP_CODE='" + uwuje.HSP_CODE + "'";

        System.out.println("  " + sql);

        try {
            Statement stat = conn.createStatement();
            updated = stat.execute(sql);
            System.out.println("Updated HSP" + uwuje.HSP_NAME);
            return true;
        } catch (SQLException ex) {
            System.err.println("Error " + ex);
            return false;
        }
//        return updated;
    }
    
     public static boolean updateINSTRUCTION(INEZA_ITEM_INSTRUCTIONS uwuje, Statement stat) {
      
        String sql = "update VCDC.RECEIVE_INEZA_DRUGS_INSTRUCTIONS set "
                + " INSTRUCTION ='" + uwuje.INSTRUCTION
                + "' where CATEGORY='" + uwuje.CATEGORY + "' AND INN_RWA_CODE='" + uwuje.INN_RWA_CODE + "' ";

        System.out.println("  " + sql);

        try { 
            stat.execute(sql); 
            return true;
        } catch (SQLException ex) {
            System.err.println("Error " + ex);
            return false;
        }
//        return updated;
    }

    public static boolean isValide(String table_name, Statement st) { //VALIDITY argument: UPDATE IN 7 DAYS otherwise Local DB is Invalid
       
        String sql = "SELECT LAST_UPDATED FROM VCDC.VCDC_UPDATE_TRACKER WHERE TABLE_NAME='"+table_name+"'";
        System.out.println("Is db not older Query:"+sql);
        long last_updated_time=0;
         Date date= new Date();
	 long time = date.getTime();
	 Timestamp now = new Timestamp(time);
	 System.out.println("Current Time Stamp: "+now);
        try (ResultSet outResult = st.executeQuery(sql)) {
            System.out.println("Inside getting last update");
                while (outResult.next()) {
                    last_updated_time= outResult.getTimestamp("LAST_UPDATED").getTime();
                    System.out.println("Last Update"+last_updated_time);
                } 
            return (time-last_updated_time)/(1000*60*60*24) <= 70;
        } catch (SQLException ex) {
            System.err.println("encountered "+ex);
            return false;
        } 
    }

    public static String lastUpdate(String table_name, Statement st) { //VALIDITY argument UPDATE IN 7 DAYS

        String sql = "SELECT LAST_UPDATED FROM VCDC.VCDC_UPDATE_TRACKER WHERE TABLE_NAME='" + table_name + "'";
        String last_updated_time = "2020-01-01 00:00:00.000";
        Date date = new Date();
        long time = date.getTime();
        Timestamp now = new Timestamp(time);
        System.out.println("Current Time Stamp: " + now);
        try (ResultSet outResult = st.executeQuery(sql)) {

            while (outResult.next()) {
                last_updated_time = outResult.getString("LAST_UPDATED");
            }
        } catch (SQLException ex) {
            System.err.println("encountered getting last update" + ex);
        }
        return last_updated_time;
    }
         
 public static boolean updateHDOT(
         String SIGNATURE,String PUSHED_XML,
         String SYNCED_RESPONSE,String BENEFICIAL_NEMBER,
         String HEURE,String GLOBAL_ACCOUNT,String MRC,Connection conn) {
        boolean updated=false; 
        
        String sql = " update VCDC.VIRTUAL_CARD_ODT_SENT set " 
                + " MRC ='"+MRC
                + "', SIGNATURE ='"+SIGNATURE
                + "', PUSHED_XML ='"+PUSHED_XML
                + "', SYNCED_RESPONSE ='"+SYNCED_RESPONSE
                + "' where BENEFICIAL_NEMBER='" + BENEFICIAL_NEMBER + "' and "
                + "HEURE='" + HEURE + "' and "
                + "GLOBAL_ACCOUNT='" + GLOBAL_ACCOUNT + "'";
        System.out.println(" "+sql);
        try {
            Statement stat = conn.createStatement();
            updated = stat.execute(sql);

        } catch (SQLException ex) {
            System.err.println("Error " + ex);
        }
       // JOptionPane.showInputDialog(" "+updated);
  return updated;
    }     
 
 public static boolean updateHDOTREPUSHED( 
         String SYNCED_RESPONSE,String BENEFICIAL_NEMBER,
         String HEURE,String GLOBAL_ACCOUNT,String MRC,Connection conn) {
        boolean updated=false; 
        
        String sql = " update VCDC.VIRTUAL_CARD_ODT_SENT set " 
                + " MRC ='"+MRC 
                + "', SYNCED_RESPONSE ='"+SYNCED_RESPONSE
                + "' where BENEFICIAL_NEMBER='" + BENEFICIAL_NEMBER + "' and "
                + "HEURE='" + HEURE + "' and "
                + "GLOBAL_ACCOUNT='" + GLOBAL_ACCOUNT + "'";
        System.out.println(" "+sql);
        try {
            Statement stat = conn.createStatement();
            updated = stat.execute(sql);

        } catch (SQLException ex) {
            System.err.println("Error " + ex);
        }
        JOptionPane.showInputDialog(" "+updated);
  return updated;
    }     
 
 
  static public int insertDOT(ODT prescription,
            Connection conn) {
        try {

            try (PreparedStatement psInsert = conn.prepareStatement
        ("insert into VCDC.VIRTUAL_CARD_ODT_SENT"
                    + "(BENEFICIAL_NEMBER,GLOBAL_ACCOUNT,HEURE,SIGNATURE,MRC) "
                    + " values (?,?,?,?,?)")) {  
                psInsert.setString(1, prescription.ria.BENEFICIARY_NUMBER);
                psInsert.setString(2, prescription.GLOBAL_ACCOUNT);
                psInsert.setString(3, prescription.TIME);  
                psInsert.setString(4,prescription.TIME); 
                psInsert.setString(5,prescription.MRC); 
                psInsert.executeUpdate();
            } 
            return getODTid(  prescription,conn.createStatement());
        } catch (SQLException ex) { 
            System.err.println("insertDOT "+ex);
            return -1;
        }
        
    }
  
  
  static public boolean insertDISEASE(ODT_ICD10 indwara,PreparedStatement psInsert) {
        try {
   
                psInsert.setString(1, indwara.ID_ICD10);
                psInsert.setString(2, indwara.CODE_ICD10);
                psInsert.setString(3, indwara.NAME_SHORT);  
                psInsert.setString(4,indwara.NAME_LONG); 
                psInsert.setString(5,indwara.CLASS); 
                psInsert.executeUpdate();
            
            return true;
        } catch (SQLException ex) {  
            System.err.println(" "+indwara);
            return false;
        }
        
    }
  
  
     public static int getODTid(ODT prescription,Statement stat) {

        try {
            String sql = "select ID_RECEIPT from VCDC.VIRTUAL_CARD_ODT_SENT WHERE "
                    + " BENEFICIAL_NEMBER = '"+prescription.ria.BENEFICIARY_NUMBER+"' and "
                    + " GLOBAL_ACCOUNT = '"+prescription.GLOBAL_ACCOUNT+"' and "
                    + " HEURE = '"+prescription.TIME+"'  ";
            System.out.println("sql  " + sql);

            try (ResultSet outResult = stat.executeQuery(sql)) {
                while (outResult.next()) {
                    return outResult.getInt(1);
                }
            }
        } catch (SQLException e) {

            System.out.println("ndamemneste " + e);
            //  GuiSdc.console += ("\n ERROR " + " ... GETAXPAYER  :" + e);
        }
        return 0;
    }  
     
     
     static String verification_Response(String code, String table, Statement stat) {
        String col = "";
        if (table.equals("RECEIVE_INEZA_HEALTH_SERVICE_PROVIDERS")) {
            col = " HSP_CODE";
        }
        if (table.equals("RECEIVE_INEZA_PRESCRIBERS")) {
            col = " COUNCIL_NUMBER";
        }

        try {
            String sql = "SELECT * FROM VCDC." + table + "  WHERE " + col + " = '" + code + "'";
//            GuiSdc.console +=("\n" + sql);
            ResultSet outResult = stat.executeQuery(sql);
            System.out.println(sql);
            while (outResult.next()) {
                if (outResult.getString(1) != null && !"".equals(outResult.getString(1))) {
                    return "FOUND";
                }
            }

        } catch (SQLException e) {
            System.err.println("ERROR :::" + e);

        }

        return "MISSING," + code;
    }

    static String total_Verification(double tot, LinkedList<ODT_LIST> list, Statement stat) {
        double sum = 0;
        for (int i = 0; i < list.size(); i++) {
            try {
                ODT_LIST l = list.get(i);
                ResultSet outResult = stat.executeQuery("select * from vcdc.RECEIVE_INEZA_DRUGS where ITEM_INN_RWA_CODE = '" + l.INN_RW_CODE_REQUESTED + "'");
                System.out.println("select * from vcdc.RECEIVE_INEZA_DRUGS where ITEM_INN_RWA_CODE = '" + l.INN_RW_CODE_REQUESTED + "'");
                while (outResult.next()) {
                    if (outResult.getDouble("SELLING_PRICE") != l.PRICE) {
                        return "MISSING";
                    }
                    sum += l.QUANTITY_REQUESTED * l.PRICE;
                }
            } catch (SQLException ex) {
                System.err.println("karabaye da>>>" + ex);
            }
        }
        if (tot == sum) {
            return "SUCCESS";
        }

        return "MISSING";
    }

    public static LinkedList<Integer> getIdInvoiceUnPushedReceipt(String table, Statement STATEMENTDATA) {
        LinkedList<Integer> list = new LinkedList();
        try {

            String sql = "select  * from APP." + table + " where  "
                    + "  vcdc_doc !=5  and NUM_CLIENT = 'RAMA' and NUM_CLIENT IS NOT NULL";
//      String sql = "select  * from APP."+table+" where  "
//            + "  ID_INVOICE =16190  and NUM_CLIENT IS NOT NULL";
            System.out.println(sql);
            ResultSet outResult = STATEMENTDATA.executeQuery(sql);
            while (outResult.next()) {
                list.add(outResult.getInt("ID_" + table));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public static String updatePUSHEDReceipt(String invoice, String table, Statement STATEMENT) {
        try {
            STATEMENT.execute("update APP." + table + " set  vcdc_doc = 5  where   ID_" + table + "=" + invoice + "");
        } catch (SQLException e) {
            return "SQL ERROR " + e;
        }
        return "SUCCESS";
    }
    
    public static String getValue(String s, Statement stat) {
        String ret = " ";
        Variable var = getParam("RUN", "", s, stat);
        if (var != null) {
            ret = var.value;
        }
        return ret;
    }
    
    static Variable getParam(String famille, String mode, String nom, Statement s) {
        Variable par = null;
        try {
            String sql = "select  * from APP.VARIABLES where APP.variables.nom_variable= '" + nom + "'  and APP.variables.famille_variable= '" + famille + "' ";
           // System.out.println("select  * from APP.VARIABLES where APP.variables.nom_variable= '" + nom + "'  and APP.variables.famille_variable= '" + famille + "' ");
            ResultSet outResult = s.executeQuery(sql);
//System.out.println("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"'  and APP.variables.famille_variable= '"+famille+"' ");

            while (outResult.next()) {
                String num = outResult.getString(1);
                String value = outResult.getString(2);
                String fam = outResult.getString(3);
                String value2 = outResult.getString(4);
                par = (new Variable(num, value, fam, value2));
            }
        } catch (SQLException e) {
            System.err.println("DbhandlerVCDC : " + e);
        }
        return par;
    }

}
