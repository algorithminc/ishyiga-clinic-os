package ineza_vcdc;

import java.util.LinkedList;

/* INEZA_RECEIPT for Ishyiga copyright 2007 Kimenyi Aimable Gestion des factures
 */
public class INEZA_RECEIPT {

    String MRC;
    public int ID_INVOICE, ID_INVOICE_REFUNDED;
    String DATE, AUTHORITY_DATA;
    String PRINT4;
    double RESTE;
    boolean niOk = false;
    String clientTin;
    double TOTAL4;
    String EMPLOYE;
    String HEURE;
    double TVA4;
    int DOCUMENT;
    char SCANNED;
    String MODE;
    String STATUS;
    String SERVED;
    String RETOUR;
    String NUM_FACT2;
    String RACINE;
    public String REFERENCE;
    String COMPTABILISE;
    String IMPRIME;
    String TARIF;
    String MONNAIE_INVOICE;
    double DEVISE_INVOICE;
    public int REDUCTION;
    String DEPOT;
    String CODEBAR, PROFIT_CENTER, NAME_CLIENT, NAME_GOOD, TIN_NUMBER;
    public LinkedList<INEZA_RECEIPT_ITEM> productList;
    String devise = "";
    String clientName, clientAdress;
    String gerne = "";
    double CASH, CREDIT;

    double totA, totB, totC, totD, HORSTAXE, taxB, taxC, taxD;
    String export = "";
    public String extData = "", totRcptSeq;
    double SALESPRICE, EXONERE, EXPORT, TAXABLE;
    int rcptSeq;
    CLIENT client;
    String ID_COMPANY_BRANCH, PHARMACY_CODE;

    public INEZA_RECEIPT(int ID_INVOICE, String DATE,
            double TOTAL, String EMPLOYE, String HEURE, double TVA, int DOCUMENT, char SCANNED,
            String MODE, String STATUS, String SERVED, String RETOUR, String NUM_FACT, String RACINE, String REFERENCE,
            String COMPTABILISE, String IMPRIME, String TARIF, double DEVISE_INVOICE, String MONNAIE_INVOICE,
            int REDUCTION, String DEPOT, String PROFIT_CENTER, double CASH, double CREDIT,
            LinkedList<INEZA_RECEIPT_ITEM> productList) {
        this.ID_INVOICE = ID_INVOICE;
        this.DATE = DATE;
        this.DEPOT = DEPOT;
        this.TOTAL4 = TOTAL;
        this.EMPLOYE = EMPLOYE;
        this.HEURE = HEURE;
        this.TVA4 = TVA;
        this.DOCUMENT = DOCUMENT;
        this.SCANNED = SCANNED;
        this.MODE = MODE;
        this.STATUS = STATUS;
        this.SERVED = SERVED;
        this.RETOUR = RETOUR;
        this.NUM_FACT2 = NUM_FACT;
        this.REFERENCE = REFERENCE;
        this.COMPTABILISE = COMPTABILISE;
        this.IMPRIME = IMPRIME;
        this.TARIF = TARIF;
        this.DEVISE_INVOICE = DEVISE_INVOICE;
        this.MONNAIE_INVOICE = MONNAIE_INVOICE;
        this.productList = new LinkedList();
        this.REDUCTION = REDUCTION;
        this.CASH = CASH;
        this.CREDIT = CREDIT;
        this.PROFIT_CENTER = PROFIT_CENTER;
        this.RACINE = RACINE;
    }

    String paymentMode() {
//    07 04 BANK CHECK BANK CHECK PAYMENT
//07 05 DEBIT/CREDIT CARD PAYMENT USING CARD
//07 06 MOBILE MONEY ANY TRANSACTION USING MOBILE MONEY SYSTEM
//07 07 OTHER OTHER MEANS OF PAYMENT
//07 01 CASH CASH
//07 02 CREDIT CREDIT
//07 03 CASH/CREDIT CASH/CREDIT
//    
        if (MODE.equals("FACTURE CASH")) {
            return "01";
        } else if (MODE.equals("FACTURE CREDIT")) {
            return "02";
        } else if (MODE.equals("MOMO")) {
            return "06";
        } else if (MODE.equals("VISA")) {
            return "05";
        } else if (MODE.equals("CHECK")) {
            return "04";
        } else if (CREDIT == 0) {
            return "01";
        } else {
            return "02";
        }
    }

    String getReIDpurchase() {
        if (AUTHORITY_DATA != null) {
            String[] sp = AUTHORITY_DATA.split(";");
            if (sp.length == 6) {
                String OPERATIONCD = sp[0];
                return OPERATIONCD;
            }
            return "";
        } else {
            return "";
        }
    }

public String doXml(LinkedList<INEZA_RECEIPT_ITEM> itemList) {
        String xml = "";
        if (itemList == null) {
            System.out.println(" ... inv itemList is null :");
        } else if (itemList.isEmpty()) {
            System.out.println(" ... inv itemList is EMPTY :");
        } else {
            for (int i = 0; i < itemList.size(); i++) {
INEZA_RECEIPT_ITEM ineza_receipt_item = itemList.get(i);
xml += "<LINE> "
+ "<ID_COMPANY_BRANCH>"+ineza_receipt_item.ID_COMPANY_BRANCH+"</ID_COMPANY_BRANCH>"
+ "<INVOICE_PHARMACY_ID>"+ineza_receipt_item.ID_INVOICE+"</INVOICE_PHARMACY_ID>" 
+ "<INN_RWA_CODE>" + ineza_receipt_item.INN_RWA_CODE + "</INN_RWA_CODE> "
+ "<ITEM_QUANTITY>" + ineza_receipt_item.ITEM_QUANTITY + "</ITEM_QUANTITY> "
+ "<ITEM_UNITY_PRICE>" + ineza_receipt_item.ITEM_UNITY_PRICE + "</ITEM_UNITY_PRICE> "
+ "<ITEM_BATCH>" + ineza_receipt_item.BATCH + "</ITEM_BATCH> "
+ "<ITEM_EXPIRE>" + ineza_receipt_item.EXPIRE + "</ITEM_EXPIRE> "
+ "<BON_LIV>" + ineza_receipt_item.BON_LIV + "</BON_LIV> "
+ "</LINE>";
            }
        }
        return xml;
    }

}
