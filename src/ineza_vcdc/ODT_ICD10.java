/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

 

/**
 *
 * @author ishyiga
 */
public class ODT_ICD10 {
  
//     create table "VCDC".RECEIVE_INEZA_ICD10
//(
//	ID_ICD10 VARCHAR(10) not null,
//	CODE_ICD10 VARCHAR(20) not null primary key,
//	NAME_SHORT VARCHAR(80),
//	NAME_LONG VARCHAR(150)
//        
    String  ID_ICD10,CODE_ICD10,NAME_SHORT,NAME_LONG,CLASS; 

    public ODT_ICD10(String ID_ICD10, String CODE_ICD10, String NAME_SHORT, String NAME_LONG,String CLASS) {
        this.ID_ICD10 = ID_ICD10;
        this.CODE_ICD10 = CODE_ICD10;
        this.NAME_SHORT = NAME_SHORT;
        this.NAME_LONG = NAME_LONG;
         this.CLASS = CLASS;
    }
      
    @Override
    public String toString()
    {
    return NAME_SHORT +" "+CODE_ICD10 ;
    }
    
}
