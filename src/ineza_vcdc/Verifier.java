package ineza_vcdc;

import static ineza_vcdc.DbhandlerVCDC.getAffectation;
import java.sql.Statement;
import java.util.LinkedList; 

/**
 *
 * @author Corneille
 */
public class Verifier {
 
    VERIFIED_DATA verified_data;
    Statement state;
    public Verifier(Statement state) {
        this.state = state;
    }

    public String receipt_verified(String data) {
        System.out.println("start verifications..." + data);
        if (data != null) {
            verified_data = new VERIFIED_DATA(data);
            int rate=100;
            String failed="";
            if (!check_hsp(verified_data.HSP)) {
                failed +="<ERROR>"+errorHandling("E01")+"</ERROR>";
                rate -=20;
            }

            if (!check_prescriber(verified_data.PRESCRIBER)) { 
                 failed +="<ERROR>"+errorHandling("E03")+"</ERROR>";
                 rate -=20;
            }
            
            if (!(getAffectation(verified_data.HSP,verified_data.PRESCRIBER,  state))) {
                
                failed +="<ERROR>"+errorHandling("E05")+"</ERROR>";
                 rate -=30;
            }
            

            if (!check_total(verified_data.TOT_AMOUNT, verified_data.ITEMS)) {
                 
                failed +="<ERROR>"+errorHandling("E02")+"</ERROR>";
                 rate -=30;
            }

            //ALL VERIFIED  ACCEPTANCE_RATE
            return "<ACCEPTANCE_RATE>"+rate+"</ACCEPTANCE_RATE>"+failed;
        }
        return errorHandling("");
    }

    boolean check_hsp(String hsp_id) {
        String resp = DbhandlerVCDC.verification_Response(hsp_id, "RECEIVE_INEZA_HEALTH_SERVICE_PROVIDERS", state);
        return !(resp != null && resp.contains("MISSING"));
    }

    boolean check_prescriber(String id_prescriber) {
        String resp = DbhandlerVCDC.verification_Response(id_prescriber, "RECEIVE_INEZA_PRESCRIBERS", state);
        return !(resp != null && resp.contains("MISSING"));
    }

    boolean check_total(double tot, LinkedList<ODT_LIST> list) {
        String resp = DbhandlerVCDC.total_Verification(tot, list, state);
        return !(resp != null && resp.contains("MISSING"));
    }

    public String errorHandling(String E) {

        if (E.contains("E00")) {
            return " NOT EXIST";
        } else if (E.contains("E01")) {
            return " HSP  NOT EXIST";
        } else if (E.contains("E02")) {
            return " ITEM(S) PRICE(S) NOT MATCHING WITH RHIA LIST";
        } else if (E.contains("E03")) {
            return " INVALID PRESCRIBER ";
        } else if (E.contains("E04")) {
            return " PRICES MISMATCH  ";
        } else if (E.contains("E05")) {
            return " PRESCRIBER NOT AFFECTED ";
        } else {
            return "ERROR NOT SUPPORTED ";
        }

    }

}
