/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;
 

/**
 *
 * @author ishyiga
 */
public class RECEIVE_INEZA_ITEM { 
    
   String  INN_RWA_CODE,ITEM_NAME,ITEM_RHIA_NAME,INN,ATC_CODE,MANUFACTURER,AVAILABILITY;
   int SELLING_UNIT,ID_PRODUCT;
   double SELLING_PRICE;  
   long LAST_UPDATE;

   String CODE_PRODUCT,NAME_PRODUCT,FAMILLE,OBSERVATION,CODE_BAR;
   double TVA,PRIX;
   int VEDETTE_ID;
   	
   public RECEIVE_INEZA_ITEM(String xml,boolean bralirwa)
   {
        this.CODE_PRODUCT = HTTP_URL.Igicu.getTagValue("CODE_PRODUCT", xml);
        this.NAME_PRODUCT = HTTP_URL.Igicu.getTagValue("NAME_PRODUCT", xml);
        this.FAMILLE = HTTP_URL.Igicu.getTagValue("FAMILLE", xml); 
        this.OBSERVATION = HTTP_URL.Igicu.getTagValue("OBSERVATION", xml);
        this.CODE_BAR = HTTP_URL.Igicu.getTagValue("CODE_BAR", xml);
        this.TVA = HTTP_URL.Igicu.getTagValueDouble("TVA", xml);
        this.PRIX = HTTP_URL.Igicu.getTagValueDouble("PRIX", xml); 
        this.VEDETTE_ID = 0; ///HTTP_URL.Igicu.getTagValueInteger("VEDETTE_ID", xml);  
   
   }
     
   
   
    public RECEIVE_INEZA_ITEM(String INN_RWA_CODE, String ITEM_NAME, String ITEM_RHIA_NAME, 
              String INN, String ATC_CODE, String MANUFACTURER, double SELLING_PRICE) {
     
        this.INN_RWA_CODE = INN_RWA_CODE;
        this.ITEM_NAME = ITEM_NAME;
        this.ITEM_RHIA_NAME = ITEM_RHIA_NAME; 
        this.INN = INN;
        this.ATC_CODE = ATC_CODE;
        this.MANUFACTURER = MANUFACTURER;
        this.SELLING_PRICE = SELLING_PRICE;
    } 
   
       public RECEIVE_INEZA_ITEM(String xml) {
           try
           { 
               
        this.SELLING_UNIT=1; 
        this.INN_RWA_CODE = HTTP_URL.Igicu.getTagValue("ITEM_INN_RWA_CODE", xml);
        this.ITEM_NAME = HTTP_URL.Igicu.getTagValue("ITEM_NAME", xml);
        this.ITEM_RHIA_NAME = HTTP_URL.Igicu.getTagValue("ITEM_RHIA_NAME", xml); 
        this.INN = HTTP_URL.Igicu.getTagValue("INN", xml);
        this.ATC_CODE = HTTP_URL.Igicu.getTagValue("ATC_CODE", xml);
        this.MANUFACTURER = HTTP_URL.Igicu.getTagValue("MANUFACTURER", xml);
        this.SELLING_PRICE = HTTP_URL.Igicu.getTagValueDouble("SELLING_PRICE", xml); 
        this.ID_PRODUCT  =  HTTP_URL.Igicu.getTagValueInteger("ID_PRODUCT", xml); 
         
           }
           catch (Throwable d)
           {
           this.INN_RWA_CODE=null;
               System.err.println(xml+" \n "+d);
           }
    }
    
   @Override
       public String toString()
       {
           String ssee ="";
           if(SELLING_PRICE>0)
           {ssee =" {"+ SELLING_PRICE +" RWF} ";}
       return  ITEM_NAME+" ["+INN+"] ("+ INN_RWA_CODE +")"+ssee;
       }
}
