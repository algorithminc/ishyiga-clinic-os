/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

import globalaccount.Account;
import static ineza_vcdc.ODT_INTERFACE_PRESCRIPTION.getItems;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

/**
 *
 * @author ishyiga
 */
public class ODT_INTERFACE_ACTIVITY extends JFrame{
   
    String title;
    Account umukozi;
    JButton REQUEST_EXO, RESULT_EXO, DIAGNOSTIC,TAKE_NOTES;
    JList allProductList, allDiseaseList;
    LinkedList<RECEIVE_INEZA_ITEM> prescriptionDrugList = new LinkedList();
    
    JTextArea NOTES; 
    JTextField searchAllICD10 = new JTextField(15);
    JTextField searchAllDrugs = new JTextField(15);
    Connection conn;
    INEZA_AFFILIATES ria;
    
    public ODT_INTERFACE_ACTIVITY(Connection conn,Account umukozi, INEZA_AFFILIATES ria) {
        
        this.title="CLINICAL ACTIVITIES MANAGEMENT INTERFACE "+" / PATIENT "+ria.BENEFICIARY_FIRST_NAME;
        this.conn=conn;
        this.umukozi=umukozi;
        this.ria=ria;
        initiate();
    } 
    
     private void initiate()
 { 
     Container content = getContentPane();
        setTitle(title);
        content.add(pane());
        setSize(900, 700); 
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        superSearchALLItDesease(); // to review second search
        superSearchALLItService();
        setVisible(true);
 }
     
    private JPanel pane() {
        JPanel masterBoutton = new JPanel();
        masterBoutton.setLayout(new GridLayout(3, 1));
        masterBoutton.add(examen());
        masterBoutton.add(notes());
        masterBoutton.add(desease());

        return masterBoutton;

    }  
     public JPanel notes() {
        JPanel jp = new JPanel();javax.swing.border.Border LoweredBevelBorder = BorderFactory.createLoweredBevelBorder();
        TitledBorder EtchedBorderRaised = BorderFactory.createTitledBorder(LoweredBevelBorder, 
                "NOTES",
                TitledBorder.CENTER, TitledBorder.TOP,
                new Font("Arial", Font.BOLD, 16), Color.GREEN);
        jp.setBorder(EtchedBorderRaised); 
        //jp.setLayout(new GridLayout(1, 2));
        NOTES = new JTextArea(30, 5); 
        JScrollPane notesScroll = new JScrollPane(NOTES);
        notesScroll.setPreferredSize(new Dimension(500, 150)); 
        jp.add(notesScroll); 
        TAKE_NOTES = new JButton("TAKE NOTES");  
        
        jp.add(TAKE_NOTES);
        
        return jp;
    }
      public JPanel desease() {
        JPanel jp = new JPanel();
        javax.swing.border.Border LoweredBevelBorder = BorderFactory.createLoweredBevelBorder();
        TitledBorder EtchedBorderRaised = BorderFactory.createTitledBorder(LoweredBevelBorder, 
                "DESEASE",
                TitledBorder.RIGHT, TitledBorder.TOP,
                new Font("Arial", Font.BOLD, 16), Color.RED);
        jp.setBorder(EtchedBorderRaised); 
       // jp.setLayout(new GridLayout(1, 2));
       
        allDiseaseList = new JList();
        searchAllICD10.setText(" search Disease... ");
        searchAllICD10.setBackground(new Color(240, 255, 255));
        
        this.indwara = getIndwara("MALARIA",conn);
        allDiseaseList.setListData(indwara.toArray());
        JScrollPane deseaseScroll = new JScrollPane(allDiseaseList);
        deseaseScroll.setPreferredSize(new Dimension(650, 180)); 
        jp.add(deseaseScroll);
        JPanel jp2 = new JPanel();
        jp2.setLayout(new GridLayout(2, 1));
        jp2.add(searchAllICD10); 
        DIAGNOSTIC = new JButton("DIAGNOSTIC");
        DIAGNOSTIC.setBackground(new Color(164, 255, 220));  
        jp2.add(DIAGNOSTIC);
        jp.add(jp2);
        return jp;
    }
      
      LinkedList<RECEIVE_INEZA_ITEM> list;
      
    public JPanel examen() {
        JPanel jp = new JPanel();
        javax.swing.border.Border LoweredBevelBorder = BorderFactory.createLoweredBevelBorder();
        TitledBorder EtchedBorderRaised = BorderFactory.createTitledBorder(LoweredBevelBorder, 
                "SERVICES",
                TitledBorder.LEFT, TitledBorder.TOP,
                new Font("Arial", Font.BOLD, 16), Color.BLUE);
        jp.setBorder(EtchedBorderRaised); 
       // jp.setLayout(new GridLayout(1, 2));
        allProductList = new JList();
        try{
         
        this.list = getItems(conn.createStatement(),"_SERVICES");
        allProductList.setListData(list.toArray()); }
        
        catch( SQLException s)
        {System.err.println(" geting services "+s);}
        searchAllDrugs.setText(" search Services... ");
        //searchAllDrugs.setBackground(Color.pink);
        
        JScrollPane exoScroll = new JScrollPane(allProductList);
        exoScroll.setPreferredSize(new Dimension(650, 180));
        allProductList.setBackground(new Color(240, 255, 225));
        jp.add(exoScroll);
        JPanel jp2 = new JPanel();
        jp2.setLayout(new GridLayout(3, 1));
        REQUEST_EXO = new JButton("REQUEST");
        REQUEST_EXO.setBackground(new Color(164, 255, 203));
        RESULT_EXO = new JButton("RESULT");
        RESULT_EXO.setBackground(new Color(255, 163, 105));
        jp2.add(searchAllDrugs);
        jp2.add(REQUEST_EXO);jp2.add(RESULT_EXO);
         
        jp.add(jp2);
        
        return jp;
    }
    
    
    private void superSearchALLItService() {
        searchAllDrugs.addKeyListener(
                new KeyListener() {
            public void actionPerformed(ActionEvent ae) {
            }

            @Override
            public void keyTyped(KeyEvent e) {

                String cc = "" + e.getKeyChar();
                String find = (searchAllDrugs.getText() + cc).toUpperCase();
                if (cc.hashCode() == 8 && find.length() > 0) {
                    find = (find.substring(0, find.length() - 1));
                }
                superSearchALL(find);
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
    }
    
    
    void superSearchALL(String find) {

        prescriptionDrugList.clear();
        String[] splited = find.split(" ");
//System.out.println(find+cashier.allProduct.size());
        for (int i = 0; i < list.size(); i++) {
            RECEIVE_INEZA_ITEM p = list.get(i);
            int trouve = 0;
            for (String findo : splited) {
                if (p.ITEM_NAME.contains(findo)
                        || p.ATC_CODE.contains(findo)
                        || p.INN.contains(findo)) {
                    trouve++;
                } else {
                    break;
                }
            }
            if (trouve == splited.length) {
                prescriptionDrugList.add(p);
            }
        }
        allProductList.setBackground(new Color(255, 255, 204));
        allProductList.setListData(prescriptionDrugList.toArray());
    }
   
    private void superSearchALLItDesease() {
        searchAllICD10.addKeyListener(
                new KeyListener() {
            public void actionPerformed(ActionEvent ae) {
            }

            @Override
            public void keyTyped(KeyEvent e) {

                String cc = "" + e.getKeyChar();
                String find = (searchAllICD10.getText() + cc).toUpperCase();

                if (cc.hashCode() == 8 && find.length() > 0) {
                    find = (find.substring(0, find.length() - 1));
                }

                System.out.println(".keyTyped() find " + find);
                if (find.length() > 4) {
                    superSearchALLDISEASE(find);
                }

            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
    }

    void superSearchALLDISEASE(String find) {

        this.indwara = getIndwara(find,conn);
        allDiseaseList.setListData(indwara.toArray());
    }
 
 
    public void searchDiseaseClicked() {
        searchAllICD10.addMouseListener(new MouseListener() {
            public void actionPerformed(ActionEvent ae) {
            }

            @Override
            public void mouseExited(MouseEvent ae) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (searchAllICD10.getText().equals(" search Disease... ")) {
                    searchAllICD10.setText("");
                }  } ;
@Override public void mousePressed(MouseEvent e) { } ;
@Override public void mouseReleased(MouseEvent e) { } ;
@Override public void mouseEntered(MouseEvent e) { }
        });
    }
    LinkedList<ODT_ICD10> prescriptionDISEASEList = new LinkedList();
    LinkedList<ODT_ICD10> indwara;
    public static LinkedList<ODT_ICD10> getCsvCOmpte(Connection conn) {

        LinkedList<ODT_ICD10> res = new LinkedList<>();
        int size = 0;
        int failed = 0;
        try {
            LinkedList<String> conf = getFichier("icd10cm_order_2018.txt");
            size = conf.size();
            System.out.println(" lines " + conf.size());

            PreparedStatement psInsert = conn.prepareStatement("insert into VCDC.RECEIVE_INEZA_ICD10"
                    + "(ID_ICD10,CODE_ICD10,NAME_SHORT,NAME_LONG,CLASS_ICD10) "
                    + " values (?,?,?,?,?)");

            for (int i = 0; i < conf.size(); i++) {
                try {
                    String line = conf.get(i);

                    // System.out.println(" lines "+line);
                    //70743 T23179A 1 Burn of first degree of unspecified wrist, initial encounter Burn of first degree of unspecified wrist, initial encounter
                    //01234567890123456789012345678901234567890123456789012345678901234567890123456
                    String ID_ICD10 = line.substring(0, 6);  //System.out.println(" code:"+ID_ICD10);
                    String CODE_ICD10 = line.substring(6, 13);  //System.out.println(" sinzi:"+CODE_ICD10);
                    String CLASS = line.substring(14, 16);
                    String NAME_SHORT = line.substring(16, 77).toUpperCase(); // System.out.println(" class:"+NAME_SHORT);
                    String NAME_LONG = line.substring(77, line.length()).toUpperCase(); //  System.out.println(" name ikase:"+NAME_LONG);
                    // System.out.println(" name full:"+);

                    ODT_ICD10 indwara = new ODT_ICD10(ID_ICD10, CODE_ICD10, NAME_SHORT, NAME_LONG, CLASS);
                    boolean ndinjiye = DbhandlerVCDC.insertDISEASE(indwara, psInsert);
                    res.add(indwara);
                    if (ndinjiye) {
                        failed++;
                    }
                } catch (Throwable ko) {
                    System.err.println("IOException " + ko);
                }
            }
        } catch (Throwable ko) {
            System.err.println("IOException " + ko);
        }
        System.out.println(size + " incoming  failed " + failed + " resultat " + res.size());
        return res;
    }

    public static LinkedList<String> getFichier(String fichier) throws FileNotFoundException, IOException {
        LinkedList<String> give = new LinkedList();

        File productFile = new File(fichier);
        String ligne;
        try {
            try (BufferedReader entree = new BufferedReader(new FileReader(productFile))) {
                ligne = entree.readLine();
                int i = 0;
                while (ligne != null) {
                    // System.out.println(i+ligne);
                    give.add(ligne);
                    i++;
                    try {
                        ligne = entree.readLine();
                    } catch (IOException e) {
                        System.out.println(e);
                    }
                }
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        return give;
    }
   
    
   public static LinkedList<ODT_ICD10> getIndwara(String where, Connection conn) {

        String[] sp = where.split(" ");

        String query;
        if (sp.length > 1) {
            query = "( NAME_LONG like '%" + sp[0] + "%' AND NAME_LONG like '%" + sp[1] + "%')";
        } else {
            query = " UPPER(NAME_LONG) like upper ('%" + sp[0] + "%')";
        }

        LinkedList<ODT_ICD10> listlotItem = new LinkedList<>();
        try {
            
            Statement stat = conn.createStatement();
            String sql = "SELECT ID_ICD10,CODE_ICD10,NAME_SHORT,NAME_LONG,CLASS_ICD10"
                    + " FROM VCDC.RECEIVE_INEZA_ICD10 WHERE " + query + " ORDER BY NAME_SHORT";
            System.out.println(sql);
            ResultSet outResult = stat.executeQuery(sql);

            while (outResult.next()) {
                listlotItem.add(new ODT_ICD10(
                        outResult.getString("ID_ICD10"),
                        outResult.getString("CODE_ICD10"),
                        outResult.getString("NAME_SHORT"),
                        outResult.getString("NAME_LONG"),
                        outResult.getString("CLASS_ICD10")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e + "formule");
        }

        return listlotItem;
    } 
    
}
