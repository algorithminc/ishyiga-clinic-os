/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

import java.awt.Color;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author ishyiga
 */
public class ODT_LIST
{
String INN_RW_CODE_REQUESTED,ITEM_NAME,NOTE,AVAILABILITE,ROUTE,MESURE,DUREE,REFILL,DISEASE;  

int QUANTITY_REQUESTED,TAKE_FREQUENCE,FREQUENCE_HOURY;
int AHO_YABONESTE=0;
double PRICE;

Color availColor;

public ODT_LIST(String INN_RW_CODE_REQUESTED, String ITEM_NAME, String NOTE,
                String AVAILABILITE, String ROUTE, int QUANTITY_REQUESTED, int TAKE_FREQUENCE, 
                int FREQUENCE_HOURY,double PRICE,String MESURE,String DUREE,String REFILL,String DISEASE) {
      
            this.REFILL = REFILL;
            this.MESURE = MESURE;
            this.DUREE  = DUREE;
            this.INN_RW_CODE_REQUESTED = INN_RW_CODE_REQUESTED;
            this.ITEM_NAME = ITEM_NAME;
            this.NOTE   = NOTE;
            this.AVAILABILITE = AVAILABILITE;
            this.DISEASE=DISEASE;
            
            if(AVAILABILITE!=null)
            {
             int days =   nb_joursBetweenToDate2(AVAILABILITE);
             
            if(days<7)
            {availColor=Color.green;}
            else if(days<14)
            {availColor=Color.yellow;}
            else 
            {availColor=Color.red;}
                
            
            }
            else
            {
            this.availColor=Color.ORANGE;
            this.AVAILABILITE="NOT FOUND";
            }
            
            
            this.ROUTE = ROUTE;
            this.QUANTITY_REQUESTED = QUANTITY_REQUESTED;
            this.TAKE_FREQUENCE = TAKE_FREQUENCE;
            this.FREQUENCE_HOURY = FREQUENCE_HOURY;
            this.PRICE  = PRICE;
        }
  public ODT_LIST(String INN_RW_CODE_REQUESTED, int QUANTITY_REQUESTED, double PRICE) {
        this.INN_RW_CODE_REQUESTED = INN_RW_CODE_REQUESTED;
        this.QUANTITY_REQUESTED = QUANTITY_REQUESTED;
        this.PRICE = PRICE;
    }
  String gauche()
  {
      return  "<INSHURO>" + TAKE_FREQUENCE + "</INSHURO>"
                + "<MASAHA>" + FREQUENCE_HOURY + "</MASAHA>"
                + "<UPIMIRE>" + MESURE + "</UPIMIRE>"
                + "<DUREE>" + DUREE + "</DUREE>" ;
      
  }
    String droite()
    {
      return  "<QUANTITY>" + QUANTITY_REQUESTED + "</QUANTITY>"
                + "<REFILL>" + REFILL + "</REFILL>"
                + "<NOTE>" + NOTE + "</NOTE>"
                + "<ROUTE>" + ROUTE + "</ROUTE>"
                + "<ICD10>" + DISEASE + "</ICD10>";
    }
 
      public static int nb_joursBetweenToDate2(String debut) {
        Date Omega = new Date();
        Date Alfa = mDateSt(debut);

        if (Omega == null || Alfa == null) {
            return 0;
        } else {
            //System.out.println(DaysIn(Omega.getTime()) +"   DaysIn(Alfa.getTime())  "+DaysIn(Alfa.getTime()));

            return DaysIn(Omega.getTime()) - DaysIn(Alfa.getTime());
        }
    }
      public static int DaysIn(long time) {
        return (int) (time / 1000 / 60 / 60 / 24);
    }

    public static Date mDateSt(String comingDate) {
        //2016-01-01 00;00;00
        //0123456789012345678

        if (comingDate == null || comingDate.length() < 17) {
            JOptionPane.showMessageDialog(null, " DATE FORMAT NOTE ACCEPTED " + comingDate);
            return null;
        } else {
            int year = Integer.parseInt(comingDate.substring(0, 4)) - 1900;

            int mm = Integer.parseInt(comingDate.substring(5, 7)) - 1;
            int dd = Integer.parseInt(comingDate.substring(8, 10));

            Date date = new Date(year, mm, dd);
            System.out.println(date + "   datedatedate  " + comingDate);
            return date;
        }
//System.out.println(date +" "+date.getTime());
    }

}
