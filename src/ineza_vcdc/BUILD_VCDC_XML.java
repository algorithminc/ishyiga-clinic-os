package ineza_vcdc;

import eaglEye.Eye;
import eaglEye.errorEye; 
import static ineza_vcdc.ODT_INTERFACE_MAIN.version;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author CORNEILLE/ KIMENYI
 */
public class BUILD_VCDC_XML {

    //public static ITEM[] lesItems = new ITEM[100000];
    static double round(double to_round) {
        // System.out.println(to_round);
        double sese = ((int) ((to_round + 0.005) * 100)) / 100.0;
        // System.out.println("sese "+sese); 
        return sese;
    }

    public static INEZA_RECEIPT receipt(int idInvoice, Statement stat,
            Statement statItem,
            String ssn, String VSCISVERSION) {
        INEZA_RECEIPT toInsert = null;
        try {

            String sql = "select * from APP.INVOICE    "
                    + " WHERE ID_INVOICE = " + idInvoice + "  ";
            System.out.println(sql);
            ResultSet outResult2 = stat.executeQuery(sql);
            if (VSCISVERSION.contains("POS")) {
                while (outResult2.next()) {
                    String sente = "RWF";//outResult2.getString("MONAIE_INVOICE");

                    toInsert = new INEZA_RECEIPT(outResult2.getInt("ID_INVOICE"), outResult2.getString("DATE"),
                            outResult2.getDouble("TOTAL"), outResult2.getString("EMPLOYE"), "" + outResult2.getTimestamp("HEURE"),
                            outResult2.getDouble("TVA"), outResult2.getInt("DOCUMENT"), ' ',
                            "", "", "", "", "", "FACTURE", outResult2.getString("REFERENCE"),
                            outResult2.getString("COMPTABILISE"), "", "", 1, sente, outResult2.getInt("REDUCTION"), "", "",
                            outResult2.getDouble("CASH"), outResult2.getDouble("CREDIT"), null);

                    toInsert.clientName = outResult2.getString("NUM_CLIENT");
                    toInsert.clientAdress = "";
                    toInsert.clientTin = outResult2.getString("TIN");
                    toInsert.extData = outResult2.getString("EXTERNAL_DATA");
                    toInsert.totA = outResult2.getDouble("TOTAL_A");
                    toInsert.totB = outResult2.getDouble("TOTAL_B");
                    toInsert.totC = outResult2.getDouble("TOTAL_C");
                    toInsert.totD = outResult2.getDouble("TOTAL_D");
                    toInsert.taxB = outResult2.getDouble("TAXE_B");
                    toInsert.taxC = outResult2.getDouble("TAXE_C");
                    toInsert.taxD = outResult2.getDouble("TAXE_D");
                    toInsert.MRC = outResult2.getString("MRC");
                }
            }
            outResult2.close();

            String q = "SELECT CODE_UNI,PRICE,TVA,(QUANTITE),LIST_ID_PRODUCT"
                    + ",NUM_LOT,DATE_EXP,BON_LIVRAISON FROM APP.LIST "
                    + " WHERE ID_INVOICE = " + idInvoice + "  ";

            System.out.println("list" + q);
            outResult2 = stat.executeQuery(q);
            int itemSeq = 1;
            double total =0;
            while (outResult2.next()) {

                double totAmt = (outResult2.getDouble("PRICE") * outResult2.getInt(4));
                double taxablAmt = (totAmt / (1 + outResult2.getDouble("TVA")));
                double vatAmt = (taxablAmt * outResult2.getDouble("TVA"));
total +=totAmt;
                String name = " ";
                String code = outResult2.getString("CODE_UNI");
                String inn = " ";

                try {

                    ITEM it = getItem_inezaOne(code, statItem);
                    if (it==null || it.ITEM_INN_RWA_CODE.equals("") || it.ITEM_INN_RWA_CODE.length() < 5) {
                        inn = "MISSING_" + code;
                    } else {
                        inn = it.ITEM_INN_RWA_CODE;
                    }

                } catch (Throwable e) {
                    JOptionPane.showConfirmDialog(null, outResult2.getInt("LIST_ID_PRODUCT")
                            + " receipt " + e);

                    new errorEye(ssn, "MISSING_VITAL_DATA",  version,
                            " NON EXISTING ITEM " + idInvoice, idInvoice + " " + outResult2.getInt("LIST_ID_PRODUCT"), "kimainyi@gmail.com");

                    System.out.println(" ... exception throw  :" + e);
                }
                INEZA_RECEIPT_ITEM ri = new INEZA_RECEIPT_ITEM(inn,
                        outResult2.getString("NUM_LOT"),
                        outResult2.getString("DATE_EXP"),
                        outResult2.getDouble("PRICE"), totAmt, vatAmt,
                        outResult2.getInt(4), outResult2.getString("BON_LIVRAISON"),
                        Eye.ID_COMPANY_BRANCH, idInvoice);
                toInsert.productList.add(ri);
                System.out.println("productList  " + toInsert.productList.size());
                itemSeq++;
            }
            toInsert.rcptSeq = itemSeq - 1;
            outResult2.close();
            // GET CLIENT INFORMATION 
            String numAff = NUMERO_AFFILIE(idInvoice, stat);
            
            if (numAff!=null && !numAff.equals("")) {
                System.out.println("client:::::" + numAff);
                CLIENT client = get_invoice_client(numAff, stat);
                if (client != null) {
                    toInsert.client = client;
                }
            }
            else 
            {System.out.println(";ERROR;MISSING CREDIT AFFILIATE;"+idInvoice);}
            
            if(total!=toInsert.TOTAL4)
            {System.out.println(";ERROR;TOTAL;"+idInvoice+";"+total+";"+toInsert.TOTAL4);}
                    
            // Get ID_COMPANY_BRANCH 
            toInsert.ID_COMPANY_BRANCH = Eye.ID_COMPANY_BRANCH;
            // Get pharmacy_codeMAIN
            toInsert.PHARMACY_CODE = DbhandlerVCDC.getValue("pharmacy_code", stat);

        } catch (SQLException e) {
            System.out.println(" ... exception throw  :" + e);
        }
        return toInsert;
    }

    public static INEZA_RECEIPT refund(int idRefund, Statement stat, Statement statItem,
            String ssn, String VSCISVERSION) {
        INEZA_RECEIPT toInsert = null;
        try {

            String sql = "select * from APP.REFUND    "
                    + " WHERE ID_REFUND = " + idRefund + "  ";
            System.out.println(sql);
            ResultSet outResult2 = stat.executeQuery(sql);
            if (VSCISVERSION.contains("POS")) {
                while (outResult2.next()) {
                    String sente = "RWF";//outResult2.getString("MONAIE_INVOICE");

                    toInsert = new INEZA_RECEIPT(outResult2.getInt("ID_INVOICE"), outResult2.getString("DATE"),
                            outResult2.getDouble("TOTAL"), outResult2.getString("EMPLOYE"), "" + outResult2.getTimestamp("HEURE"),
                            outResult2.getDouble("TVA"), outResult2.getInt("DOCUMENT"), ' ',
                            "", "", "", "", "", "REFUND", "", "", "", "",
                            1, sente, 0, "", "", outResult2.getDouble("CASH"), outResult2.getDouble("CREDIT"), null);

                    toInsert.clientName = outResult2.getString("NUM_CLIENT");
                    toInsert.clientAdress = "";
                    toInsert.clientTin = outResult2.getString("TIN");
                    toInsert.extData = outResult2.getString("EXTERNAL_DATA");
                    toInsert.totA = outResult2.getDouble("TOTAL_A");
                    toInsert.totB = outResult2.getDouble("TOTAL_B");
                    toInsert.totC = outResult2.getDouble("TOTAL_C");
                    toInsert.totD = outResult2.getDouble("TOTAL_D");
                    toInsert.taxB = outResult2.getDouble("TAXE_B");
                    toInsert.taxC = outResult2.getDouble("TAXE_C");
                    toInsert.taxD = outResult2.getDouble("TAXE_D");
                    toInsert.MRC = outResult2.getString("MRC");
                    toInsert.ID_INVOICE_REFUNDED = outResult2.getInt("ID_INVOICE");
                }
            }
            outResult2.close();

            String q = "SELECT CODE_UNI,PRICE,TVA,(QUANTITE),LIST_ID_PRODUCT"
                    + ",NUM_LOT,DATE_EXP, ID_INVOICE, ID_INVOICE FROM APP.REFUND_LIST "
                    + " WHERE ID_REFUND = " + idRefund + "  ";

            System.out.println("list" + q);
            outResult2 = stat.executeQuery(q);
            int itemSeq = 1;
            int idInvoice_Refunded = 0;
            while (outResult2.next()) {

                double totAmt = (outResult2.getDouble("PRICE") * outResult2.getInt(4));
                double taxablAmt = (totAmt / (1 + outResult2.getDouble("TVA")));
                double vatAmt = (taxablAmt * outResult2.getDouble("TVA"));

                String name = " ";
                String code = outResult2.getString("CODE_UNI");
                String inn = " ";
                idInvoice_Refunded = toInsert.ID_INVOICE_REFUNDED;
                try {

                    ITEM it = getItem_inezaOne(code, statItem);
                    inn = it.ITEM_INN_RWA_CODE;

                } catch (Exception e) {
                    JOptionPane.showConfirmDialog(null, outResult2.getInt("LIST_ID_PRODUCT")
                            + " receipt " + e);

                    errorEye errorEye = new errorEye(ssn, "MISSING_VITAL_DATA",  version,
                            " NON EXISTING ITEM " + idRefund, idRefund + " " + outResult2.getInt("LIST_ID_PRODUCT"), "kimainyi@gmail.com");

                    System.out.println(" ... exception throw  :" + e);
                }
                INEZA_RECEIPT_ITEM ri = new INEZA_RECEIPT_ITEM(inn, outResult2.getString("NUM_LOT"),
                        outResult2.getString("DATE_EXP"),
                        outResult2.getDouble("PRICE"), totAmt, vatAmt,
                        outResult2.getInt(4),
                        "" + outResult2.getInt("ID_INVOICE"),
                        Eye.ID_COMPANY_BRANCH,
                         outResult2.getInt("ID_INVOICE"));
                toInsert.productList.add(ri);
                System.out.println("productList  " + toInsert.productList.size());
                itemSeq++;
            }
            toInsert.rcptSeq = itemSeq - 1;

            // GET CLIENT INFORMATION 
            String numAff = NUMERO_AFFILIE(idInvoice_Refunded, stat);
            if (!numAff.equals("")) {
                System.out.println("client:::::" + numAff);
                CLIENT client = get_invoice_client(numAff, stat);
                if (client != null) {
                    toInsert.client = client;
                }
            }

        } catch (SQLException e) {
            System.out.println(" ... exception throw  :" + e);
        }
        return toInsert;
    }

    static String NUMERO_AFFILIE(int idInvoice, Statement stat) {
        String numAff = "";
        try {
            String sql1 = "select * from app.credit where ID_INVOICE = " + idInvoice;
            System.out.println(sql1);
            try (ResultSet outResult2 = stat.executeQuery(sql1)) {
                while (outResult2.next()) {
                    numAff = outResult2.getString("NUMERO_AFFILIE");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(BUILD_VCDC_XML.class.getName()).log(Level.SEVERE, null, ex);
        }
        return numAff;
    }

    static CLIENT get_invoice_client(String numAff, Statement s) {
        String w = "";
        CLIENT client = null;
        if (numAff.equals("")) {
            System.out.println(" ... inv list is null :");
        } else {
            try {
                w = "select * from APP.CLIENT_RAMA where NUM_AFFILIATION = '" + numAff + "' ";
                System.out.println(w);
                ResultSet outResult = s.executeQuery(w);
                while (outResult.next()) {
                    String NUM_AFFILIATIONa = outResult.getString("NUM_AFFILIATION");
                    String NOM_CLIENTa = outResult.getString("NOM_CLIENT");
                    String PRENOM_CLIENTa = outResult.getString("PRENOM_CLIENT");
                    int PERCENTAGEa = outResult.getInt("PERCENTAGE");
                    String ASSURANCEa = outResult.getString("ASSURANCE");
                    String EMPLOYEURa = outResult.getString("EMPLOYEUR");
                    String SECTEURa = outResult.getString("SECTEUR");
                    String VISAa = outResult.getString("VISA");
                    String CODE = outResult.getString("CODE");
                    String AGE = outResult.getString("AGE");
                    String SEXE = outResult.getString("SEXE");
                    String LINK = outResult.getString("LIEN");
                    String BENEFICIAIRE = outResult.getString("BENEFICIAIRE");

                    client = new CLIENT(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISAa, CODE, AGE, SEXE, LINK, BENEFICIAIRE);

                }
            } catch (SQLException ex) {
                Logger.getLogger(BUILD_VCDC_XML.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if(client==null)
        {      client = new CLIENT(numAff, "", "", 15, "", "", 
                "", "", "", "", "", "MISSING", "");
         System.out.println(";ERROR;MISSING AFFILIATE;"+numAff);
}
        return client;
    }

    public static boolean checkTotals(INEZA_RECEIPT toInsert) {
        double total = 0;
        double totalTVA = 0;
        if (toInsert != null) {
            for (int i = 0; i < toInsert.productList.size(); i++) {
                INEZA_RECEIPT_ITEM ri = toInsert.productList.get(i);

                total += ri.TOT_AMOUNT;
                totalTVA += ri.VAT_AMONT;
            }

            if ((int) round(total) != (int) round(toInsert.TOTAL4)
                    || (int) round(totalTVA) != (int) round(toInsert.TVA4)) {
//       JOptionPane.showConfirmDialog(null, "TRANSACTION  "+toInsert.NUM_FACT2 +" \n "+
//               total +" ITEM DIF TO TOTAL "+toInsert.TOTAL4 +" \n "+
//               totalTVA +" ITEM DIF TO TOTAL vat "+toInsert.TVA4);
                System.err.println("TRANSACTION  " + toInsert.NUM_FACT2 + " \n "
                        + total + " ITEM DIF TO TOTAL " + toInsert.TOTAL4 + " \n "
                        + totalTVA + " ITEM DIF TO TOTAL vat " + toInsert.TVA4);
//       if(toInsert.TOTAL4<total)
                {
                    toInsert.TOTAL4 = total;
                    toInsert.TVA4 = totalTVA;
                    return true;
                }
//       else
//       {return false;}

            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static String getOrdonance5(int id_invoice, Statement stato) {

        String ordo = null;

        String sql2 = "SELECT ODT_ID,BENEFICIARY_WEIGHT,CAUSE_OF_DESEASE,DOCTOR_COUNCIL_NUMBER,"
                + "HSP_CODE,INVOICE_RECEIVER_ID,INVOICE_COUNCIL_NUMBER,NUM_ORDO,VERIFIED FROM APP.ODT_ORDONNANCE WHERE ID_INVOICE=" + id_invoice;
        System.out.println("" + sql2);
        try {
            try (ResultSet outResult2 = stato.executeQuery(sql2)) {
                while (outResult2.next()) {

                    ordo = "<ODT_ID>" + outResult2.getString("ODT_ID") + "</ODT_ID> "
                            + "<BENEFICIARY_WEIGHT>" + outResult2.getString("BENEFICIARY_WEIGHT") + "</BENEFICIARY_WEIGHT> "
                            + "<CAUSE_OF_DESEASE>" + outResult2.getString("CAUSE_OF_DESEASE") + "</CAUSE_OF_DESEASE> "
                            + "<PRESCRIBER_COUNCIL_NUMBER>" + outResult2.getString("DOCTOR_COUNCIL_NUMBER") + "</PRESCRIBER_COUNCIL_NUMBER> "
                            + "<HSP_CODE>" + outResult2.getString("HSP_CODE") + "</HSP_CODE> "
                            + "<VERIFIER_INFO>" + outResult2.getString("VERIFIED") + "</VERIFIER_INFO> " ///  VERIFIED cg ERROR YABAYEHO
                            + "<DISPENSER_COUNCIL_NUMBER>" + outResult2.getString("INVOICE_COUNCIL_NUMBER") + "</DISPENSER_COUNCIL_NUMBER>"
                            + "<NUM_ORDONNANCE>" + outResult2.getString("NUM_ORDO") + "</NUM_ORDONNANCE> ";
                }
//                System.out.println("ORDONANCE :" + ordo);
                outResult2.close();
            }
        } catch (SQLException e) {
            System.err.println(e);
        }

        return ordo;
    }

    static void makeOrdonance(int idInvoice,
            Statement stat, String pharmacy_code, String umukozi) {
        String numQuitance = null, NUMERO_AFFILIE = "";
        try {
            String sql1 = "select * from app.credit where ID_INVOICE = " + idInvoice;
            System.out.println(sql1);
            try (ResultSet outResult2 = stat.executeQuery(sql1)) {
                while (outResult2.next()) {
                    NUMERO_AFFILIE = outResult2.getString("NUMERO_AFFILIE");
                    numQuitance = outResult2.getString("NUMERO_QUITANCE") + "-..-NO OM-..-";
                }
            }

            System.out.println("ineza_vcdc.BUILD_VCDC_XML.makeOrdonance() " 
                    + numQuitance);

            if ( numQuitance!=null 
                    && numQuitance.contains("-..-")) {

                System.out.println(" nm ");
                
//222-CELESTIN-..-Nyarusange (muhanga) CS-..-0786000005
                String quit = numQuitance.replaceAll("-..-", "-");
                String st1[] = quit.split("-");
                //20331027 21206 04 20-UR PC-..-010404113-..-
//                    if(st1.length < 4){
//                        st1[st1.length + 1] = ""+0;
//                    }

                String NUM_ORDO = "";
                String OM = "";
                String HSP_CODE = "";

                System.out.println("  length " + st1.length);

                if (st1.length >= 4) {
                    NUM_ORDO = st1[0];
                    OM = st1[3];
                    HSP_CODE = st1[2];
                }
                try {

                    String SQL5 = "insert into APP.ODT_ORDONNANCE(NUM_ORDO,DOCTOR_COUNCIL_NUMBER,HSP_CODE,"
                            + "ODT_ID, BENEFICIARY_WEIGHT, CAUSE_OF_DESEASE,ID_INVOICE,VERIFIED, INVOICE_RECEIVER_ID,INVOICE_COUNCIL_NUMBER)  "
                            + "values ('" + NUM_ORDO + "','" + OM + "','" + HSP_CODE + "','NA',0,'OTHER'," + idInvoice + ",'OLD VERSION', '" + NUMERO_AFFILIE + "', '" + umukozi + "')";

                    System.out.println("ordonance details prescriber saving \n " + SQL5);
                    stat.executeUpdate(SQL5);

                    System.out.println("ordonance details prescriber saved");
                } catch (SQLException ex) {

                    new errorEye(pharmacy_code, "MISSING_VITAL_DATA", version,
                            " NON ORDONANCE " + idInvoice, idInvoice + " ", "kimanyi@gmail.com");

                    JOptionPane.showMessageDialog(null, "VCDC EXITING ENCOUTERED UNKOWN ERROR"
                            + " \n CALL ISHYIGA TEAM \n ERROR " + ex);
                    System.exit(1);
                }

            } else {
                new errorEye(pharmacy_code, "MISSING_VITAL_DATA",
                        version,
                        " NON ORDONANCE " + idInvoice, idInvoice + " ",
                        "kimanyi@gmail.com");

                JOptionPane.showMessageDialog(null, "VCDC EXITING \n "
                        + "MISSING ORDONANCE " + idInvoice
                        + " \n CALL ISHYIGA TEAM ");
                System.exit(1);
            }

        } catch (SQLException ex) {
            new errorEye(pharmacy_code, "MISSING_VITAL_DATA",  version,
                    " NON ORDONANCE " + idInvoice, idInvoice + " ", "kimanyi@gmail.com");

            JOptionPane.showMessageDialog(null,
                    "VCDC EXITING ENCOUTERED UNKOWN ERROR"
                    + " \n CALL ISHYIGA TEAM " + ex);
            System.exit(1);
        }
    }

    public static String ordonance5(int invId, Statement stat,
            String pcy, String umukozi) {
        String ord = getOrdonance5(invId, stat);
        System.out.println("getOrdonance5 " + ord);

        if (ord == null) {
            makeOrdonance(invId, stat, pcy, umukozi);

            return getOrdonance5(invId, stat);

        } else {
            return ord;
        }

    }

    public static String doXmlReceipt5V(INEZA_RECEIPT inv,
            int invId, Statement stat) {

        String ordanance = ordonance5(invId, stat, inv.PHARMACY_CODE,
                inv.EMPLOYE);
System.out.println(" ordananceordananceordanance coming  "+ordanance);
        if (ordanance == null) {
            return null;
        }

        String invoice
                = "<GRP_ID>" + inv.clientName + "</GRP_ID>"
                + "<PHARMACY_CODE>" + inv.PHARMACY_CODE + "</PHARMACY_CODE>"
                + "<ID_COMPANY_BRANCH>" + inv.ID_COMPANY_BRANCH + "</ID_COMPANY_BRANCH>"
                + "<BENEFICIARY_NUMBER>" + inv.client.NUM_AFFILIATION + "</BENEFICIARY_NUMBER> "
                + "<AFFILIATE_NUMBER>" + inv.client.BENEFICIAIRE + "</AFFILIATE_NUMBER> "
                + "<INVOICE_PHARMACY_ID>" + inv.ID_INVOICE + "</INVOICE_PHARMACY_ID> "
                + "<INVOICE_DATE_TIME>" + inv.HEURE + "</INVOICE_DATE_TIME> "
                + "<INVOICE_TOTAL_85>" + inv.CREDIT + "</INVOICE_TOTAL_85> "
                + "<INVOICE_TOTAL_AFFILIATE>" + inv.CASH + "</INVOICE_TOTAL_AFFILIATE> "
                + "<INVOICE_TOTAL_100>" + inv.TOTAL4 + "</INVOICE_TOTAL_100> "
                + ordanance;

        return invoice;
    }

    public static String doItemXml(INEZA_RECEIPT inv) {

        String item = "<ITEM>" + inv.doXml(inv.productList) + "</ITEM> ";

        return item;
    }

    public static String getDateTime() {
        Date d = new Date();

        String day, mm, year, SS, MM, HH;

        if (d.getDate() < 10) {
            day = "0" + d.getDate();
        } else {
            day = "" + d.getDate();
        }
        if ((d.getMonth() + 1) < 10) {
            mm = "0" + (d.getMonth() + 1);
        } else {
            mm = "" + (d.getMonth() + 1);
        }
        year = "" + ((d.getYear() - 100) + 2000);

        if ((d.getHours() + 1) < 10) {
            HH = "0" + (d.getHours() + 1);
        } else {
            HH = "" + (d.getHours() + 1);
        }
        if ((d.getMinutes() + 1) < 10) {
            MM = "0" + (d.getMinutes() + 1);
        } else {
            MM = "" + (d.getMinutes() + 1);
        }
        if ((d.getSeconds() + 1) < 10) {
            SS = "0" + (d.getSeconds() + 1);
        } else {
            SS = "" + (d.getSeconds() + 1);
        }

        return year + mm + day + HH + MM + SS;
    }

    public static String makeDate(String heure) {

        String day, mm, year, h, m, s;
//2017-01-01 00:00:00
//0123456789012345678

        day = heure.substring(8, 10);
        mm = heure.substring(5, 7);
        year = heure.substring(0, 4);

        h = heure.substring(11, 13);
        m = heure.substring(14, 16);
        s = heure.substring(17, 19);

        return year + mm + day + h + m + s;
    }

    //SUED 170220 REPORT NUMBER
    public static String makeDateDanny(String heure) {

        String day, mm, year;
//2017-01-01 00:00:00
//0123456789012345678

        day = heure.substring(8, 10);
        mm = heure.substring(5, 7);
        year = heure.substring(0, 4);

        return year + mm + day;
    }

    public static String makeDateRcpt(String heure) {

        String day, mm, year, h, m, s;
//2017-01-01 00:00:00
//0123456789012345678

        day = heure.substring(8, 10);
        mm = heure.substring(5, 7);
        year = heure.substring(0, 4);

        h = heure.substring(11, 13);
        m = heure.substring(14, 16);
        s = heure.substring(17, 19);

        return day + mm + year + h + m + s;
    }

    public static boolean upDate_VARIABLE(Statement stat, String sql) {
        try {
            stat.execute(sql);
            // JOptionPane.showMessageDialog(null," VARIABLE UPDATED SUCCESSFULLY "," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, " UPDATE FAIL \n " + sql,
                    " MURAKOZE ", JOptionPane.PLAIN_MESSAGE);

            return false;
        }
    }
//
//    public static void buildItem_ineza2(Statement stat) {
//
//        try {
//            String sql = "SELECT ID_PRODUCT,CODE,NAME_PRODUCT,CODE_BAR,CODE_CLASSEMENT,"
//                    + "TVA,ITEMS_CLASS,TAX_CODE, ITEM_INN_RWA_CODE"
//                    + " FROM APP.PRODUCT ORDER BY  ID_PRODUCT ";
//System.out.println(sql); 
//            ResultSet outResult = stat.executeQuery(sql);
//int i=0;
//            while (outResult.next()) {
//                int ID_PRODUCT = outResult.getInt(1);
//                String CODE = outResult.getString(2);
//                String NAME_PRODUCT = outResult.getString(3);
//                String CODE_BAR = outResult.getString(4);
//                String CODE_CLASSEMENT = outResult.getString(5);
//                double TVA_VALUE = outResult.getDouble(6);
//                String ITEMS_CLASS = outResult.getString(7);
//                String TAXCODE = outResult.getString(8);
//                i++;
//                if (TAXCODE == null || !(TAXCODE.contains("A") || TAXCODE.contains("B") || TAXCODE.contains("C")
//                        || TAXCODE.contains("D"))) {
//                    JOptionPane.showMessageDialog(null, TAXCODE + " CALL ISHYIGA SUPPORT YOU HAVE AN ITEM WITHOUT TAX CODE"
//                            + " \n  " + NAME_PRODUCT, "ISHYIGA IS CLOSING ", JOptionPane.ERROR_MESSAGE);
//
//                    System.exit(0);
//
//                }
//
//                lesItems[ID_PRODUCT] = new ITEM(ID_PRODUCT, CODE, NAME_PRODUCT, CODE_BAR,
//                        CODE_CLASSEMENT, TVA_VALUE, ITEMS_CLASS, TAXCODE);
//                lesItems[ID_PRODUCT].ITEM_INN_RWA_CODE = outResult.getString(9);
//            }
//           System.out.println("lotLine  "+i);
//            outResult.close();
//
//        } catch (SQLException e) {
//
//            System.out.println(" ... exception  :" + e);
//        }
//    }
//    

    public static ITEM getItem_inezaOne(String code, Statement stat) {

        try {
            String sql = "SELECT ID_PRODUCT,CODE,NAME_PRODUCT,CODE_BAR,CODE_CLASSEMENT,"
                    + "TVA,ITEMS_CLASS,TAX_CODE, ITEM_INN_RWA_CODE"
                    + " FROM APP.PRODUCT  WHERE CODE='" + code + "' ";
            System.out.println(sql);
            ResultSet outResult = stat.executeQuery(sql);
            int i = 0;
            while (outResult.next()) {
                int ID_PRODUCT = outResult.getInt(1);
                String CODE = outResult.getString(2);
                String NAME_PRODUCT = outResult.getString(3);
                String CODE_BAR = outResult.getString(4);
                String CODE_CLASSEMENT = outResult.getString(5);
                double TVA_VALUE = outResult.getDouble(6);
                String ITEMS_CLASS = outResult.getString(7);
                String TAXCODE = outResult.getString(8);
                i++;
                if (TAXCODE == null || !(TAXCODE.contains("A") || TAXCODE.contains("B") || TAXCODE.contains("C")
                        || TAXCODE.contains("D"))) {
                    JOptionPane.showMessageDialog(null, TAXCODE + " CALL ISHYIGA SUPPORT YOU HAVE AN ITEM WITHOUT TAX CODE"
                            + " \n  " + NAME_PRODUCT, "ISHYIGA IS CLOSING ", JOptionPane.ERROR_MESSAGE);

                    System.exit(0);

                }

                ITEM it = new ITEM(ID_PRODUCT, CODE, NAME_PRODUCT, CODE_BAR,
                        CODE_CLASSEMENT, TVA_VALUE, ITEMS_CLASS, TAXCODE);
                it.ITEM_INN_RWA_CODE = outResult.getString(9);
                System.out.println("lotLine  " + i);
                return it;
            }

            outResult.close();

        } catch (SQLException e) {

            System.out.println(" ... exception  :" + e);
        }
        return null;
    }
}
