package ineza_vcdc;


import java.util.LinkedList;

/**
 *
 * @author Corneille
 */
public class VERIFIED_DATA {

    String HSP, PRESCRIBER;
    double TOT_AMOUNT;
    LinkedList<ODT_LIST> ITEMS = new LinkedList<>();

    public VERIFIED_DATA(String DATA) {
        this.HSP = HTTP_URL.GetUrl.getTagValue("HSP", DATA);
        this.PRESCRIBER = HTTP_URL.GetUrl.getTagValue("PRESCRIBER", DATA);
        this.TOT_AMOUNT = Double.parseDouble(HTTP_URL.GetUrl.getTagValue("TOTAL", DATA));
        String[] item = HTTP_URL.GetUrl.getTagValue("ITEMS", DATA).split("</LIN>");
//        System.err.println(" initiator... list");
        for (String line : item) {
            String code = HTTP_URL.GetUrl.getTagValue("CODE_ISHYIGA", line);
            int qty = Integer.parseInt(HTTP_URL.GetUrl.getTagValue("QUANTITY", line));
            double price = Double.parseDouble(HTTP_URL.GetUrl.getTagValue("PRICE", line));
            ITEMS.add(new ODT_LIST(code, qty, price));
        }
//        System.err.println("nsoje initiator...");
    }

}
