/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

import static ineza_vcdc.DbhandlerVCDC.getMaxItemIdProduct;
import static ineza_vcdc.SOAPClient4XG.sendRSSB2;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author ishyiga
 */
public class Jobber {

    static public String injizAbarwayi5(Connection conn)//ONE PATIENT
    {

        try {
            Statement stateme = conn.createStatement();

            //  String LAST_UPDATE = DbhandlerVCDC.lastUpdate("VCDC.RECEIVE_INEZA_AFFILIATES", stateme);
            String LAST_UPDATE = DbhandlerVCDC.getLastLongPatient(stateme);

            String abarwayi = HTTP_URL.Igicu.kohereza("VCDC_GET_AFFILIATES", "bridge",
                    "<ASSURANCE>RSSB</ASSURANCE>"
                    + "<LASTUPDATED>1</LASTUPDATED>"
                    + " <LIMIT>" + LAST_UPDATE + "</LIMIT> </header></request>");
            System.out.println("ineza_vcdc.Jobber.injizAbarwayi() \n " + abarwayi);

            String[] sp = abarwayi.split("</LIN>");

            LinkedList<INEZA_AFFILIATES> lesPatients = new LinkedList<>();
            for (String sp1 : sp) {
                INEZA_AFFILIATES ria = new INEZA_AFFILIATES(sp1,"RSSB");
//            System.out.println(" uwinjiye ni " + ria + "  ben  " );
                if (ria.BENEFICIARY_NUMBER != null) {
                    lesPatients.add(ria);
                    System.out.println(ria.DOB + " uwinjiye ni " + ria + "  benbenbenbenbenben  ");
                } else {

                    System.out.println(ria.DOB + " NTAWINJIYE  " + ria + "   benbenbenbenbenben  ");
                }
            }
            JOptionPane.showMessageDialog(null, "length " + sp.length + "lesPatients.size()! "
                    + "\n  " + lesPatients.size());
            System.out.println(" size ije " + lesPatients.size());

            int inserted = 0;
            int updated = 0;

            for (int i = 0; i < lesPatients.size(); i++) {
                INEZA_AFFILIATES uwuje = lesPatients.get(i);
                String uwurimo = DbhandlerVCDC.getPatient(uwuje.BENEFICIARY_NUMBER,
                        stateme, conn);
                if (!uwurimo.equals("ERROR : NO AFFILIATE FOUND")) {
                    boolean done = DbhandlerVCDC.updatePatient2(uwuje, conn);

                    if (done) {
                        updated++;
                    }
                    System.out.println(uwuje + " ; " + done + " ; updatePatient ; " + updated);
                } else {
                    boolean done = DbhandlerVCDC.insertPatient(uwuje, conn);

                    if (done) {
                        inserted++;

                    }
                    System.out.println(uwuje + " ; " + done + " ; inserted ; " + inserted);
                }

                //  System.out.println( uwuje+" ; "+inserted); 
            }

            return (inserted + " / " + lesPatients.size() + " Affiliates Inserted\n"
                    + updated + "/" + lesPatients.size() + " Affiliates Updated");

        } catch (Exception ex) {
            System.out.println(" Exception " + ex);
        }
        return "FAILED";
    }

    public static String injizaInezaClient5(String MSIDN, Connection conn) {
        try {
            String arriva = " <ID_PATIENT>2587268</ID_PATIENT>                \n"
                    + "    <AFFILIATES_NUMBER>" + MSIDN + "</AFFILIATES_NUMBER>                \n"
                    + "    <BENEFICIARY_NUMBER>" + MSIDN + "</BENEFICIARY_NUMBER>                \n"
                    + "    <BENEFICIARY_FIRST_NAME>INEZA CLIENT</BENEFICIARY_FIRST_NAME>                \n"
                    + "    <BENEFICIARY_LAST_NAME> </BENEFICIARY_LAST_NAME>                \n"
                    + "    <RELATIONSHIP>SELF</RELATIONSHIP>                \n"
                    + "    <DOB>2020-04-16</DOB>                \n"
                    + "    <PERCENTAGE>100</PERCENTAGE>                \n"
                    + "    <GENDER></GENDER>                \n"
                    + "    <EMPLOYER></EMPLOYER>                \n"
                    + "    <EMPLOYER_CATEGORY></EMPLOYER_CATEGORY>                \n"
                    + "    <AFFILIATION_STATUS>ACTIVATED</AFFILIATION_STATUS>                \n"
                    + "    <PHONE_NUMBER>" + MSIDN + "</PHONE_NUMBER>                \n"
                    + "    <ID_CARD_NUMBER>INEZA</ID_CARD_NUMBER>                \n"
                    + "    <ASSURANCE>RSSB</ASSURANCE>                 \n"
                    + "    <LAST_UPDATED>2020-04-30 01:56:49</LAST_UPDATED>";

            INEZA_AFFILIATES uwuje = new INEZA_AFFILIATES(arriva,"RSSB");

            boolean done;
            Statement stateme = conn.createStatement();
            String uwurimo = DbhandlerVCDC.getPatient(uwuje.BENEFICIARY_NUMBER,
                     stateme, conn);
            System.out.println(uwuje.BENEFICIARY_NUMBER + " ineza_vcdc.Jobber.injizAbarwayiONE()"
                    + " before insert into DB\n" + uwurimo);
            if (!uwurimo.equals("ERROR : NO AFFILIATE FOUND")) {
                done = DbhandlerVCDC.updatePatient2(uwuje, conn);
            } else {
                done = DbhandlerVCDC.insertPatient(uwuje, conn);
            }
            uwurimo = DbhandlerVCDC.getPatient(uwuje.BENEFICIARY_NUMBER,
                     stateme, conn);
            System.err.println(done + " done :" + uwuje.BENEFICIARY_FIRST_NAME);
            return uwurimo;
        } catch (SQLException ex) {
            Logger.getLogger(Jobber.class.getName()).log(Level.SEVERE, null, ex);
            return "ERROR EXCEPTION:" + ex;
        }

    }

    public static String injizAbarwayiONE5(String BENEFICIARY, Connection conn, String ASSURANCE) {
        System.out.println("Inside getpatient one  " + conn);

        try {
            Statement stateme = conn.createStatement();
//        long LAST_UPDATE = DbhandlerVCDC.getLastLongPatient(stateme);
            String abarwayi = HTTP_URL.Igicu.kohereza(
                    "VCDC_GET_AFFILIATES_ONE", "bridge",
                    "<ASSURANCE>" + ASSURANCE + "</ASSURANCE>"
                    + "<BENEFICIARY_NUMBER>" + BENEFICIARY + "</BENEFICIARY_NUMBER>"
                    + "</header></request>");
            System.out.println("umurwayi bampaye beneficialy number 3\n " + abarwayi);

            if (!abarwayi.equals("NULL RESULT") && !abarwayi.equals(""))//get patient live
            {
                String[] sp = abarwayi.split("</LIN>");

                LinkedList<INEZA_AFFILIATES> lesPatients = new LinkedList<>();
                for (String sp1 : sp) {
                    System.out.println("ineza_vcdc.Jobber.injizAbarwayiONE umwe \n " + sp1);
                    INEZA_AFFILIATES ria = new INEZA_AFFILIATES(sp1,ASSURANCE);
                    System.out.println(ria.DOB + " uwinjiye ni " + ria + "  ben  ");
                    if (ria.BENEFICIARY_NUMBER != null) {
                        lesPatients.add(ria);
                    } else {
                        System.out.println(ria.DOB + "uyu ntiyinjirane " + ria + "  benbenbenbenbenben  ");
                    }

                }

                System.out.println(" size ije " + lesPatients.size());

                int inserted = 0;
                int updated = 0;

                for (int i = 0; i < lesPatients.size(); i++) {
                    INEZA_AFFILIATES uwuje = lesPatients.get(i);
                    String uwurimo = DbhandlerVCDC.getPatient(uwuje.BENEFICIARY_NUMBER,
                             stateme, conn);
                    System.out.println(uwuje.BENEFICIARY_NUMBER + " ineza_vcdc.Jobber.injizAbarwayiONE()"
                            + " before insert into DB\n" + uwurimo);
                    // JOptionPane.showMessageDialog(null, "Nahageze! \n "+uwurimo); 
                    if (!uwurimo.equals("ERROR : NO AFFILIATE FOUND")) {
                        boolean done = DbhandlerVCDC.updatePatient2(uwuje, conn);
                        // JOptionPane.showMessageDialog(null, "updatePatient! \n "+done);
                        if (done) {
                            updated++;
                        }
                    } else {
                        boolean done = DbhandlerVCDC.insertPatient(uwuje, conn);
                        if (done) {
                            inserted++;
                            System.err.println("inserted :" + uwuje.BENEFICIARY_FIRST_NAME);
                        }

                    }

                }
                System.out.println("umurwayi muhaye POS MUHAYE \n" + abarwayi);
                return abarwayi;
            } else // can't get patient from up, get local
            {
                Statement stat = conn.createStatement();
                if (DbhandlerVCDC.isValide("RECEIVE_INEZA_AFFILIATES", stat)) {
                    System.out.println("patient got from local DB " + DbhandlerVCDC.getPatient(BENEFICIARY, stateme, conn));
                    return DbhandlerVCDC.getPatient(BENEFICIARY, stateme, conn);
                } else {
                    System.out.println("RSSB Database out of date; Connect to update");
                    return "ERROR : RSSB Database out of date";
                }
            }
        } catch (Exception ex) {
            System.out.println(" Exception " + ex);
            return "ERROR : FAILED";
        }
//        JOptionPane.showMessageDialog(null, "Nahageze!");
//        System.out.println("Something is wrong here");
//        return "ERROR : FAILED";
    }

    public static LinkedList<RECEIVE_INEZA_ITEM> getItemsa2() {
        System.out.println("Inside getItems one  ");
        LinkedList<RECEIVE_INEZA_ITEM> lesItems = new LinkedList<>();
        try {
            //     Statement stateme = conn.createStatement();
//        long LAST_UPDATE = DbhandlerVCDC.getLastLongPatient(stateme);
            String items = HTTP_URL.Igicu.kohereza(
                    "GET_ITEM_ALIMANTATION", "BRALIRWA_ULK",
                    "<ID_CLIENT>ALG000022402</ID_CLIENT>"
                    + "<FIELD>MARKET</FIELD></header></request>");

            System.out.println("itemsitemsitemsitems\n " + items);

            if (!items.equals("NULL RESULT") && !items.equals(""))//get patient live
            {
                String[] sp = items.split("</ITEM>");

                LinkedList<RECEIVE_INEZA_ITEM> lesPatients = new LinkedList<>();
                int success = 0;
                int fail = 0;
                for (String sp1 : sp) {

                    RECEIVE_INEZA_ITEM ria = new RECEIVE_INEZA_ITEM(sp1, true);

                    if (ria.CODE_PRODUCT != null) {
                        lesPatients.add(ria);
                        success++;
                    } else {
                        System.out.println(sp1 + " ");
                        fail++;
                    }
                }
                System.out.println(fail + " size ije " + lesPatients.size() + "  sp " + sp.length);
                JOptionPane.showMessageDialog(null, fail + " size ije " + lesPatients.size());
            } else {
                System.out.println(" shiiiidaaaa ije ");
            }
        } catch (Exception ex) {
            System.out.println(" Exception " + ex);
        }
        return lesItems;
    }

//public static  String injizAbarwayiONERSSB( String BENEFICIARY,Connection conn)
//{ 
//      System.out.println("Inside getpatient one  "+conn);
//    
//      try {
//        Statement stateme = conn.createStatement();
// 
//             INEZA_AFFILIATES uwuje = sendRSSB2(BENEFICIARY) ;
//             
//             //JOptionPane.showMessageDialog(null, ""+uwuje);
//            String uwurimo = DbhandlerVCDC.getPatient(uwuje.BENEFICIARY_NUMBER
//                    , stateme,conn); 
//            System.out.println(uwuje.BENEFICIARY_NUMBER+" ineza_vcdc.Jobber.injizAbarwayiONE()"
//                    + " before insert into DB\n"+uwurimo); 
//            // JOptionPane.showMessageDialog(null, "Nahageze! \n "+uwurimo); 
//            if(!uwurimo.equals("ERROR : NO AFFILIATE FOUND"))
//            {
//            boolean done= DbhandlerVCDC.updatePatient(uwuje,conn);
//            // JOptionPane.showMessageDialog(null, "updatePatient! \n "+done);
//            if(done)
//            { System.err.println("updated :"+uwuje.BENEFICIARY_FIRST_NAME);;}
//            }
//            else
//            { 
//                boolean done= DbhandlerVCDC.insertPatient(uwuje,conn);
//                   //JOptionPane.showMessageDialog(null, "insertPatient! \n "+done);
//                        if (done) { 
//                            System.err.println("inserted :"+uwuje.BENEFICIARY_FIRST_NAME);
//                        }
//                        
//                    }
//uwurimo = DbhandlerVCDC.getPatient(uwuje.BENEFICIARY_NUMBER
//                    , stateme,conn);
//     System.out.println("umurwayi muhaye POS MUHAYE \n" + uwurimo);
//                return uwurimo;
//            
//        } catch (Exception ex) {
//            System.out.println(" Exception " + ex);
//        }
//        //JOptionPane.showMessageDialog(null, "Nahageze!");
//        System.out.println("Something is wrong here");
//        return "ERROR : FAILED";
//    }
//
    public static String injizAbarwayiONERSSBVV(String BENEFICIARY, Connection conn) {
        System.out.println("Inside getpatient one  " + conn);

        try {
            Statement stateme = conn.createStatement();

            INEZA_AFFILIATES uwuje = sendRSSB2(BENEFICIARY);
            //  System.out.println(uwuje.BENEFICIARY_NUMBER+" BENEFICIARY_NUMBER ");  
            if (uwuje == null) {
                return null;
            }
            //JOptionPane.showMessageDialog(null, ""+uwuje);
            String uwurimo = DbhandlerVCDC.getPatient(uwuje.BENEFICIARY_NUMBER,
                     stateme, conn);
            System.out.println(uwuje.BENEFICIARY_NUMBER + " ineza_vcdc.Jobber.injizAbarwayiONE()"
                    + " before insert into DB\n" + uwurimo);
            if (!uwurimo.equals("ERROR : NO AFFILIATE FOUND")) {
                boolean done = DbhandlerVCDC.updatePatient2(uwuje, conn);
                // JOptionPane.showMessageDialog(null, "updatePatient! \n "+done);
                if (done) {
                    System.err.println("updated :" + uwuje.BENEFICIARY_FIRST_NAME);;
                }
            } else {
                boolean done = DbhandlerVCDC.insertPatient(uwuje, conn);
                if (done) {
                    System.err.println("inserted :" + uwuje.BENEFICIARY_FIRST_NAME);
                }
            }

            uwurimo = DbhandlerVCDC.getPatient(uwuje.BENEFICIARY_NUMBER,
                     stateme, conn);
            System.out.println("umurwayi muhaye POS MUHAYE \n" + uwurimo);
            return uwurimo;
        } catch (Exception ex) {
            System.out.println(" Exception " + ex);
        }
        //JOptionPane.showMessageDialog(null, "Nahageze!");
        System.out.println("Something is wrong here");
        return "ERROR : FAILED";
    }

    static public String injizaItem(Connection conn, String ASSURANCE) {

        try {
            Statement stateme = conn.createStatement();
            Statement stateme2 = conn.createStatement();
            String items = HTTP_URL.Igicu.kohereza("VCDC_GET_ITEMS", "bridge",//this Get API is not yet Available
                    "<ASSURANCE>" + ASSURANCE + "</ASSURANCE>"
                    + "<FETCH_SIZE>500</FETCH_SIZE>"
                    + "<LAST_MAX_ID>"+getMaxItemIdProduct(stateme2)+"</LAST_MAX_ID>"
                    + " </header></request>");
            System.out.println("" + items);
            String[] sp = items.split("</LIN>");

            LinkedList<RECEIVE_INEZA_ITEM> lesItems = new LinkedList<>();
            int size = sp.length;
            //JOptionPane.showMessageDialog(null, " size ije ju " + sp.length);

            while(size>0)
            {
            for (String sp1 : sp) { 
                try {
                    RECEIVE_INEZA_ITEM rii = new RECEIVE_INEZA_ITEM(sp1);
                    // System.err.println(" "+rii);
                    if (rii.INN_RWA_CODE != null)//&& rii.INN_RWA_CODE!=null) //utazwi mu rwanda ntucuruzwe
                    {  lesItems.add(rii);  } 
                } catch (Throwable bb) {
                    System.err.println(" bb  " + bb);
                } 
            }

            System.out.println(" size ije " + lesItems.size()); 
            int inserted = 0; int updated = 0; 
            for (int i = 0; i < lesItems.size(); i++) {
                RECEIVE_INEZA_ITEM uwuje = lesItems.get(i); 
                String uwurimo = DbhandlerVCDC.getItem(uwuje.INN_RWA_CODE,stateme);
                if (uwurimo != null && !uwurimo.equals("ITEM NOT FOUND")) {
                    boolean done = DbhandlerVCDC.updateItem(uwuje, conn);
                    if (done) { updated++;   }
                } else {
                    boolean done = DbhandlerVCDC.insertItem(uwuje, conn);
                    if (done) {  inserted++;  }
                } 
            }
            String resultat = (inserted + " / " + lesItems.size() + " Drugs Inserted\n"
                    + updated + "/" + lesItems.size() + " Drugs Updated");
            
          items = HTTP_URL.Igicu.kohereza("VCDC_GET_ITEMS", "bridge",//this Get API is not yet Available
                    "<ASSURANCE>" + ASSURANCE + "</ASSURANCE>"
                    + "<FETCH_SIZE>500</FETCH_SIZE>"
                    + "<LAST_MAX_ID>"+getMaxItemIdProduct(stateme2)+"</LAST_MAX_ID>"
                    + " </header></request>");
            System.out.println("" + items);
            
            if(items==null || !items.contains("</LIN>"))
            {   System.out.println("ZASHIZE" + items);
                break;}
            sp = items.split("</LIN>");

           lesItems = new LinkedList<>();
           size = sp.length;
        }
           // JOptionPane.showMessageDialog(null, resultat);

            return "SUCCESS";
        } catch (Exception ex) {
            System.out.println(" Exception " + ex);
        }
        return "FAILED";
    }

    static public String injizaAffectation(Connection conn) {

        try {
            Statement stateme = conn.createStatement();
            //String LAST_UPDATE = DbhandlerVCDC.lastUpdate("VCDC.RECEIVE_INEZA_DRUGS", stateme);

            String items = HTTP_URL.Igicu.kohereza("VCDC_GET_AFFECTATION", "bridge",//this Get API is not yet Available
                    "<ASSURANCE>RAMA</ASSURANCE>"
                    + "<LAST_UPDATE>1</LAST_UPDATE> </header></request>");
            // System.out.println("ineza_vcdc.Jobber.injizItem() \n "+items);

            String[] sp = items.split("</LIN>");

            LinkedList<RECEIVE_INEZA_AFFECTATION> lesItems = new LinkedList<>();

            JOptionPane.showMessageDialog(null, " size ije ju " + sp.length);

            for (String sp1 : sp) {

                try {
                    RECEIVE_INEZA_AFFECTATION rii = new RECEIVE_INEZA_AFFECTATION(sp1);
                    // System.err.println(" "+rii);
                    if (rii.HSP_CODE != null)//&& rii.INN_RWA_CODE!=null) //utazwi mu rwanda ntucuruzwe
                    {
                        lesItems.add(rii);
                    }

                } catch (Throwable bb) {
                    System.err.println(" bb  " + bb);
                }

                // JOptionPane.showMessageDialog(null, " size ije ju "+sp1);
            }

            System.out.println(" size ije " + lesItems.size());

            JOptionPane.showMessageDialog(null, " size iri bwinjire " + lesItems.size());

            int inserted = 0;
            int updated = 0;

            for (int i = 0; i < lesItems.size(); i++) {
                RECEIVE_INEZA_AFFECTATION uwuje = lesItems.get(i);

                boolean uwurimo = DbhandlerVCDC.getAffectation(uwuje.HSP_CODE,
                         uwuje.COUNCIL_NUMBER, stateme);
                if (!uwurimo) {
                    boolean done = DbhandlerVCDC.insertAffectation(uwuje, conn);
                    if (done) {
                        inserted++;
                    }
                }

            }
            String resultat = (inserted + " / " + lesItems.size() + " Drugs Inserted\n"
                    + updated + "/" + lesItems.size() + " Drugs Updated");

            JOptionPane.showMessageDialog(null, resultat);

            return resultat;
        } catch (Exception ex) {
            System.out.println(" Exception " + ex);
        }
        return "FAILED";
    }

    public static String injizaItemImwe(String ATC_CODE, Connection conn)//pull item direct
    {
        System.out.println("Inside get one prescriber " + conn);

        try {
            Statement stateme = conn.createStatement();
//        long LAST_UPDATE = DbhandlerVCDC.getLastLongPatient(stateme);
            String imiti = HTTP_URL.Igicu.kohereza(
                    "VCDC_GET_ITEM_ONE", "bridge", //API not yet available
                    "<ASSURANCE>RAMA</ASSURANCE>"
                    + "<ATC_CODE>" + ATC_CODE + "</ATC_CODE>"
                    + "</header></request>");
            System.out.println("umuti bampaye\n " + imiti);

            if (!imiti.equals("NULL RESULT") && !imiti.equals(""))//get patient live
            {
                String[] sp = imiti.split("</LIN>");

                LinkedList<RECEIVE_INEZA_ITEM> lesPatients = new LinkedList<>();
                for (String sp1 : sp) {
                    System.out.println("PRESCRIBER  umwe \n " + sp1);
                    RECEIVE_INEZA_ITEM rii = new RECEIVE_INEZA_ITEM(sp1);
                    System.out.println(rii.ATC_CODE + " uwinjiye  ");
                    if (rii.ATC_CODE != null) {
                        lesPatients.add(rii);
                    } else {
                        System.out.println(rii.ATC_CODE + "not inserted check data length");

                    }
                }
                System.out.println(" size yimite ije " + lesPatients.size());

                int inserted = 0;
                int updated = 0;

                for (int i = 0; i < lesPatients.size(); i++) {
                    RECEIVE_INEZA_ITEM uwuje = lesPatients.get(i);
                    String uwurimo = DbhandlerVCDC.getPrescriber(uwuje.ATC_CODE,
                             stateme);
                    System.out.println(uwuje.ATC_CODE + " niwe uje"
                            + " before insert into umuti DB\n" + uwurimo);
                    JOptionPane.showMessageDialog(null, "Nahageze! \n " + uwurimo);
                    if (!uwurimo.equals("NO PRESCRIBER FOUND")) {
                        boolean done = DbhandlerVCDC.updateItem(uwuje, conn);
                        JOptionPane.showMessageDialog(null, "update Item! \n " + done);
                        if (done) {
                            updated++;
                        }
                    } else {
                        boolean done = DbhandlerVCDC.insertItem(uwuje, conn);
                        JOptionPane.showMessageDialog(null, "insertPRESCRIBER! \n " + done);
                        if (done) {
                            inserted++;
                            System.err.println("inserted Drug / Item:" + uwuje.ATC_CODE);
                        }
                    }

                }
                JOptionPane.showMessageDialog(null, inserted + " / " + lesPatients.size() + " Affiliates Inserted\n"
                        + updated + "/" + lesPatients.size() + " Affiliates Updated");
                System.out.println(" POS MUHAYE \n" + imiti);
                return imiti;
            } else // can't get umuti from up, get local
            {
                Statement stat = conn.createStatement();
                if (DbhandlerVCDC.isValide("RECEIVE_INEZA_ITEM", stat)) {
                    System.out.println("Drug got from local DB " + DbhandlerVCDC.getItem(ATC_CODE, stateme));
                    return DbhandlerVCDC.getPrescriber(ATC_CODE, stateme);
                } else {
                    System.out.println("RSSB Database out of date; Connect to update");
                    return "RSSB Database out of date";
                }
            }
        } catch (Exception ex) {
            System.out.println(" Exception while getting prescriber" + ex);
        }
        JOptionPane.showMessageDialog(null, "Nahageze!");
        System.out.println("Something is wrong here");
        return "FAILED";
    }

    static public String injizaPrescriber(Connection conn) {

        try {
            Statement stateme = conn.createStatement();
//        long LAST_UPDATE = DbhandlerVCDC.getLastLongPrescriber(stateme);
            String LAST_UPDATE = DbhandlerVCDC.lastUpdate("VCDC.RECEIVE_INEZA_PRESCRIBERS", stateme);
            String prescribers = HTTP_URL.Igicu.kohereza("VCDC_GET_PRESCRIBER", "bridge",
                    "<ASSURANCE>RAMA</ASSURANCE>"
                    + "<LAST_UPDATE>" + LAST_UPDATE + "</LAST_UPDATE> </header></request>");

            //System.out.println("ineza_vcdc.Jobber.injizPrescribers() \n "+prescribers);
            String[] sp = prescribers.split("</LIN>");

            LinkedList<RECEIVE_INEZA_PRESCRIBERS> lesprescribers = new LinkedList<>();
            for (String sp1 : sp) {
                RECEIVE_INEZA_PRESCRIBERS rip = new RECEIVE_INEZA_PRESCRIBERS(sp1);
                //  System.err.println(" "+rip);
                if (rip.COUNCIL_NUMBER != null && !rip.COUNCIL_NUMBER.equals(""))//valid council number
                {
                    lesprescribers.add(rip);
                    //  System.out.println("Prescriber added to list");
                }
            }

            System.out.println(" number of prescribers " + lesprescribers.size());

            JOptionPane.showMessageDialog(null, " number of prescribers " + lesprescribers.size());

            int inserted = 0;
            int updated = 0;

            for (int i = 0; i < lesprescribers.size(); i++) {
                RECEIVE_INEZA_PRESCRIBERS uwuje = lesprescribers.get(i);
                String uwurimo = DbhandlerVCDC.getPrescriber(uwuje.COUNCIL_NUMBER,
                         stateme);

                if (!uwurimo.equals("NO PRESCRIBER FOUND") && !uwurimo.equals("")) {
                    boolean done = DbhandlerVCDC.updatePrescriber(uwuje, conn);
                    if (done) {
                        updated++;
                    }
                } else {
                    boolean done = DbhandlerVCDC.insertPrescriber2(uwuje, conn);
                    if (done) {
                        inserted++;
                    }
                }

                // JOptionPane.showMessageDialog(null, updated+" updated  inserted "+inserted);
            }

            String res = (inserted + " / " + lesprescribers.size() + " Prescribers Inserted\n"
                    + updated + "/" + lesprescribers.size() + " Prescribers Updated");
            JOptionPane.showMessageDialog(null, res);
            return res;
        } catch (Exception ex) {
            System.out.println(" Exception " + ex);
        }
        return "FAILED";
    }

    public static String injizaPRESCRIBERUmwe(String council_number, Connection conn) {
        System.out.println("Inside get one prescriber " + conn);

        try {
            Statement stateme = conn.createStatement();
//        long LAST_UPDATE = DbhandlerVCDC.getLastLongPatient(stateme);
            String ABAGANGA = HTTP_URL.Igicu.kohereza(
                    "VCDC_GET_PRESCRIBER_ONE", "bridge",
                    "<ASSURANCE>RAMA</ASSURANCE>"
                    + "<COUNCIL_NUMBER>" + council_number + "</COUNCIL_NUMBER>"
                    + "</header></request>");
            System.out.println("PRESCRIBER umwe bampaye\n " + ABAGANGA);

            if (ABAGANGA.contains("</LIN>"))//got patient list should contain this; otherwise no Prescriber found 
            {
                String[] sp = ABAGANGA.split("</LIN>"); //

                LinkedList<RECEIVE_INEZA_PRESCRIBERS> lesPatients = new LinkedList<>();
                for (String sp1 : sp) {
                    System.out.println("PRESCRIBER  umwe \n " + sp1);
                    RECEIVE_INEZA_PRESCRIBERS rii = new RECEIVE_INEZA_PRESCRIBERS(sp1);
//            System.out.println(rii.COUNCIL_NUMBER+" uwinjiye  ");
                    if (rii.COUNCIL_NUMBER != null) {
                        lesPatients.add(rii);
                    } else {
                        System.out.println(rii.COUNCIL_NUMBER + "not inserted check data length");

                    }
                }
                System.out.println(" size  baje on council number: " + lesPatients.size());

                int inserted = 0;
                int updated = 0;

                for (int i = 0; i < lesPatients.size(); i++) {
                    RECEIVE_INEZA_PRESCRIBERS uwuje = lesPatients.get(i);
                    String uwurimo = DbhandlerVCDC.getPrescriber(uwuje.COUNCIL_NUMBER,
                             stateme);
                    System.out.println(uwuje.COUNCIL_NUMBER + " niwe uje"
                            + " before insert into DB\n");
                    JOptionPane.showMessageDialog(null, "Nahageze! \n ");
                    if (!uwurimo.equals("NO PRESCRIBER FOUND")) {
                        System.out.println("Updating....");
                        boolean done = DbhandlerVCDC.updatePrescriber(uwuje, conn);
                        JOptionPane.showMessageDialog(null, "updatePRESCRIBER! \n " + done);
                        if (done) {
                            updated++;
                        }
                    } else {
                        boolean done = DbhandlerVCDC.insertPrescriber2(uwuje, conn);
                        JOptionPane.showMessageDialog(null, "insertPRESCRIBER! \n " + done);
                        if (done) {
                            inserted++;
                            System.err.println("inserted PRESCRIBER:" + uwuje.COUNCIL_NUMBER);
                        }
                    }

                }
                JOptionPane.showMessageDialog(null, inserted + " / " + lesPatients.size() + " prescriber Inserted\n"
                        + updated + "/" + lesPatients.size() + " prescriber Updated");
                System.out.println(" POS MUHAYE \n" + ABAGANGA);
                return ABAGANGA;
            } else // can't get patient from up, get local if DB is not older than 7 days
            {
                Statement stat = conn.createStatement();
                if (DbhandlerVCDC.isValide("RECEIVE_INEZA_PRESCRIBERS", stat)) {
                    System.out.println("prescriber got from local DB " + DbhandlerVCDC.getPrescriber(council_number, stateme));
                    return DbhandlerVCDC.getPrescriber(council_number, stateme);
                } else {
                    System.out.println("RSSB Database out of date; Connect to update");
                    return "RSSB Database out of date";
                }
            }
        } catch (Exception ex) {
            System.out.println(" Exception while getting prescriber" + ex);
        }
        JOptionPane.showMessageDialog(null, "Nahageze!");
        System.out.println("Something is wrong here");
        return "FAILED";
    }

    static public String injizaHSP(Connection conn) {

        try {
            Statement stateme = conn.createStatement();
            //   long LAST_UPDATE = DbhandlerVCDC.getLastLongHSP(stateme);
            //   String LAST_UPDATE = DbhandlerVCDC.lastUpdate("VCDC.RECEIVE_INEZA_HSP", stateme);

            String hsps = HTTP_URL.Igicu.kohereza("VCDC_GET_HEALTHFACILITY", "bridge",
                    "<ASSURANCE>RAMA</ASSURANCE>"
                    + "<LAST_UPDATE>1</LAST_UPDATE> </header></request>");
            //System.out.println("ineza_vcdc.Jobber.injizHSP() \n "+hsps);
            // handle when it is empty
            String[] sp = hsps.split("</LIN>");

            LinkedList<RECEIVE_INEZA_HSP> lesHsps = new LinkedList<>();
            for (String sp1 : sp) {
                RECEIVE_INEZA_HSP hsp
                        = new RECEIVE_INEZA_HSP(sp1);
                // System.err.println(" "+hsp.HSP_NAME);
                if (hsp.HSP_CODE != null)//&& rii.INN_RWA_CODE!=null) //utazwi mu rwanda ntucuruzwe
                {
                    lesHsps.add(hsp);
                }
            }

            System.out.println(" number of hsp " + lesHsps.size());
            JOptionPane.showMessageDialog(null, " number of hsp " + lesHsps.size());
            int inserted = 0;
            int updated = 0;
            int failed = 0;

            for (int i = 0; i < lesHsps.size(); i++) {
                try {
                    RECEIVE_INEZA_HSP uwuje = lesHsps.get(i);
                    String uwurimo = DbhandlerVCDC.getRECEIVE_INEZA_HSP(uwuje.HSP_CODE,
                             stateme);

                    System.out.println(uwuje.HSP_CODE + ""
                            + " ineza_vcdc.Jobber.injizaHSP() " + uwurimo + " \n " + (!uwurimo.equals("NO HSP FOUND")));

                    if (uwurimo != null && !uwurimo.equals("")) {
                        System.out.println(" irimo " + uwuje.HSP_NAME);

                        boolean done = DbhandlerVCDC.updateHSP(uwuje, conn);
                        if (done) {
                            updated++;
                        }
                    } else {
                        boolean done = DbhandlerVCDC.insertHSP(uwuje, conn);
                        if (done) {
                            inserted++;
                        }
                    }
                } catch (Throwable tr) {
                    System.err.println("  failed " + tr);
                    failed++;
                }
            }
            String res = (inserted + " / " + lesHsps.size() + " HSPs Inserted\n"
                    + updated + "/" + lesHsps.size() + " HSPs Updated \n Failed " + failed);

            JOptionPane.showMessageDialog(null, res);

            return res;
        } catch (Exception ex) {
            System.out.println(" Exception " + ex);
        }
        return "FAILED";
    }

    static public String injizaInstruction(Connection conn) {

        try {
            Statement stateme = conn.createStatement();
            //   long LAST_UPDATE = DbhandlerVCDC.getLastLongHSP(stateme);
            //   String LAST_UPDATE = DbhandlerVCDC.lastUpdate("VCDC.RECEIVE_INEZA_HSP", stateme);

            String hsps = HTTP_URL.Igicu.kohereza("VCDC_GET_INSTRUCTION", "bridge",
                    "<ASSURANCE>RAMA</ASSURANCE>"
                    + "<LAST_UPDATE>1</LAST_UPDATE> </header></request>");
            System.out.println("ineza_vcdc.Jobber.injizHSP() \n " + hsps);
            // handle when it is empty
            String[] sp = hsps.split("</LIN>");

            LinkedList<INEZA_ITEM_INSTRUCTIONS> lesHsps = new LinkedList<>();
            for (String sp1 : sp) {
                INEZA_ITEM_INSTRUCTIONS hsp
                        = new INEZA_ITEM_INSTRUCTIONS(sp1);
                // System.err.println(" "+hsp.HSP_NAME);
                if (hsp.INN_RWA_CODE != null)//&& rii.INN_RWA_CODE!=null) //utazwi mu rwanda ntucuruzwe
                {
                    lesHsps.add(hsp);
                }
            }

            System.out.println(" number of hsp " + lesHsps.size());
            JOptionPane.showMessageDialog(null, " number of hsp " + lesHsps.size());
            int inserted = 0;
            int updated = 0;
            int failed = 0;
            Statement stat = conn.createStatement();
            for (int i = 0; i < lesHsps.size(); i++) {
                try {
                    INEZA_ITEM_INSTRUCTIONS uwuje = lesHsps.get(i);
                    String uwurimo = DbhandlerVCDC.getInstructionExist(uwuje.INN_RWA_CODE, uwuje.CATEGORY,
                             stateme);

                    if (uwurimo != null && !uwurimo.equals("NO INSTRUTIONS FOUND")) {
                        boolean done = DbhandlerVCDC.updateINSTRUCTION(uwuje, stat);
                        if (done) {
                            updated++;
                        }
                    } else {
                        boolean done = DbhandlerVCDC.insertINSTR(uwuje, conn);
                        if (done) {
                            inserted++;
                        }
                    }
                } catch (Throwable tr) {
                    System.err.println("  failed " + tr);
                    failed++;
                }
            }
            String res = (inserted + " / " + lesHsps.size() + " HSPs Inserted\n"
                    + updated + "/" + lesHsps.size() + " HSPs Updated \n Failed " + failed);

            JOptionPane.showMessageDialog(null, res);

            return res;
        } catch (Exception ex) {
            System.out.println(" Exception " + ex);
        }
        return "FAILED";
    }

    public static String injizaHSPUmwe(String HSP_CODE, Statement state) {
        System.out.println(HSP_CODE + "  Inside get one hsp ");
        return DbhandlerVCDC.getRECEIVE_INEZA_HSP(HSP_CODE.toUpperCase(), state);
    }

    public static String injizaPRESSUmwe(String PRESS, Statement state) {
        System.out.println(PRESS + "  Inside get one PRESS ");
        return DbhandlerVCDC.getPrescriber(PRESS.toUpperCase(), state);
    }

}
