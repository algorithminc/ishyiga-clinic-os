/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

import clinic.Main_Clinic;
import globalaccount.Account;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import static sdc.comm.CommISHYIGA.KoherezaKuIgicu;

/**
 *
 * @author ishyiga
 */
public class ODT_INTERFACE_MAIN extends JFrame {

    String title;
    Account umukozi;
    JButton MY_WORK, PRIN, RECORDS, ACTIVITY, PRESCRIPTION, MESSAGE, LOAD_APPOINTMENT, ASK_PATIENTE;
    JList APPOINTMENT_LIST, INSURANCE;
    JTextField patientID;
    LinkedList<JLabel> aliste = new LinkedList<>();
    String TIN, SSN, MRC;
    Connection conn;
    String ID_BRANCH_COMPANY;
    INEZA_AFFILIATES ria;
    public static String version = "VSDC R. 7.0.0 KAMALIZA" ; //"VSDC R. 6.0.4";
    
    
    public ODT_INTERFACE_MAIN(String TIN, String SSN, String MRC,
            Connection conn, Account umukozi) {

        this.umukozi = umukozi;
        this.title = "ISHYIGA CLINIC LITE FOR OM : "
                 + umukozi.AUTO_DEPOT   + " / NAME " + umukozi.NAMES_ACCOUNT; 
        this.TIN=TIN;this.SSN=SSN;this.MRC=MRC;this.conn=conn;  
        this.ID_BRANCH_COMPANY = umukozi.BP_EMPLOYE; 
        initiate();
    }

    private void initiate() {
        Container content = getContentPane();
        setTitle(title);
        setSize(900, 700);
        content.add(pane());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        activity();
        prescription();
        searchPATEINTClicked();
        PATIENT_FILE();
        printQr();
        patientCall();
        message();
        LOAD_APPOINTMENT();
        setVisible(true);
    }

    private JPanel pane() {
        JPanel masterBoutton = new JPanel();
        masterBoutton.setLayout(new GridLayout(3, 1));
        masterBoutton.add(hejuru());
        masterBoutton.add(umurwayi(aliste));
        masterBoutton.add(functions());

        return masterBoutton;

    }

    public JPanel hejuru() {
        JPanel jp = new JPanel();
        javax.swing.border.Border LoweredBevelBorder = BorderFactory.createLoweredBevelBorder();
        TitledBorder EtchedBorderRaised = BorderFactory.createTitledBorder(LoweredBevelBorder, 
                "SEARCH OPTIONS",
                TitledBorder.LEFT, TitledBorder.TOP,
                new Font("Arial", Font.BOLD, 16), Color.BLUE);
        jp.setBorder(EtchedBorderRaised); 
        jp.setLayout(new GridLayout(1, 3));
        patientID = new JTextField(15);
        patientID.setText("...search patient ID ...");

        Icon load = new ImageIcon(("image/appointment.jpg"));
        LOAD_APPOINTMENT = new JButton(load);
        LOAD_APPOINTMENT.setBackground(Color.lightGray);

        APPOINTMENT_LIST = new JList();
        JScrollPane rdvScroll = new JScrollPane(APPOINTMENT_LIST);
        rdvScroll.setPreferredSize(new Dimension(100, 20));
        APPOINTMENT_LIST.setBackground(new Color(240, 255, 255));
        String[] rdv = new String[]{"NO APPOINTMENT"};
        APPOINTMENT_LIST.setListData(rdv);

        Icon patiente = new ImageIcon(("image/info.png"));
        ASK_PATIENTE = new JButton(patiente);
        ASK_PATIENTE.setBackground(Color.white);

        INSURANCE = new JList();
        String[] insurance = new String[]{
            "INEZA", "RAMA", "UAP", "SANLAM", "RADIANT", "OTHER"};
        INSURANCE.setListData(insurance);
        JScrollPane insuranceScroll = new JScrollPane(INSURANCE);
        insuranceScroll.setPreferredSize(new Dimension(100, 20));
        INSURANCE.setBackground(new Color(240, 255, 225));

        JPanel hW = new JPanel();
        hW.setLayout(new GridLayout(2, 1));
        hW.add(new JLabel("INSURANCE"));
        hW.add(insuranceScroll);
        JPanel hM = new JPanel();
        hM.setLayout(new GridLayout(2, 1));
        hM.add(patientID);
        hM.add(ASK_PATIENTE);
        JPanel hE = new JPanel();
        hE.setLayout(new GridLayout(2, 1));
        hE.add(LOAD_APPOINTMENT);
        hE.add(rdvScroll);
        jp.add(hW);
        jp.add(hM);
        jp.add(hE);
        return jp;
    }

    public static JPanel umurwayi(LinkedList<JLabel> aliste) {
        JPanel jp = new JPanel();

        javax.swing.border.Border LoweredBevelBorder = BorderFactory.createLoweredBevelBorder();
        TitledBorder EtchedBorderRaised = BorderFactory.createTitledBorder(LoweredBevelBorder,
                "PATIENT INFO",
                TitledBorder.CENTER, TitledBorder.TOP,
                new Font("Arial", Font.BOLD, 16), Color.GREEN);
        jp.setBorder(EtchedBorderRaised);
        jp.setLayout(new GridLayout(6, 2));
        INEZA_AFFILIATES.getPatientInfo(aliste);

        aliste.forEach((jl) -> {
            jp.add(jl);
        });
        return jp;
    }

    public JPanel functions() {
        JPanel jp = new JPanel();
        javax.swing.border.Border LoweredBevelBorder = BorderFactory.createLoweredBevelBorder();
        TitledBorder EtchedBorderRaised = BorderFactory.createTitledBorder(LoweredBevelBorder, 
                "FUNCTIONS",
                TitledBorder.RIGHT, TitledBorder.TOP,
                new Font("Arial", Font.BOLD, 16), Color.RED);
        jp.setBorder(EtchedBorderRaised); 
        jp.setLayout(new GridLayout(2, 3));
        Icon mywor = new ImageIcon(("image/mywork.jpg"));
        MY_WORK = new JButton(mywor);
        MY_WORK.setBackground(Color.white);

        Icon file = new ImageIcon(("image/records.jpg"));
        RECORDS = new JButton(file);
        RECORDS.setBackground(Color.lightGray);

        ACTIVITY = new JButton("ACTIVITY");
        ACTIVITY.setBackground(new Color(240, 250, 200));

        PRESCRIPTION = new JButton("PRESCRIPTION");
        PRESCRIPTION.setPreferredSize(new Dimension(50, 20));
        PRESCRIPTION.setBackground(new Color(240, 250, 175));

        Icon printIcon = new ImageIcon(("image/print.png"));
        PRIN = new JButton(printIcon);
        PRIN.setBackground(Color.white);

        MESSAGE = new JButton("MESSAGE");
        MESSAGE.setBackground(new Color(240, 250, 150));

        jp.add(ACTIVITY);
        jp.add(PRESCRIPTION);
        jp.add(MESSAGE);
        jp.add(MY_WORK);
        jp.add(RECORDS);
        jp.add(PRIN);

        return jp;
    }

    private void activity() {
        ACTIVITY.addActionListener((ActionEvent ae) -> {
            if (ACTIVITY == ae.getSource()) { 
                if(ria==null)
                {JOptionPane.showMessageDialog(null, "Please select a valid Patient");}
                else
                {new ODT_INTERFACE_ACTIVITY(conn, umukozi, ria);}
            }
        });
    }

    private void prescription() {
        PRESCRIPTION.addActionListener((ActionEvent ae) -> {
            if (PRESCRIPTION == ae.getSource()) { 
                if(ria==null)
                {JOptionPane.showMessageDialog(null, "Please select a valid Patient");}
                else
                {new ODT_INTERFACE_PRESCRIPTION(conn, umukozi, ria, MRC, TIN, SSN);}
            }
        });
    }

     
    public void searchPATEINTClicked() {
        patientID.addMouseListener(new MouseListener() {
            public void actionPerformed(ActionEvent ae) {  }
 @Override public void mouseExited(MouseEvent ae) { }
 @Override  public void mouseClicked(MouseEvent e) {
                if (patientID.getText().equals("...search patient ID ...")) {
                    patientID.setText("");
                }
            };
@Override public void mousePressed(MouseEvent e) { } ;
@Override public void mouseReleased(MouseEvent e) { } ;
@Override public void mouseEntered(MouseEvent e) { }
        });
    } 

  private void printQr() {
        PRIN.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == PRIN) { 
                MY_WORK.doClick(); 
            }
        }); 
    } 
  
  public static String getPatientLivePatientFile(String BENEFICIARY,String group_id,
          Account umukozi,String ID_BRANCH_COMPANY )
  { 
   try {

                    String ishyura
                            = "        <card_id>" + BENEFICIARY + "</card_id>\n"
                            + "        <amount_credit>100</amount_credit>\n"
                            + "        <comment>devdcbc9ca61285869472767a554a907222</comment> \n"
                            + "        <partner_id>ALGORITHM_01</partner_id> \n"
                            + "        <agent_id>" + umukozi.NAMES_ACCOUNT + "</agent_id>\n"
                            + "        <affiliate_percentage>0</affiliate_percentage>\n"
                            + "        <ineza_percentage>0</ineza_percentage>\n"
                            + "        <group_id>" + group_id + "</group_id>\n"
                            + "        <affiliate_id></affiliate_id>\n"
                            + "	</header>\n"
                            + "		<LIN>\n"
                            + "			<item_code>PAT001</item_code>\n"
                            + "			<item_amount>100</item_amount>\n"
                            + "			<item_qty>1</item_qty>\n"
                            + "		</LIN>";

//                    String abarwayi = sdc.comm.CommISHYIGA.KoherezaKuIgicu(ID_BRANCH_COMPANY,
//                            "GET_PATIENT_FILE",ishyura+"</request>");
                    String abarwayi = KoherezaKuIgicu(ID_BRANCH_COMPANY, "PAY_PATIENT_FILE ",
                            ishyura + "</request>");

                    String result = HTTP_URL.Igicu.getTagValue("MESSAGE", abarwayi);
                    JOptionPane.showMessageDialog(null, "THE RECORDS YOU ASKED WENT FOR " + BENEFICIARY
                            + " \n " + result);
                } catch (Exception ex) {
                    Logger.getLogger(ODT_INTERFACE.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null, "THE RECORDS YOU ASKED WENT FOR " + BENEFICIARY
                            + " \n ENCOUNTED ERROR: " + ex);
                }
   return "NOT FOUND";
  }
  
    private void PATIENT_FILE() {
        RECORDS.addActionListener((ActionEvent ae) -> {

            if (mezefresh() && ae.getSource() == RECORDS) {
                String BENEFICIARY = patientID.getText();
                String group_id = (String) INSURANCE.getSelectedValue();
                getPatientLivePatientFile(BENEFICIARY,group_id,umukozi, ID_BRANCH_COMPANY);
            }
        });
    }

    void myRDV() {
        String valueToSendM = "<DOCTOR_OM>" + umukozi.AUTO_DEPOT + "</DOCTOR_OM>"
                + "<ID_BRANCH_COMPANY>" + ID_BRANCH_COMPANY + "</ID_BRANCH_COMPANY>"
                + " </header>  </request>";

        System.out.println("ineza_vcdc.ODT_INTERFACE.------() \n " + valueToSendM);
        
        String abarwayiM = KoherezaKuIgicu(ID_BRANCH_COMPANY, "GET_MY_APPOINTMENT",
                valueToSendM);
        System.out.println("ineza_vcdc.ODT_INTERFACE.LOAD_APPOINTMENT() \n " + abarwayiM);

        String resultM = HTTP_URL.Igicu.getTagValue("RESPONSE", abarwayiM);
        
        System.out.println("ineza_vcdc.ODT_INTERFACE.LOAD_APPOINTMENT() \n " + resultM);

         String[] sp = resultM.split("</appointment>");
         String ress = "";
  for (String result : sp) {   
          ress += HTTP_URL.Igicu.getTagValue("service", result)
                            + " " + HTTP_URL.Igicu.getTagValue("booked_date", result)
                            + " " + HTTP_URL.Igicu.getTagValue("start_date", result) + " - "
                            + HTTP_URL.Igicu.getTagValue("end_time", result)
                            + HTTP_URL.Igicu.getTagValue("price", result)+ "\n ";
  }      
  JOptionPane.showMessageDialog(null, "MY APPONTEMENT(S) " 
                            + " \n " + ress);
    }

    private void LOAD_APPOINTMENT() {
        LOAD_APPOINTMENT.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == LOAD_APPOINTMENT) {
                String BENEFICIARY = patientID.getText();
                System.out.println("naaa  "+BENEFICIARY);
                try {

                    if(BENEFICIARY== null || BENEFICIARY.equals("...search patient ID ..."))
                    {
                        myRDV();
                    System.err.println("naaa  "+BENEFICIARY);
                    }
                    else {
                    String valueToSend = "<DOCTOR_OM>" + umukozi.AUTO_DEPOT + "</DOCTOR_OM>"
                            + "<BENEFICIARY_NUMBER>" + BENEFICIARY + "</BENEFICIARY_NUMBER>"
                            + "<ID_BRANCH_COMPANY>" + ID_BRANCH_COMPANY + "</ID_BRANCH_COMPANY> </header>  </request>";

                    String abarwayi = KoherezaKuIgicu(ID_BRANCH_COMPANY, "GET_APPOINTMENT_ODT ",
                            valueToSend);
                    String result = HTTP_URL.Igicu.getTagValue("RESPONSE", abarwayi);

                    String ress = HTTP_URL.Igicu.getTagValue("ineza_id", result)
                            + "\n " + HTTP_URL.Igicu.getTagValue("doctor_om", result)
                            + "\n " + HTTP_URL.Igicu.getTagValue("service", result)
                            + "\n " + HTTP_URL.Igicu.getTagValue("booked_date", result)
                            + "\n " + HTTP_URL.Igicu.getTagValue("start_date", result) + " - "
                            + HTTP_URL.Igicu.getTagValue("end_time", result)
                            + "\n " + HTTP_URL.Igicu.getTagValue("price", result);

                    JOptionPane.showMessageDialog(null, "APPONTEMENT FOR " + BENEFICIARY
                            + " \n " + ress);
                    ASK_PATIENTE.doClick();
                }
                } catch (Exception ex) {
                    Logger.getLogger(ODT_INTERFACE.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null,
                            "THE RECORDS YOU ASKED WENT FOR " + BENEFICIARY
                            + " \n ENCOUNTED ERROR: " + ex);
                }
            }
        });
    } 
  
     
    public boolean mezefresh() {
        String aff = patientID.getText();
        if (aff == null || aff.length() < 3) {
            JOptionPane.showMessageDialog(null, "UNEXPECTED LENGTH " + aff, " ERROR ",
                    JOptionPane.WARNING_MESSAGE);
            return false;
        }
        if (INSURANCE.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(null, "SELECT AN INSURANCE", " ERROR ",
                    JOptionPane.WARNING_MESSAGE);
            return false;
        }
        String insuraString = (String) INSURANCE.getSelectedValue();
        if (insuraString.equals("INEZA") && aff.length() != 12) {
            JOptionPane.showMessageDialog(null, "INEZA NUMBER(MSIDN) LENGHT NOT 12 " + aff.length(),
                    " ERROR ",
                    JOptionPane.WARNING_MESSAGE);
            return false;
        }
        return true;
    }

    private void patientCall() {
        ASK_PATIENTE.addActionListener((ActionEvent ae) -> { 
            System.out.println("ineza_vcdc.ODT_INTERFACE.patientCall()");

            if (mezefresh() && ASK_PATIENTE == ae.getSource()) {
                String aff = patientID.getText();
                System.out.println("ineza_vcdc.ODT_INTERFACE.patientCall()" + aff);
                 
                String insuraString = (String) INSURANCE.getSelectedValue();
                String st = Jobber.injizAbarwayiONE5(aff, conn, insuraString);
                if (!st.contains("ERROR")) {
                    ria = new INEZA_AFFILIATES(st,insuraString);
           
                    ria.ASSURANCE=insuraString;
                    if (ria.BENEFICIARY_NUMBER != null) {

                        if (ria.AFFILIATION_STATUS.equals("ACTIVATED")) {
                            
                            INEZA_AFFILIATES.setPatientInfo(ria, aliste);
                            setTitle(title + " / Beneficial Number " + aff);
                        } else {
                            JOptionPane.showMessageDialog(null, " BENEFICIAL NOT ACTIVATED " + aff);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, " BENEFICIAL NOT FOUND " + aff);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, " BENEFICIAL NUMBER NOT FOUND " + aff);
                }
            }

        });
    }

    
    private void message() {
        MESSAGE.addActionListener((ActionEvent ae) -> {
         new Main_Clinic( conn,  umukozi);
              });
    }

}
