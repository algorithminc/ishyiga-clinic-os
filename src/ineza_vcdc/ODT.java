/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

import static ecommerce.EETransaction.getTagValue;
import java.sql.Statement;
import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author ishyiga
 */
public class ODT {//Online Data Transmission object
    
String  ODT_CODE,COUNCIL_NUMBER,HSP_CODE,MRC,BENEFICIARY,INSURANCE; 
String  TIME,GLOBAL_ACCOUNT,SIGNATURE,SYNCED_RESPONSE,PUSHED_XML;
//int NUM;
LinkedList <ODT_LIST> laListe;
INEZA_AFFILIATES  ria; 

    public ODT(String ODT_CODE, String COUNCIL_NUMBER, 
            String BENEFICIAL, String SYNCED_RESPONSE,String MRC, String TIME,String PUSHED_XML) {
        this.ODT_CODE = ODT_CODE;
        this.COUNCIL_NUMBER = COUNCIL_NUMBER; 
        this.BENEFICIARY = BENEFICIAL;
        this.MRC = MRC;
        this.TIME = TIME;
        this.SYNCED_RESPONSE=SYNCED_RESPONSE; 
        this.PUSHED_XML=PUSHED_XML;
        
        this.laListe = new LinkedList<>();
    }

    public ODT(String ODT_CODE, String COUNCIL_NUMBER, INEZA_AFFILIATES ria,
            String HSP_CODE, String MRC, String TIME,String GLOBAL_ACCOUNT) {
        this.ODT_CODE = ODT_CODE;
        this.COUNCIL_NUMBER = COUNCIL_NUMBER; 
        this.HSP_CODE = HSP_CODE;
        this.MRC = MRC;
        this.TIME = TIME;
        this.GLOBAL_ACCOUNT=GLOBAL_ACCOUNT;
        this.ria = ria;
        this.laListe = new LinkedList<>();
    }
    
       public ODT(String xml,String MRC,Statement stato) {
           
           try{
        this.ODT_CODE = getTagValue("TRANSACTION_ID", xml);
        this.COUNCIL_NUMBER = getTagValue("USER_INITIATOR", xml);
        this.HSP_CODE = getTagValue("ID_INITIATOR", xml);
        this.MRC = MRC;
        this.TIME = getTagValue("TIME", xml);
        this.GLOBAL_ACCOUNT=getTagValue("INFO_2", xml); // NOT CLEAR
        this.BENEFICIARY=getTagValue("INFO_2", xml);
        this.INSURANCE=getTagValue("CLIENT_GROUP", xml); 
        this.SIGNATURE=getTagValue("INFO_1", xml);
        
           System.out.println(" "+xml);
        
        this.laListe = new LinkedList<>();
        
           String ITEMSLINE = getTagValue("ITEMSLINE", xml); 
        System.out.println("ITEMSLINE   " + ITEMSLINE);
        String[] spList = ITEMSLINE.split("</LIN>"); 
        
        for (int i = 0; i < spList.length; i++) {
            
            String ss = spList[i]; 
       
            String CODE_ISHYIGA = getTagValue("CODE_ISHYIGA", ss);
            String ITEM_NAME = CODE_ISHYIGA;
            RECEIVE_INEZA_ITEM item  = ODT_INTERFACE.getItem(CODE_ISHYIGA,stato);
           
            if(item!=null)
            {ITEM_NAME = item.ITEM_NAME;} // db.getProd(); }
           
            String ROUTE = getTagValue("ROUTE", ss);
            String PRICE = getTagValue("PRICE", ss); 
            System.out.println("PRICE "+PRICE);
            double p = Double.parseDouble(PRICE);
            String QUANTITY_REQUESTED =getTagValue("QUANTITY", ss);
            System.out.println("QUANTITY "+QUANTITY_REQUESTED);
            int qty = Integer.parseInt(QUANTITY_REQUESTED);
            String TAKE_FREQUENCE = getTagValue("INSHURO", ss);
            System.out.println("TAKE_FREQUENCE "+TAKE_FREQUENCE);
            int fq = Integer.parseInt(TAKE_FREQUENCE);
            String MASAHA = getTagValue("MASAHA", ss);
            System.out.println("MASAHA "+MASAHA);
            int ms = Integer.parseInt(MASAHA);
              String DUREE = getTagValue("DUREE", ss);
              System.out.println("DUREE "+DUREE);
            int d = Integer.parseInt(DUREE);
            
            String UPIMIRE = getTagValue("UPIMIRE", ss);
            String REFILL = getTagValue("REFILL", ss);
            String NOTE = getTagValue("NOTE", ss);
            String ICD10 = getTagValue("ICD10", ss);
    ODT_LIST li = new ODT_LIST(CODE_ISHYIGA,ITEM_NAME, NOTE,
                     "", ROUTE,qty, fq,ms, p, UPIMIRE, ""+d, REFILL, ICD10);
        laListe.add(li);
        }
           }
           catch(NumberFormatException ex)
           {
               System.err.println(" izihe "+ex);
               JOptionPane.showMessageDialog(null, "WRONG DATA FORMAT", "PRINTING", 
                       JOptionPane.ERROR_MESSAGE);
           }
    }
    
  
    String qr()   
 {
 return SIGNATURE+";;"+ODT_CODE+";;"+COUNCIL_NUMBER+";;"+BENEFICIARY
         +";;"+HSP_CODE+";;"+MRC+";;"
         +TIME+";;"  ;
 } 

}
