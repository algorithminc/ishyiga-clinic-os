/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;
 
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class ODT_INTERFACE {

       
    
    public static LinkedList<RECEIVE_INEZA_ITEM> getItems(Statement stat) {

        LinkedList<RECEIVE_INEZA_ITEM> listlotItem = new LinkedList<>();
        try {
            String sql = "SELECT  ITEM_INN_RWA_CODE,ITEM_NAME,ITEM_RHIA_NAME,"
                    + " INN,ATC_CODE,MANUFACTURER,"
                    + " SELLING_PRICE FROM VCDC.RECEIVE_INEZA_DRUGS ORDER BY ITEM_NAME";
            System.out.println(sql);
            ResultSet outResult = stat.executeQuery(sql);

            while (outResult.next()) {
                listlotItem.add(new RECEIVE_INEZA_ITEM(
                        outResult.getString("ITEM_INN_RWA_CODE"), outResult.getString("ITEM_NAME"),
                        outResult.getString("ITEM_RHIA_NAME"),
                        outResult.getString("INN"),
                        outResult.getString("ATC_CODE"),
                        outResult.getString("MANUFACTURER"), outResult.getDouble("SELLING_PRICE")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e + "formule");
        }
        return listlotItem;
    }

    public static RECEIVE_INEZA_ITEM getItem(String ITEM_INN_RWA_CODE, Statement stato) {

        try {
            String sql = "SELECT  ITEM_INN_RWA_CODE,ITEM_NAME,ITEM_RHIA_NAME,"
                    + " INN,ATC_CODE,MANUFACTURER,"
                    + " SELLING_PRICE FROM VCDC.RECEIVE_INEZA_DRUGS WHERE"
                    + " ITEM_INN_RWA_CODE='" + ITEM_INN_RWA_CODE + "'";
            System.out.println(sql);
            ResultSet outResult = stato.executeQuery(sql);

            while (outResult.next()) {
                return (new RECEIVE_INEZA_ITEM(
                        outResult.getString("ITEM_INN_RWA_CODE"), outResult.getString("ITEM_NAME"),
                        outResult.getString("ITEM_RHIA_NAME"),
                        outResult.getString("INN"),
                        outResult.getString("ATC_CODE"),
                        outResult.getString("MANUFACTURER"),
                        outResult.getDouble("SELLING_PRICE")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e + "formule");
        }
        return null;
    }

    public static LinkedList<ODT_ICD10> getIndwara(String where, Statement stat) {

        String[] sp = where.split(" ");

        String query;
        if (sp.length > 1) {
            query = "( NAME_LONG like '%" + sp[0] + "%' AND NAME_LONG like '%" + sp[1] + "%')";
        } else {
            query = " UPPER(NAME_LONG) like upper ('%" + sp[0] + "%')";
        }

        LinkedList<ODT_ICD10> listlotItem = new LinkedList<>();
        try {
            String sql = "SELECT ID_ICD10,CODE_ICD10,NAME_SHORT,NAME_LONG,CLASS_ICD10"
                    + " FROM VCDC.RECEIVE_INEZA_ICD10 WHERE " + query + " ORDER BY NAME_SHORT";
            System.out.println(sql);
            ResultSet outResult = stat.executeQuery(sql);

            while (outResult.next()) {
                listlotItem.add(new ODT_ICD10(
                        outResult.getString("ID_ICD10"),
                        outResult.getString("CODE_ICD10"),
                        outResult.getString("NAME_SHORT"),
                        outResult.getString("NAME_LONG"),
                        outResult.getString("CLASS_ICD10")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e + "formule");
        }

        return listlotItem;
    }

   public static String getParam(String var, String famille,Statement stat) {

        String par = "";

        try {

            ResultSet outResult = stat.executeQuery("select  * from APP.VARIABLES where"
                    + " APP.variables.famille_variable= '" + famille + "' and nom_variable='" + var + "' ");

            while (outResult.next()) {

                par = outResult.getString(5);

            }
            outResult.close();
        } catch (SQLException e) {
            System.err.println(e + " select  * from APP.VARIABLES where"
                    + " APP.variables.famille_variable= '" + famille + "' and nom_variable='" + var + "' ");

        }
        return par;
    }
}
