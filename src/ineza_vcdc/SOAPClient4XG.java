/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

/**
 *
 * @author ishyiga
 */
/** 
 * SOAPClient4XG. Read the SOAP envelope file passed as the second
 * parameter, pass it to the SOAP endpoint passed as the first parameter, and
 * print out the SOAP envelope passed as a response.  with help from Michael
 * Brennan 03/09/01
 * 
 *
 * @author  Bob DuCharme
 * @version 1.1
 * @param   SOAPUrl      URL of SOAP Endpoint to send request.
 * @param   xmlFile2Send A file with an XML document of the request.  
 *
 * 5/23/01 revision: SOAPAction added
*/
 
import java.io.*;
import java.net.*; 
 
public class SOAPClient4XG {
    
    
    
public static INEZA_AFFILIATES sendRSSB2(String AFFILIATEnUMBER) throws MalformedURLException, IOException {
   
    
    String [] sp = AFFILIATEnUMBER.split("AAAAA");//type+"??"+nAff
    String categ="0";
    if(sp[0].equals("MAIN_AFFILIATE"))
    {categ ="1";}
    String xmlInput="<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:affiliateServer\">\n" +
"   <soapenv:Header/>\n" +
"   <soapenv:Body>\n" +
"      <urn:findAffiliate soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n" +
"         <userCode xsi:type=\"xsd:string\">ishyigac</userCode>\n" +
"         <pinNo xsi:type=\"xsd:string\">rS#cK204Q</pinNo>\n" +
" \n" +
"         <affiliateNo xsi:type=\"xsd:string\">"+sp[1]+"</affiliateNo> \n" +
"         <affiliateCateg xsi:type=\"xsd:string\">"+categ+"</affiliateCateg>\n" +
"      </urn:findAffiliate>\n" +
"   </soapenv:Body>\n" +
"</soapenv:Envelope>";
    
 //   System.err.println(xmlInput);
    
             
 String wsURL ="http://197.243.10.69/pharmacy_api/service.php"; 
//Code to make a webservice HTTP request
String responseString  ;
String outputString = "";
//String wsURL = "<Endpoint of the webservice to be consumed>";
URL url = new URL(wsURL);
URLConnection connection = url.openConnection();
HttpURLConnection httpConn = (HttpURLConnection)connection;
ByteArrayOutputStream bout = new ByteArrayOutputStream(); 
 
byte[] buffer  = xmlInput.getBytes();
bout.write(buffer);
byte[] b = bout.toByteArray();
String SOAPAction = "petition";
// Set the appropriate HTTP parameters.
httpConn.setRequestProperty("Content-Length",
String.valueOf(b.length));
httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
httpConn.setRequestProperty("SOAPAction", SOAPAction);
httpConn.setRequestMethod("POST");
httpConn.setDoOutput(true);
httpConn.setDoInput(true);
//Write the content of the request to the outputstream of the HTTP Connection.
    try (OutputStream out = httpConn.getOutputStream()) {
//Write the content of the request to the outputstream of the HTTP Connection.
out.write(b);
//Ready with sending the request.
    }

//Read the response.
InputStreamReader isr  ;
if (httpConn.getResponseCode() == 200) {
  isr = new InputStreamReader(httpConn.getInputStream());


BufferedReader in = new BufferedReader(isr);

//Write the SOAP message response to a String.
while ((responseString = in.readLine()) != null) {
outputString = outputString + responseString;
}
  

INEZA_AFFILIATES aff = new INEZA_AFFILIATES(outputString,true);
 System.out.println("insurancepull.SOAPClient4XG.sendRSSB2() \n "+aff);

    return aff;
    } else {
  return null;
}
}
 
 
}