/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.ByteMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.awt.*;
import java.util.Properties;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author Kimenyi
 */
public class Print_QR extends JFrame {

    private final Properties properties = new Properties();
    ODT prescription; String NAMES_ACCOUNT;
    int pageNum, margin = 0;
    boolean byempurimwe = false;
    int pageW = 0;
    Image rraLogo;
    Graphics pg;
    PrintJob pjob;
    int negatif = 1;
    boolean duplicate;

    public Print_QR(ODT lotList,String NAMES_ACCOUNT) {
        this.prescription = lotList;
        this.NAMES_ACCOUNT=NAMES_ACCOUNT;
        this.byempurimwe = PrintCommand();
    }

    private boolean PrintCommand() {
        Toolkit TK = getToolkit();
        System.out.println(" getToolkit() " + TK);
        pjob = TK.getPrintJob(this, "ISHYIGA ODT " + prescription.ODT_CODE, properties);

        if (pjob != null) {
            pg = pjob.getGraphics();
            pageW = pjob.getPageDimension().width - margin;
            System.out.println("pageW.TIN " + pageW);
            if (pg != null) {
                hagati();
                pg.dispose();
            }
            pjob.end();
            return true;
        } else {
            return false;
        }
    }

    void printQR(Graphics pg, String data, int x, int y) throws WriterException {
        int dim = 100;
        QRCodeWriter qrCode = new QRCodeWriter();
        ByteMatrix bitMatrix = qrCode.encode(data, BarcodeFormat.QR_CODE, dim, dim);

        try {
            pg.drawImage(MatrixToImageWriter.toBufferedImage(bitMatrix), x, y, dim, dim, rootPane);
        } catch (Exception ex) {
            Logger.getLogger("" + ex);
        }
    }

    private void hagati() {

        Font enteteFont = new Font("Helvetica", Font.BOLD, 8);
        Font dataFont = new Font("Helvetica", Font.PLAIN, 8);
        pg.setFont(dataFont);
        FontMetrics fm = pg.getFontMetrics(dataFont);

        int w = 20;
        int curHeightLigne = 10;
        int y = 10;
        int vv = 10;
        int saut = 10;
        int west = 90 ;
        
        Font helv = new Font("Helvetica", Font.BOLD, 12);
        pg.setFont(helv);
        pg.drawString(" ONLINE DATA TRANSMITION ", w, curHeightLigne + vv);
        vv += saut;

        helv = new Font("Helvetica", Font.BOLD, 10);
        pg.setFont(helv);

        pg.drawString(" ODT CODE ", w, curHeightLigne + vv);
        pg.drawString(": " +prescription.ODT_CODE, w+west, curHeightLigne + vv);
        vv += saut;
        
        pg.drawString(" PRESCRIBER ", w, curHeightLigne + vv);
        pg.drawString(": " +NAMES_ACCOUNT,  w+west, curHeightLigne + vv);
        vv += saut;
        pg.drawString(" HEALTY FACILITY "  , w, curHeightLigne + vv);
        pg.drawString(": " + prescription.HSP_CODE, w+west, curHeightLigne + vv);
        vv += saut; 
        pg.drawString(" PATIENT ID ", w, curHeightLigne + vv);
        pg.drawString(": " +prescription.ria.BENEFICIARY_FIRST_NAME+" "+prescription.ria.BENEFICIARY_LAST_NAME, w+west, curHeightLigne + vv);
        vv += saut;
        pg.drawString(" INSURANCE ", w, curHeightLigne + vv);
        pg.drawString(": " +prescription.INSURANCE, w+west, curHeightLigne + vv);
        vv += saut;
        pg.drawString(" MRC " , w, curHeightLigne + vv);
         pg.drawString(": " +  prescription.MRC , w+west, curHeightLigne + vv);
        vv += saut;
        pg.drawString(" TIME " , w, curHeightLigne + vv);
         pg.drawString(": " +prescription.TIME , w+west, curHeightLigne + vv);
        vv += saut;
 
        pg.drawString(" SIGNATURE " , w, curHeightLigne + vv);
         pg.drawString(": " +set4Virgule1(prescription.SIGNATURE) , w+west, curHeightLigne + vv);
        vv += saut;
        
        pg.drawString(" ---------------------------- ", w, curHeightLigne + vv);
        vv += saut;
helv = new Font("Helvetica", Font.PLAIN, 8);
        pg.setFont(helv);
        for (int j = 0; j < prescription.laListe.size(); j++) {

            ODT_LIST ci = prescription.laListe.get(j);

            pg.drawString("DRUG CODE  : " + ci.INN_RW_CODE_REQUESTED, w, curHeightLigne + vv);
            vv += saut;
            int leg = ci.ITEM_NAME.length();
            int kata = kataNeza(ci.ITEM_NAME, 50);

            if (leg > 50) {
                pg.drawString("DRUG NAME  : " +ci.ITEM_NAME.substring(0, kata), w, curHeightLigne + vv);
                vv += saut;
                pg.drawString(ci.ITEM_NAME.substring(kata, leg), w, curHeightLigne + vv);
                vv += saut;
            } else {
                pg.drawString("DRUG NAME  : " +ci.ITEM_NAME, w, curHeightLigne + vv);
                vv += saut;
            }

      
            pg.drawString("DOSAGE : " + ci.TAKE_FREQUENCE + " -  EVERY : " + ci.FREQUENCE_HOURY+" HOURS", w, curHeightLigne + vv);
            vv += saut;
            pg.drawString("USE MEASUREMENT AS  : " + ci.MESURE + " - TO BE TAKEN IN : " + ci.DUREE+" DAYS", w, curHeightLigne + vv);
            vv += saut;
            pg.drawString("TOTAL QUANTITY " + ci.QUANTITY_REQUESTED +"   REFILL : "+ci.REFILL, w, curHeightLigne + vv);
            vv += saut;
            pg.drawString("INSTRUCTION NOTES: " + ci.NOTE, w, curHeightLigne + vv);
            vv += saut;
            pg.drawString(" ---------------------------- ", w, curHeightLigne + vv);
            vv += saut;
        } // FIN GUSHUSHANYA AMADATA 

        try {
            printQR(pg, prescription.qr(), w + 50, curHeightLigne  + vv);
        }
        catch (WriterException e) {
            System.out.println(" WriterException  " + e);
        }

    }

    public static int kataNeza(String m, int l) {
        String[] espaces = m.split(" ");
        int s = espaces.length;
        int index = 0;
        for (int i = 0; i < s - 1; i++) {
            espaces[i] = espaces[i] + " ";
        }

        for (int i = 0; i < s; i++) {
            index += espaces[i].length();
            if (index > l) {
                index -= espaces[i].length();
                break;
            }
        }
        return index;
    }
    

    public static String set4Virgule1(String frw) {
        String setString = "";
        if(frw!=null)
        {int k = 0;
        for (int i = 0; i <= (frw.length() - 1); i++) {
            k++;
            if ((k - 1) % 4 == 0 && (k - 1) != 0) {
                setString += "-";
            }
            setString += frw.charAt(i);
        }}
        return (setString);
    }

    static String inverse(String frw) {
        String l = "";
        for (int i = (frw.length() - 1); i >= 0; i--) {
            l += frw.charAt(i);
        }
        return l;
    }

}
