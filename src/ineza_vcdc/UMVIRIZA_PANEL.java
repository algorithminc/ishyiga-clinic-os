/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

import static eaglEye.Eye.textAreaEye;
import java.awt.Container;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;

/**
 *
 * @author kimai
 */
 
public class UMVIRIZA_PANEL  {

   private int PORT_VSDC = 9999; 
   private Connection conn ;
   String TIN;
    
    public UMVIRIZA_PANEL(Connection conn,String TIN) {
 
       this.TIN=TIN;
       this.conn=conn;
        draw();
       // initate ();
       sunika();
    }
//    JTextArea textAreaEye;   
    private void initate ()
    {
           
                try { 
                    textAreaEye.append(" \n SERVER STARTED  on "+PORT_VSDC);
            ServerSocket socketServeur = new ServerSocket(PORT_VSDC);
               textAreaEye.append(" \n SERVER STARTED "); 

            while (true) {
                Socket socketClient = socketServeur.accept();
                  textAreaEye.append(" \n WAITING ");
                UMVIRIZA t = new UMVIRIZA(socketClient, conn,TIN);
                textAreaEye.append(" \n START ");
                t.start();
            }
        } catch (IOException e) {
            textAreaEye.append("SERVER SOCKET " + e);
        }
    }
     private void draw() {  
         textAreaEye.append(" \n STARTING LISTERNER "); 

    }

        private void sunika() {
        final SwingWorker<String, String> worker = new SwingWorker<String, String>() {
            @Override
            protected String doInBackground() {
              initate ();
              return "";
            }

            @Override
            protected void done() {
                textAreaEye.append("  \n Result is...");
            }

            @Override
            protected void process(java.util.List<String> chunks) {
                String latestResult = chunks.get(chunks.size() - 1);
                textAreaEye.append("  \n Result is..." + latestResult);
            }
        };
        worker.execute();  // ( the worker thread 
        textAreaEye.append("  Running...");
        worker.addPropertyChangeListener((PropertyChangeEvent evt) -> {
            if (evt.getPropertyName().equals("progress")) {
            }
        });
    }


}
