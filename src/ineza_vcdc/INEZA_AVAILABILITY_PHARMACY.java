/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

import java.util.LinkedList;

/**
 *
 * @author ishyiga
 */
public class INEZA_AVAILABILITY_PHARMACY implements Comparable{
    
String ID_CLIENT,NAMES,PHONE,FIELD,LOCATION,DISTRICT,ID_COMPANY_BRANCH,
        STATUS,STOCK_STATUS,WORKING_HOURS,WORKING_DAYS;
double TOTAL_MOUNT;
int DISCOUNT_AMOUNT,FOUND_ITEM;

LinkedList <INEZA_AVAILABILITY> listItem;

    public INEZA_AVAILABILITY_PHARMACY(String ID_CLIENT, String NAMES, String PHONE, String FIELD,
            String LOCATION, String DISTRICT, String ID_COMPANY_BRANCH, String STATUS, 
            String STOCK_STATUS, String WORKING_HOURS, String WORKING_DAYS ) {
        this.ID_CLIENT = ID_CLIENT;
        this.NAMES = NAMES;
        this.PHONE = PHONE;
        this.FIELD = FIELD;
        this.LOCATION = LOCATION;
        this.DISTRICT = DISTRICT;
        this.ID_COMPANY_BRANCH = ID_COMPANY_BRANCH;
        this.STATUS = STATUS;
        this.STOCK_STATUS = STOCK_STATUS;
        this.WORKING_HOURS = WORKING_HOURS;
        this.WORKING_DAYS = WORKING_DAYS; 
        
        this.listItem=new LinkedList<>();
        
    }
 
    boolean addItem(INEZA_AVAILABILITY item)
    {
    
    if(item.ID_CLIENT.equals(ID_CLIENT) && item.NAMES.equals(NAMES))
    {
        double DISC = (item.PRICE*item.QUANTITE)*item.DISCOUNT/100;
        TOTAL_MOUNT +=(item.PRICE*item.QUANTITE)-DISC;
        FOUND_ITEM++;
        DISCOUNT_AMOUNT += DISC;
        listItem.add(item);
        return true;
    }
    return false;
    }

@Override
    public int compareTo(Object oo)
    { 
        INEZA_AVAILABILITY_PHARMACY ineza = (INEZA_AVAILABILITY_PHARMACY)oo;
        double gauche = (ineza.FOUND_ITEM * 1000000)-ineza.TOTAL_MOUNT;
        double droite = (FOUND_ITEM * 1000000)      - TOTAL_MOUNT;
        if(gauche==droite)
        {return 1;}
        return (int) (gauche-droite);
    }
     
    
@Override
    public String toString ()
    { 
        return  NAMES +" "+ TOTAL_MOUNT +" "+DISCOUNT_AMOUNT+" "+FOUND_ITEM;  
    }
}
