/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ineza_vcdc;

/**
 *
 * @author AlgorithmDev
 */
public class RECEIVE_INEZA_PRESCRIBERS {
    
    String PRESCRIBER_NAME,PRESCRIBER_TYPE,COUNCIL_NUMBER,QUALIFICATION,PHONE_NUMBER,EMAIL,LAST_UPDATE;

    public RECEIVE_INEZA_PRESCRIBERS(String PRESCRIBER_NAME, String PRESCRIBER_TYPE, String COUNCIL_NUMBER, 
                                String QUALIFICATION, String PHONE_NUMBER, String EMAIL,String LAST_UPDATE) {
        
        this.PRESCRIBER_NAME = PRESCRIBER_NAME;
        this.PRESCRIBER_TYPE = PRESCRIBER_TYPE;
        this.COUNCIL_NUMBER = COUNCIL_NUMBER;
        this.QUALIFICATION = QUALIFICATION;
        this.PHONE_NUMBER = PHONE_NUMBER;
        this.EMAIL = EMAIL;
        this.LAST_UPDATE=LAST_UPDATE;
    }
      public RECEIVE_INEZA_PRESCRIBERS(String xml) {
           try
           {
        this.PRESCRIBER_NAME = HTTP_URL.Igicu.getTagValue("PRESCRIBER_NAME", xml);
        this.PRESCRIBER_TYPE=HTTP_URL.Igicu.getTagValue("PRESCRIBER_TYPE", xml);
        this.COUNCIL_NUMBER = HTTP_URL.Igicu.getTagValue("COUNCIL_NUMBER", xml);
        this.QUALIFICATION = HTTP_URL.Igicu.getTagValue("QUALIFICATION", xml);
        this.PHONE_NUMBER = HTTP_URL.Igicu.getTagValue("PHONE_NUMBER", xml);
        this.EMAIL = HTTP_URL.Igicu.getTagValue("EMAIL", xml); 
        this.LAST_UPDATE = HTTP_URL.Igicu.getTagValue("LAST_UPDATE", xml); 
     
        
           }
           catch (Throwable d)
           {
           this.COUNCIL_NUMBER=null;
           }
    }
    
    
}
