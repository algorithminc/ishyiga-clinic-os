/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic;

import static eaglEye.dbEye.credentials;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author kimai
 */
public class DbHandlerClinic {
 
     
    public static LinkedList<Clinic_interface> getInterface(Statement s, String andsql) {
        
        LinkedList<Clinic_interface> liste = new LinkedList<>();
        
        try {
            String sql = "select  * from VCDC.CLINIC_INTERFACE "+andsql+" ";
            System.out.println("ineza_vcdc.DbhandlerVCDC.getInterface() \n "+sql);
            ResultSet outResult = s.executeQuery(sql);
 
            while (outResult.next()) {                
                
             liste.add(new Clinic_interface(outResult.getInt("ID_BUTTON"), 
                    outResult.getInt("ID_SORT"), outResult.getInt("PARENT_ID_BUTTON"), 
                     outResult.getString("NAME_BUTTON"), outResult.getString("NAME_FR"), 
                     outResult.getString("NAME_EN"), outResult.getString("NAME_KI"), 
                     outResult.getString("IMAGE_PATH_BUTTON"), 
                     outResult.getString("FUNCTION_NAME_BUTTON"),
             outResult.getInt("RED"),
             outResult.getInt("GREEN"),
             outResult.getInt("BLUE"),
             outResult.getInt("LEVEL_BUTTON"))) ;   
             
            }
        } catch (SQLException e) {
            System.err.println("DbhandlerVCDC : " + e);
        }
        return liste;
    }

public static Clinic_interface getNameGroupe(Statement s, String where) {
         try {
            String sql = "select  * from VCDC.CLINIC_INTERFACE  where "+where;
            // System.out.println(" "+sql);
            ResultSet outResult = s.executeQuery(sql);
 
            while (outResult.next()) {
                
                return  new Clinic_interface(outResult.getInt("ID_BUTTON"), 
                    outResult.getInt("ID_SORT"), outResult.getInt("PARENT_ID_BUTTON"), 
                     outResult.getString("NAME_BUTTON"), outResult.getString("NAME_FR"), 
                     outResult.getString("NAME_EN"), outResult.getString("NAME_KI"), 
                     outResult.getString("IMAGE_PATH_BUTTON"), 
                     outResult.getString("FUNCTION_NAME_BUTTON"),
             outResult.getInt("RED"),
             outResult.getInt("GREEN"),
             outResult.getInt("BLUE"),
             outResult.getInt("LEVEL_BUTTON")) ;             
             
            }
        } catch (SQLException e) {
            System.err.println("DbhandlerVCDC : " + e);
        }
        return null;
    }

public static int getSizeGroup(Statement s, int PARENT_ID_BUTTON) {
         try {
            String sql = "select  count(PARENT_ID_BUTTON) from VCDC.CLINIC_INTERFACE  "
                    + "where  PARENT_ID_BUTTON="+PARENT_ID_BUTTON; 
            ResultSet outResult = s.executeQuery(sql); 
            while (outResult.next()) { 
                return  
             outResult.getInt(1) ;  
            }
        } catch (SQLException e) {
            System.err.println("DbhandlerVCDC : " + e);
        }
        return 4;
    }

public static LesDynamic getDynamics(ConnectorDyn connDyn) {
    
    System.out.println("ngeze 18 ");
       int i=0; 
    String data [][] = new String[connDyn.FETCH+1][connDyn.ins.header.length];
    String [] headerLangue= new String [connDyn.ins.header.length]; 
            for(int l=0;l<connDyn.ins.header.length;l++)
            {  headerLangue[l]= lala(Main_Clinic.lang, connDyn.ins.header[l], connDyn.s, 
                    connDyn.pie, connDyn.pil) ; }
            data[0]=headerLangue; i++;
   // System.out.println("clinic.DbHandlerClinic.getDynamics() "+headerLangue.toString());
    
        try {      
            
            String sql = "select  * from "+connDyn.ins.tableName+" "+connDyn.andsql
                    +" FETCH FIRST "+connDyn.FETCH+" ROWS ONLY "; 
          
         //   System.out.println("sqlsql "+sql);  
         
         System.out.println("ngeze 18.1 ");
            
            ResultSet outResult = connDyn.s.executeQuery(sql); 
                         
            while (outResult.next()) { 
                
                String coming="NA";
               for (int j=0; j< connDyn.ins.variables.length;j++)
                { 
                switch (connDyn.ins.variables[j]) {
                    case "STRING":
                        coming=outResult.getString(connDyn.ins.header[j]);
                        break;
                    case "DOUBLE":
                        coming= ""+outResult.getDouble(connDyn.ins.header[j]); 
                        break;
                    case "INT":
                        coming=""+outResult.getInt(connDyn.ins.header[j]);
                        break;
                    default:
                        break;
                }
                data[i][j] = coming; 
                   // System.out.println(" "+coming);
            }
             i++;
            }
            System.out.println("ngeze 18.2 ");
        } catch (SQLException e) {
            System.err.println("get Les Dyn : " + e);
        } 
        
         String data1 [][] = new String[i][connDyn.ins.header.length];
        
        System.arraycopy(data,0,data1,0,i);
        connDyn.ins.data = data1;
        connDyn.ins.headerLangue=headerLangue;
        System.out.println("ngeze 19 ");
        return connDyn.ins;
    }


    static public boolean insertDynamic(LesDynamic dyn,
            Connection conn) {
        try {
            
            String header ="(";
            String value ="values (";
            
            for(int i=0;i<dyn.header.length;i++)
            { 
                if(dyn.izemeweInsert[i])
                {header +=dyn.header[i]+",";
                value +="?,";}
            }
            
            header +="#"; 
            header=header.replaceAll(",#", ") ");
            value +="#";
            value=value.replaceAll(",#", ") "); 
            
            
String sql="insert into "+dyn.tableName
        + header
        + value;
            System.out.println(sql+"   "+dyn.data.length);

            PreparedStatement psInsert = conn.prepareStatement(sql); 
 System.out.println(sql);
                 for (int i=0; i< dyn.data.length;i++)
                { 
                    
                    int ndemewe =1;
                 for (int j=0; j< dyn.variables.length;j++)
                { 
                    
                    System.out.println(ndemewe+"  "+dyn.izemeweInsert[j]);
                  if(dyn.izemeweInsert[j])  
                  {
                      System.out.println(ndemewe+" "+dyn.data[i][j]);
                      switch (dyn.variables[j]) {
                    case "STRING":
                        psInsert.setString(ndemewe,dyn.data[i][j]);
                        break;
                    case "DOUBLE":
                        psInsert.setDouble(ndemewe,Double.parseDouble(dyn.data[i][j])); 
                        break;
                    case "INT":
                        psInsert.setInt(ndemewe,Integer.parseInt(dyn.data[i][j]));
                        break;
                    default:
                        break;
                } 
                       ndemewe++;
                }
                 
                }
                 psInsert.executeUpdate();
            }
            System.out.println("Dynamic insertion");
            return true;
        } catch (SQLException ex) {
            System.err.println(" " + ex);
            JOptionPane.showMessageDialog(null, "inseert! \n " + ex);
            return false;
        }
    }

    
    static public boolean updateDynamic(LesDynamic dyn,
            Statement state,int selectedToUpdate) {
        try { 
            String sqlToUpdate ="UPDATE "+dyn.tableName+" set "; 
            String updateIndex="";
            for(int i=0;i<dyn.header.length;i++)
            { 
                if(dyn.izemeweUpdate[i])
                {
                    sqlToUpdate +=dyn.header[i]+" = ";
                 switch (dyn.variables[i]) {
                    case "STRING":
                        sqlToUpdate += "'" +dyn.getComponentData(i)+"'";
                        break;
                    case "DOUBLE":
                        sqlToUpdate += "" +dyn.getComponentData(i)+""; 
                        break;
                    case "INT":
                        sqlToUpdate += "" +dyn.getComponentData(i)+"";
                        break;
                    default:
                        break;
                }
                
                 sqlToUpdate +=",";
                
                }
                
                if(i==dyn.index_update)
                {
                 switch (dyn.variables[i]) {
                    case "STRING":
                        updateIndex += " where "+dyn.header[dyn.index_update]+"='" 
                                + dyn.data[selectedToUpdate][i] + "'" ;
               
                        break;
                    case "DOUBLE":
                        updateIndex += " where "+dyn.header[dyn.index_update]+"=" 
                                + dyn.data[selectedToUpdate][i]  ;
                 break;
                    case "INT":
                        updateIndex += " where "+dyn.header[dyn.index_update]+"=" 
                                + dyn.data[selectedToUpdate][i] ;
               
                        break;
                    default:
                        break;
                }
                }
            } 
            
            sqlToUpdate +=updateIndex;
            
            System.out.println("Dynamic update "+sqlToUpdate);
            sqlToUpdate =sqlToUpdate.replaceAll(", where ", " where ");
            
            System.out.println("Dynamic update "+sqlToUpdate);
            
            
            state.execute(sqlToUpdate);
            
            
            return true;
        } catch (SQLException ex) {
            System.err.println(" " + ex);
            JOptionPane.showMessageDialog(null, "UPDATE! \n " + ex);
            return false;
        }
    }

    
    
    
 public final static DateFormat DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

 public static String getNow()
 {
 
 Date toDay = new Date();
return  DATEFORMAT.format(toDay);
 
 }
 public static String lala(String lang, String var,
        Statement statLangue,PreparedStatement pie,PreparedStatement pil) {
        String value = "";
        try {
            String sql= "  select  * from VCDC.LANGUE where variable= '" + var + "'";
         //   System.out.println(lang+" ql sss  "+sql);
            ResultSet outResult = statLangue.executeQuery(sql);
            
            while (outResult.next()) {
                value = outResult.getString(lang); 
            }
        } catch (SQLException e) {
            System.err.println(var+"FFF"+e); 
            insertError(e + "", "lala","DbHandlerClinic","lala",
                    "0","LOW",pie);
        }
         // System.out.println("clinic.DbHandlerClinic.getDynamics() "+value);
        if (value == null || value.equals("")) {
          insertLang(var,pil,pie);
        }

        return value; 
    }
static boolean insertLang(String MISSING_LANG,PreparedStatement psInsertLangue
,PreparedStatement pie) {
        try { 
  // System.out.println("clinic.psInsertLangue.getDynamics()");
            psInsertLangue.setString(1, MISSING_LANG);
            psInsertLangue.setString(2, MISSING_LANG);
            psInsertLangue.setString(3, MISSING_LANG);
            psInsertLangue.setString(4, MISSING_LANG);
            psInsertLangue.setString(5, MISSING_LANG);

            psInsertLangue.executeUpdate();
            //psInsertLangue.close();
            return true;
        } catch (SQLException ex) {
      //System.out.println("clinic.psInsertLangue.getDynamics() "+ex);
         
            insertError(ex + "", "insertVariable","DbHandlerClinic","insertLangue",
                    "0","LOW",pie);
            return false;
        }
    }

 public static void insertError(String desi, String ERR,String CLASSS, String METHOD,
         String FLAG, String STATUS,
         PreparedStatement psInsertERROR) {
        try { 
             
            psInsertERROR.setString(1, desi);
            psInsertERROR.setString(2, ERR);
            psInsertERROR.setString(3, credentials.MAC+":"+Main_Clinic.umukozi.UNIVERSE_ACCOUNT);
            psInsertERROR.setString(4, credentials.server);
            psInsertERROR.setString(5, Main_Clinic.VERSION);
            psInsertERROR.setString(6, CLASSS);
            psInsertERROR.setString(7, METHOD);
            psInsertERROR.setString(8, credentials.DERBY_VERSION);
            psInsertERROR.setString(9, credentials.JAVA_VERSION);
            psInsertERROR.setString(10, FLAG);
            psInsertERROR.setString(11,credentials.tin );
            psInsertERROR.setString(12, STATUS); 
            psInsertERROR.executeUpdate();
            //psInsertERROR.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "  " + ex, " ERROR ", 
                    JOptionPane.PLAIN_MESSAGE);
        }

    }
    
}
