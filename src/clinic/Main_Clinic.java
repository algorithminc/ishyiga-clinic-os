/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic;

import static clinic.DbHandlerClinic.getNameGroupe;
import static clinic.DbHandlerClinic.getSizeGroup;
import eaglEye.Eye;
import eaglEye.dbEye;
import globalaccount.Account;
import ineza_vcdc.INEZA_AFFILIATES;
import static ineza_vcdc.INEZA_AFFILIATES.getPatientInfo;
import static ineza_vcdc.INEZA_AFFILIATES.setPatientInfo;
import ineza_vcdc.Jobber;
import ineza_vcdc.ODT_INTERFACE_MAIN;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.border.TitledBorder;

/**
 *
 * @author kimai
 */
public class Main_Clinic extends JFrame implements ActionListener {

    static Connection conn;
    static Statement statee;
    static Account umukozi;
    static String lang;
    public static final String VERSION = "ISHYIGA CLINIC INTEGRATED 4.0.8";
    static JTextField search = new JTextField("    search Functions ...  ");
    JRadioButton jrb = new JRadioButton("");
    LinkedList<Clinic_interface> liste;
    JList clinicList = new JList<>();
    static PreparedStatement pie;
    static PreparedStatement pil;
    static String[] OUT_IN_PATIENT = new String[]{"OUT", "IN", "DISCHARGED", "DEAD", "TRANSFERED"};
    static String[] ACTIVITY_STATUS = new String[]{"OPEN", "PENDING", "CLOSED"};
    static ConnectorDyn connDyn;
    static final Color LIGHT_BACKGROUND_COLOR = new Color(210, 209, 209);
    static final Color LIGHT_FOREGROUND_COLOR = Color.BLACK;

    static final Color DARK_BACKGROUND_COLOR = new Color(71, 71, 107);
    static final Color DARK_FOREGROUND_COLOR = Color.white;

    public Main_Clinic(Connection conn, Account umukozi) {
        this.conn = conn;
        Main_Clinic.umukozi = umukozi;
        Main_Clinic.lang = umukozi.LANG;
        draw();
        lesFunctionEcoute();
        lesSearchEcoute();
        superSearchFunction();
    }

    static Color getBackGroundColor() {
        if (dbEye.credentials.VIEW_MODE != null
                && dbEye.credentials.VIEW_MODE.equals("LIGHT")) {
            return LIGHT_BACKGROUND_COLOR;
        } else {
            return DARK_BACKGROUND_COLOR;
        }
    }

    static Color getForeGroundColor() {
        if (dbEye.credentials.VIEW_MODE != null
                && dbEye.credentials.VIEW_MODE.equals("LIGHT")) {
            return LIGHT_FOREGROUND_COLOR;
        } else {
            return DARK_FOREGROUND_COLOR;
        }
    }

    private void draw() {
        try {
            Container content = getContentPane();
            content.setBackground(getBackGroundColor());
            String title = VERSION + "/ OM : "
                    + umukozi.AUTO_DEPOT + " / NAME " + umukozi.NAMES_ACCOUNT;
            setTitle(title);
            setSize(1000, 700);
            JPanel lepanMain = new JPanel();
            lepanMain.setPreferredSize(new Dimension(1000, 700));
            lepanMain.setBackground(getBackGroundColor());
            lepanMain.setLayout(new GridLayout(1, 3));

            Statement s = conn.createStatement();
            Statement ss = conn.createStatement();
            Main_Clinic.pie = conn.prepareStatement("insert "
                    + "into VCDC.ERROR(DESI,USERNAME,MAC_ADDRESS,IP_ADDRESS,PACKAGE"
                    + ",CLASS,METHOD,DERBY_VERSION,JAVA_VERSION,FLAG,CLIENT_ID,STATUS) "
                    + " values (?,?,?,?,?,?,?,?,?,?,?,?)");

            Main_Clinic.pil = conn.prepareStatement("insert into VCDC.LANGUE"
                    + "(VARIABLE,KIN,FRA,ANG,SWA) "
                    + " values (?,?,?,?,?)");
            Main_Clinic.statee = conn.createStatement();
            this.liste = DbHandlerClinic.getInterface(s, " order by ID_BUTTON ");
            Main_Clinic.connDyn = new ConnectorDyn(s, pie, pil, "", null, 100);
            JPanel lepanLeft = new JPanel();
            lepanLeft.setBackground(getBackGroundColor());
            lepanLeft.setPreferredSize(new Dimension(300, 700));
            JPanel lepanright = new JPanel();
            lepanright.setBackground(getBackGroundColor());
            lepanright.setPreferredSize(new Dimension(300, 700));
            
            clinicList.setBackground(getBackGroundColor());
            clinicList.setForeground(getForeGroundColor()); 
            clinicList.setListData(liste.toArray());

            JScrollPane rdvScroll = new JScrollPane(clinicList);
            rdvScroll.setPreferredSize(new Dimension(300, 650));

            lepanLeft.add(search);
            lepanLeft.add(RadioButton());
            lepanLeft.add(rdvScroll);
            int LAST = 1;

            LinkedList<JPanel> lespanier = buildPanier(DbHandlerClinic.getInterface(s,
                    "  WHERE LEVEL_BUTTON=1 order by PARENT_ID_BUTTON,ID_SORT"), s, ss, LAST);
            int sizeGroupe = lespanier.size();
            JPanel lepan = new JPanel();
            lepan.setBackground(getBackGroundColor());
            lepan.setLayout(new GridLayout(sizeGroupe, 1));
            lespanier.forEach(jp -> {
                lepan.add(jp);
            });

            
            LinkedList<JPanel> lespanierRIGTH = buildPanier(DbHandlerClinic.getInterface(s,
                    "  WHERE LEVEL_BUTTON=11 order by PARENT_ID_BUTTON,ID_SORT"), s, ss, LAST);
            int sizeGroupeR = lespanierRIGTH.size(); 
            lepanright.setBackground(getBackGroundColor());
            lepanright.setLayout(new GridLayout(sizeGroupeR, 1));
            lespanierRIGTH.forEach(jp -> {
                lepanright.add(jp);
            });

            
            lepanMain.add(lepanLeft);
            lepanMain.add(lepan);
            lepanMain.add(lepanright);
            
            content.add(lepanMain);
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            setVisible(true);
            
        } catch (SQLException ex) {
            Logger.getLogger(Main_Clinic.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    static LinkedList<JPanel> buildPanier(LinkedList<Clinic_interface> liste,
            Statement s, Statement ss, int LAST) {
        LinkedList<JPanel> lespanier = new LinkedList<>();
        try {

            LinkedList<JButton> aliste = new LinkedList<>();

            System.out.println(" liste size " + liste.size());

            for (int i = 0; i < liste.size(); i++) {

                Clinic_interface cl = liste.get(i);
                Icon icon = new ImageIcon((cl.IMAGE_PATH_BUTTON));
                JButton jb = new JButton(icon);
                int SIZEgROUPE = getSizeGroup(ss, cl.PARENT_ID_BUTTON);
                jb.setName(cl.FUNCTION_NAME_BUTTON);
                //System.out.println("<html> " + cl.img(SIZEgROUPE) + "  </html>");
                jb.setText("<html> " + cl.img(SIZEgROUPE) + "  </html>");
                jb.setBackground(new Color(cl.R, cl.G, cl.B));
                jb.addActionListener((ActionEvent ae) -> {
                    String name = ((JButton) ae.getSource()).getName();
                    Main_Clinic.performClinicCall(name);
                });

                //  System.out.println(cl + " CLLL " + LAST);
                if (cl.PARENT_ID_BUTTON != LAST && !aliste.isEmpty()) {
                    lespanier.add(makeZone("" + getNameGroupe(s, " ID_BUTTON=" + LAST),
                            aliste, 3));
                    LAST = cl.PARENT_ID_BUTTON;
                    aliste = new LinkedList<>();
                }
                aliste.add(jb);
            }
            lespanier.add(makeZone("" + getNameGroupe(s, " ID_BUTTON=" + LAST),
                    aliste, 3));

        } catch (Exception sq) {
            System.err.println(" error Clinic Interface " + sq);
        }

        System.out.println("clinic.Main_CLinic.buildPanier() lespanier " + lespanier.size());
        return lespanier;
    }

    public static JPanel makeZone(String ZoneName, LinkedList<JButton> aliste, int col) {
        JPanel jp = new JPanel();
        jp.setBackground(getBackGroundColor());
        javax.swing.border.Border LoweredBevelBorder = BorderFactory.createLoweredBevelBorder();
        TitledBorder EtchedBorderRaised = BorderFactory.createTitledBorder(LoweredBevelBorder,
                ZoneName, TitledBorder.LEFT, TitledBorder.TOP,
                new Font("Arial", Font.BOLD, 16), getForeGroundColor());
        jp.setBorder(EtchedBorderRaised);
        int lin = (aliste.size() / col) + 1;
        //   System.out.println(aliste.size() + " clinic.Main_CLinic.makeZone()" + lin);
        jp.setLayout(new GridLayout(lin, col));

        aliste.forEach(cl -> {
            jp.add(cl);
        });

        return jp;
    }

    public static void performClinicCall(String functionName) {

        if (Eye.checkPermission(functionName, statee, conn, umukozi)) {
            try {
                Statement sta = conn.createStatement();
                Statement st2 = conn.createStatement();
                Clinic_interface cl = getNameGroupe(sta, " NAME_BUTTON='" + functionName + "'");
                System.out.println(" " + cl);
                if (cl != null) {
                    LinkedList<Clinic_interface> listo = DbHandlerClinic.getInterface(sta,
                            "  WHERE LEVEL_BUTTON=" + (cl.LEVEL + 1) + "  AND PARENT_ID_BUTTON"
                            + " = " + cl.ID_BUTTON + " order by PARENT_ID_BUTTON,ID_SORT");
                    if (!listo.isEmpty()) {
                        LinkedList<JPanel> lespanier = buildPanier(listo, sta, st2, cl.ID_BUTTON);
                        int sizeGroupe = lespanier.size();
                        JPanel lepan = new JPanel();
                        lepan.setPreferredSize(new Dimension(400, 300));
                        lepan.setBackground(getBackGroundColor());
                        lepan.setLayout(new GridLayout(sizeGroupe, 1));
                        lespanier.forEach(jp -> {
                            lepan.add(jp);
                        });
                        JOptionPane.showMessageDialog(null, lepan, " " + cl,
                                JOptionPane.INFORMATION_MESSAGE);
                    } else {

                        switch (functionName) {
                            case "GENERATE":
                                new ODT_INTERFACE_MAIN(dbEye.credentials.tin,
                                        dbEye.credentials.ssn,
                                        dbEye.credentials.MRC_VCIS, conn, umukozi);
                                break;
                            case "VIEW_INSURANCE": {
                                LesDynamicInsurance ins = new LesDynamicInsurance();
                                connDyn.setParamans(" order by " + ins.header[1], ins, 100);
                                LesDynamic le = DbHandlerClinic.getDynamics(connDyn);
                                JPanel jp = DynamicView.getJtable(le, connDyn);
                                JOptionPane.showMessageDialog(null, jp, " hey " + cl,
                                        JOptionPane.INFORMATION_MESSAGE);
                                break;
                            }
                             case "SURGERY":
                             {  LesDynamicSurgery ins = new LesDynamicSurgery();
                                connDyn.setParamans(" order by " + ins.header[0], ins, 100);
                                LesDynamic le = DbHandlerClinic.getDynamics(connDyn);
                                JPanel jp = DynamicView.getJtable(le, connDyn);
                                JOptionPane.showMessageDialog(null, jp, " hey " + cl,
                                        JOptionPane.INFORMATION_MESSAGE);
                                break;
                        }
                             case "MEALS": {
                                LesDynamicMeals ins = new LesDynamicMeals();
                                connDyn.setParamans(" order by " + ins.header[1], ins, 100);
                                
                                 String CODE = JOptionPane.showInputDialog(null, 
                                         "CODEC");
                                String NAME = JOptionPane.showInputDialog(null, 
                                        "NAME");
                                String IBIRO = JOptionPane.showInputDialog(null,
                                        "IBIRO");
                               
                                ins.data= new String [1][1];
                                ins.data[0] = new String[]{CODE,NAME,IBIRO};

                                JOptionPane.showMessageDialog(null, DynamicView.insertDyn(
                                        ins, conn),  " DYNAMIC INSERT MEAL",
                                        JOptionPane.INFORMATION_MESSAGE);
                                
                                LesDynamic le = DbHandlerClinic.getDynamics(connDyn);
                                JPanel jp = DynamicView.getJtable(le, connDyn);
                                JOptionPane.showMessageDialog(null, jp, " hey " + cl,
                                        JOptionPane.INFORMATION_MESSAGE);
                                break;
                            }
                             case "THEATER": {
                                 
                                  System.out.println("ngeze 11 ");
                                LesDynamicSalleDoperation ins = new LesDynamicSalleDoperation();
                                
                                System.out.println("ngeze 14 ");
                                connDyn.setParamans(" order by " + ins.header[1], ins, 100);
                                
                                  LesDynamic le = DbHandlerClinic.getDynamics(connDyn);
                                
                                System.out.println("ngeze 19 ");
                                JPanel jp = DynamicView.getJtable(le, connDyn);
                                
                                System.out.println("ngeze 20 ");
                                
                                JOptionPane.showMessageDialog(null, jp, " hey " + cl,
                                        JOptionPane.INFORMATION_MESSAGE);
                                
                                System.out.println("ngeze 21 ");
                                
                                System.out.println("ngeze 15 ");
                                
                                 String CODE = JOptionPane.showInputDialog(null, 
                                         "CODESALLE");
                                String NAME = JOptionPane.showInputDialog(null, 
                                        "NAME");
                                String IBIRO = JOptionPane.showInputDialog(null,
                                        "SPECIALITE");
                               
                                ins.data= new String [1][3];// 3 = number of column
                                ins.data[0] = new String[]{CODE,NAME,IBIRO};
                                
                                System.out.println("ngeze 16 ");

                                JOptionPane.showMessageDialog(null, DynamicView.insertDyn(
                                        ins, conn),  " DYNAMIC INSERT SALLE D'OPERATION",
                                        JOptionPane.INFORMATION_MESSAGE);
                                
                                System.out.println("ngeze 17 ");
                                
                                le = DbHandlerClinic.getDynamics(connDyn);
                                
                                
                                
                                System.out.println("ngeze 19 ");
                                jp = DynamicView.getJtable(le, connDyn);
                                
                                System.out.println("ngeze 20 ");
                                
                                JOptionPane.showMessageDialog(null, jp, " hey " + cl,
                                        JOptionPane.INFORMATION_MESSAGE);
                                
                                System.out.println("ngeze 21 ");
                                
                                break;
                            }
                            case "IN_PATIENT": {
                                LesDynamicVisit ins = new LesDynamicVisit();
                                connDyn.setParamans(" where GLOBAL_ID_OPEN ='" + umukozi.UNIVERSE_ACCOUNT
                                        + "' order by " + ins.header[3], ins, 100);
                                LesDynamic le = DbHandlerClinic.getDynamics(connDyn);
                                JPanel jp = DynamicView.getJtable(le, connDyn);
                                JOptionPane.showMessageDialog(null, jp, " " + cl,
                                        JOptionPane.INFORMATION_MESSAGE);
                                break;
                            }
                            case "CHECK_IN": {
                                LesDynamicVisit ins = new LesDynamicVisit();
                                ins.data = new String[1][1];
                                LesDynamicInsurance assurance = new LesDynamicInsurance();
                                connDyn.setParamans(" order by " + assurance.header[0], assurance, 100);
                                LesDynamic le = DbHandlerClinic.getDynamics(connDyn);

                                String[] lesAss = new String[le.data.length];

                                for (int k = 0; k < assurance.data.length; k++) {
                                    lesAss[k] = assurance.data[k][0];
                                }

                                String insurance = (String) JOptionPane.showInputDialog(null,
                                        DbHandlerClinic.lala(lang, "OPTIONS", statee, pie, pil),
                                        DbHandlerClinic.lala(lang, "IMPRESSION", statee, pie, pil),
                                        JOptionPane.QUESTION_MESSAGE, null, lesAss, "INEZA");

                                String aff;

                                if (search != null
                                        && !search.getText().equals("    search Functions ...  ")
                                        && search.getText().length() > 3) {
                                    aff = search.getText();
                                } else {
                                    aff = JOptionPane.showInputDialog(null,
                                            DbHandlerClinic.lala(lang, "BENEFICIAL NUMBER", statee, pie, pil));
                                }

                                String st = Jobber.injizAbarwayiONE5(aff, conn, insurance);
                                String deposit = "";
                                if (!st.contains("ERROR")) {
                                    LinkedList<JLabel> aliste = new LinkedList<>();
                                    getPatientInfo(aliste);
                                    INEZA_AFFILIATES ria = new INEZA_AFFILIATES(st, insurance);
                                    setPatientInfo(ria, aliste);
                                    JPanel jp = new JPanel();

                                    javax.swing.border.Border LoweredBevelBorder = BorderFactory.createLoweredBevelBorder();
                                    TitledBorder EtchedBorderRaised = BorderFactory.createTitledBorder(LoweredBevelBorder,
                                            "PATIENT INFO",
                                            TitledBorder.CENTER, TitledBorder.TOP,
                                            new Font("Arial", Font.BOLD, 16), Color.GREEN);
                                    jp.setBorder(EtchedBorderRaised);
                                    jp.setLayout(new GridLayout(6, 2));
                                    aliste.forEach((jl) -> {
                                        jp.add(jl);
                                    });

                                    JOptionPane.showMessageDialog(null, jp, " " + cl,
                                            JOptionPane.INFORMATION_MESSAGE);

                                } else {
                                    deposit = JOptionPane.showInputDialog(null,
                                            DbHandlerClinic.lala(lang, "DEPOSIT", statee, pie, pil) + " \n"
                                            + aff + "  " + DbHandlerClinic.lala(lang, "NOT RECOGNIZED BY ISHYIGA INSURANCES CLOUD", statee, pie, pil));
                                }

                                ins.data[0] = new String[]{"", umukozi.UNIVERSE_ACCOUNT,
                                    "", insurance, aff, deposit,
                                    "", "", "", "NEW"};

                                JOptionPane.showMessageDialog(null, DynamicView.insertDyn(ins, conn),
                                        " DYNAMIC INSERT CHECK IN",
                                        JOptionPane.INFORMATION_MESSAGE);
                                connDyn.setParamans(" order by " + ins.header[3], ins, 100);
                                le = DbHandlerClinic.getDynamics(connDyn);
                                JPanel jp = DynamicView.getJtable(le, connDyn);
                                JOptionPane.showMessageDialog(null, jp, " " + cl,
                                        JOptionPane.INFORMATION_MESSAGE);
                                break;
                            }
                            case "CHECK_OUT": {
                                LesDynamicVisit ins = new LesDynamicVisit();
                                connDyn.setParamans(" order by " + ins.header[3], ins, 100);
                                LesDynamic le = DbHandlerClinic.getDynamics(connDyn);
                                JPanel jp = DynamicView.getJtable(le, connDyn);
                                JOptionPane.showMessageDialog(null, jp, " " + cl,
                                        JOptionPane.INFORMATION_MESSAGE);
                                break;
                            }
                            case "PATIENT": {
                                LesDynamicPatient ins = new LesDynamicPatient();
                                connDyn.setParamans(" order by " + ins.header[3], ins, 100);
                                LesDynamic le = DbHandlerClinic.getDynamics(connDyn);
                                JPanel jp = DynamicView.getJtable(le, connDyn);
                                JOptionPane.showMessageDialog(null, jp, " " + cl,
                                        JOptionPane.INFORMATION_MESSAGE);
                                break;
                            }
                            case "DRUGS": {
                                LesDynamicDrugs ins = new LesDynamicDrugs();
                                connDyn.setParamans(" order by " + ins.header[1], ins, 15000);
                                LesDynamic le = DbHandlerClinic.getDynamics(connDyn);
                                JPanel jp = DynamicView.getJtable(le, connDyn);
                                JOptionPane.showMessageDialog(null, jp, " " + cl,
                                        JOptionPane.INFORMATION_MESSAGE);
                                break;
                            }
                            case "ICD10": {
                                LesDynamicICD10 ins = new LesDynamicICD10();
                                connDyn.setParamans(" order by " + ins.header[3], ins, 30000);

                                LesDynamic le = DbHandlerClinic.getDynamics(connDyn);
                                JPanel jp = DynamicView.getJtable(le, connDyn);
                                JOptionPane.showMessageDialog(null, jp, " " + cl,
                                        JOptionPane.INFORMATION_MESSAGE);
                                break;
                            }
                            case "MY_DIAGNOSTIC": {
                                LesDynamicDiagnostic diag = new LesDynamicDiagnostic();

                                String notes = JOptionPane.showInputDialog(null, "PUT YOUR DIAGNOSTIC");
                                diag.data = new String[1][1];
                                diag.data[0] = new String[]{null, umukozi.UNIVERSE_ACCOUNT,
                                    null, "25078888006601", notes, "6"};

                                JOptionPane.showMessageDialog(null,
                                        DynamicView.insertDyn(diag, conn),
                                        " DYNAMIC INSERT DIAGNOSTIC ",
                                        JOptionPane.INFORMATION_MESSAGE);
                                LesDynamicDiagnostic ins = new LesDynamicDiagnostic();
                                connDyn.setParamans(" order by " + ins.header[3], ins, 30000);

                                LesDynamic le = DbHandlerClinic.getDynamics(connDyn);
                                JPanel jp = DynamicView.getJtable(le, connDyn);
                                JOptionPane.showMessageDialog(null, jp, " " + cl,
                                        JOptionPane.INFORMATION_MESSAGE);
                                break;
                            }
                            default:
                                JOptionPane.showMessageDialog(null, "MY NAME IS " + functionName);
                                break;
                        }

                    }

                } else {
                    JOptionPane.showMessageDialog(null, "shida " + functionName);
                }

            } catch (SQLException ex) {
                Logger.getLogger(Main_Clinic.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    private void lesFunctionEcoute() {
        clinicList.addMouseListener(new MouseListener() {
            public void actionPerformed(ActionEvent ae) {
            }

            @Override
            public void mouseExited(MouseEvent ae) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                Clinic_interface c = (Clinic_interface) clinicList.getSelectedValue();
                if (c != null) {
                    Main_Clinic.performClinicCall(c.NAME_BUTTON);
                    search.setText("    search Functions ...  ");
                    clinicList.setListData(liste.toArray());
                }
            }

            ;  
            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }
        });
    }

    private void lesSearchEcoute() {
        search.addMouseListener(new MouseListener() {
            public void actionPerformed(ActionEvent ae) {
            }

            @Override
            public void mouseExited(MouseEvent ae) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (search.getText().equals("    search Functions ...  ")) {
                    search.setText("");
                }
            }

            ;  
            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }
        });
    }

    private void superSearchFunction() {
        search.addKeyListener(
                new KeyListener() {
            public void actionPerformed(ActionEvent ae) {
            }

            @Override
            public void keyTyped(KeyEvent e) {

                String cc = "" + e.getKeyChar();
                String find = (search.getText() + cc).toUpperCase();
                if (cc.hashCode() == 8 && find.length() > 0) {
                    find = (find.substring(0, find.length() - 1));
                }
                superSearchFunction(find);
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
    }

    void superSearchFunction(String find) {

        LinkedList<Clinic_interface> res = new LinkedList();
        find = find.replaceAll(" ", "");
        for (int i = 0; i < liste.size(); i++) {
            Clinic_interface p = liste.get(i);

            if (p.NAME_EN.toUpperCase().contains(find) || p.NAME_FR.toUpperCase().contains(find)
                    || p.NAME_KI.toUpperCase().contains(find)) {
                res.add(p);
            }
        }
        clinicList.setListData(res.toArray());
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        System.out.println(" (\"\"+e.getActionCommand()) " + ("" + e.getActionCommand()));
        if (("" + e.getActionCommand()).equals("LIGHT")) {
            dbEye.updateParameter("VIEW_MODE", "LIGHT", statee);
        } else if (("" + e.getActionCommand()).equals("DARK")) {
            dbEye.updateParameter("VIEW_MODE", "DARK", statee);
        }
        dbEye.credentials.VIEW_MODE = dbEye.getParam("VIEW_MODE", statee);
        dispose();
        new Main_Clinic(conn, umukozi);

    }

    public JPanel RadioButton() {

//Create the radio buttons.
        String DARK = DbHandlerClinic.lala(lang, "DARK", statee, pie, pil);
        JRadioButton dark = new JRadioButton(DARK);
        dark.setMnemonic(KeyEvent.VK_P);
        dark.setActionCommand(DARK);
        String LIGHT = DbHandlerClinic.lala(lang, "LIGHT", statee, pie, pil);
        JRadioButton light = new JRadioButton(LIGHT);
        light.setMnemonic(KeyEvent.VK_L);
        light.setActionCommand(LIGHT);
        ButtonGroup group = new ButtonGroup();
        group.add(dark);
        group.add(light);

        dark.addActionListener(this);
        light.addActionListener(this);

        if (dbEye.credentials.VIEW_MODE != null
                && dbEye.credentials.VIEW_MODE.equals("LIGHT")) {
            light.setSelected(true);
        } else {
            dark.setSelected(true);
        }

//Put the radio buttons in a column in a panel.
        JPanel radioPanel = new JPanel(new GridLayout(1, 2));
        radioPanel.add(dark);
        radioPanel.add(light);

        add(radioPanel, BorderLayout.LINE_START);
//  setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
        return radioPanel;
    }

      public static void main(String[] arghs) {
//          
//     Conne     
//          
      new Main_Clinic(conn, umukozi);
    } 
              
    
    
}
