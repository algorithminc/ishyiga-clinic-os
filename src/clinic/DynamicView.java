/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.Statement;
import java.util.LinkedList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author kimai
 */
public class DynamicView {
   
   
 
  public static JPanel getJtable(LesDynamic le, ConnectorDyn connDyn ) {
 
      JPanel jp = new JPanel();
      jp.setPreferredSize(new Dimension(le.width, le.height));
             JTable jtable = new javax.swing.JTable();
     
       JButton sortby = new JButton("TO SORT CLICK ON HEADER ");
       sortby.setBackground(Color.pink);
       sortby.addActionListener((ActionEvent ae) -> {
           
           le.sortBy(le.index_update);
           JOptionPane.showMessageDialog(null, getJtable(le,connDyn), " SORT RESULT " ,
                                JOptionPane.INFORMATION_MESSAGE); 
           
                });
       JTextField jt= new JTextField(" search table ");
       
       JButton sEARCHButton = new JButton(" SEARCH ");
       sEARCHButton.addActionListener((ActionEvent ae) -> {                 
           
           le.search(jt.getText());
  JOptionPane.showMessageDialog(null, getJtable(le,connDyn), " SEARCH RESULT " ,
                                JOptionPane.INFORMATION_MESSAGE);                   
           
                });
       
       
  jtable.addMouseListener(new MouseAdapter() {
  public void mouseClicked(MouseEvent e) {
    //if (e.getClickCount() == 2) 
    {
      JTable target = (JTable)e.getSource();
      int row = target.getSelectedRow();
      int column = target.getSelectedColumn(); 
     JOptionPane.showMessageDialog(null, updateDyn(le,row,connDyn ), " DYNAMIC UPDATE " ,
                                JOptionPane.INFORMATION_MESSAGE);
    }
  }
});
       jtable.getTableHeader().addMouseListener(new MouseListener() {
            public void actionPerformed(ActionEvent ae) {
            }

            @Override
            public void mouseExited(MouseEvent ae) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                 System.out.println("e.getClickCount() " + e.getX());
                 
                 le.sortBy(e.getX());
                  JOptionPane.showMessageDialog(null, getJtable(le,connDyn), 
                          " SEARCH RESULT " ,JOptionPane.INFORMATION_MESSAGE);   
            }

            ;
@Override
            public void mousePressed(MouseEvent e) {
            }

            ;
@Override
            public void mouseReleased(MouseEvent e) {
            }

            ;
@Override
            public void mouseEntered(MouseEvent e) {
            }
        });
       
       jp.add(sortby);
       jp.add(jt);
       jp.add(sEARCHButton);
       // le.printData();le.printHeader();
        
        jtable.setModel(new javax.swing.table.DefaultTableModel(le.data, le.header));

    
        jtable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
//        System.out.println( "le.header.length  "+le.header.length);
        for(int i=0; i<le.header.length;i++)
        {
//        System.out.print (" >> "+i+"  "+le.uburebure[i]);
        jtable.getColumnModel().getColumn(i).setPreferredWidth(le.uburebure[i]);
        }
        jtable.doLayout();
        jtable.setRowHeight(30);
        jtable.validate();
        //System.out.println("  "+le.uburebure[i]);
           JScrollPane agasekeView = new JScrollPane(jtable);
        agasekeView.setPreferredSize(new Dimension(le.width-10, le.height-10));
        jp.add(agasekeView);
       // jp.add(jtable);
        return jp;
    }
  

public static JPanel updateDyn(LesDynamic le, int selected, ConnectorDyn connDyn ) {
    
    //10 % 8;
    
    int ligne = le.getIzigiyeGuhinduka()+1;
    int colonne = 2;
    int wido = 500;
    
    if(ligne>8)
    {
    ligne = 10;
    colonne=4;
    wido = 700;
    }
    
    JPanel jp = new JPanel(new GridLayout(ligne+1,colonne)); 
    
    jp.setPreferredSize(new Dimension(wido, ligne*40));
        
    JButton action = new JButton(" UPDATE ");
    action.setBackground(Color.yellow);
    le.liste = new LinkedList<>();
  
    for (int i=0; i<le.header.length;i++)
    {
    
    if(le.izemeweUpdate[i])
    { 
         le.liste.add(le.getComponent(le.data[selected][i],i,connDyn));
         jp.add(new JLabel(le.header[i]));
         jp.add(le.liste.get(i));
         
    }
    else {
        le.liste.add(new JTextField(le.data[selected][i]));
      //  jp.add(new JLabel(le.data[selected][i]));
    }
     
      
    }
    jp.add(new JLabel("ACTION"));
       action.addActionListener((ActionEvent ae) -> {
            
            boolean res =  DbHandlerClinic.updateDynamic(le, connDyn.s,selected); 
                   JOptionPane.showMessageDialog(null, "RECORD UPDATED ? "+res,
                               " " +le.data[selected][le.index_update], 
                                    JOptionPane.INFORMATION_MESSAGE);    
                });
    
    jp.add(action); 
    
return jp;
}


public static JPanel insertDyn(LesDynamic le, Connection conn ) {
    JPanel jp = new JPanel(new GridLayout(le.header.length+1, 2)); 
    
    jp.setPreferredSize(new Dimension(500, le.header.length*40));
    
    
    JButton action = new JButton(" INSERT ");
    action.setBackground(Color.orange);
    le.liste = new LinkedList<>();
  
    for (int i=0; i<le.header.length;i++)
    {
   
    if(le.izemeweInsert[i])
    { 
         le.liste.add(new JTextField(le.data[0][i]));
         jp.add(new JLabel(le.header[i]));
         jp.add(le.liste.get(i));
         
    }
    else {
        le.liste.add(new JTextField(le.data[0][i]));
        //jp.add(new JLabel(le.data[0][i]));
    }
     
      
    }
    jp.add(new JLabel("ACTION"));
       action.addActionListener((ActionEvent ae) -> {
            
            boolean res =  DbHandlerClinic.insertDynamic(le, conn); 
                   JOptionPane.showMessageDialog(null, "RECORD UPDATED ? "+res,
                               " " +le.data[0][le.index_update], 
                                    JOptionPane.INFORMATION_MESSAGE);    
                });
    
    jp.add(action); 
    
return jp;
}
  
}
