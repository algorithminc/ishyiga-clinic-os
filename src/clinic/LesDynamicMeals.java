/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic;

/**
 *
 * @author kimai
 */
public class LesDynamicMeals extends LesDynamic{
 
     String header [] = new String []{"CODE_MEALS", "MEALS_NAME","IBIRO"};
    String variables [] = new String []{"STRING", "STRING", "DOUBLE" };
    String variableBehavior [] = new String []{"STRING#_#VISIBLE", "STRING#_#EDITABLE",
        "DOUBLE#_#VISIBLE"};
     
    
       public LesDynamicMeals( ) { 
        super.setHeader(header);
        super.setVariables(variables);
        super.setVariablesBehavior(variableBehavior);
        super.setTable("VCDC.CLINIC_MEALS");
          
        super.setIzikenewe(
                new boolean []{true, true,true}, 
                new boolean []{false, true,true}, 
                new boolean []{true, true,true}); 
        int width2 = 700;
        int prefered = 300;
        int izindi = (width2-prefered)/(header.length-1);
        super.setWidth(width2);
        super.setHeight(400);
        super.setUburebure(new int []{izindi,prefered,izindi});  
        super.setIndexUpdate(0);
    } 
    
} 
