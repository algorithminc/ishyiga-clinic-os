/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic;

import java.sql.PreparedStatement;

/**
 *
 * @author kimai
 */
public class LesDynamicVisit extends LesDynamic{
  
     String header [] = new String []{"ID_VISIT", "GLOBAL_ID_OPEN", 
            "HEURE_OPENED", "GROUP_CLIENT", "BENEFICIAL_NUMBER", "DEPOSIT",
            "OUT_IN_PATIENT","STATUS","HEURE_CLOSED","GLOBAL_ID_CLOSED"};
    String variables [] = new String []{"INT", "STRING", 
            "STRING","STRING", "STRING","DOUBLE", "STRING", "STRING", "STRING", "STRING"};
    String variableBehavior [] = new String []{"INT#_#VISIBLE", "Account#UNIVERSE_ACCOUNT#HIDDEN", 
            "DbHandlerClinic#getNow()#HIDDEN","LesDynamicInsurance#data#selectlist", 
            "STRING#_#VISIBLE","DOUBLE#_#EDITABLE", "Main_Clinic#OUT_IN_PATIENT#selectlist",
            "Main_Clinic#ACTIVITY_STATUS#selectlist", "DbHandlerClinic#getNow()#HIDDEN", 
            "Account#UNIVERSE_ACCOUNT#HIDDEN"};
    
//    create table "VCDC".CLINIC_VISIT
//(
//	ID_VISIT INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
//	GLOBAL_ID_OPEN VARCHAR(50) not null,
//	HEURE_OPENED TIMESTAMP default CURRENT TIMESTAMP,
//	GROUP_CLIENT VARCHAR(50) not null,
//	BENEFICIAL_NUMBER VARCHAR(50) not null,
//	DEPOSIT DOUBLE default 0,
//	OUT_IN_PATIENT VARCHAR(10) default 'OUT',
//	STATUS  VARCHAR(10) default 'OPEN',
//	HEURE_CLOSED TIMESTAMP,
//	GLOBAL_ID_OPEN VARCHAR(50) not null
//)
    
    
       public LesDynamicVisit( ) { 
        super.setHeader(header);
        super.setVariables(variables);
        super.setVariablesBehavior(variableBehavior);
        super.setTable("VCDC.CLINIC_VISIT");
          
        super.setIzikenewe(
                new boolean []{false, true,false,true,true,true,false,false,false,true}, 
                new boolean []{false, false,false,false,false,true,true,true,true,true}, 
                new boolean []{true, true,true,true,true,true,true,true,true,true}); 
        int width2 = 700;
        int prefered = 200;
        int izindi = (width2-prefered)/(header.length-1);
        super.setWidth(width2);
        super.setHeight(400);
        super.setUburebure(new int []{izindi,prefered,izindi,izindi,izindi,
            izindi,izindi,izindi,izindi,izindi});  
        super.setIndexUpdate(0);
    }
       
       
       
       
       
}
