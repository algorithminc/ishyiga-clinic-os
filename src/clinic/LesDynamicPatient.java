/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic;

/**
 *
 * @author kimai
 */
public class LesDynamicPatient extends LesDynamic{
 
String AFFILIATES_NUMBER, AFFILIATE_NOM,AFFILIATE_PRENOM,
            BENEFICIARY_NUMBER, BENEFICIARY_FIRST_NAME, BENEFICIARY_LAST_NAME,
            RELATIONSHIP, GENDER, EMPLOYER, EMPLOYER_CATEGORY,
            AFFILIATION_STATUS, PHONE_NUMBER, ID_CARD_NUMBER,ALLEGIES,DOB,ASSURANCE;

    String LAST_UPDATE;
    int PERCENTAGE =15,PIN;    
String header [] = new String []{"AFFILIATES_NUMBER","BENEFICIARY_NUMBER","BENEFICIARY_FIRST_NAME",
"BENEFICIARY_LAST_NAME","RELATIONSHIP","DOB","GENDER","EMPLOYER",
"EMPLOYER_CATEGORY","AFFILIATION_STATUS","PHONE_NUMBER","ID_CARD_NUMBER",
"LAST_UPDATE","PIN","AFFILIATE_NOM","AFFILIATE_PRENOM","ALLEGIES","PERCENTAGE","INSURANCE"};
 
String variables [] = new String []{"STRING", "STRING", "STRING"
        , "STRING", "STRING", "STRING", "STRING", "STRING"
        , "STRING", "STRING", "STRING", "STRING"
        , "STRING","INT", "STRING", "STRING", "STRING", "INT", "STRING"};


     public LesDynamicPatient() { 
        super.setHeader(header);
        super.setVariables(variables);
        super.setTable("VCDC.RECEIVE_INEZA_AFFILIATES");
        super.setIzikenewe(new boolean []{true, true,true,true,true, true,true,true,
        true, true,true,true,true, true,true,true,true, true, true}, 
                new boolean []{false, false,true,true,true, true,true,true,
        true, true,true,true,true, true,true,true,true, false, false}, 
                new boolean []{true, true,true,true,true, true,true,true,
        true, true,true,true,true, true,true,true,true, true, true}); 
        int width2 = 900;
        int prefered = 150;
        int izindi = (width2-prefered)/(header.length-1);
        super.setWidth(width2);
        super.setHeight(400);
        super.setUburebure(new int []{izindi,izindi,prefered,izindi,izindi,izindi,izindi,izindi
        ,izindi,izindi,izindi,izindi,izindi,izindi,izindi,izindi,izindi,izindi,izindi});  
        super.setIndexUpdate(1);
    }
    
    
    @Override
    public String toString()
    {
    return BENEFICIARY_FIRST_NAME+" "+BENEFICIARY_LAST_NAME;
    }    
    
}
//"VCDC".RECEIVE_INEZA_AFFILIATES
//(
//	AFFILIATES_NUMBER VARCHAR(50),
//	BENEFICIARY_NUMBER VARCHAR(50) not null primary key,
//	BENEFICIARY_FIRST_NAME VARCHAR(150),
//	BENEFICIARY_LAST_NAME VARCHAR(150),
//	RELATIONSHIP VARCHAR(50),
//	DOB VARCHAR(50),
//	GENDER VARCHAR(10),
//	EMPLOYER VARCHAR(120),
//	EMPLOYER_CATEGORY VARCHAR(20),
//	AFFILIATION_STATUS VARCHAR(20),
//	PHONE_NUMBER VARCHAR(20),
//	ID_CARD_NUMBER VARCHAR(50),
//	LAST_UPDATE VARCHAR(30),
//	PIN INTEGER,
//	AFFILIATE_NOM VARCHAR(63),
//	AFFILIATE_PRENOM VARCHAR(63),
//	ALLEGIES VARCHAR(500) default 'NONE'
