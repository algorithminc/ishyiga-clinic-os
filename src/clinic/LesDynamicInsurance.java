/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic;

/**
 *
 * @author kimai
 */
public class LesDynamicInsurance extends LesDynamic{
    
    String INSURANCE_SIGLE,INSURANCE_FULL_NAME;
    double MAX_AMOUNT;
    int MAX_SERVICES;
    
    String header [] = new String []{"INSURANCE_SIGLE", "INSURANCE_FULL_NAME", 
            "MAX_AMOUNT", "MAX_SERVICES"};
    String variables [] = new String []{"STRING", "STRING", 
            "DOUBLE", "INT"};
    
//    create table "VCDC".RECEVEIVE_INEZA_INSURANCE
//(
//	INSURANCE_SIGLE VARCHAR(20) not null primary key,
//	INSURANCE_FULL_NAME VARCHAR(150) not null,
//	MAX_AMOUNT DOUBLE default 40000,
//	MAX_SERVICES INTEGER default 4
//)

    public LesDynamicInsurance(String INSURANCE_SIGLE, String INSURANCE_FULL_NAME, 
            double MAX_AMOUNT, int MAX_SERVICES) {
        this.INSURANCE_SIGLE = INSURANCE_SIGLE;
        this.INSURANCE_FULL_NAME = INSURANCE_FULL_NAME;
        this.MAX_AMOUNT = MAX_AMOUNT;
        this.MAX_SERVICES = MAX_SERVICES; 
    }
    
     public LesDynamicInsurance( ) { 
        super.setHeader(header);
        super.setVariables(variables);
        super.setTable("VCDC.RECEVEIVE_INEZA_INSURANCE");
        super.setIzikenewe(new boolean []{true, true,true,true}, 
                new boolean []{false, true,true,true}, 
                new boolean []{true, true,true,true}); 
        int width2 = 700;
        int prefered = 300;
        int izindi = (width2-prefered)/(header.length-1);
        super.setWidth(width2);
        super.setHeight(400);
        super.setUburebure(new int []{izindi,prefered,izindi,izindi});  
        super.setIndexUpdate(1);
    }
    
    
    @Override
    public String toString()
    {
    return INSURANCE_SIGLE;
    }
    
}
