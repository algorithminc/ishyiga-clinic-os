/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic;

/**
 *
 * @author kimai
 */
public class LesDynamicICD10 extends LesDynamic{
    
    String ID_ICD10,CODE_ICD10,NAME_SHORT,NAME_LONG,CLASS_ICD10;
    
    String header [] = new String []{"ID_ICD10", "CODE_ICD10", 
            "NAME_SHORT","NAME_LONG", "CLASS_ICD10"};
    String variables [] = new String []{"STRING", "STRING", "STRING", 
            "STRING", "STRING"};
    
     public LesDynamicICD10( ) { 
        super.setHeader(header);
        super.setVariables(variables);
        super.setTable("VCDC.RECEIVE_INEZA_ICD10");
        super.setIzikenewe(new boolean []{true, true,true,true,true}, 
                new boolean []{false, false,false,true,true}, 
                new boolean []{true, true,true,true,true}); 
        int width2 = 700;
        int prefered = 300;
        int izindi = (width2-prefered)/(header.length-1);
        super.setWidth(width2);
        super.setHeight(400);
        super.setUburebure(new int []{izindi,izindi,izindi,prefered,izindi});  
        super.setIndexUpdate(2);
    }
    
    
    @Override
    public String toString()
    {
    return NAME_SHORT;
    }
    
    
}
  
	
