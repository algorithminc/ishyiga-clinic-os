/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic;

import static clinic.DbHandlerClinic.getNow;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.LinkedList;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

/**
 *
 * @author kimai
 */
public class LesDynamic {

    public String[] header, headerLangue;
    public String[] variables, variableBehavior;
    public boolean[] izemeweInsert, izemeweUpdate, izemeweSelect;
    public String[][] data;
    public String tableName;
    public int[] uburebure;
    public int width;
    public int height;
    public int index_update;
    LinkedList<Component> liste;

    public void setHeader(String[] header) {
        this.header = header;
        this.headerLangue = header;
    }

    public void setVariables(String[] variables) {
        this.variables = variables;
    }

    public void setVariablesBehavior(String[] variableBehavior) {
        this.variableBehavior = variableBehavior;
    }

    public void setData(String[][] data) {
        this.data = data;
    }

    public void setTable(String tblName) {
        this.tableName = tblName;
    }

    public void setUburebure(int[] uburebure) {
        this.uburebure = uburebure;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setIzikenewe(boolean[] izemeweInsert,
            boolean[] izemeweUpdate,
            boolean[] izemeweSelect) {

        this.izemeweInsert = izemeweInsert;
        this.izemeweUpdate = izemeweUpdate;
        this.izemeweSelect = izemeweSelect;
    }

    public void setIndexUpdate(int index_update) {
        this.index_update = index_update;
    }

    public void printData() {
        System.out.println("-------------------printData----------------");
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                System.out.println(data[i][j]);
            }
        }
    }

    public void printHeader() {
        System.out.println("-------------------printHeader----------------");

        for (int i = 0; i < header.length; i++) {
            System.out.println(header[i]);

        }
    }

    public void sortBy(int x) {
        int ahongeze = 0;
        for (int i = 0; i < uburebure.length; i++) {
            ahongeze += uburebure[i];
            if (ahongeze > x) {
                sortByX(i);
                break;
            }
        }

    }

    public void sortByX(int indeToSort) {
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                if (j == indeToSort) {
                    data[i][j] = "hey " + indeToSort;
                }
            }
        }
    }

    public void search(String text) {

        LinkedList<String[]> laListe = new LinkedList<>();

        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                // System.out.println("clinic.LesDynamic.search()" + text);

                if (data[i][j] != null && data[i][j].toUpperCase().contains(text.toUpperCase())) {
                    laListe.add(data[i]);
                    //data[i][j]=text;
                    continue;
                }
            }
        }

        data = new String[laListe.size()][header.length];

        for (int i = 0; i < laListe.size(); i++) {
            data[i] = laListe.get(i);
        }

    }
    
    public int getIzigiyeGuhinduka()
    {
        int izigiye=0;
      for (int i = 0; i < header.length; i++) {
          if(izemeweUpdate[i])
          {izigiye++;}
        }
      return izigiye;
    }
    public int getIzigiyeKwinjira()
    {
        int izigiye=0;
      for (int i = 0; i < header.length; i++) {
          if(izemeweInsert[i])
          {izigiye++;}
        }
      return izigiye;
    }
    
    
    public Component getComponent(Object defaultValue, int index, ConnectorDyn connDyn) {
        if (variableBehavior[index] != null) {
            switch (variableBehavior[index]) {
                case "LesDynamicDrugs#data#selectlist":
                    LesDynamicDrugs ins = new LesDynamicDrugs();
                    connDyn.setParamans(" order by " + ins.header[1], ins, 15000);
                    LesDynamic le = DbHandlerClinic.getDynamics(connDyn);

                    return new JList(le.data[0]);
                case "LesDynamicPatient#data#selectlist":
                    LesDynamicPatient pat = new LesDynamicPatient();
                    connDyn.setParamans(" order by " + pat.header[3], pat, 100);
                    LesDynamic lee = DbHandlerClinic.getDynamics(connDyn);
                    return new JList(lee.data[0]);
                case "Account#UNIVERSE_ACCOUNT#HIDDEN":
                    return new JLabel("" + Main_Clinic.umukozi.UNIVERSE_ACCOUNT);
                case "DbHandlerClinic#getNow()#HIDDEN":
                    return new JLabel("" + getNow());
                case "Main_Clinic#ACTIVITY_STATUS#selectlist":
                    return new JList(Main_Clinic.ACTIVITY_STATUS);
                case "DOUBLE#_#EDITABLE":
                    return new JTextField("" + defaultValue);
                    case "STRING#_#EDITABLE":
                    return new JTextField("" + defaultValue);
                    
                case "Main_Clinic#OUT_IN_PATIENT#selectlist":
                    return new JList(Main_Clinic.OUT_IN_PATIENT);
                case "LesDynamicInsurance#data#selectlist":
                    LesDynamicInsurance assurance = new LesDynamicInsurance();
                    connDyn.setParamans(" order by " + assurance.header[0],
                            assurance, 100);
                    LesDynamic leAss = DbHandlerClinic.getDynamics(connDyn);
                    return new JList(leAss.data[0]);
                case "INT#_#VISIBLE":
                    return new JLabel("" + defaultValue);
                default: {
                    return new JTextField("" + defaultValue);
                }
            }

        } else {
            return new JTextField("" + defaultValue);
        }

    }

    JScrollPane scrollThisList(JList list)
    {
    JScrollPane rdvScroll = new JScrollPane(list);
     rdvScroll.setPreferredSize(new Dimension(100, 50));
     rdvScroll.setBackground(Color.pink);
     return rdvScroll;
    
    }
    
    String getComponentData(int index)
    {
     Component comp = liste.get(index);
    if(comp instanceof JLabel)
    { return ((JLabel)comp).getText();}
    else  if(comp instanceof JTextField)
    { return ((JTextField)comp).getText();}
    else  if(comp instanceof JList)
    { return (String)((JList)comp).getSelectedValue();}
    else   
    { return "unknown";}
            
    }
   
     
}
