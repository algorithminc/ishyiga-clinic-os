/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic;

/**
 *
 * @author kimai
 */
public class LesDynamicDrugs extends LesDynamic {
    
String ITEM_INN_RWA_CODE,ITEM_NAME,ITEM_RHIA_NAME,INN,ATC_CODE,MANUFACTURER;
double SELLING_PRICE;
int    ID_PRODUCT;
    
  String header [] = new String []{"ITEM_INN_RWA_CODE", "ITEM_NAME", 
  "INN", "ATC_CODE", "MANUFACTURER",
            "SELLING_PRICE", "ID_PRODUCT"};
    String variables [] = new String []{"STRING", "STRING","STRING", "STRING","STRING", 
            "DOUBLE", "INT"};
    
      public LesDynamicDrugs( ) { 
        super.setHeader(header);
        super.setVariables(variables);
        super.setTable("VCDC.RECEIVE_INEZA_DRUGS");
        super.setIzikenewe(new boolean []{true, true,true,true, true,true,true}, 
                new boolean []{false, false,false,false, false,true,true}, 
                new boolean []{true, true,true,true, true,true,true}); 
        int width2 = 700;
        int prefered = 300;
        int izindi = (width2-prefered)/(header.length-1);
        super.setWidth(width2);
        super.setHeight(400);
        super.setUburebure(new int []{izindi,prefered,izindi,izindi,izindi,izindi,izindi});  
        super.setIndexUpdate(0);
    }
    
    
    @Override
    public String toString()
    {
    return ITEM_NAME;
    }
}
//create table "VCDC".RECEIVE_INEZA_DRUGS
//(
//	ITEM_INN_RWA_CODE VARCHAR(50) not null primary key,
//	ITEM_NAME VARCHAR(250),
//	ITEM_RHIA_NAME VARCHAR(250),
//	INN VARCHAR(150),
//	ATC_CODE VARCHAR(150),
//	MANUFACTURER VARCHAR(100),
//	SELLING_PRICE DOUBLE default 0,
//	ID_PRODUCT INTEGER default 1
//)
