/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic;

import java.sql.PreparedStatement;
import java.sql.Statement;

/**
 *
 * @author kimai
 */
public class ConnectorDyn {
Statement s;
PreparedStatement pie;
PreparedStatement pil;
String andsql;
LesDynamic ins;
int FETCH;    

    public ConnectorDyn(Statement s, PreparedStatement pie, PreparedStatement pil, 
            String andsql, LesDynamic ins, int FETCH) {
        this.s = s;
        this.pie = pie;
        this.pil = pil;
        this.andsql = andsql;
        this.ins = ins;
        this.FETCH = FETCH;
    }

    public void setParamans(String andsql, LesDynamic ins, int FETCH) {
        this.andsql = andsql;
        this.ins = ins;
        this.FETCH = FETCH;
    }
 
}
