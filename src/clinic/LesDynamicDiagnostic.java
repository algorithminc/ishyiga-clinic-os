/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinic;

/**
 *
 * @author kimai
 */
public class LesDynamicDiagnostic extends LesDynamic {
    
    String GLOBAL_ID,HEURE,BENEFICIAL_NUMBER,DIAGNOSTIC_NOTES;
   int ID_DIAGNOSTIC;
	
    String header [] = new String []{"ID_DIAGNOSTIC", "GLOBAL_ID", 
            "HEURE","BENEFICIAL_NUMBER", "DIAGNOSTIC_NOTES","ID_VISIT"};
    String variables [] = new String []{"INT", "STRING", "STRING", 
            "STRING", "STRING","INT"};
    
     public LesDynamicDiagnostic( ) { 
        super.setHeader(header);
        super.setVariables(variables);
        super.setTable("VCDC.CLINIC_DIAGNOSTIC");
        super.setIzikenewe(new boolean []{false, true,false,true,true,true}, 
                new boolean []{false, false,false,false,true,false}, 
                new boolean []{true, true,true,true,true,true}); 
        int width2 = 700;
        int prefered = 400;
        int izindi = (width2-prefered)/(header.length-1);
        super.setWidth(width2);
        super.setHeight(400);
        super.setUburebure(new int []{izindi,izindi,izindi,izindi,prefered,izindi});  
        super.setIndexUpdate(1);
    }
    
    
    @Override
    public String toString()
    {
    return GLOBAL_ID;
    }
    
}
 
