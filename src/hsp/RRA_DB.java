/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hsp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author kimai
 */
public class RRA_DB {
    
    
     String driver = "org.apache.derby.jdbc.NetWorkDriver";
    String connectionURL;
    ResultSet ou;
    Connection conn = null;
    Connection connDATA = null;
    public Connection connBcp = null;
    public Statement STATEMENT, STATEMENTDATA, STATEMENT_EXTRA;
    PreparedStatement psInsert;
    public String server, dataBase;
    double tvaRateA, tvaRateB, tvaRateC, tvaRateD;
    String ssn, TIN4;
    public RRA_DB(String server, String dataBase, String ssn, String TIN4) {
        this.server = server;
        this.dataBase = dataBase;
        this.ssn = ssn;
        this.TIN4 = TIN4;
        connectionURL = "jdbc:derby://" + server + ":1527/" + dataBase;

        read(); 
    }
    
    
    static public String derbyPwd = ";user=sa;password=kimenyi";

    public void read() {
        try {

            System.out.println("connecting  " + connectionURL + derbyPwd);

            conn = DriverManager.getConnection(connectionURL + derbyPwd);
            connBcp = DriverManager.getConnection(connectionURL + derbyPwd);
            connDATA = DriverManager.getConnection(connectionURL + derbyPwd);
            conn.setSchema("APP");
            connBcp.setSchema("APP");
            connDATA.setSchema("APP");
            STATEMENT = conn.createStatement();
            STATEMENTDATA = connDATA.createStatement();
            STATEMENT_EXTRA = connDATA.createStatement();
            System.out.println("connection etablie TO  " + connectionURL);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "CONNECTION FAIL TO Auto " + connectionURL);
            System.out.println("Connection Fail " + e);
            System.exit(1);

        }
        //JOptionPane.showMessageDialog(null, "CONN");
    }
    
    
}
