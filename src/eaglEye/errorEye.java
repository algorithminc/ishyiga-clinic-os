/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eaglEye;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.swing.JOptionPane; 
import static sdc.comm.CommISHYIGA.KoherezaKuIgicu; 
/**
 *
 * @author ishyiga
 */
public class errorEye {

    String SDC_ID, CODE_ERROR, VERSION_ERRORED, INFO_1, INFO_2, EMAIL;
    
    public static String  algo_email_alert="aimable.kimenyi@ishyiga.info";

    public errorEye(String SDC_ID, String CODE_ERROR, String VERSION_ERRORED,
            String INFO_1, String INFO_2, String email) {
        this.SDC_ID = SDC_ID;
        this.CODE_ERROR = CODE_ERROR;
        this.VERSION_ERRORED = VERSION_ERRORED;
        this.INFO_1 = clearInfo(INFO_1);
        this.INFO_2 = clearInfo(INFO_2);
        this.EMAIL = email;
           takeADecision();
           
         
    }

    private String clearInfo(String INFO) {
        if (INFO != null) {
            INFO = INFO.replaceAll("<", "TAG_S");
            INFO = INFO.replaceAll(">", "TAG_I");

        }

        return INFO;
    }

    private void takeADecision() {

        switch (CODE_ERROR) {

            case "ERROR_MESSAGE_WRONG":
                JOptionPane.showConfirmDialog(null, CODE_ERROR + " \n " + INFO_2, "EXITING ",
                         JOptionPane.ERROR_MESSAGE);
                writeError(makeXmlResponse(), CODE_ERROR);
                exiting("ERROR_MESSAGE_WRONG");
                break;
            case "RRN_ERROR":
                JOptionPane.showConfirmDialog(null, CODE_ERROR + " \n " + INFO_2, "EXITING ",
                         JOptionPane.ERROR_MESSAGE);
                writeError(makeXmlResponse(), CODE_ERROR);
                exiting("RRN_ERROR");
                break;
            case "MISSING_VITAL_DATA":
                JOptionPane.showConfirmDialog(null, CODE_ERROR + " \n " + INFO_2, "EXITING ",
                         JOptionPane.ERROR_MESSAGE);
                writeError(makeXmlResponse(), CODE_ERROR);
                exiting("MISSING_VITAL_DATA");
                break;
            case "WRONG_SDC_ID":
                JOptionPane.showConfirmDialog(null, CODE_ERROR + " \n " + INFO_2, "EXITING ",
                         JOptionPane.ERROR_MESSAGE);
                writeError(makeXmlResponse(), CODE_ERROR);
                exiting("WRONG_SDC_ID");
                break;
            case "NEGATIVE_RNUM":
                JOptionPane.showConfirmDialog(null, CODE_ERROR + " \n " + INFO_2, "EXITING ",
                         JOptionPane.ERROR_MESSAGE);
                
                writeError(makeXmlResponse(), CODE_ERROR);
                exiting("NEGATIVE_RNUM");
                break;
            case "SKMM_FAIL_TO_START_SAFETLY":
                JOptionPane.showConfirmDialog(null, CODE_ERROR + " \n " + INFO_2, "EXITING ",
                         JOptionPane.ERROR_MESSAGE);
                writeError(makeXmlResponse(), CODE_ERROR);
                exiting("SKMM_FAIL_TO_START_SAFETLY");
                break;
            case "SKMM_WRONG_RESPONSE_FORMAT":
                JOptionPane.showConfirmDialog(null, CODE_ERROR + " \n " + INFO_2, "EXITING ",
                         JOptionPane.ERROR_MESSAGE);
                writeError(makeXmlResponse(), CODE_ERROR);
                exiting("SKMM_WRONG_RESPONSE_FORMAT");
                break;
            case "SKMM_FAIL_TO_START":
                JOptionPane.showConfirmDialog(null, CODE_ERROR + " \n " + INFO_2, "EXITING ",
                         JOptionPane.ERROR_MESSAGE);
                writeError(makeXmlResponse(), CODE_ERROR);
                exiting("SKMM_FAIL_TO_START");
                break;
            case "VCIS_ID_REQUEST_AWAKE":
//                JOptionPane.showConfirmDialog(null,CODE_ERROR+" \n "+INFO_2, "EXITING "
//                         , JOptionPane.ERROR_MESSAGE);
//                 writeError(makeXmlResponse(), CODE_ERROR);
//                 System.exit(1);
                break;
            case "FAIL_TO_START_ENGINE":
                JOptionPane.showConfirmDialog(null, CODE_ERROR + " \n " + INFO_2, "EXITING ",
                         JOptionPane.ERROR_MESSAGE);
                writeError(makeXmlResponse(), CODE_ERROR);
                exiting("FAIL_TO_START_ENGINE");
                break;
            case "VSDC_ID_REQUEST_AWAKE":
//                JOptionPane.showConfirmDialog(null, CODE_ERROR + " \n " + INFO_2, "EXITING ",
//                         JOptionPane.ERROR_MESSAGE);
                writeError(makeXmlResponse(), CODE_ERROR);
                // System.exit(1);
                break;
            case "VSDC_LIVE_UNCLASSFIED":
                // RESTART EYE
                break;
            case "EAGLE_LIVE_PUSH_RCPT_FAIL":
                // RESTART EYE
                break;
            case "EAGLE_UNABLE_TO_BUILD_RCPT":
                // RESTART EYE
                break;
            case "EAGLE_FAIL_TO_PERFORM":
                exiting("EAGLE_FAIL_TO_PERFORM");
                // RESTART EYE
                break;
            case "EAGLE_INTERRUPTED":
                // RESTART EYE
                break;
            case "LOCAL_DB_UNREACHEABLE":
                // RESTART EYE
                break;
            case "FAIL_TO_START_DERBY_EYE":
                // RESTART EYE
                break;
            case "FAIL_TO_GET_CREDENTIALS":
                JOptionPane.showConfirmDialog(null, " FAIL TO GET CREDENTIALS \n " + INFO_2, "EXITING ",
                         JOptionPane.ERROR_MESSAGE);
                writeError(makeXmlResponse(), "takeADecision");
                exiting("FAIL_TO_GET_CREDENTIALS");
                break;
            case "FAIL_TO_START_VSDC_EYE":
                // RESTART EYE
                break;
            case "VSDC_DB_UNREACHEABLE":
                // RESTART VSDC
                break;
            case "VSDC_LIVE_UNREACHEABLE":
                writeError(makeXmlResponseView(), "VSDC_LIVE_UNREACHEABLE");
                break;
            case "VSDC_LIVE_WRONG_FORMAT":
                //writeError(makeXmlResponse(), "VSDC_LIVE_WRONG_FORMAT");
                break;
            case "EMAIL_UNSENT":
                writeError(makeXmlResponseView(), "EMAIL_UNSENT");
                break;
            case "AWAKE_WEB_FAILED":
                writeError(makeXmlResponseView(), "AWAKE_WEB_FAILED");
                break;
            case "SKMM_QUEUE_EMPTY":
                writeError(makeXmlResponseView(), "SKMM_QUEUE_EMPTY");
                break;
            case "VSDC_BACKUP_FAIL":
                writeError(makeXmlResponseView(), "VSDC_BACKUP_FAIL");
                break;
            case "CIS_MODE_ERROR":
                writeError(makeXmlResponseView(), "CIS_MODE_ERROR");
                break;
            case "CIS_EXPORT_ERROR":
                writeError(makeXmlResponseView(), "CIS_EXPORT_ERROR");
                break;
            case "INVALID_TOKEN":
                writeError(makeXmlResponseView(), "INVALID_TOKEN");
                
                case "RECEIPT_DATA":
                writeError(makeXmlResponseView(), "RECEIPT_DATA");
                break;
            default:
                break;
        }

//        sendEmail();
        sendError();
        writeError(makeXmlResponseView(), CODE_ERROR);

    }

    public void sendError() {
        String valueToSend = "</header>       " + makeXmlResponse() + "  </request> ";
        String response = KoherezaKuIgicu("", "VSDC_SEND_ERROR", valueToSend);
        response = HTTP_URL.Igicu.getTagValue("RESPONSE", response);

        System.err.println(valueToSend+" \n response "+response); 
        //JOptionPane.showMessageDialog(null, "WAIT");
        writeError(valueToSend, "VSDC_SEND_ERROR_RESPONSE");
    }

    public String makeXmlResponse() {
        String response = " <ERROR> "
                + "<CODE_ERROR>" + CODE_ERROR + "</CODE_ERROR> "
                + "<VERSION_ERRORED>" + VERSION_ERRORED + "</VERSION_ERRORED> "
                + "<SDC_ID>" + SDC_ID + "</SDC_ID> "
                + "<INFO_1>" + INFO_1 + "</INFO_1> "
                + "<INFO_2>" + INFO_2 + "</INFO_2> "
                + "</ERROR> ";
        return response;

    }

    public String makeXmlResponseView() {
        String response = " <ERROR> "
                + "\n <CODE_ERROR>" + CODE_ERROR + "  </CODE_ERROR> "
                + "\n <VERSION_ERRORED>" + VERSION_ERRORED + "  </VERSION_ERRORED> "
                + "\n <INFO_1>" + INFO_1 + "  </INFO_1> "
                + "\n <INFO_2>" + INFO_2 + "  </INFO_2> "
                + "\n </ERROR> ";
        Eye.textAreaEye.append("\n " + CODE_ERROR + " " + INFO_2);
        return response;
    }

    public void sendEmail() {
        String subject = " EAGLE EYE ERROR " + CODE_ERROR + " ";
         new emailEye(algo_email_alert, subject, makeXmlResponse(), SDC_ID);
    }
   
    public static void exiting(String orig) {
        System.out.println("EXITING IN ERROR EYE "+orig);
        System.exit(0);
    }


    public static void writeError(String message, String fileName) {
        try {

            String file = "EAGLEYE/" + fileName + (new Date()).getTime() + ".txt";
            File f = new File(file);
            try (PrintWriter sorti = new PrintWriter(new FileWriter(f))) {
                sorti.println(message);
            }
        } catch (IOException ex) {
            JOptionPane.showConfirmDialog(null, ex + " writeError " + fileName + "  CONTACT ISHYIGA TEAM ");
            System.exit(0);
        }
    }
}
