/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eaglEye;

import FDI_SMS_MANAGER.SMS;
import FDI_SMS_MANAGER.SMSResponse;
import FDI_SMS_MANAGER.SMS_Manager;
import static ineza_vcdc.ODT_INTERFACE_MAIN.version;
import java.util.LinkedList;
import java.util.logging.Level; 
import static eaglEye.Eye.ISH_LOGGER;

/**
 *
 * @author amris
 */
public class smsEye {
    
    static private FDI_SMS_MANAGER.SMS_Manager smsManager;
    
    private static String className = smsEye.class.getName();

    public smsEye(String connURL) {
        if (Eye.proxyIp == null) {
            this.smsManager = new SMS_Manager(connURL, version);
        } else {
            this.smsManager = new SMS_Manager(connURL, version, Eye.proxyIp, Eye.proxyPort);
        }
        System.out.println("smsManager.isValid() "+smsManager.isValid());
        Eye.textAreaEye.append("\n Init sms manager "+smsManager.isValid());
        if(smsManager.isValid())
            ISH_LOGGER.Log("smsManager.isValid() "+smsManager.isValid(), Level.INFO, className);
        else
            ISH_LOGGER.Log("smsManager.isValid() "+smsManager.isValid(), Level.SEVERE, className);
    }
    
    public void sendFDISMS(LinkedList<String> receivers, String message) {
        
            //console+=" SMS ALERT TO SEND : "+message;
            ISH_LOGGER.Log(" Receivers size() "+receivers.size()+", SMS ALERT TO SEND : "+message, Level.INFO, className);
        if (smsManager.isValid()) {
            for (int i = 0; i < receivers.size(); i++) {
                String phone = receivers.get(i);
                System.out.println(i+" Phone "+phone);
                if(phone.length()>=10)
                    sendSMS(phone,message);
            }
            
        } else {
            String invalid = "SMS manager not valid could not send sms alert";
            //console+="\n "+invalid;
            Eye.textAreaEye.append("\n "+invalid);
            ISH_LOGGER.Log(" "+invalid, Level.WARNING, className);
        }
    }
    
    private void sendSMS(String phone, String message) {
        ISH_LOGGER.Log("sending to "+phone, Level.INFO, className);
        smsManager.sms = new SMS(phone, message, smsManager);
        SMSResponse resp = smsManager.sms.sendSMS(smsManager.getToken().getAccess_token(), smsManager.user, smsManager.db);

        if (resp != null) {
            String respMessage = resp.getMessage();
            if (resp.getSuccess()) {
                //console += "\n send sms alert success " + respMessage;
                ISH_LOGGER.Log("send sms alert success"+respMessage, Level.INFO, className);
            } else {
                //console += "\n send sms failed " + respMessage;
                Eye.textAreaEye.append("\n Alert send sms FAILED ");
                ISH_LOGGER.Log("send sms failed "+respMessage, Level.WARNING, className);
            }
        } else {
            //console += "\n SMS RESPONSE NULL ";
            ISH_LOGGER.Log("SMS RESPONSE NULL ", Level.SEVERE, className);
        }
    }
}
