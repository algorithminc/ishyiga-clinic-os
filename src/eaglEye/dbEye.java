/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eaglEye;

import static eaglEye.Eye.textAreaEye;
import static ineza_vcdc.ODT_INTERFACE_MAIN.version;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList; 
import java.util.logging.Level;
import javax.swing.JOptionPane; 
import org.apache.derby.drda.NetworkServerControl;
import static sdc.comm.CommISHYIGA.KoherezaKuIgicu; 

/**
 *
 * @author ishyiga
 */
public class dbEye {

    String databaseName;
    Statement statement, statement2, statementVcdc;
    Connection connection, connection2, conneVCDC;
    String connectionURL;
    String email;
    int sleeptimeMin;
    String ssn;
    public static credentiasEye credentials;
    public static ArrayList<String> logFile;
    private static ArrayList<String> serials;
    static private String className = dbEye.class.getName();
    public static int SERIAL_CHOSEN_INDEX =0;

    public dbEye() {
        logFile = readLog();
        serials = readSerials();
      String serial ="NA";
      
   
        try {
            if(Eye.ndiTest)
            {
               if (serials.size() > 0) {
                    serial = (String) JOptionPane.showInputDialog(null, 
                            "CHOOSE ENGINE MASTER SERIAL", "ENGINE SERIAL",
                            JOptionPane.QUESTION_MESSAGE, null,
                            serials.toArray(), 1);
                    if(serial!=null)
                    SERIAL_CHOSEN_INDEX = serials.indexOf(serial);
                } else {
                    serial = "GoogleCloud-1F75271A281B112ECB912BF3A1E1832B";
                }
                
                if(serial==null){
                    serial = getSerialNumber();
                }
            System.out.println("serial " + serial);
            textAreaEye.append("MY SERIAL : " + serial );
            this.credentials = getSecurityKeysLive("", "" +
                    version, serial,logFile); 
             
            } else {
                if (serials.size() > 0) {
                    serial = (String) JOptionPane.showInputDialog(null, 
                            "CHOOSE ENGINE MASTER SERIAL", "ENGINE SERIAL",
                            JOptionPane.QUESTION_MESSAGE, null,
                            serials.toArray(), 1);
                    if (serial != null) {
                        SERIAL_CHOSEN_INDEX = serials.indexOf(serial);
                    }
                } else {
                    serial = getSerialNumber();
                }
                System.out.println("serial " + serial);
                textAreaEye.append(" SERIAL : " + serial);
                this.credentials = getSecurityKeysLive("", "" + version, serial, logFile);
            }

            if (credentials != null) {

                this.ssn = credentials.ssn;
                String DB_PWD = credentials.DB_PWD;
                String DB_SCHEMA = credentials.DB_SCHEMA;
               
                  String derbyPwdSdc = ";user=" + DB_SCHEMA + ";password=" + DB_PWD + "";
                if (credentials.ENGINES > 1) {
                    if(credentials.isBushali()){
                        this.databaseName = credentials.databaseName + credentials.START_INDEX;//aha niho turi kujya! oky na db ntiri kongeraho i
                        //BRING BACK TO DMS  derbyPwdSdc += credentials.START_INDEX;//iyi index ubanza ariyo wise NOOODE// njye naei nayise START_INDEX oya index iragumaho NODE ni ukumenya aho ikorera
                        //muri ya selet query wakoze, I get it. so mbwira iyi motor hari aho wayikozeho?
                    } else {//ubundi yinjira aha, nuko nari nashyizeho iyo index for CVL na DMS na Yego 
                        this.databaseName = credentials.databaseName + 1;
                        ///derbyPwdSdc += "1";
                    }
                } else {
                    this.databaseName = credentials.databaseName;
                }

                String user = credentials.user;
                this.email = credentials.email;
                this.sleeptimeMin = credentials.sleeptimeMin;
              

                connectionURL = "jdbc:derby://" + credentials.server + ":1527/" + databaseName 
                        + ";bootPassword=mySuperSecretBootPassword" + derbyPwdSdc;

                //System.out.println(credentials.isBushali()+"  ------ connectionURL " + connectionURL);
                int port = 1527;
                startDbServer(port, ssn);

                connecting(connectionURL);
                //   System.out.println("connected " + connection);

            } else {
                new errorEye(ssn, "FAIL_TO_GET_CREDENTIALS", version,
                        "LIVE", "", "dev@ishyiga.info");
            }

        } catch (Throwable ex) {
            new errorEye(serial, "FAIL_TO_START_VSDC_EYE", version, "Throwable", "" + ex, "dev@ishyiga.info");
        }
    }

    public static ArrayList<String> readLog() {
        ArrayList<String> logValues = new ArrayList<>();
        File file = new File("log.txt");
        if (file.exists()) {
            //System.out.println("Log file found");
            BufferedReader br;
            try {
                br = new BufferedReader(new FileReader(file));
                String s;
                while ((s = br.readLine()) != null) {
                    logValues.add(s);
                }
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Could not locate log file\n" + ex);
            } catch (IOException ex) {
                System.err.println("ikibazo" + ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Log file does not exist in the "
                    + "specified directory\n log.txt");
            System.exit(0);
        }
        return logValues;
    }
    
    private static ArrayList<String> readSerials() {
        String serial="";
        File file = new File("ENGINES_STARTUP.xml");
        if (file.exists()) {
            //System.out.println("Log file found");
            BufferedReader br;
            try {
                br = new BufferedReader(new FileReader(file));
                String s;
                while ((s = br.readLine()) != null) {
                    serial+=(s);
                }
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Could not locate log file\n" + ex);
            } catch (IOException ex) {
                System.err.println("ikibazo" + ex);
            }
        }
        
        ArrayList<String> serials = new ArrayList<>();
        if(!serial.equals("")){
        String ssns = HTTP_URL.Igicu.getTagValue("SSN_ENGINES", serial);
        System.out.println(" file ssns "+ssns);
        String sp[]=ssns.split("</SERIAL>");
        for(String ser:sp){
            serials.add( HTTP_URL.Igicu.getTagValue("SERIAL", ser));
        }
        }
        System.out.println("serials.size() "+serials.size());
        return serials;
    }
    
    public dbEye(String connectionURL,credentiasEye credentials,String dbname)
    {
        
                this.credentials  = credentials; 
                this.databaseName = dbname;
                this.email        = credentials.email;
                this.sleeptimeMin = credentials.sleeptimeMin;
                connecting(connectionURL);
            
    }
 

    public static void startDbServer(int port, String ssn) {
        System.setProperty("derby.drda.startNetworkServer", "true");
        //stopDbServer();
        try {

            //  String 
            NetworkServerControl dbServer = new NetworkServerControl(
                    InetAddress.getLocalHost(), port);
            java.io.PrintWriter consoleWriter = new java.io.PrintWriter(
                    System.out, true);

            dbServer.start(consoleWriter);
            System.out.println("eaglEye.dbEye.startDbServer( stardet )");
            //checkForDatabase();
        } catch (Exception e) {
            new errorEye(ssn, "FAIL_TO_START_DERBY_EYE", version,
                    "Exception", "" + e, "dev@ishyiga.info");
        }
    }

    public static credentiasEye getSecurityKeysLive(String ID_COMPANY_BRANCH,
            String pwd, String mac, ArrayList<String> logFile) {

        /////// PRODUCTION
        String valueToSend = "<PASSWORD>" + pwd + "</PASSWORD> "
                + "<MAC>" + mac + "</MAC>"
                + "</header>   </request>      ";
    // System.err.println("valueToSend "+valueToSend);
    
    System.out.println("log file size() "+logFile.size());
        if(logFile.size()>10){
            Eye.proxyIp = logFile.get(10);
            try{
                Eye.proxyPort = Integer.parseInt(logFile.get(11));
            } catch(NumberFormatException ex){
                Eye.proxyIp = null;
            } catch(IndexOutOfBoundsException ex){
                Eye.proxyIp = null;
            }
        }
        
           String response = null;
           if(Eye.ndiTest){  //_TEST
               response = KoherezaKuIgicu(ID_COMPANY_BRANCH, "VSDC_GET_CREDENTIALS", valueToSend);  //VSDC_GET_CREDENTIALS_TEST
           } else {
               response = KoherezaKuIgicu(ID_COMPANY_BRANCH, "VSDC_GET_CREDENTIALS", valueToSend);
           }
        
        System.out.println("response-------------  \n "+response);
        
        if (response == null || response.contains("ERROR")) {
            if(response.contains("MAC is Not found")){
                new errorEye("", "MAC_NOT_FOUND", version,
                    "live error", response, Constants.DEV_EMAIL_ALERT);
            } else if(response.contains("Update Your Version")){
                new errorEye("", "VERSION_NOT_FOUND", version,
                    "live error", response, Constants.DEV_EMAIL_ALERT);
            }else {
                new errorEye("", "FAIL_TO_GET_CREDENTIALS", version,
                    "live error", "CHECK YOUR INTERNET CONNECTION\n\n IF THE PROBLEM PERSISTS, PLEASE INFORM ISHYIGA SUPPORT! ", Constants.DEV_EMAIL_ALERT);
            }
        } else {
            String server = "127.0.0.1";
            credentiasEye cred = new credentiasEye(response, server);
            System.out.println(" creeeeed "+cred);
            try {
                cred.server = "" + InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException ex) {
                new errorEye("", "FAIL_TO_START_DERBY_EYE", version,
                        "UnknownHostException", "" + ex, Constants.DEV_EMAIL_ALERT);
            }
            return cred;
        }
//////////TEST
//        String response = " <CREDENTIALS> "
//                + "<SSN>SDC008000004</SSN> "
//                + "<DB_PWD>tessi</DB_PWD> "
//                + "<DB_SCHEMA>sdc</DB_SCHEMA> "
//                + "<databaseName>umubano</databaseName> "
//                + "<user>ADMIN</user> "
//                + "<email>kimainyi@gmail.com</email> "
//                + "<sleeptimeMin>10000</sleeptimeMin> "
//                + "<token>1234567890123456</token> "
//                + "</CREDENTIALS> ";
//        credentiasEye cred = new credentiasEye(HTTP_URL.Igicu.getTagValue("CREDENTIALS", response));
        return null;
    }

    public static String getSerialNumber() throws IOException, InterruptedException {

        ProcessBuilder pb = new ProcessBuilder("wmic", "bios", "get", "serialnumber");
        Process process = pb.start();
        process.waitFor();
        String serialNumber = "";
        try (BufferedReader br = new BufferedReader(new InputStreamReader(
                process.getInputStream()))) {
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                if (line.length() < 1 || line.startsWith("SerialNumber")) {
                    continue;
                }
                serialNumber = line;
                break;
            }
        }
        if (serialNumber.length() > 3) {
            return serialNumber.replaceAll(" ", "");
        }

        pb = new ProcessBuilder("wmic", "baseboard", "get", "serialnumber");
        process = pb.start();
        process.waitFor();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(
                process.getInputStream()))) {
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                if (line.length() < 1 || line.startsWith("SerialNumber")) {
                    continue;
                }
                serialNumber = line;
                break;
            }
        }
        if (serialNumber.length() > 3) {
            return serialNumber.replaceAll(" ", "");
        } else {
            return getMac();
        }
    }

    public static String getMac() {
        try {
            Process p = Runtime.getRuntime().exec("getmac /fo csv /nh");
            java.io.BufferedReader in = new java.io.BufferedReader(new java.io.InputStreamReader(p.getInputStream()));
            String line;
            line = in.readLine();
            String[] result = line.split(",");
            String res = result[0].replace('"', ' ').trim();
            return res;
        } catch (IOException ex) {
            new errorEye("NA", "UNABLE_TO_READ_MAC", version, "IOException", "" + ex, "dev@ishyiga.info");
        }
        return "Disabled";
    }

    private void connecting(String connectionURL) {

        try {
            
            System.out.println(" connection conn" + connectionURL);
            connection = DriverManager.getConnection(connectionURL);
            conneVCDC = DriverManager.getConnection(connectionURL);
            credentials.DERBY_VERSION= conneVCDC.getMetaData().getDatabaseProductVersion();
            credentials.JAVA_VERSION=System.getProperty("java.version");
            
            connection.setSchema("SDC");
            //conneVCDC.setSchema("VCDC");
            System.out.println(" connected ");
            statement = connection.createStatement();
            statement2 = connection.createStatement();
            statementVcdc = conneVCDC.createStatement();
            credentials.VIEW_MODE= getParam( "VIEW_MODE",statementVcdc);
            System.out.println(" connected statment");
            
        } catch (SQLException ex) {
            System.err.println(" fail "+ex);
            Eye.textAreaEye.append("\n COULD NOT CONNECT TO DATABASE . . . ");
            Eye.textAreaEye.setForeground(Color.red);
            new errorEye(ssn, "UNABLE_TO_CONNECT_DERBY", version, "SQLException",
                    "" + ex + ";" + connectionURL, "dev@ishyiga.info");
        } 
    }
    
static public  String getParam( String nomParam,Statement stat) {
   
        try {
            String sql = "select  * from VCDC.VARIABLES where "
                    + "VCDC.variables.nom_variable= '" + nomParam + "'   ";

           // System.out.println(sql);
            ResultSet outResult = stat.executeQuery(sql); 
            while (outResult.next()) {  
               return outResult.getString(2); 
            }
        } catch (SQLException e) {
            System.err.println("getParam "+e);
        }
        return null;
    }
  
public static void updateParameter(String nom, String value,Statement stat) {
           try {
 String sql = " update VCDC.VARIABLES set value_variable ='" + value + "' "
                        + " where nom_variable='" + nom + "' ";

            System.out.println(sql);
                stat.execute(sql);
            } catch (SQLException ex) { 
                System.err.println(nom+" updateparam "+ex);
            }
        
    } 
}
