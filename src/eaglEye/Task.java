/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eaglEye;

/**
 *
 * @author ishyiga
 */
public class Task {

    int ID_TASK_REQUEST;
    String CODE_TASK, SDC_ID, VERSION_SDC,REPETION, INFO_1, INFO_2;
    String RESPONSE,CATEGORY;

    public Task(int ID_TASK_REQUEST, String CODE_TASK, String SDC_ID,
            String REPETION, String INFO_1, String INFO_2,String CATEGORY,String VERSION_SDC) {
        this.ID_TASK_REQUEST = ID_TASK_REQUEST;
        this.CODE_TASK = CODE_TASK;
        this.SDC_ID = SDC_ID;
        this.REPETION = REPETION;
        this.INFO_1 = INFO_1;
        this.INFO_2 = INFO_2;
        this.RESPONSE = "INITIATED";
        this.CATEGORY=CATEGORY;
        this.VERSION_SDC=VERSION_SDC;
    }

    public String makeXmlResponse() {
        String response = "<TASK_PERFORMANCE> "
                + "<ID_TASK_REQUEST>" + ID_TASK_REQUEST + "</ID_TASK_REQUEST> "
                + "<CODE_TASK>"       + CODE_TASK       + "</CODE_TASK> "
                + "<SDC_ID>"          + SDC_ID + "</SDC_ID> "
                + "<VSDC_VERSION>"          + VERSION_SDC + "</VSDC_VERSION> "
                + "<RESPONSE>"        + RESPONSE + "</RESPONSE> "
                + "<REPETION>"        + REPETION + "</REPETION> "
                + "<DEFINITION>HEY</DEFINITION> "
                + "<INFO_1>"       + INFO_1       + "</INFO_1> "
                + "<INFO_2>"          + INFO_2 + "</INFO_2> "
                + "  </TASK_PERFORMANCE> " ;
         
        return response;
    }
 @Override
    public String toString()
    {
    return ID_TASK_REQUEST+". "+CODE_TASK+ " "+SDC_ID ;
    }
    public String toString2()
    {
    return "ID TASK: "+ID_TASK_REQUEST+"\n CODE TASK: "+CODE_TASK
            + "\nSDC ID: "+SDC_ID+"\nSTATUS: "+RESPONSE
            +"\nINFO1: "+INFO_1+"\nINFO2: "+INFO_2 ;
    }
    
}
