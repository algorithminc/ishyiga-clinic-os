/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eaglEye;

/**
 *
 * @author Kimenyi
 */
public class RRA_RECEIVED_PACKAGE {

//Messages from the SDC to the CIS
//<01><LEN><SEQ><CMD><DATA><04><STATUS><05><BCC><03>
    public String recMsg = "";
    public String data = "";
    public String status = "";
    public String cmd = "";
    public String tin = "";

    public RRA_RECEIVED_PACKAGE(String CMD) {
        this.cmd = CMD;
    }

    public void setTin(String tin){
        this.tin = tin;
    }
    public void setData(String data) {
        this.data = data;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return "<STATUS>" + this.status + "</STATUS>";
    }

    public String getData() {
        return "<DATA>" + this.data + "</DATA>";
    }

    public String getCmd() {
        return "<CMD>" + this.cmd + "</CMD>";
    }
    
    public String getTin(){
        return "<TIN>" + this.tin + "</TIN>";
    }

    /* 
        240920 SUED returns VSDC -> VCIS Response format 
        <TIN></TIN><CMD></CMD><DATA></DATA><STATUS></STATUS>
    */
    public String getSdcResponse() {
        return getTin() + getCmd() + getData() + getStatus();
    }

    public RRA_RECEIVED_PACKAGE(String stat, String dat) {

        this.status = stat;
        this.data = dat;
        //System.out.println(dat);

    }

}
