/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eaglEye;

import java.text.SimpleDateFormat;

/**
 *
 * @author amris
 */
public class Constants {
    public final static int IMPORT_ITEM_APPROVED = 3;
    public final static int IMPORT_ITEM_REJECTED = 4;
    
    public final static String REG_TYPE_AUTO = "A";
    public final static String REG_TYPE_MANUAL = "M";
    
    //Const emails
    public static String DEV_EMAIL_ALERT = "dev@urugendo.com";//"dev@ishyiga.info"
    public static String SUP_EMAIL_ALERT = "";//"dev@ishyiga.info"
    public static String TAXPAYER_EMAIL_ALERT = "";//"dev@ishyiga.info"
    public static String RRA_EMAIL_ALERT = "vsdcsupport@rra.gov.rw";//"dev@ishyiga.info"
    //Const phones
    public static String DEV_PHONE_ALERT = "0788970708";//"0788888888"
    public static String SUP_PHONE_ALERT = "";//"0788888888"
    public static String TAXPAYER_PHONE_ALERT = "";//"0788888888"
    
    public static String ACCESS_REPORT_PWD = "ADMIN";
    public static String PLAFOND_PWD = "DANGER";
    public static String SDC_DATA_DIR = "SDCDATA";
    public static String SDC_EXPORT_DIR = "SDCEXPORT";
    
    public static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat formatterSdcDataRecovery = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    public static SimpleDateFormat formatterSdcDataBackup = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
    
    public static String clearXML(String INFO) {
        if (INFO != null) {
            INFO = INFO.replaceAll("<", "TAG_S");
            INFO = INFO.replaceAll(">", "TAG_I");
        }
        return INFO;
    }
    
    public static String cleanText(String txt) {

        if (txt == null) {
            return "";
        }
        txt = txt.replaceAll("<", "").replaceAll(">", "").replaceAll("", " ");
        txt = txt.replaceAll("'", "`");
        return txt;
    }
    
    public static final String csv_SDC_DATA_HEADER="GENERAL_COUNTER,RT_COUNTER,RTYPE,TTYPE,MRC,TIN,RNUM"
                        + ",TAXE_RATE_A,TAXE_RATE_B,TAXE_RATE_C,TAXE_RATE_D,"
                        + "TOTAL_A,TOTAL_B,TOTAL_C,TOTAL_D,"
                        + "TAXE_A,TAXE_B,TAXE_C,TAXE_D,CLIENTSTIN,DATE_TIME_SDC,"
                        + "RECEIPT_SIGNATURE,EJ,ISAHA,DATE_TIME_CIS,INTERNAL_DATA\n";
    public static final String csv_SDC_EXPORT_RECEIPT_HEADER = "INVOICEID,RECEIVED_TIME,"
                        + "SYNCED_TIME,SYNCED_RESPONSE\n";
}
