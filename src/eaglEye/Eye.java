/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eaglEye;
  
 
import clinic.Main_Clinic;
import globalaccount.Account;
import hsp.RRA_DB;
import static ineza_vcdc.DbhandlerVCDC.getAffectation;
import static ineza_vcdc.ODT_INTERFACE_MAIN.version;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Level;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
//import javax.swing.JOptionPane;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JPasswordField;
import sdc.comm.CommISHYIGA;

/**
 *
 * @author ishyiga
 */
public class Eye  {

    private int PORT_VSDC = 7771;
    private int PORT_VCIS = 9632;
    public static String ID_COMPANY_BRANCH = "";
    static public JTextArea textAreaEye = new JTextArea(5, 2);
    //public static boolean offlineMode = false;
    public dbEye db;
    public dbEye[] dbListe; 
    public Integer[] portSeries_VSDC;
    public Integer[] portSeries_VCIS;
    public static boolean iPushedZD = false;
    //private static final String log = "log.txt";
    private String dropBoxPath, dbPath, dbToBcp;
    private int countBcp = 0, countFilesDb = 0;
    private Task taskBackup;
    static private RRA_DB dbo;
    String currentDate = todaySingle();
    String currentDateStock = todaySingle();

    static public boolean ndiTest = true;
   private smsEye smsEye;
    private boolean alertedSync = false;
    static public ISHYIGA_LOGGING ISH_LOGGER;
    private static String className = Eye.class.getName();
    private boolean exported = false;

    public static String proxyIp = null;
    public static int proxyPort = 0;
  
    
    public Eye() {

        ISH_LOGGER = new ISHYIGA_LOGGING();
        textAreaEye.append(" \n STARTING VCDC ENGINE ");
        draw();

        this.db = new dbEye();
        this.smsEye = new smsEye(db.connectionURL);

        buildEngines();
        
        if (db.credentials != null
                && db.credentials.IS_INEZA.equals("YES")) {

            Eye.ID_COMPANY_BRANCH = db.credentials.ID_COMPANY_BRANCH;
            if (Eye.ID_COMPANY_BRANCH == null
                    && Eye.ID_COMPANY_BRANCH.length() < 4) {
                JOptionPane.showMessageDialog(null, " Eye.ID_COMPANY_BRANCH FORMAT EXIT"
                        + Eye.ID_COMPANY_BRANCH);
                System.exit(1);
            }
        }
    }

    public static String todaySingle() {

        Date toDay = new Date();

        String year = "" + (2000 + (toDay.getYear() - 100));//garantie de un millenaire, a changer muri 3000

        String mounth, dd;

        if ((toDay.getMonth() + 1) < 10) {
            mounth = "0" + (toDay.getMonth() + 1);
        } else {
            mounth = "" + (toDay.getMonth() + 1);
        }
        if ((toDay.getDate() + 1) < 10) {
            dd = "0" + (toDay.getDate());
        } else {
            dd = "" + (toDay.getDate());
        }

        return year + "-" + mounth + "-" + dd;

    }

    
    private void buildEngines() {

//        JOptionPane.showConfirmDialog(null, "ENGINES to run "+db.credentials);
//          JOptionPane.showConfirmDialog(null, "ENGINES to run "+db.credentials.ENGINES);
        //db.credentials.ENGINES=2;
        ISH_LOGGER.Log(" buildEngines " + db.credentials.ENGINES, Level.INFO, className);
        this.dbListe = new dbEye[db.credentials.ENGINES];
 
        this.portSeries_VSDC = new Integer[db.credentials.ENGINES];
        this.portSeries_VCIS = new Integer[db.credentials.ENGINES];

        if (db.credentials.ENGINES == 1) {
            dbListe[0] = db;
            this.portSeries_VCIS[0] = PORT_VCIS;
            this.portSeries_VSDC[0] = PORT_VSDC; 
              rehuslte(new Task(1, "REHUSTLE", db.credentials.ssn, "1", "", "", "", version));
      
        } else {
            int i = 1;
            int garukira = db.credentials.ENGINES;
            if(db.credentials.isBushali()){
                i = db.credentials.START_INDEX;
                this.dbListe = new dbEye[db.credentials.GARUKIRA_INDEX]; 
                this.portSeries_VSDC = new Integer[db.credentials.GARUKIRA_INDEX];
                this.portSeries_VCIS = new Integer[db.credentials.GARUKIRA_INDEX];
                garukira=db.credentials.GARUKIRA_INDEX;
            }
            for (; i <= garukira; i++) {

                String dbname = (db.credentials.databaseName + i);
                String derbyPwdSdc = ";user=" + db.credentials.DB_SCHEMA + ";password=" + db.credentials.DB_PWD + "" + i; //nukureba ko nukuzindi ari uku
                String connectionURL = "jdbc:derby://" + db.credentials.server + ":1527/"
                        + dbname + ";bootPassword=mySuperSecretBootPassword" + derbyPwdSdc;

                dbEye theDb = new dbEye(connectionURL, db.credentials, dbname);
                theDb.ssn = db.credentials.SSN_ENGINES[i - 1];
                this.portSeries_VCIS[i - 1] = PORT_VCIS + (i - 1);
                this.portSeries_VSDC[i - 1] = PORT_VSDC + (i - 1);
                dbListe[i - 1] = theDb; 
            }
//         JOptionPane.showMessageDialog(null, " REHUSTLE ");
            rehuslte(new Task(1, "REHUSTLE", db.credentials.ssn, "1", "", "", "", version));
        }

    }

    private void draw() {
//        Container content = getContentPane();
//        JPanel panel2 = new JPanel();
//        JScrollPane jt = new JScrollPane(textAreaEye);
//        jt.setPreferredSize(new Dimension(300, 250));
//        panel2.add(new JLabel("ISHYIGA VSDC EAGLE EYE"));
//        panel2.add(jt);
//        content.add(jt);
//
//        setSize(300, 300);
//        setVisible(true);

    }

    private void eagle2() {
        boolean done = true;
 
        System.out.println("FINANCE STATUS " + done);
        textAreaEye.append("  \n FINANCE STATUS " + done);

        if (done) {
            
            Thread t=new Thread(new Runnable() {
                @Override
                public void run() {
                    textAreaEye.append("\n SDC ID : " + db.ssn);
                    textAreaEye.append("\n Trace : " + db.databaseName);
                    textAreaEye.append("\n Email : " + db.email);
                    textAreaEye.append("\n Sleep time Min : " + db.sleeptimeMin); 
                    int interval = 1000 * 60 * db.sleeptimeMin;

                    try {
                        while (true) {

                            textAreaEye.append("\n Current Date: " + currentDate);
                            String newDate = todaySingle();
                            textAreaEye.append("\n Now Date: " + newDate);
                        
                            LinkedList<Task> lesTasks = getMyTasks(db.ssn);
                            lesTasks.forEach((leTask) -> {
                                leTask.VERSION_SDC = version;
                                if (leTask.SDC_ID.equals("ALL")) {
                                    leTask.SDC_ID = db.ssn;
                                }
                                textAreaEye.append("\n PERFORM TASK " + leTask.toString2());
                                performTaskSync(leTask, db, db.ssn);
                            });

                            textAreaEye.append("\n GOING TO SLEEP : " + db.sleeptimeMin + " Min");

                            Thread.sleep(interval);  /////issue

                        }
                    } catch (InterruptedException ex) {
                        new errorEye(db.ssn, "EAGLE_INTERRUPTED", version,
                                "InterruptedException", "" + ex, Constants.DEV_EMAIL_ALERT);

                    }
                }
            });
            t.start();
            
        } else {
            int n = JOptionPane.showConfirmDialog(null, "EXITING ISHYIGA VSDC",
                    "ATTENTION", JOptionPane.YES_NO_OPTION);
            if (n == 0) {
                errorEye.exiting("FINANCE_EXIT");
            }
        }
    }

    private boolean dateChangeStock(String today) {
        String newDate = todaySingle();
        if (!newDate.equals(today)) {
            currentDateStock = newDate;
            return true;
        }
        return false;
    }
    
    public static boolean checkPermission(String permissionToCheck,
            Statement state, Connection conn, Account umukozimushya ) {
//        System.out.println(permissionToCheck + " umukozi.AUTHORISATION  " + umukozimushya.AUTHORISATION);
//        System.out.println(permissionToCheck + " umukozi.AUTO_DEPOT  " + umukozimushya.AUTO_DEPOT);
        boolean check = umukozimushya.AUTHORISATION.contains(permissionToCheck);
        if (!check) {

            umukozimushya = globalaccount.GlobalAccount.GufataUmukozi(umukozimushya.ID_ACCOUNT,
                    umukozimushya.PWD_ACCOUNT, state, conn);
 System.out.println("umukozimushya.AUTHORISATION "+umukozimushya.AUTHORISATION);
            check = umukozimushya.AUTHORISATION.contains(permissionToCheck);
            if (!check) {
                JOptionPane.showMessageDialog(null, permissionToCheck + " NOT AUTHORISED ",
                        "ACCESS DENIED", JOptionPane.PLAIN_MESSAGE);
            }
        }
        return check;
    }

    
    public static void canaVsdc( dbEye db,String MRC, int portSDC) {
        textAreaEye.append("\n VCDC STARTING " + portSDC);
        //System.out.println("VSDC STARTING " + portSDC);
         int id = 2;//Integer.parseInt(JOptionPane.showInputDialog(null, "ID (format number)"));
//int id = 2;
            if (id != -1) {
//                JPanel pw = new JPanel();
//                JPasswordField passwordField = new JPasswordField(10);
//                JLabel label = new JLabel("PWD: ");
//                pw.add(label);
//                pw.add(passwordField);
//                passwordField.setText("");
//                JOptionPane.showConfirmDialog(null, pw);
//                Integer in_int = new Integer(passwordField.getText());
                int carte = 200;

                Account umukozi = globalaccount.GlobalAccount.GufataUmukozi(id, carte, 
                        db.statement, db.conneVCDC);

                if (umukozi != null) {
 
                    boolean isAffected = getAffectation(umukozi.BP_EMPLOYE, umukozi.TOKEN,
                            db.statement);

                    if (isAffected) {
                        db.credentials.MRC_VCIS=MRC;
                    // new   Main_Clinic(db.conneVCDC,  umukozi);
                    

                    } else {
                        JOptionPane.showMessageDialog(null, umukozi.TOKEN + "  NOT AFFECTED TO "
                                + umukozi.BP_EMPLOYE, " ERROR ",
                                JOptionPane.ERROR_MESSAGE);
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "  ACCESS DENIED (PWD NOT FOUND) ", " ERROR ",
                            JOptionPane.PLAIN_MESSAGE);
                }
            }
         
//        mngSkmm.leVsdc = new GuiSdc(ID_ENGINE,
//                new SDC_DB(db.databaseName, db.connectionURL, db.connection),
//                mngSkmm.getSkmmPeekStack(), portSDC);
        textAreaEye.append("\n VCDC STARTED UFUNGURE " + portSDC);
        //System.out.println(" VSDC STARTED " + portSDC);
    }

    public static LinkedList<Task> getMyTasks(String SDC_ID) {
        LinkedList<Task> laListe = new LinkedList<>();
        String valueToSend
                = "           <SDC_ID>" + SDC_ID + "</SDC_ID> "
                + "           </header>   </request> ";
        try {

            String xmlAll = CommISHYIGA.KoherezaKuIgicu(SDC_ID, "VSDC_GET_TASKS",
                    valueToSend);

            valueToSend += xmlAll;
            String[] spList = xmlAll.split("</TASK>");
            if (spList != null) {
                textAreaEye.append("\n COMMING TASKS LENGTH : " + spList.length);
            } else {
                textAreaEye.append("\n COMMING TASKS LENGTH : 0 ");
                return laListe;
            }

            for (String xml : spList) {

                // System.err.println("xml "+xml);
                if (xml != null && xml.contains("ID_TASK_REQUEST")) {
                    Task task = new Task(
                            Integer.parseInt(HTTP_URL.Igicu.getTagValue("ID_TASK_REQUEST", xml)),
                            HTTP_URL.Igicu.getTagValue("CODE_TASK", xml),
                            HTTP_URL.Igicu.getTagValue("SDC_ID", xml),
                            HTTP_URL.Igicu.getTagValue("REPETION", xml),
                            HTTP_URL.Igicu.getTagValue("INFO_1", xml),
                            HTTP_URL.Igicu.getTagValue("INFO_2", xml),
                            HTTP_URL.Igicu.getTagValue("CATEGORY", xml), version);
                    laListe.add(task);
                    textAreaEye.append("\n " + task);
                }

            }
        } catch (Throwable t) {
            new errorEye(SDC_ID, "VSDC_LIVE_WRONG_FORMAT", version,
                    "Throwable" + t, valueToSend, Constants.DEV_EMAIL_ALERT);
        }
        return laListe;
    }
 
//    @Override
//    protected void processWindowEvent(WindowEvent evt) {
//        if (WindowEvent.WINDOW_CLOSING == evt.getID()) {
//            //default icon, custom title
//            int n = JOptionPane.showConfirmDialog(null, "EXIT ISHYIGA VSDC  ",
//                    "ATTENTION", JOptionPane.YES_NO_OPTION);
//     
//        }
//    }

    void rehuslte(Task task) {

         
            canaVsdc( db, db.credentials.MRC_VCIS,portSeries_VSDC[0]);
         
    }

    public Task performTaskSync(Task task, dbEye db, String ssn) {
        //System.out.println("task.CATEGORY " + task.CATEGORY);
        //System.out.println("task.CODE_TASK " + task.CODE_TASK);
//        JOptionPane.showMessageDialog(null, ""+task.CODE_TASK);
        ISH_LOGGER.Log(" task.CODE_TASK " + task.CODE_TASK, Level.INFO, className);
        iPushedZD = false;
        try {
            switch (task.CATEGORY) {
                case "WINDOWS": {
                    switch (task.CODE_TASK) {
                        case "REHUSTLE":
                            rehuslte(task);
                            break;
                        case "VSDC_SHUTDOWN":
                            task.RESPONSE = "SUCCESS";
                            task.INFO_1 = "SHUTING DOWN";
                            task.INFO_2 = "" + (new Date()).toLocaleString();
                            endTask(task, db);
                            errorEye.exiting("CMD_VSDC_SHUTDOWN");
                            break; 

                        case "PULL_RECEIPT_VCIS":
                             break; 
                        case "PUSH_RECEIPT_INSURANCE": 
                            break; 
                        case "PUSH_RECEIPT_ZDAY": 
                            break; 
                        default:
                            break;
                    }
                }

                break; 
                default:
                    new errorEye(ssn, "VSDC_LIVE_UNCLASSFIED", version,
                            task.CODE_TASK, "" + task, Constants.DEV_EMAIL_ALERT);
                    break;
            }

        } catch (Throwable t) {
            new errorEye(db.ssn, "EAGLE_FAIL_TO_PERFORM", version,
                    "Throwable" + t, "" + task, Constants.DEV_EMAIL_ALERT);
            task.RESPONSE = "FAILED";
            ///// endTask(  task,   db);
        }

        return task;
    }
 
    public static boolean awakeVCIS(  String MRC, String MAC,
            int port, String ip, int sleepTIme,String tin2) {
        String toSend
                = " <ID_REQUEST_AWAKE> "
                + "<ORIGINE>EAGLE</ORIGINE> "
                + "<SLEEPTIME>" + sleepTIme + "</SLEEPTIME> "
                + "<AMOUNT>0</AMOUNT> "
                + "</ID_REQUEST_AWAKE> ";
        //System.out.println(ip + " eaglEye.Eye.awakeVCIS() " + port);
        String data = Rehustle_Response.sendRequestVCIS_Iport2("ID_REQUEST_AWAKE", 
                toSend, "", "",tin2, ip, port);
        //System.out.println(ip + " eaglEye.Eye.awakeVCIS() Rehustle_Response data: " + data);

        if (data == null) {
            return false;
        }

        if (data.contains("ERROR MESSAGE WRONG")) {
            JOptionPane.showMessageDialog(null, data + " \n EXITING ");
            new errorEye(tin2, "ERROR_MESSAGE_WRONG", version,
                    "ERROR MESSAGE WRONG", data, Constants.DEV_EMAIL_ALERT);

        }

        String SKMM_MIN_DECISION = HTTP_URL.Igicu.getTagValue("SKMM_MIN_DECISION", data);
        //System.out.println("ndi hano 1");
        if (SKMM_MIN_DECISION.equals("false")) {  
            data = Rehustle_Response.sendRequestVCIS_Iport2("ID_REQUEST_AWAKE", toSend, "", "",
                     tin2, ip, port);
            SKMM_MIN_DECISION = HTTP_URL.Igicu.getTagValue("SKMM_MIN_DECISION", data);
            if (SKMM_MIN_DECISION.equals("false")) {
                new errorEye(tin2, "VSDC_ID_REQUEST_AWAKE", version,
                        "EAGLE SWITCH FAILED", data, Constants.DEV_EMAIL_ALERT);
                new errorEye(tin2, "VSDC_ID_REQUEST_AWAKE", version,
                        "EAGLE SWITCH FAILED", data, "aimable.kimenyi@ishyiga.info");
            }
        }
        //System.out.println("ndi hano final");
        Rehustle_Response response = new Rehustle_Response("", data, SKMM_MIN_DECISION);

        // JOptionPane.showMessageDialog(null, "awakeVCIS \n "+response);
        return response.status.equals("true");
    }
 
    public static boolean awakeVSDC( String MRC, String MAC,
            int port, String ip, int sleepTIme,String tin2,String ssn) {

        String toSend
                = " <ID_REQUEST_AWAKE> "
                + "<ORIGINE>EAGLE</ORIGINE> "
                + "<SLEEPTIME>" + sleepTIme + "</SLEEPTIME> "
                + "<AMOUNT>0</AMOUNT> "
                + "</ID_REQUEST_AWAKE> ";
        System.out.println("mngSkmm cisDb gucana " +  tin2);
        String data = Rehustle_Response.sendRequestVSDC_Iport2( tin2,  ssn,
                MRC, MAC, "ID_REQUEST_AWAKE", toSend, port, ip);
        System.err.println("mngSkmm data " + data);
        if (data == null) {
            return false;
        }

          
        String SKMM_MIN_DECISION = HTTP_URL.Igicu.getTagValue("SKMM_MIN_DECISION", data);
        System.out.println("SKMM_MIN_DECISION data " + SKMM_MIN_DECISION);
        textAreaEye.append("\n SKMM_MIN_DECISION 1: " + SKMM_MIN_DECISION);
        if (SKMM_MIN_DECISION.equals("false")) { 
            data = Rehustle_Response.sendRequestVSDC_Iport2( tin2,  ssn,
                    MRC, MAC, "ID_REQUEST_AWAKE", toSend, port, ip);
            SKMM_MIN_DECISION = HTTP_URL.Igicu.getTagValue("SKMM_MIN_DECISION", data);
            textAreaEye.append("\n SKMM_MIN_DECISION 2: " + SKMM_MIN_DECISION);
            if (SKMM_MIN_DECISION.equals("false")) {
                new errorEye( ssn, "VSDC_ID_REQUEST_AWAKE", version,
                        "EAGLE SWITCH FAILED", data, Constants.DEV_EMAIL_ALERT);
                new errorEye( ssn, "VSDC_ID_REQUEST_AWAKE", version,
                        "EAGLE SWITCH FAILED", data, "aimable.kimenyi@ishyiga.info");
            }
        }

        Rehustle_Response response = new Rehustle_Response("", data, SKMM_MIN_DECISION);

        // JOptionPane.showMessageDialog(null, response.data);
        if (response.status.equals("true")) {
            return true;
        } else {
            new errorEye( ssn, "VSDC_ID_REQUEST_AWAKE", version,
                    "EAGLE", response.data, Constants.DEV_EMAIL_ALERT);
            return false;
        }
    }

    public static void endTask(Task task, dbEye db) {

        String valueToSend
                = "</header>" + task.makeXmlResponse()
                + "</request> ";
        CommISHYIGA.KoherezaKuIgicu(db!=null?db.databaseName:"", "VSDC_SEND_TASK_PERFORMANCE", valueToSend);
    }

    private void endTaskBackup(Task task, dbEye db) {

        String valueToSend
                = "</header>" + task.makeXmlResponse()
                + "</request> ";

        System.out.println("eaglEye.Eye.endTask()" + valueToSend);

        String res = CommISHYIGA.KoherezaKuIgicu(db.databaseName, "BACKUP_SEND_TASK_PERFORMANCE", valueToSend);

        System.out.println("res  " + res);

    }

    public static void canaVcis2(credentiasEye vcis, String dbName, int portCis,
            int portSDC, String mrc,int NODE) {
        //System.out.println("VSDC Controller");

        try {
            //System.out.println("cana 1 vcis");
            textAreaEye.append("\n VCIS STARTING");
            //System.out.println("cana 2 vcis " + dbName);
            dbo = new RRA_DB(vcis.server, dbName, vcis.ssn, vcis.tin);

            //System.out.println(mngSkmm + "  cana 3 vcis  " + vcis);
            //System.out.println(dbo + "  cana 3 vcis v " + vcis.user);
//            mngSkmm.leVcis = new RraSoftIshyiga(dbo, mngSkmm.leVsdc.db2, "hello", 
//                    vcis.VCIS_MODEL,
//                    portCis, portSDC,NODE);
            // JOptionPane.showMessageDialog(null, "BITE BYAWE");
            //System.out.println("cana 4 vcis");
            textAreaEye.append("\n HSP TO DO STARTED " + dbName + "  " + vcis.VCIS_MODEL);
            //System.out.println("sdc controller started");
        } catch (Throwable te) {
            JOptionPane.showMessageDialog(null, " canaVcisIntegration failed " + te);

            //System.out.println(".doInBackground()" + te);
        }

    }
 
    static String freeMemory() {
        try {
            OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
            for (Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
                method.setAccessible(true);
                if (method.getName().startsWith("get")
                        && Modifier.isPublic(method.getModifiers())) {
                    Object value;
                    try {
                        value = method.invoke(operatingSystemMXBean);
                    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                        value = e;
                    } // try 
                    if (method.getName().equals("getFreePhysicalMemorySize")) {
                        //System.out.println(method.getName() + " = " + value);
                        return "" + ((Long.parseLong("" + value)) / (1024 * 1024));
                    }
                } // if
            }
        } catch (Throwable e) {
            return "Throwable " + e;
        } // try 
        return "";
    }

    public static String printUsage2() {

        OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();

        long getTotalPhysicalMemorySize = 1;
        long getFreePhysicalMemorySize = 0;

        for (Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
            method.setAccessible(true);
            if (method.getName().startsWith("get")
                    && Modifier.isPublic(method.getModifiers())) {
                Object value;
                try {
                    value = method.invoke(operatingSystemMXBean);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    value = e;
                } // try 
                if (method.getName().equals("getFreePhysicalMemorySize")) {
                    getFreePhysicalMemorySize = Long.parseLong("" + value);
                }
                if (method.getName().equals("getTotalPhysicalMemorySize")) {
                    getTotalPhysicalMemorySize = Long.parseLong("" + value);
                }

                //System.out.println(method.getName() + " = " + value);
            } // if
        } // for
        double bitToMegaBit = 1024 * 1024;
        double free = (getFreePhysicalMemorySize / bitToMegaBit);
        double totalMemorie = (getTotalPhysicalMemorySize / bitToMegaBit);
        int percentage = (int) ((free / totalMemorie) * 100.0);
        int heapSize = (int) (Runtime.getRuntime().totalMemory() / bitToMegaBit);
        String decision = "SUCCESS";
        if (percentage < 5 && heapSize < (free)) {
            decision = "PHYSICAL_MEMORY_NOT_ENOUGH";
        }
        String response = "<FREE_RAM>" + (int) free + "</FREE_RAM>"
                + "<TOTAL_RAM>" + (int) totalMemorie + "</TOTAL_RAM>"
                + "<FREE_PERCENTAGE>" + (int) percentage + "</FREE_PERCENTAGE>"
                + "<HEAP_SIZE>" + (int) heapSize + "</HEAP_SIZE>"
                + "<DECISION>" + decision + "</DECISION>"
                + "<VSDC_VERSION>" + version + "</VSDC_VERSION>";
        return response;
    }

    private void backup2() {
        System.err.println("backing up");

        dbToBcp = db.credentials.databaseName;
        long repeatMinute;
        if (db.logFile.get(9) != null) {
            repeatMinute = Long.parseLong(db.logFile.get(9));
        } else {
            repeatMinute = 180;
        }

        taskBackup = new Task(
                1,
                "VSDC_BACKUP",
                db.credentials.ssn,
                "" + repeatMinute,
                db.credentials.databaseName,
                db.logFile.get(5),
                "WINDOWS", version);

        if (db.logFile.get(4) != null && db.logFile.get(5) != null) {

            dbPath = db.logFile.get(4) + "\\" + dbToBcp;//directory or file to backup
            dropBoxPath = db.logFile.get(5) + "\\" + dbToBcp + todayDate() + "auto.zip";//aho tuyishyira n'izina ryayo

            //System.out.println("To backup: " + dbPath);
            //System.out.println("Dest Path name: " + dropBoxPath);
            ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
            Runnable periodicTask = () -> {
                taskBackup = new Task(
                        1,
                        "VSDC_BACKUP",
                        db.credentials.ssn,
                        "" + repeatMinute,
                        db.credentials.databaseName,
                        db.logFile.get(5),
                        "WINDOWS", version);
                countFilesDb = 0;
                countBcp++;
                textAreaEye.append("\n " + countBcp + ". BACKUP STARTING");
                zipDirectory(new File(dbPath), dropBoxPath, taskBackup, "DATABASE");
                textAreaEye.append("\n " + countBcp + ". BACKUP STARTED");
                textAreaEye.append("\n BACKUP REPEAT " + repeatMinute + " Min");

                endTask(taskBackup, db);
                String subject = "VSDC EAGLE EYE BACKUP: " + taskBackup.SDC_ID + " ";
                new emailEye(errorEye.algo_email_alert, subject,
                        taskBackup.toString2(), taskBackup.SDC_ID);
            };
            executor.scheduleAtFixedRate(periodicTask, 1, repeatMinute, TimeUnit.MINUTES);

            /////////////////ZIP SDC DATA FOLDER/////////////////
            try {
                String sdcDataPath = System.getProperty("user.dir") + "\\"
                        + Constants.SDC_DATA_DIR;
                String backupPath = db.logFile.get(5) + "\\" + Constants.SDC_DATA_DIR
                        + todayDate() + "auto.zip";
                ISH_LOGGER.Log("SDCDATA PATH: " + sdcDataPath
                        + "<br>BACKUP PATH: " + backupPath, Level.INFO, className);
                zipDirectory(new File(sdcDataPath), backupPath, taskBackup, "SDCDATA");
                ISH_LOGGER.Log("BACKUP SDCDATA SUCCESS", Level.INFO, className);
            } catch (SecurityException ex) {
                ISH_LOGGER.Log("FAILED BACKUP SDCDATA: <br> " + ex, Level.SEVERE, className);
            }
        } else {
            System.err.println("backup yanze");
            taskBackup.RESPONSE = "VSDC_BACKUP_FAIL LOG FILE VALUES";
            endTaskBackup(taskBackup, db);
            new errorEye(db.credentials.ssn, "VSDC_BACKUP_FAIL", version,
                    "Fail", "LOG FILE VALUES", errorEye.algo_email_alert);
            endTask(taskBackup, db);
        }
    }

    private String todayDate() {
        Date toDay = new Date();
        String year = "" + (2000 + (toDay.getYear() - 100));//garantie de un millenaire, a changer muri 3000
        String mounth, dd;

        if ((toDay.getMonth() + 1) < 10) {
            mounth = "0" + (toDay.getMonth() + 1);
        } else {
            mounth = "" + (toDay.getMonth() + 1);
        }
        if ((toDay.getDate() + 1) < 10) {
            dd = "0" + (toDay.getDate());
        } else {
            dd = "" + (toDay.getDate());
        }
        return year + "-" + mounth + "-" + dd;
    }

    private void zipDirectory(File dir, String zipDirName, Task taskBackup, String flag) {
        try {
            java.util.List<String> filesListInDir = new ArrayList<>();

            System.out.println("NTANGIRE COUNT ");
            int dbFiles = populateFilesList(dir, filesListInDir);
            System.out.println("dbfilesssssss " + dbFiles);
            if (dbFiles == 0) {
                if (flag.equals("DATABASE")) {
                    taskBackup.RESPONSE = "VSDC_DB_BACKUP_FAIL DB FILES NSANZE ARI 0";
                    new errorEye(db.credentials.ssn, "VSDC_DB_BACKUP_FAIL", version,
                            "BACK UP FAILED", "DB FILES NI 0", errorEye.algo_email_alert);
                } else {
                    taskBackup.RESPONSE = "VSDC_DATA_BACKUP_FAIL FILES NSANZE ARI 0";
                    new errorEye(db.credentials.ssn, "VSDC_DATA_BACKUP_FAIL", version,
                            "BACK UP FAILED", "DATA FILES NI 0",  errorEye.algo_email_alert);
                }
                return;
            }
            try ( //now zip files one by one
                    //create ZipOutputStream to write to the zip file
                    FileOutputStream fos = new FileOutputStream(zipDirName); ZipOutputStream zos = new ZipOutputStream(fos)) {
                for (String filePath : filesListInDir) {
                    //System.out.println("Zipping " + filePath);
                    //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
                    ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length() + 1, filePath.length()));
                    zos.putNextEntry(ze);
                    try ( //read the file and write to ZipOutputStream
                            FileInputStream fis = new FileInputStream(filePath)) {
                        byte[] buffer = new byte[1024];
                        int len;
                        while ((len = fis.read(buffer)) > 0) {
                            zos.write(buffer, 0, len);
                        }
                        zos.closeEntry();
                        taskBackup.RESPONSE = "BACKED";
                    }
                }
            }
            ZipFile zipFile = new ZipFile(zipDirName);
            taskBackup.INFO_2 += "#" + zipFile.size() + "#" + dbFiles;
        } catch (IOException e) {
            System.err.println("path to db irapfuye");
            if (flag.equals("DATABASE")) {
                taskBackup.RESPONSE = "VSDC_DB_BACKUP_FAIL " + e;
                new errorEye(db.credentials.ssn, "VSDC_DB_BACKUP_FAIL", version,
                        "DB BACK UP FAILED", "" + e,  errorEye.algo_email_alert);
            } else {

                taskBackup.RESPONSE = "VSDC_DATA_BACKUP_FAIL " + e;
                new errorEye(db.credentials.ssn, "VSDC_DATA_BACKUP_FAIL", version,
                        "DATA BACK UP FAILED", "" + e,  errorEye.algo_email_alert);
            }
            JOptionPane.showMessageDialog(null, "BACK UP FAILED PLEASE INFORM ISHYIGA SUPPORT! "
                    + "\n IOException " + e);

        }
    }

    private int populateFilesList(File dir, java.util.List<String> filesListInDir) {
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                countFilesDb++;
                filesListInDir.add(file.getAbsolutePath());
            } else {
                populateFilesList(file, filesListInDir);
            }
        }
        return countFilesDb;
    }
 

    public static void main(String[] arghs) {
        new Eye();
    }
}
