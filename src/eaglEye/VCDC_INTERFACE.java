/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eaglEye;

import static eaglEye.Eye.ISH_LOGGER;
import static eaglEye.Eye.canaVsdc;
import ineza_vcdc.DbhandlerVCDC;
import static ineza_vcdc.DbhandlerVCDC.getPatient;
import ineza_vcdc.Jobber;
import static ineza_vcdc.Jobber.injizaAffectation;
import static ineza_vcdc.Jobber.injizaHSP;
import static ineza_vcdc.Jobber.injizaInstruction;
import static ineza_vcdc.Jobber.injizaItem;
import static ineza_vcdc.Jobber.injizaPrescriber;
import ineza_vcdc.SMS;
import ineza_vcdc.Verifier;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel; 
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;

/**
 *
 * @author kimai
 */
public class VCDC_INTERFACE extends JFrame  {
    
   
    public dbEye db;
    
    JTextArea textArea = new JTextArea();
    JButton rama,mmi,sanlam,uap,britam,ineza,radiant,unr;
    
    
    public VCDC_INTERFACE() {

        ISH_LOGGER = new ISHYIGA_LOGGING();
        textArea.append(" \n STARTING VCDC ENGINE ");
        draw();

        this.db = new dbEye(); 
        buildEngines();
        rebaNet();
        getIneza();
        getRama();
    } 
    
       int PORT_VSDC = 7778;
       int PORT_VCIS = 5551;
      public dbEye[] dbListe; 
      public Integer[] portSeries_VSDC;
      public Integer[] portSeries_VCIS;
    
    private void buildEngines() {

//        JOptionPane.showConfirmDialog(null, "ENGINES to run "+dbEye.credentials);
//          JOptionPane.showConfirmDialog(null, "ENGINES to run "+dbEye.credentials.ENGINES);
        //dbEye.credentials.ENGINES=2;
      //  ISH_LOGGER.Log(" buildEngines " + dbEye.credentials.ENGINES, Level.INFO, className);
        this.dbListe = new dbEye[dbEye.credentials.ENGINES];
 
        this.portSeries_VSDC = new Integer[dbEye.credentials.ENGINES];
        this.portSeries_VCIS = new Integer[dbEye.credentials.ENGINES];

        if (dbEye.credentials.ENGINES == 1) {
            dbListe[0] = db;
            this.portSeries_VCIS[0] = PORT_VCIS;
            this.portSeries_VSDC[0] = PORT_VSDC; 
              rehuslte();
      
        } else {
            int i = 1;
            int garukira = dbEye.credentials.ENGINES;
            if(dbEye.credentials.isBushali()){
                i = dbEye.credentials.START_INDEX;
                this.dbListe = new dbEye[dbEye.credentials.GARUKIRA_INDEX]; 
                this.portSeries_VSDC = new Integer[dbEye.credentials.GARUKIRA_INDEX];
                this.portSeries_VCIS = new Integer[dbEye.credentials.GARUKIRA_INDEX];
                garukira=dbEye.credentials.GARUKIRA_INDEX;
            }
            for (; i <= garukira; i++) {

                String dbname = (dbEye.credentials.databaseName + i);
                String derbyPwdSdc = ";user=" + dbEye.credentials.DB_SCHEMA + ";password=" + dbEye.credentials.DB_PWD + "" + i; //nukureba ko nukuzindi ari uku
                String connectionURL = "jdbc:derby://" + dbEye.credentials.server + ":1527/"
                        + dbname + ";bootPassword=mySuperSecretBootPassword" + derbyPwdSdc;

                dbEye theDb = new dbEye(connectionURL, dbEye.credentials, dbname);
                theDb.ssn = dbEye.credentials.SSN_ENGINES[i - 1];
                this.portSeries_VCIS[i - 1] = PORT_VCIS + (i - 1);
                this.portSeries_VSDC[i - 1] = PORT_VSDC + (i - 1);
                dbListe[i - 1] = theDb; 
             }
            
//         JOptionPane.showMessageDialog(null, " REHUSTLE ");
            rehuslte();
        }

    }

    
    void rehuslte( ) {  
            canaVsdc( db, dbEye.credentials.MRC_VCIS,portSeries_VSDC[0]); 
    }
    
    private void draw() {
        Container content = getContentPane();
        JPanel panel2 = new JPanel();
        JScrollPane jt = new JScrollPane(textArea);
        jt.setPreferredSize(new Dimension(300, 250));
        panel2.add(new JLabel("ISHYIGA VCDC EAGLE EYE"));
        panel2.add(jt);
        panel2.add(getInsurance());
        
        content.add(panel2);

        setSize(300, 600);
        setVisible(true); 
    }

    private JPanel getInsurance()
    {
     JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(4, 2));
        panel.setPreferredSize(new Dimension(300, 250)); 
        
        rama = new JButton(); rama.setIcon( new ImageIcon("image/insurance/rama.PNG")); panel.add(rama) ; 
        mmi = new JButton(); mmi.setIcon( new ImageIcon("image/insurance/mmi.PNG")); panel.add(mmi) ; 
        sanlam = new JButton(); sanlam.setIcon( new ImageIcon("image/insurance/sanlam.PNG")); panel.add(sanlam) ; 
        uap = new JButton(); uap.setIcon( new ImageIcon("image/insurance/uap.PNG")); panel.add(uap) ; 
        britam = new JButton(); britam.setIcon( new ImageIcon("image/insurance/britam.PNG")); panel.add(britam) ; 
        ineza = new JButton(); ineza.setIcon( new ImageIcon("image/insurance/ineza.PNG")); panel.add(ineza) ; 
        radiant = new JButton(); radiant.setIcon( new ImageIcon("image/insurance/radiant.PNG")); panel.add(radiant) ; 
        unr = new JButton(); unr.setIcon( new ImageIcon("image/insurance/unr.png")); panel.add(unr) ; 
         
        
    return  panel;
    }
    
 
    RRA_RECEIVED_PACKAGE VCDC (String CMD)
  {
  
      System.out.println("ndajeeee \n  "+CMD);
      
     String toSend = CMD.replaceAll("'", " ");
        
     // VCDC_GET_AFFILIATE#123456#corneille#123456@ @ RAMA
     
     
     try
     {
            String sps[] = toSend.split("@ @");
      
            String sp []  =  sps[0].split("#");
      
            CMD= sp[0];
            
         System.out.println("eaglEye.VCDC_INTERFACE.VCDC() CMD "+CMD);
           
         System.out.println("eaglEye.VCDC_INTERFACE.VCDC() CMD "+sp[1]);
         
         textArea.append("\n "+CMD);
         
  switch (CMD) { 
      
                case "VCDC_GET_AFFILIATE_ONE": { 
                    System.out.println(" ndimooooo affiliate"+sp[1]);
                    
                    String st = Jobber.injizAbarwayiONE5(sp[1], db.connection, sps[1]);
                            
                    if(st== null ||  st.contains("ERROR"))
                    {
                        st =  getPatient(sp[1],  db.statement,db.connection);
                    }   
                     
                    System.out.println(" reponse kuri Marc tumusubije"+st);
                     System.out.println(" reponse tumusubije"+st);
                    return new RRA_RECEIVED_PACKAGE("P", "" + st);
                }
                 case "VCDC_GET_ITEM_ONE": { //API not yet available
                    System.out.println(" ndimooooo affiliate"+sp[1]);
                    String st= DbhandlerVCDC.getItem(sp[1], db.statement);   
                    //String st="YIMUKIYE MUNZU YAYO";
                    System.out.println(" reponse tumusubije"+st);
                    return new RRA_RECEIVED_PACKAGE("P", "" + st); 
                }
                 case "VCDC_GET_PRESCRIBER_ONE": { 
                    System.out.println(" ndimooooo prescriber"+sp[1]);
                    String st= DbhandlerVCDC.getPrescriber(sp[1], db.statement); 
                     System.out.println(st+" \n ddd ndimooooo prescriber  "+sp[1]);
                   
                    return new RRA_RECEIVED_PACKAGE("P", "" + st);
                }
             case "VCDC_GET_PRESCRIBER_ALL": { 
                    System.out.println(" ndimooooo prescriber"+sp[1]);
                    String st= DbhandlerVCDC.getPrescriberALL(sp[1], db.statement); 
                     System.out.println(st+" \n ddd ndimooooo prescriber  "+sp[1]);
                   
                    return new RRA_RECEIVED_PACKAGE("P", "" + st);
                }
               case "VCDC_GET_ACCEPTANCE_RATE": { 
                    System.out.println(" ndimooooo rate"+sp[1]);
                    Verifier ver = new Verifier(db.statement) ;
                    
                    String st= ver.receipt_verified(sp[1]); 
                     System.out.println(st+" \n ddd ndimooooo prescriber  "+sp[1]);
                   
                    return new RRA_RECEIVED_PACKAGE("P", "" + st);
                }
             
                 case "VCDC_GET_ITEM_INSTRUCTIONS":
                 { 
                     
                     String HSP_CODE= sp[0];
                     String BENEFICIARY_NUMBER = sp[1];
                     String INN_RWA_CODE = sp[2]; 
                      System.out.println(" jdjd ");
                     String specialite  =sp[4];
                      System.out.println(" jdjd ");
                     String gender = sp[5];
                     int weigt = Integer.parseInt(sp[3]) ;
                     int age   = Integer.parseInt(sp[6]);
                     System.out.println(" jdjd ");
                     
                     try{
                         
                         String vin = "YIMUKIYE MUNZU YAYO";
                     System.out.println(" jdjd "+vin);
                  return new RRA_RECEIVED_PACKAGE("P", "" + vin.replaceAll("\n", "#"));
                  
                     } catch (Exception ex) {
            System.out.println(" Exception " + ex);
            return new RRA_RECEIVED_PACKAGE("E", "ERROR "+ex);
        } 
                 } 
                case "VCDC_GET_HEALTHFACILITY_ONE": { //API NOT YET AVAILAB
                    System.out.println(" ndimooooo HSp");
                    String st= DbhandlerVCDC.getRECEIVE_INEZA_HSP(sp[1], db.statement);
                    System.out.println(st+" \n ddd ndimooooo prescriber  "+sp[1]);
                    
                    return new RRA_RECEIVED_PACKAGE("P", "" + st);
                }
                   case "VCDC_GET_FINANCIAL": { 
                    System.out.println(" NDAJEE FINANCIAL"+sp[1]);
                    
                    String st ;
                    
                    try{
                        
                       
                      double amount=Double.parseDouble(sp[1]);
                    st= DbhandlerVCDC.getOpenSms(amount, db.statement); 
                    System.out.println(st+" \n ddd ndimooooo FINANCIAL  "+sp[1]);
                    }
                    catch(NumberFormatException n){  st=sp[2]+" NOT AN AMOUNT ";}
                     
                    return new RRA_RECEIVED_PACKAGE("P", "" + st);
                }
      default:
            return new RRA_RECEIVED_PACKAGE("E", "ERROR NDAYIBUZE");
        
            
  }
    
     } catch (Exception ex)
     {
         System.err.println(" ------  "+ex);
     }
     
      return new RRA_RECEIVED_PACKAGE("E", "ERROR NDAYIBUZE");
     
  }   
    
  
     private void getRama() {
        rama.addActionListener((ActionEvent e) -> {
            if (e.getSource().equals(rama) ) {
           viewVCDC ("RAMA");
            } 
        });
    }
        private void getIneza() {
        ineza.addActionListener((ActionEvent e) -> {
            if (e.getSource().equals(ineza) ) {
           viewVCDC ("INEZA");
            } 
        });
    } 
    
  void viewVCDC (String ASS)
  {
  
      
       String OPTIONS = (String) JOptionPane.showInputDialog(null,"CHOOSE", " ",
      JOptionPane.QUESTION_MESSAGE, null, new String[]{
          "VIEW DATA","DOWNLOAD" }, "VIEW DATA");
      
      if(OPTIONS==null)
      {
      
      JOptionPane.showMessageDialog(null, "CHOOSE ONE OPTION PLEASE", "ERROR IN ENTRY", JOptionPane.ERROR_MESSAGE);
      }
      else
      {
           if(OPTIONS.equals("DOWNLOAD"))
          {
              String CMD = (String) JOptionPane.showInputDialog(null,"CHOOSE", " ",
      JOptionPane.QUESTION_MESSAGE, null, new String[]{
          "VCDC_GET_AFFECTATION",
          "VCDC_GET_ITEM",
          "VCDC_GET_PRESCRIBER","VCDC_GET_HEALTHFACILITY","VCDC_GET_ITEM_INSTRUCTIONS"
      }, "VCDC_GET_PRESCRIBER");
               
              
              
                switch (CMD) { 
      
                case "VCDC_GET_AFFECTATION": {  
                  String res= injizaAffectation(db.connection); 
                 // JOptionPane.showMessageDialog(null, " "+st); 
                }
                 case "VCDC_GET_ITEM": {   
                    String st= injizaItem(db.connection, ASS);
                  //  JOptionPane.showMessageDialog(null, " "+st);
                }
                 case "VCDC_GET_PRESCRIBER": {  
                      String res= injizaPrescriber(db.connection); 
                 // JOptionPane.showMessageDialog(null, " "+st); 
                }
                   case "VCDC_GET_HEALTHFACILITY": {   
                    String st= injizaHSP(db.connection);
                  //  JOptionPane.showMessageDialog(null, " "+st);
                }
                 case "VCDC_GET_ITEM_INSTRUCTIONS": {  
                      String res= injizaInstruction(db.connection); 
                 // JOptionPane.showMessageDialog(null, " "+st); 
                }
             
      default:
            JOptionPane.showMessageDialog(null, " NDAYIBUZE");
        
            
  }
          }   
           else if(OPTIONS.equals("VIEW DATA"))
          {String CMD = (String) JOptionPane.showInputDialog(null,"CHOOSE", " ",
      JOptionPane.QUESTION_MESSAGE, null, new String[]{
          "VCDC_GET_AFFILIATE_ONE","VCDC_GET_FINANCIAL","VCDC_GET_HEALTHFACILITY_ONE",
                           "VCDC_GET_PRESCRIBER_ALL","VCDC_GET_PRESCRIBER_ONE","VCDC_GET_ITEM_ONE"
      }, "VCDC_GET_AFFILIATE_ONE");
      
       String data = JOptionPane.showInputDialog(null, "STRUCTURE NAME TO SEARCH");
      try
     {  
  switch (CMD) { 
      
                case "VCDC_GET_AFFILIATE_ONE": {  
                    String st = Jobber.injizAbarwayiONE5(data, db.connection, ASS);
                            
                    if(st== null ||  st.contains("ERROR"))
                    {
                        st =  getPatient(data,  db.statement,db.connection);
                    }   
                     
                    System.out.println(" reponse kuri Marc tumusubije"+st);
                     System.out.println(" reponse tumusubije"+st);
                     
                  JOptionPane.showMessageDialog(null, " "+st);
                     
                }
                 case "VCDC_GET_ITEM_ONE": { //API not yet available
                    
                    String st= DbhandlerVCDC.getItem(data, db.statement);   
                    //String st="YIMUKIYE MUNZU YAYO";
                    System.out.println(" reponse tumusubije"+st);
                    JOptionPane.showMessageDialog(null, " "+st);
                }
                 case "VCDC_GET_PRESCRIBER_ONE": {  
                    String st= DbhandlerVCDC.getPrescriber(data, db.statement); 
                     JOptionPane.showMessageDialog(null, " "+st);
                }
             case "VCDC_GET_PRESCRIBER_ALL": {  
                    String st= DbhandlerVCDC.getPrescriberALL(data, db.statement);  
                   
                    JOptionPane.showMessageDialog(null, " "+st);
                }
                 case "VCDC_GET_ITEM_INSTRUCTIONS":
                 { 
                     
                     
                 } 
                case "VCDC_GET_HEALTHFACILITY_ONE": { //API NOT YET AVAILAB
                    System.out.println(" ndimooooo HSp");
                    String st= DbhandlerVCDC.getRECEIVE_INEZA_HSP(data, db.statement);
                    
                    JOptionPane.showMessageDialog(null, " "+st);
                }
                   case "VCDC_GET_FINANCIAL": { 
                     
                    String st ;
                    
                    try{
                        
                       
                      double amount=Double.parseDouble(data);
                    st= DbhandlerVCDC.getOpenSms(amount, db.statement);  
                    }
                    catch(NumberFormatException n){  st=data+" NOT AN AMOUNT ";}
                     
                    JOptionPane.showMessageDialog(null, " "+st);
                }
      default:
            JOptionPane.showMessageDialog(null, " NDAYIBUZE");
        
            
  }
    
     } catch (Exception ex)
     {
         System.err.println(" ------  "+ex);
     }
      }
      
  }
     
  }
  
  public static String separateurLine="==";
  
    public void traitements(Socket socket) {
        try {

            System.out.println("Connexion avec le client : " + socket.getInetAddress());
            String currentCommunication = "\n Connexion avec le client : " + socket.getInetAddress();
            textArea.append("\n-----------session-------------");
            textArea.append(currentCommunication);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            //FORMAT  CMD##DATA
            String message = "";

            String line = in.readLine();
               message += line;
            while (line != null) {
                message +=separateurLine+ line;
                System.out.println("Message: " + message);

                if (line.contains("<--TAIL-->") || line.contains("@ @")) {
                    message=message.replace("<--TAIL-->", "");
                    break;
                } 
                line = in.readLine(); 
            }
            
            System.out.println("Message.............: " + message); 
            
           if(message.contains("Saurce:"))
           { SMS sms = new SMS(message);
         
            boolean done=   DbhandlerVCDC.insertSMSfincial(sms, db.conneVCDC);
            System.out.println("done "+done);
            
          System.out.println(sms);}
            
//            textArea.append("\n message : " + message);
             
            message = message.replace("<--TAIL-->", "");
            
           RRA_RECEIVED_PACKAGE res = VCDC (message);
          
            try (PrintStream out = new PrintStream(socket.getOutputStream())) {
                out.println(res.getSdcResponse());
            }
            textArea.append("\n-----------End-------------");
            socket.close();
            
        } catch (IOException e) {
            System.out.println(e + " Exception ");
            textArea.append("\n Exception: " + e);
        }
    }
 
      void rebaNet() {
        textArea.append("Starting   ");

        final SwingWorker<String, String> worker = new SwingWorker<String, String>() {
            @Override
            protected String doInBackground() throws Exception {

                try {

                    ServerSocket socketServeur = new ServerSocket(PORT_VCIS);
                    System.out.println("Lancement du serveur");

//                    textArea.append("\n PORT  : " + PORT);
                    textArea.append("\n IP  : " + InetAddress.getLocalHost().getHostAddress());
                    textArea.append("\n PORT_VCIS  : " + PORT_VCIS);
                   
                    while (true) {
                        ///  Thread.sleep(80000);
                        Socket socketClient = socketServeur.accept();
                        traitements(socketClient);

                    }
                } catch (IOException e) {
                    textArea.append("SERVER SOCKET " + e);
                    System.err.println("Communication fatal: " + e);
                }

                return " ";
            }

            @Override
            protected void done() {
                textArea.append("  \n Result is...");
            }

            @Override
            protected void process(java.util.List<String> chunks) {
                // Get the latest result from the list
                String latestResult = chunks.get(chunks.size() - 1);
                textArea.append("  \n Result is..." + latestResult);
            }
        };
        worker.execute();  // start the worker thread 
        textArea.append("\n Running...");
        worker.addPropertyChangeListener((PropertyChangeEvent evt) -> {
            if (evt.getPropertyName().equals("progress")) {  // check the property name
//pbWorker.setValue((Integer)evt.getNewValue());  // update progress bar
            }
        });
    } 
      
        @Override
    protected void processWindowEvent(WindowEvent evt) {
        if (WindowEvent.WINDOW_CLOSING == evt.getID()) {
            //default icon, custom title
            int n = JOptionPane.showConfirmDialog(null, "VCDC",
                    "ATTENTION", JOptionPane.YES_NO_OPTION);
            if (n == 0) { 
                System.exit(0);
            }
        }
    }
      
     public static void main(String[] arghs) {
        new VCDC_INTERFACE();
    }
    
}
