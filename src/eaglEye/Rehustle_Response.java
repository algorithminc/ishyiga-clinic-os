/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eaglEye;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket; 

/**
 *
 * @author ishyiga
 */
public class Rehustle_Response {
 
 
//Messages from the SDC to the CIS
//<01><LEN><SEQ><CMD><DATA><04><STATUS><05><BCC><03>
 public String recMsg="";
 public String data="";
 public String status="";
 
public Rehustle_Response(String recMsg,String data,String status)
{
    this.recMsg=recMsg;
    this.data=data;
    this.status=status;    
} 

 @Override
 public String toString()
{return data+"  "+status;}
static public String line(String line,String tag)
{ 
    return "<"+tag+">"+line+"</"+tag+">  ";
}

public static  String sendRequestVCIS_Iport2(String rule,String toSend,String mesg,String isaha,
         String tin_cl,String serverName,int port_sdc  ) { 
  
 String dataTosend=rule+"#"+toSend+"#N#"+mesg+"@"+isaha+"@"
         +tin_cl+"@N@S@"+0;
    System.out.println("eaglEye.Rehustle_Response.sendRequestVCIS_Iport2()");
     try { 
     //  JOptionPane.showMessageDialog(null, serverName+"/"+port_sdc+" Data to Send  ");
     System.out.println(serverName+"/"+port_sdc+" Data to Send  "+dataTosend);
     String answer ;
 //JOptionPane.showMessageDialog(null, dataTosend);
         InetAddress serveur = InetAddress.getByName(serverName);
         Socket socket = new Socket(serveur, port_sdc);
         BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         PrintStream out = new PrintStream(socket.getOutputStream());
         out.println(dataTosend);
         answer=in.readLine();
         System.out.println("Answer from SDC   "+answer); 
 
         
     if(answer.contains("AN ERROR MSG"))
     {
         String [] msg=answer.split("#"); 
         return answer;  
     }
     
     if(answer.contains("ERROR"))
     {   return answer;  
     }
 
 return answer;
 } 
 catch (  IOException e) 
 {
      System.out.println("error:"+e);  
 return null;
 } 
     
 }

 static public String sendRequestVSDC_Iport2(
        String tinSkmm, String ssnSkmm , String MRC,String MAC,
         String rule,String toSend,int port,String ip) { 
 
  String dataTosend=  
       "<tasklist> "
          +line(tinSkmm,"Tin_SDC")
          +line(MRC,"Mrc_SDC")
          +line(MAC,"Mac_SDC")
          +line(ssnSkmm,"Serial_SDC")
          +line(rule,"Cmd_SDC")+
          line(toSend,"Data_SDC")+" </tasklist> "; 
     
  
     try { 
System.out.println( "  ===================sendRequestVSDC_Iport2===============  SENDING ================  " );
System.out.println("IP "+ip+" PORT  "+port );
System.out.println( "Send  "+dataTosend);
  
     
     String answer ;

         InetAddress serveur = InetAddress.getByName(ip);
         Socket socket = new Socket(serveur, port);
         BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         PrintStream out = new PrintStream(socket.getOutputStream());
         out.println(dataTosend);
         answer=in.readLine();
         System.out.println( "  ======================sendRequestVSDC_Iport2============  RECEIVEING ================  " );
         System.out.println("Answer:  "+answer); 
 
         
     if(answer.contains("AN ERROR MSG"))
     {
         String [] msg=answer.split("#");
        // JOptionPane.showMessageDialog(null, ""+msg[0], ""+msg[1], JOptionPane.WARNING_MESSAGE);
         return answer;  
     }
     
     if(answer.contains("ERROR"))
     { 
         //JOptionPane.showMessageDialog(null, ""+answer, "ATTENTION", JOptionPane.WARNING_MESSAGE);
         return answer;  
     }
 
 return answer;
 } 
 catch (  IOException e) 
 {
      System.out.println("error:"+e); 
 return null;
 } 
 }



}
