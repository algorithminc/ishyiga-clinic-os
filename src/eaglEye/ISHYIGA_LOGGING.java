/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eaglEye;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
/**
 *
 * @author amris
 */
public class ISHYIGA_LOGGING {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);;
    
    static private FileHandler fileHtml, fileTxt;;
    static private Formatter formatterHTML;
    static private SimpleFormatter formmatterTxt;
    
    public ISHYIGA_LOGGING() {
        System.out.println(" ... setup ishyiga logger .... ");
        try {
            setup();
            System.out.println(" ... endsetup ishyiga logger .... ");
        } catch (IOException e){
            e.printStackTrace();
        }
    }
    
    public void Log(String logMessage, Level logLevel, String className) {
        
        LOGGER.setLevel(logLevel);

        logMessage = className+": "+ logMessage;
        if (logLevel.intValue() == Level.SEVERE.intValue()) {
            LOGGER.severe(logMessage);
        } else if (logLevel.intValue() == Level.WARNING.intValue()) {
            LOGGER.warning(logMessage);
        } else if (logLevel.intValue() == Level.INFO.intValue()) {
            LOGGER.info(logMessage);
        } else if (logLevel.intValue() == Level.SEVERE.intValue()) {
            LOGGER.fine(logMessage);
        } else if (logLevel.intValue() == Level.FINEST.intValue()) {
            LOGGER.finest(logMessage);
        }
    }
    
    static public void setup() throws IOException
    {
        Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
        
        logger.setLevel(Level.INFO);
        
        SimpleDateFormat todaysDate = new SimpleDateFormat("yyyyMMddHHmmss");
        String formattedDate = todaysDate.format(new Date());
        fileTxt = new FileHandler("EAGLEYE/VSDC_LOG_"+formattedDate+".txt");
        fileHtml = new FileHandler("EAGLEYE/VSDC_LOG_"+formattedDate+".html");
        
        formmatterTxt = new SimpleFormatter();
        fileTxt.setFormatter(formmatterTxt);
        logger.addHandler(fileTxt);
        
        formatterHTML = new MyHtmlFormatter();
        fileHtml.setFormatter(formatterHTML);
        logger.addHandler(fileHtml);
        
    }
}
