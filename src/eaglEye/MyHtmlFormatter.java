/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eaglEye;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 *
 * @author amris
 */
 class MyHtmlFormatter extends Formatter{

    @Override
    public String format(LogRecord record) {
    
        StringBuffer buffer  = new StringBuffer(1000);
        
        buffer.append("<tr>\n");
//        String tdSyleRed = "\t<td style=\"color:red\">";
        String tdSyleRed = "\t<td style=\"color:#8c0e00\">";
        String tdSyleOrange = "\t<td style=\"color:#cc5602\">";
//        String tdSyleOrange = "\t<td style=\"color:orange\">";
        
        //coloring based level >= WARNING in red
        if (record.getLevel().intValue() == Level.SEVERE.intValue()) {
            buffer.append(tdSyleRed);
            buffer.append("<b>");
            buffer.append(record.getLevel());
            buffer.append("</b>");
        } else if (record.getLevel().intValue() == Level.WARNING.intValue()) {
            buffer.append(tdSyleOrange);
            buffer.append("<b>");
            buffer.append(record.getLevel());
            buffer.append("</b>");
        } else {
            buffer.append("\t<td>");
            buffer.append(record.getLevel());
        }
        
        buffer.append("</td>\n");
        buffer.append("\t<td>");
        buffer.append(calcDate(record.getMillis()));
        buffer.append("</td>\n");
        
        if (record.getLevel().intValue() == Level.SEVERE.intValue())
            buffer.append(tdSyleRed);
        else if (record.getLevel().intValue() == Level.WARNING.intValue())
            buffer.append(tdSyleOrange);
        else
            buffer.append("\t<td>");
        
        buffer.append(formatMessage(record));
//        buffer.append(record.getMessage());
        buffer.append("</td>\n");
        
        return buffer.toString();
        
    }
    
    private String calcDate(long millisecs){
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date resultDate = new Date(millisecs); 
        return date_format.format(resultDate);
    }
    
    // this method is called just after the handler using this
    // formatter is created
    public String getHead(Handler h) {
        return "<!DOCTYPE html>\n<head>\n<style>\n"
            + "table { width: 100% }\n"
            + "th { font:bold 10pt Tahoma; }\n"
            + "td { font:normal 10pt Tahoma; }\n"
            + "h1 {font:normal 11pt Tahoma;}\n"
            + "</style>\n"
            + "</head>\n"
            + "<body>\n"
            + "<h1>" + (new Date()) + "</h1>\n"
            + "<table border=\"0\" cellpadding=\"5\" cellspacing=\"3\">\n"
            + "<tr align=\"left\">\n"
            + "\t<th style=\"width:10%\">Loglevel</th>\n"
            + "\t<th style=\"width:15%\">Time</th>\n"
            + "\t<th style=\"width:75%\">Log Message</th>\n"
            + "</tr>\n";
      }

    // this method is called just after the handler using this
    // formatter is closed
    public String getTail(Handler h) {
        return "</table>\n</body>\n</html>";
    }
    
}
